/* Add here all your JS customizations */

function select2 (Tag) {
    $(Tag).select2({
        theme: 'bootstrap'
    })
}

function slugify(Tag, Slug) {
    $(Tag).keyup(function () {
        var Text = $(this).val();
        Text = Text.toLowerCase();
        var regExp = /\s+/g;
        Text = Text.replace(regExp, '-');
        Text = Text.replace(/[`~!@#$%^&*()_\-+=\[\]{};:'"\\|\/,.<>?\s]/g, ' ').toLowerCase();
        Text = Text.replace(/^\s+|\s+$/gm, '');
        Text = Text.replace(/\s+/g, '-');
        $(Slug).val(Text);
    });
}

function delete_data(id,path) {
	if (confirm("Apakah yakin akan menghapus Data?")) {
		$.ajax({
			url: path+"/hapus/"+id,
			type: "DELETE",
            data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
            success: function ( data ) {
                $('.alert-success').css('display', 'none');
                $('.alert-success').css('display', 'block').delay(6000).fadeOut()
                $('.msg-success').empty().append(' ' + data.message);
                $('.table').DataTable().ajax.reload();
            },
            error: function ( response ) {
                var errors = $.parseJSON(response.responseText);
                $('.alert-danger').css('display', 'none');
                $('.alert-danger').css('display', 'block').delay(6000).fadeOut()
                $('.msg-danger').empty().append(response.message);
                $('#modal-form').modal('hide');
            }
			// success: function (data) {
            //     alert("http://{{ route('"+route+"') }}")
			// 	// window.location.href = "http://{{ route('"+route+"') }}";
			// },
			// error: function () {    
			// 	alert("Error: Tidak dapat menghapus Data!");
			// }
		});
	}
}

function view_form(route, title, btn_option, modal_type, method, id='', mod){
    $('.card-title').html(title)
    $('.modal-dialog').addClass(modal_type)
    $('.m-body').html('')
    $('.loading').show()
    $('.m-body').load(route.replace(/ /g, '-'), function () {
        $('#save_method').val(method)
        if(method != 'add'){
            $('#id_'+mod).val(id)
            $('input[name=_method]').val('PATCH')
        }else{
            $('#id_'+mod).val()
        }
		$('.modal-open').removeAttr('style');
        $('.loading').hide()
    })
    $('.modal').modal('show');
    $('input[name=_method]').val('POST');
    $('.img-preview').attr('src',"{{asset('img/no-image.png')}}");
    $('.b-option').html(btn_option)
    $('.modal-open').removeAttr('style')
}

function view_data(route, title, route_edit, btn_option, modal_type){
    $('.card-title').html(title)
    $('.modal-dialog').addClass(modal_type)
    $('.m-body').html('')
    $('.loading').show()
    $('.m-body').load(route.replace(/ /g, '-'), function () {
        $('.loading').hide()
    })
    $('.b-action').attr('onclick','window.location.href=\''+route_edit+'\'')
    $('.b-option').html(btn_option)
    $('.modal').modal('show')
    $('.modal-open').removeAttr('style')
}

function toTitleCase(str) {
    return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

// Slider
// $('#revolutionSlider').revolution({
// 	sliderType: 'standard',
// 	sliderLayout: 'fullwidth',
// 	delay: 5000,
// 	gridwidth: 1170,
// 	gridheight: 530,
// 	spinner: 'off',
// 	disableProgressBar: 'on',
// 	parallax:{
// 		type:"on",
// 		levels:[5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85],
// 		origo:"enterpoint",
// 		speed:400,
// 		bgparallax:"on",
// 		disable_onmobile:"off"
// 	},
// 	navigation: {
// 		keyboardNavigation:"off",
// 		keyboard_direction: "horizontal",
// 		mouseScrollNavigation:"off",
// 		onHoverStop:"on",
// 		touch:{
// 			touchenabled:"on",
// 			swipe_threshold: 75,
// 			swipe_min_touches: 1,
// 			swipe_direction: "horizontal",
// 			drag_block_vertical: false
// 		}
// 		,
// 		bullets: {
// 			enable:true,
// 			hide_onmobile:true,
// 			hide_under:778,
// 			style:"hermes",
// 			hide_onleave:false,
// 			direction:"horizontal",
// 			h_align:"center",
// 			v_align:"bottom",
// 			h_offset:0,
// 			v_offset:20,
// 			space:5,
// 			tmp:''
// 		}
// 	}
// });
