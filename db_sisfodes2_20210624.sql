-- MySQL dump 10.16  Distrib 10.1.48-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: db_sisfodes2
-- ------------------------------------------------------
-- Server version	10.1.48-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sfd_berita`
--

DROP TABLE IF EXISTS `sfd_berita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `id_kategori_berita` int(11) DEFAULT NULL,
  `judul_berita` varchar(200) DEFAULT NULL,
  `slug_judul_berita` varchar(200) DEFAULT NULL,
  `isi_berita` text,
  `dilihat` int(11) DEFAULT NULL,
  `publikasi` tinyint(1) DEFAULT '0',
  `post_img` varchar(191) DEFAULT 'img/blog/medium/blog-1.jpg',
  `pin_berita` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_berita`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  KEY `id_kategori` (`id_kategori_berita`) USING BTREE,
  CONSTRAINT `lnk_sfd_berita_sfd_berita_kategori` FOREIGN KEY (`id_kategori_berita`) REFERENCES `sfd_berita_kategori` (`id_kategori_berita`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `lnk_sfd_desa_sfd_berita` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_berita`
--

LOCK TABLES `sfd_berita` WRITE;
/*!40000 ALTER TABLE `sfd_berita` DISABLE KEYS */;
INSERT INTO `sfd_berita` VALUES (43,11,3,'Lorem ipsum dolor sit amet','lorem-ipsum-dolor-sit-amet','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',2,1,NULL,0,'2021-01-11 07:39:24','2021-06-17 04:15:03'),(44,11,1,'ini adalah judul beritassssss nya','ini-adalah-judul-beritassssss-nya','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',7,1,NULL,1,'2021-02-02 00:30:23','2021-06-17 04:15:17'),(45,11,2,'nihil molestias vero aspernatur','nihil-molestias-vero-aspernatur','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',0,1,NULL,0,'2021-03-16 00:30:23','2021-06-17 04:15:07'),(46,11,4,'Consequatur id quasi at optio','consequatur-id-quasi-at-optio','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',1,1,NULL,1,'2021-04-19 00:30:23','2021-06-17 04:15:18'),(47,11,3,'amet consectetur adipisicing elit','amet-consectetur-adipisicing-elit','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',3,1,NULL,1,'2021-05-26 00:30:23','2021-06-23 09:02:29');
/*!40000 ALTER TABLE `sfd_berita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_berita_banner`
--

DROP TABLE IF EXISTS `sfd_berita_banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_berita_banner` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `nama_banner` varchar(255) NOT NULL,
  `deskripsi_banner` text NOT NULL,
  `status` int(2) DEFAULT '1',
  `gambar` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_banner`),
  KEY `lnk_sfd_desa_sfd_berita_banner` (`id_desa`),
  CONSTRAINT `lnk_sfd_desa_sfd_berita_banner` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_berita_banner`
--

LOCK TABLES `sfd_berita_banner` WRITE;
/*!40000 ALTER TABLE `sfd_berita_banner` DISABLE KEYS */;
INSERT INTO `sfd_berita_banner` VALUES (11,11,'Portal Desa Loce','Sahu Timur adalah sebuah kecamatan di Kabupaten Halmahera Barat, Maluku Utara, Indonesia.',1,'banner__11_11_20200714152338.jpg','2020-07-09 07:39:45','2020-07-14 08:34:42');
/*!40000 ALTER TABLE `sfd_berita_banner` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_berita_detail`
--

DROP TABLE IF EXISTS `sfd_berita_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_berita_detail` (
  `id_berita_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_berita` varchar(50) DEFAULT NULL,
  `id_tag_berita` varchar(50) DEFAULT NULL,
  `id_desa` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_berita_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_berita_detail`
--

LOCK TABLES `sfd_berita_detail` WRITE;
/*!40000 ALTER TABLE `sfd_berita_detail` DISABLE KEYS */;
INSERT INTO `sfd_berita_detail` VALUES (21,NULL,NULL,NULL,'2021-04-05 17:37:34',NULL);
/*!40000 ALTER TABLE `sfd_berita_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_berita_kategori`
--

DROP TABLE IF EXISTS `sfd_berita_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_berita_kategori` (
  `id_kategori_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `nama_kategori` varchar(200) DEFAULT NULL,
  `slug_kategori` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kategori_berita`),
  KEY `lnk_sfd_desa_sfd_berita_kategori` (`id_desa`),
  CONSTRAINT `fk_sfd_desa_sfd_berita_kategori` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_berita_kategori`
--

LOCK TABLES `sfd_berita_kategori` WRITE;
/*!40000 ALTER TABLE `sfd_berita_kategori` DISABLE KEYS */;
INSERT INTO `sfd_berita_kategori` VALUES (1,11,'Kategori Berita 1','sluk-kategori-1',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(2,11,'Kategori Berita 2','sluk-kategori-2',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(3,11,'Kategori Berita 3','sluk-kategori-3',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(4,11,'Kategori Berita 4','sluk-kategori-4',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(5,11,'Kategori Berita 5','sluk-kategori-5',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(6,11,'Kategori Berita 6','sluk-kategori-6',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(7,11,'Kategori Berita 7','sluk-kategori-7',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(8,11,'Kategori Berita 8','sluk-kategori-8',1,'2021-04-03 21:49:01','2021-04-03 21:49:01'),(9,11,'Kategori Berita 9','sluk-kategori-9',1,'2021-04-03 21:49:01','2021-04-03 21:49:01');
/*!40000 ALTER TABLE `sfd_berita_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_berita_kategori_sub`
--

DROP TABLE IF EXISTS `sfd_berita_kategori_sub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_berita_kategori_sub` (
  `id_sub_kategori` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `id_kategori_berita` int(11) NOT NULL,
  `nama_sub_kategori` varchar(255) NOT NULL,
  `slug_sub_kategori` varchar(200) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_sub_kategori`),
  KEY `lnk_sfd_desa_sfd_berita_kategori_sub` (`id_desa`),
  KEY `lnk_sfd_berita_kategori_sfd_berita_kategori_sub` (`id_kategori_berita`),
  CONSTRAINT `fk_sfd_berita_kategori_sfd_berita_kategori_sub` FOREIGN KEY (`id_kategori_berita`) REFERENCES `sfd_berita_kategori` (`id_kategori_berita`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_sfd_desa_sfd_berita_kategori_sub` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_berita_kategori_sub`
--

LOCK TABLES `sfd_berita_kategori_sub` WRITE;
/*!40000 ALTER TABLE `sfd_berita_kategori_sub` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_berita_kategori_sub` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_berita_tags`
--

DROP TABLE IF EXISTS `sfd_berita_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_berita_tags` (
  `id_tag_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `nama_tag` varchar(255) NOT NULL,
  `slug_tag` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_tag_berita`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `fk_sfd_berita_tags_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_berita_tags`
--

LOCK TABLES `sfd_berita_tags` WRITE;
/*!40000 ALTER TABLE `sfd_berita_tags` DISABLE KEYS */;
INSERT INTO `sfd_berita_tags` VALUES (42,11,'eko sos bud',NULL,1,'2021-04-05 10:33:14','2021-04-05 10:33:14'),(43,11,'eko sos bud',NULL,1,'2021-04-05 10:33:37','2021-04-05 10:33:37'),(44,11,'eko sos bud',NULL,1,'2021-04-05 10:34:07','2021-04-05 10:34:07'),(45,11,'eko sos bud',NULL,1,'2021-04-05 10:35:25','2021-04-05 10:35:25'),(46,11,'sosial budaya',NULL,1,'2021-04-05 10:35:25','2021-04-05 10:35:25'),(47,11,'perekonomian',NULL,1,'2021-04-05 10:35:25','2021-04-05 10:35:25'),(48,11,'eko sos bud',NULL,1,'2021-04-05 10:37:16','2021-04-05 10:37:16'),(50,11,'sosial budaya',NULL,1,'2021-04-05 10:37:34','2021-04-05 10:37:34');
/*!40000 ALTER TABLE `sfd_berita_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_desa`
--

DROP TABLE IF EXISTS `sfd_desa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_desa` (
  `id_desa` int(11) NOT NULL AUTO_INCREMENT,
  `kode_wilayah` varchar(100) DEFAULT NULL,
  `kepala_desa` varchar(200) DEFAULT NULL,
  `id_kecamatan` int(11) DEFAULT NULL,
  `nama_desa` varchar(100) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `desa_id` int(11) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `peta_wilayah` text,
  `logo` varchar(200) DEFAULT NULL,
  `kode_pos` varchar(10) DEFAULT NULL,
  `visi` text,
  `misi` text,
  `status` tinyint(1) DEFAULT '1',
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_desa`),
  KEY `id_kecamatan` (`id_kecamatan`) USING BTREE,
  KEY `id_kepala_desa` (`kepala_desa`) USING BTREE,
  KEY `kode_wilayah` (`kode_wilayah`) USING BTREE,
  CONSTRAINT `lnk_sfd_desa_kecamatan` FOREIGN KEY (`id_kecamatan`) REFERENCES `sfd_master_kecamatan` (`id_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_desa`
--

LOCK TABLES `sfd_desa` WRITE;
/*!40000 ALTER TABLE `sfd_desa` DISABLE KEYS */;
INSERT INTO `sfd_desa` VALUES (0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,1,'2020-08-23 17:01:54',NULL),(11,'82.01.09.2001','Drs. Joko Purnomo, M.Pd, M.Si, M.Kom, MH',5483,'Loce','Jl. Loce, Kecamatan Sahu Timur, Kabupaten Halmahera Barat, Maluku Utara',165,'0853-4004-6724','0853-4004-6724','desaloce.desa.dev','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15956.47836281679!2d127.4550362767079!3d1.0723140374848803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x329b500beeadbcfb%3A0xfe9e3f84c455a46e!2sJl.%20Baru%2C%20Jailolo%2C%20Kabupaten%20Halmahera%20Barat%2C%20Maluku%20Utara!5e0!3m2!1sid!2sid!4v1582555104368!5m2!1sid!2sid\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>','logo__1594741540.png','97758','<p><em>&quot;Terwujudnya kehidupan masyarakat desa loce yang religius, aman, harmonis, maju, adil dan berkah&quot;</em></p>','<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto cupiditate ex ab tenetur laudantium ducimus tempora nulla quo unde distinctio doloribus eos, amet in et vero repellendus vel expedita praesentium?</p>',1,'2020-08-23 16:38:54','2020-07-27 15:48:38'),(26,'82.01.09.2018','Drs. Eko Nugroho, M.Pd, M.Si, M.Kom, MH',5483,'Air Panas','Jl. Air Panas, Kecamatan Sahu Timur, Kabupaten Halmahera Barat, Maluku Utara',NULL,NULL,NULL,'airpanas.desa.dev',NULL,NULL,'97753',NULL,NULL,1,'2021-06-17 07:02:13','2020-08-23 10:05:27');
/*!40000 ALTER TABLE `sfd_desa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_desa_dokumen`
--

DROP TABLE IF EXISTS `sfd_desa_dokumen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_desa_dokumen` (
  `id_dokumen_desa` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `judul_dokumen` varchar(200) DEFAULT NULL,
  `file` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_dokumen_desa`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `fk_sfd_dokumen_desa_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_desa_dokumen`
--

LOCK TABLES `sfd_desa_dokumen` WRITE;
/*!40000 ALTER TABLE `sfd_desa_dokumen` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_desa_dokumen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_desa_layanan`
--

DROP TABLE IF EXISTS `sfd_desa_layanan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_desa_layanan` (
  `id_layanan` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `kode_wilayah` varchar(100) DEFAULT NULL,
  `slug_layanan` varchar(200) DEFAULT NULL,
  `nama_layanan` varchar(200) NOT NULL,
  `deskripsi_layanan` text,
  `post_img` varchar(255) DEFAULT NULL,
  `dilihat` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_layanan`),
  KEY `id_desa` (`id_desa`),
  CONSTRAINT `FK_sfd_layanan_desa_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_desa_layanan`
--

LOCK TABLES `sfd_desa_layanan` WRITE;
/*!40000 ALTER TABLE `sfd_desa_layanan` DISABLE KEYS */;
INSERT INTO `sfd_desa_layanan` VALUES (3,11,NULL,'surat-keterangan-lahir','Surat Keterangan Lahir','Salah satu tugas atau layanan desa ialah melakukan pengurusan akte lahir atau surat keterangan lahir.&nbsp;<br />\r\nUrus akte kelahiran&nbsp;di kantor desa harus membawa persyaratan, berupa fotokopi KK dan KTP, fotokopi surat nikah, surat pengantar dari Rt/Rw, fotokopi lunas PBB, fotokopi surat keterangan lahir yang diberikan oleh pihak Rumah sakit atau Bidan Desa.',NULL,NULL,'2021-06-21 14:16:59','2021-06-21 08:11:03'),(4,11,NULL,'pelatihan-desa','Pelatihan Desa','Pelatihan desa&nbsp;biasanya dilakukan untuk aparatur desa, dalam rangka meningkatkan kapasitas dari pihak pemerintah desa sekaligus BPD, dengan tujuan agar para peserta yang mengikuti pelatihan bisa mengetahui fungsi kerja dan tugas-tugas dalam pemerintah desa. Selain itu, para peserta juga bisa menguraikan tanggung jawab dan tugas kerja masing-masing. Seperti tugas-tugas dari pihak aparatur desa berikut ini :<br />\r\nSekretaris desa<br />\r\nTugasnya ialah membantu Kepala Desa untuk melaksanakan dan mempersiapkan pengelolaan administrasi desa sekaligus mempersiapkan penyusunan laporan mengenai penyelenggaraan pemerintah.<br />\r\nAdapun fungsinya ialah untuk penyelenggaraan kegiatan administrasi serta mempersiapkan bahan laporan guna kelancaran tugas Kades (Kepala Desa).<br />\r\nKaur Umum<br />\r\nBertugas untuk membantu sekretaris desa melalui pelaksanaan pengelolaan sumber APBD, mempersiapkan bahan laporan penyusunan APBDes dan pengelolaan administrasi untuk keuangan desa.&nbsp;<br />\r\nAdapun fungsinya yaitu untuk melaksanakan persiapan penyusunan APBDes, pengelolaan administrasi pada keuangan desa,&nbsp; dan sebagainya.<br />\r\nKaur Kesos<br />\r\nBertugas untuk mempersiapkan bahan perumusan tentang kebijakan teknis dari penyusunan keagamaan dan melaksanakan program berkaitan dengan pemberdayaan masyarakat.<br />\r\nAdapun fungsinya mempersiapkan bahan dalam program pemberdayaan masyarakat dan persiapan bahan pelaksanaan program yang berkaitan dengan kegiatan keagamaan.<br />\r\nKaur Ekbang<br />\r\nBertugas untuk membantu desa melaksanakan persiapan bahan perumusan tentang kebijakan teknis dalam pengembangan potensi desa dan ekonomi masyarakat, serta pengelolaan administrasi pembangunan.<br />\r\nFungsinya ialah mempersiapkan bantuan kajian perkembangan ekonomi masyarakat dan membantu pelaksanaan administrasi pembangunan serta pengelolaan tugas pembangunan yang diberikan kepala desa.',NULL,NULL,'2021-06-21 14:16:43','2021-06-21 08:10:59'),(5,11,NULL,'daftar-ktp','Daftar KTP','Daftar&nbsp;KTP&nbsp;online&nbsp;atau biasa disebut sebagai KTP elektronik maupun&nbsp;e-KTP juga dilakukan di kantor desa. KTP elektronik ialah kartu tanda penduduk dilengkapi oleh cip sebagai identitas resmi dari penduduk yang terdaftar. Jika tak ada perubahan berdasarkan kebijakan pemerintah, maka KTP dapat berlaku hingga seumur hidup.<br />\r\nUntuk persyaratan pengajuan e-KTP diantaranya meliputi perekaman, fotokopi KK, surat pengantar dari Kepala&nbsp; Desa yang diketahui camat, dan fotokopi akta nikah untuk pemohon yang usianya belum genap 17 tahun. Sesudah Anda merekam data, maka selanjutnya petugas akan mencetak KTP elektronik dengan syarat form permohonan cetak KTP-elektronik, surat keterangan atau KTP lama dan fotokopi KK.<br />\r\njika KTP Anda hilang, bisa mengajukan pencetakan KTP pengganti langsung di kantor kecamatan dan harus melampirkan fotokopi KK, surat keterangan KTP hilang dari pihak kepolisian, dan surat pernyataan kehilangan dengan materai.',NULL,NULL,'2021-06-21 14:15:49','2021-06-21 08:10:54'),(7,11,NULL,'pembuatan-ktp','Pembuatan KTP','Adapun syarat keterangan untuk pembuatan KTP para pemula, diwajibkan membawa surat pengantar dari RT atau RW, menyertakan fotokopi KK, fotokopi lunas PBB dan pas foto berukuran 3 x 4 sebanyak 3 lembar. Ketentuannya usia pemohon harus berusia 17 tahun minimal.<br />\r\nSementara untuk membuat surat keterangan perpanjangan KTP, pemohon harus menyertakan surat pengantar RT/RW, KTP asli, pas foto berukuran 3 x 4 sebanyak 3 lembar, fotokopi lunas PBB, dan fotokopi KK.',NULL,NULL,'2021-06-21 14:15:34','2021-06-21 08:10:50'),(8,11,NULL,'perubahan-data-kartu-keluarga','Perubahan Data Kartu Keluarga','Pelayanan birokrasi desa/administrasi lainnya yaitu untuk pembuatan kartu keluarga. Untuk urus kartu keluarga di desa, Anda harus membawa surat pengantar dari Rt/Rw dan KK asli.',NULL,NULL,'2021-06-21 14:15:20','2021-06-21 08:10:45'),(9,11,NULL,'pengurusan-surat-kematian','Pengurusan Surat Kematian','Urus surat kematian&nbsp;sangat penting untuk mencegah agar data-data almarhum/almarhumah disalahgunakan pihak tertentu yang tidak bertanggung jawab. Sementara tujuan pemerintah agar dapat memastikan keakuratan informasi dan data penduduk yang potensial untuk dijadikan pemilih dalam Pilkada atau Pemilu.&nbsp;<br />\r\nAdapun persyaratan yang harus dipenuhi saat mengurus surat keterangan kematian, ialah surat keterangan kematian dari Rumah Sakit jika yang meninggal di rumah sakit, fotokopi KTP yang meninggal dan surat pengantar RT/RW.',NULL,NULL,'2021-06-21 14:15:09','2021-06-21 08:10:40'),(10,11,NULL,'apbd-desa','APBD Desa','Anggaran Pendapatan dan Belanja Desa atau APBDesa, yaitu rencana keuangan pemerintah desa tahunan yang ditetapkan dan dibahas Kepala Desa dengan BPD atau Badan Permusyawaratan Desa lewat Peraturan Desa.<br />\r\nAPB Desa merupakan dokumen dengan kekuatan hukum untuk menjamin kepastian terhadap rencana kegiatan, artinya mengikat aparatur dan pemerintah desa terkait, dalam melaksanakan kegiatan berdasarkan rencana yang sudah ditetapkan sekaligus untuk menjamin ketersediaan anggaran dengan jumlah tertentu. APB Desa sendiri menjamin kelayakan kegiatan dalam hal pendanaan, karenanya bisa dipastikan kelayakan pada hasil aktivitas secara teknis.',NULL,NULL,'2021-06-21 14:14:55','2021-06-21 08:10:35'),(11,11,NULL,'jaringan-aspirasi-rakyat','Jaringan Aspirasi Rakyat','Badan Permusyawaratan Desa disingkat BPD ialah lembaga perwujudan dari demokrasi penyelenggaraan pemerintah desa. BPD inilah yang berfungsi untuk menetapkan semua peraturan desa dengan kepala desa, sekaligus dapat menyalurkan dan menampung aspirasi masyarakat sesuai peraturan Undang-Undang No. 6 Tahun 2014.&nbsp;',NULL,NULL,'2021-06-21 14:14:44','2021-06-21 08:10:31'),(12,11,NULL,'lapor-pemerintah-desa','Lapor Pemerintah Desa','Salah satu pelayanan desa yaitu dapat mempermudah masyarakat atau warga untuk melaporkan keluhan sekaligus aspirasi Anda mengenai pemerintah desa terkait. Masyarakat bisa melaporkan secara langsung ke Badan Permusyawaratan Desa. Selain itu, kini pemerintah juga menyediakan layanan berupa&nbsp;call center&nbsp;di 1500040 untuk masyarakat agar bisa melaporkan berbagai macam masalah yang berhubungan dengan desa. Itulah beberapa jenis dan bentuk pelayanan desa dalam mewadahi aktivitas masyarakat sekaligus sebagai jaringan aspirasi warga terkait hal-hal yang berkaitan dengan kegiatan desa. Sehingga semua fungsi dan bentuk layanan desa dapat berjalan dengan baik dalam lingkungan masyarakat, sebagaimana mestinya.',NULL,NULL,'2021-06-21 14:14:33','2021-06-21 08:07:49');
/*!40000 ALTER TABLE `sfd_desa_layanan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_desa_lembaga`
--

DROP TABLE IF EXISTS `sfd_desa_lembaga`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_desa_lembaga` (
  `id_lembaga` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `kode_wilayah` varchar(50) DEFAULT NULL,
  `nama_lembaga` varchar(200) DEFAULT NULL,
  `deskripsi_lembaga` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_lembaga`),
  KEY `index_id_desa` (`id_desa`),
  CONSTRAINT `FK_sfd_lembaga_desa_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_desa_lembaga`
--

LOCK TABLES `sfd_desa_lembaga` WRITE;
/*!40000 ALTER TABLE `sfd_desa_lembaga` DISABLE KEYS */;
INSERT INTO `sfd_desa_lembaga` VALUES (3,11,'82.01.09.2001','zxczxc','ccccccccccccccccccccccccccccc','2020-11-08 06:30:34','2020-11-14 09:03:11');
/*!40000 ALTER TABLE `sfd_desa_lembaga` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_desa_potensi`
--

DROP TABLE IF EXISTS `sfd_desa_potensi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_desa_potensi` (
  `id_potensi` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `nama_potensi` varchar(200) DEFAULT NULL,
  `deskripsi_potensi` text,
  `gambar` varchar(200) DEFAULT NULL,
  `slug_potensi` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_potensi`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `potensi_desa_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_desa_potensi`
--

LOCK TABLES `sfd_desa_potensi` WRITE;
/*!40000 ALTER TABLE `sfd_desa_potensi` DISABLE KEYS */;
INSERT INTO `sfd_desa_potensi` VALUES (1,11,'Pertanian Tanaman Pangan','Pertanian tanaman pangan memiliki andil besar dalam perekonomian di Kabupaten Wonosobo dengan komoditasnya meliputi tanaman padi dan palawija, tanaman sayuran dan tanaman buah-buahan. Pada tahun 2011, luas panen padi sawah mengalami penurunan sebesar 978 Ha (3,21%) diikuti penurunan produksi padi sawah sebesar 12.115 ton, begitu pula padi gogo mengalami penurunan sebesar 126 ton dari 1.225 ton di tahun 2010 yang disebabkan turunnya luas panen sebesar 335 Ha. Produksi jagung juga turun sebesar 8,99% sementara ubi kayu mengalami kenaikan 7,63%.\r\n<p>&nbsp;</p>\r\n\r\n<p>Sedangkan produksi sayuran mengalami peningkatan yang cukup signifikan kecuali kacang panjang, tomat, kobis dan kacang merah. Sementara produksi buah-buahan sebagian besar mengalami penurunan.</p>','potensi__11_1_20200723154252.jpeg','pertanian-tanaman-pangan','2020-07-21 08:32:26','2020-07-23 08:42:52'),(2,11,'Perkebunan','Tanaman perkebunan yang potensial di Kabupaten Wonosobo adalah kelapa dalam, kelapa deres, kopi dan tembakau.&nbsp;\r\n<p>&nbsp;</p>\r\n\r\n<p>Pada tahun 2011 produksi kelapa dalam naik 26,98%, produksi kelapa deres naik 34,24%, produksi tembakau naik 56,69% dan produksi kopi turun sangat drastis yaitu mencapai 99%.</p>','potensi__11_2_20200723154239.jpeg','perkebunan','2020-06-29 08:33:45','2020-07-24 14:27:07'),(3,11,'Peternakan','Jenis ternak yang banyak diusahakan di Kabupaten Wonosobo tahun 2010 dan populasinya cukup besar adalah sapi potong (27.687 ekor), kambing (136.706 ekor), domba (90.267 ekor) dan ayam buras (665.238 ekor).\r\n<p>&nbsp;</p>\r\n\r\n<p>Hasil produksi peternakan tahun 2011 yaitu daging naik menjadi 47.452 kw dari 42.873 kw pada tahun 2010, produksi susu meningkat dari tahun 2010 sebesar 556.316 lt menjadi 559.676 lt pada tahun 2011 dan produksi telor menurun dari 25.321 kw tahun 2010 menjadi 23.758 kw tahun 2011.</p>','potensi__11_3_20200723154110.jpeg','peternakan','2020-05-02 08:34:17','2020-07-24 13:47:17'),(4,11,'Perikananan','Usaha perikanan di Kabupaten Wonosobo banyak diusahakan di kolam, karamba, waduk, telaga dan sungai dengan produksi pada tahun 2011 mencapai 5.830,24 ton. Di samping itu juga ada usaha pembenihan yang dilakukan di pembenihan rakyat, balai benih ikan maupun pembenihan sawah.','potensi__11_4_20200723154034.jpeg','perikananan','2020-04-10 08:34:36','2020-07-24 13:47:26'),(5,11,'Kehutanan','Hutan yang ada di Kabupaten Wonosobo terdiri dari hutan rakyat dan hutan negara. Luas hutan rakyat sebesar 18.982 Ha dengan sebagian besar tanaman berupa sengon/albasia, mahoni, suren d an jemitri. Sedangkan hasil dari hutan negara adalah kayu pertukangan, kayu bakar, kopal, getah pinus, gondorukem dan terpenting.','potensi__11_5_20200723135659.jpg','kehutanan','2020-03-17 08:35:05','2020-07-24 13:47:39');
/*!40000 ALTER TABLE `sfd_desa_potensi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_desa_wilayah`
--

DROP TABLE IF EXISTS `sfd_desa_wilayah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_desa_wilayah` (
  `id_wilayah` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `luas_wilayah_tanah` decimal(20,2) DEFAULT NULL,
  `luas_daratan` decimal(20,2) DEFAULT NULL,
  `luas_persawahan` decimal(20,2) DEFAULT NULL,
  `luas_tegalan` decimal(20,2) DEFAULT NULL,
  `luas_lain_lain` decimal(20,2) DEFAULT NULL,
  `luas_tanah_desa` decimal(20,2) DEFAULT NULL,
  `jumlah_dusun` int(11) DEFAULT NULL,
  `jumlah_rt` int(11) DEFAULT NULL,
  `jumlah_rw` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_wilayah`),
  UNIQUE KEY `id_wilayah` (`id_wilayah`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `fk_sfd_desa_wilayah_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_desa_wilayah`
--

LOCK TABLES `sfd_desa_wilayah` WRITE;
/*!40000 ALTER TABLE `sfd_desa_wilayah` DISABLE KEYS */;
INSERT INTO `sfd_desa_wilayah` VALUES (1,11,120.00,123.00,127.00,123.00,123.00,123.00,123,123,123,'2020-02-11 08:41:22','2020-02-11 08:54:11');
/*!40000 ALTER TABLE `sfd_desa_wilayah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_detail_realisasi_belanja`
--

DROP TABLE IF EXISTS `sfd_detail_realisasi_belanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_detail_realisasi_belanja` (
  `id_detail_realisasi` int(11) NOT NULL AUTO_INCREMENT,
  `id_realisasi_belanja` int(11) DEFAULT NULL,
  `id_rencana_belanja` int(11) DEFAULT NULL,
  `deskripsi` text,
  `jumlah` decimal(20,2) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `harga` decimal(20,2) DEFAULT NULL,
  `subtotal` decimal(20,2) DEFAULT NULL,
  `file_foto` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_detail_realisasi`),
  KEY `id_realisasi_belanja` (`id_realisasi_belanja`) USING BTREE,
  KEY `id_rencana_belanja` (`id_rencana_belanja`) USING BTREE,
  CONSTRAINT `detail_realisasi_belanja_ibfk_1` FOREIGN KEY (`id_realisasi_belanja`) REFERENCES `sfd_realisasi_anggaran_belanja` (`id_realisasi_belaja`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `detail_realisasi_belanja_ibfk_2` FOREIGN KEY (`id_rencana_belanja`) REFERENCES `sfd_detail_rencana_anggaran_biaya` (`id_rencana_belaja`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_detail_realisasi_belanja`
--

LOCK TABLES `sfd_detail_realisasi_belanja` WRITE;
/*!40000 ALTER TABLE `sfd_detail_realisasi_belanja` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_detail_realisasi_belanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_detail_rencana_anggaran_biaya`
--

DROP TABLE IF EXISTS `sfd_detail_rencana_anggaran_biaya`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_detail_rencana_anggaran_biaya` (
  `id_detail_rab` int(11) NOT NULL AUTO_INCREMENT,
  `id_rencana_belaja` int(11) DEFAULT NULL,
  `deskripsi` text,
  `jumlah` decimal(20,2) DEFAULT NULL,
  `satuan` varchar(20) DEFAULT NULL,
  `harga` decimal(20,2) DEFAULT NULL,
  `subtotal` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_detail_rab`),
  KEY `id_rencana_belaja` (`id_rencana_belaja`) USING BTREE,
  CONSTRAINT `detail_rencana_anggaran_biaya_ibfk_1` FOREIGN KEY (`id_rencana_belaja`) REFERENCES `sfd_rencana_anggaran_belanja` (`id_rencana_belanja`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_detail_rencana_anggaran_biaya`
--

LOCK TABLES `sfd_detail_rencana_anggaran_biaya` WRITE;
/*!40000 ALTER TABLE `sfd_detail_rencana_anggaran_biaya` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_detail_rencana_anggaran_biaya` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_geografi`
--

DROP TABLE IF EXISTS `sfd_geografi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_geografi` (
  `id_geografi` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `barat` varchar(200) DEFAULT NULL,
  `utara` varchar(200) DEFAULT NULL,
  `selatan` varchar(200) DEFAULT NULL,
  `timur` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_geografi`),
  KEY `id_desa` (`id_desa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_geografi`
--

LOCK TABLES `sfd_geografi` WRITE;
/*!40000 ALTER TABLE `sfd_geografi` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_geografi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_keadaan_statistik`
--

DROP TABLE IF EXISTS `sfd_keadaan_statistik`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_keadaan_statistik` (
  `id_statistik` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` enum('LAHAN SAWAH','LAHAN KERING','LAHAN LAINYA') DEFAULT NULL,
  `keterangan` text,
  `luas` decimal(10,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_statistik`),
  UNIQUE KEY `id_statistik` (`id_statistik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_keadaan_statistik`
--

LOCK TABLES `sfd_keadaan_statistik` WRITE;
/*!40000 ALTER TABLE `sfd_keadaan_statistik` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_keadaan_statistik` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_lok_kabupaten`
--

DROP TABLE IF EXISTS `sfd_lok_kabupaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_lok_kabupaten` (
  `id_kabupaten` int(10) NOT NULL AUTO_INCREMENT,
  `provinsi_id` int(10) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_kabupaten`)
) ENGINE=InnoDB AUTO_INCREMENT=8273 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_lok_kabupaten`
--

LOCK TABLES `sfd_lok_kabupaten` WRITE;
/*!40000 ALTER TABLE `sfd_lok_kabupaten` DISABLE KEYS */;
INSERT INTO `sfd_lok_kabupaten` VALUES (8201,82,'Kabupaten Halmahera Barat'),(8202,82,'Kabupaten Halmahera Tengah'),(8203,82,'Kabupaten Kepulauan Sula'),(8204,82,'Kabupaten Halmahera Selatan'),(8205,82,'Kabupaten Halmahera Utara'),(8206,82,'Kabupaten Halmahera Timur'),(8207,82,'Kabupaten Pulau Morotai'),(8208,82,'Kabupaten Pulau Taliabu'),(8271,82,'Kota Ternate'),(8272,82,'Kota Tidore Kepulauan');
/*!40000 ALTER TABLE `sfd_lok_kabupaten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_master_anggaran_belanja`
--

DROP TABLE IF EXISTS `sfd_master_anggaran_belanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_master_anggaran_belanja` (
  `id_anggaran_belanja` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `kode_rekening` varchar(8) DEFAULT NULL,
  `nama_angggaran_belanja` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_anggaran_belanja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_master_anggaran_belanja`
--

LOCK TABLES `sfd_master_anggaran_belanja` WRITE;
/*!40000 ALTER TABLE `sfd_master_anggaran_belanja` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_master_anggaran_belanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_master_desa`
--

DROP TABLE IF EXISTS `sfd_master_desa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_master_desa` (
  `desa_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kecamatan` int(11) DEFAULT NULL,
  `nama_desa` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`desa_id`),
  UNIQUE KEY `desa_id` (`desa_id`),
  KEY `id_kecamatan` (`id_kecamatan`) USING BTREE,
  CONSTRAINT `sfd_masterdesa_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `sfd_master_kecamatan` (`id_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_master_desa`
--

LOCK TABLES `sfd_master_desa` WRITE;
/*!40000 ALTER TABLE `sfd_master_desa` DISABLE KEYS */;
INSERT INTO `sfd_master_desa` VALUES (1,5476,'Ake Boso','2020-02-21 16:24:25','2020-02-21 16:24:25'),(2,5476,'Akesibu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(3,5476,'Gam Ici','2020-02-21 16:24:25','2020-02-21 16:24:25'),(4,5476,'Gam Lamo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(5,5476,'Kampung Baru','2020-02-21 16:24:25','2020-02-21 16:24:25'),(6,5476,'Kie Lei (Kie Ici)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(7,5476,'Maritango','2020-02-21 16:24:25','2020-02-21 16:24:25'),(8,5476,'Naga','2020-02-21 16:24:25','2020-02-21 16:24:25'),(9,5476,'Soana Masungi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(10,5476,'Tahafo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(11,5476,'Tobaol','2020-02-21 16:24:25','2020-02-21 16:24:25'),(12,5476,'Togola Sanger/Sangir','2020-02-21 16:24:25','2020-02-21 16:24:25'),(13,5476,'Togola Wayolo/Wayoli','2020-02-21 16:24:25','2020-02-21 16:24:25'),(14,5476,'Tongute Goin','2020-02-21 16:24:25','2020-02-21 16:24:25'),(15,5476,'Tongute Sungi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(16,5476,'Tongute Ternate','2020-02-21 16:24:25','2020-02-21 16:24:25'),(17,5476,'Tongute Ternate Selatan','2020-02-21 16:24:25','2020-02-21 16:24:25'),(18,5477,'Adu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(19,5477,'Baru','2020-02-21 16:24:25','2020-02-21 16:24:25'),(20,5477,'Bataka','2020-02-21 16:24:25','2020-02-21 16:24:25'),(21,5477,'Gamkonora','2020-02-21 16:24:25','2020-02-21 16:24:25'),(22,5477,'Gamsida','2020-02-21 16:24:25','2020-02-21 16:24:25'),(23,5477,'Gamsungi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(24,5477,'Jere','2020-02-21 16:24:25','2020-02-21 16:24:25'),(25,5477,'Nanas','2020-02-21 16:24:25','2020-02-21 16:24:25'),(26,5477,'Ngalo Ngalo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(27,5477,'Ngawet','2020-02-21 16:24:25','2020-02-21 16:24:25'),(28,5477,'Sarau','2020-02-21 16:24:25','2020-02-21 16:24:25'),(29,5477,'Talaga','2020-02-21 16:24:25','2020-02-21 16:24:25'),(30,5477,'Tobelos','2020-02-21 16:24:25','2020-02-21 16:24:25'),(31,5477,'Tobobol (Tabobol)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(32,5477,'Tosoa','2020-02-21 16:24:25','2020-02-21 16:24:25'),(33,5477,'Tuguaer','2020-02-21 16:24:25','2020-02-21 16:24:25'),(34,5478,'Aru Jaya','2020-02-21 16:24:25','2020-02-21 16:24:25'),(35,5478,'Borona','2020-02-21 16:24:25','2020-02-21 16:24:25'),(36,5478,'Duono','2020-02-21 16:24:25','2020-02-21 16:24:25'),(37,5478,'Goin','2020-02-21 16:24:25','2020-02-21 16:24:25'),(38,5478,'Pasalulu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(39,5478,'Podol','2020-02-21 16:24:25','2020-02-21 16:24:25'),(40,5478,'Sangaji Nyeku','2020-02-21 16:24:25','2020-02-21 16:24:25'),(41,5478,'Soasangaji','2020-02-21 16:24:25','2020-02-21 16:24:25'),(42,5478,'Tengowango (Teongowango)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(43,5478,'Todoke','2020-02-21 16:24:25','2020-02-21 16:24:25'),(44,5478,'Togoreba Sungi (Tugureba Sungi)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(45,5478,'Togoreba Tua','2020-02-21 16:24:25','2020-02-21 16:24:25'),(46,5478,'Togowo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(47,5478,'Tolisaor','2020-02-21 16:24:25','2020-02-21 16:24:25'),(48,5478,'Tuguis','2020-02-21 16:24:25','2020-02-21 16:24:25'),(49,5478,'Tukuoku','2020-02-21 16:24:25','2020-02-21 16:24:25'),(50,5479,'Acango','2020-02-21 16:24:25','2020-02-21 16:24:25'),(51,5479,'Akediri','2020-02-21 16:24:25','2020-02-21 16:24:25'),(52,5479,'Bobanehena','2020-02-21 16:24:25','2020-02-21 16:24:25'),(53,5479,'Bobo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(54,5479,'Bobo Jiko','2020-02-21 16:24:25','2020-02-21 16:24:25'),(55,5479,'Buku Bualawa','2020-02-21 16:24:25','2020-02-21 16:24:25'),(56,5479,'Buku Maadu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(57,5479,'Bukumatiti','2020-02-21 16:24:25','2020-02-21 16:24:25'),(58,5479,'Galala','2020-02-21 16:24:25','2020-02-21 16:24:25'),(59,5479,'Gamlamo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(60,5479,'Gamtala','2020-02-21 16:24:25','2020-02-21 16:24:25'),(61,5479,'Guaemaadu (Guaimaadu)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(62,5479,'Guaeria','2020-02-21 16:24:25','2020-02-21 16:24:25'),(63,5479,'Gufasa','2020-02-21 16:24:25','2020-02-21 16:24:25'),(64,5479,'Hate Bicara','2020-02-21 16:24:25','2020-02-21 16:24:25'),(65,5479,'Hoku Hoku Kie (Huku-Huku Kie)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(66,5479,'Idamdehe','2020-02-21 16:24:25','2020-02-21 16:24:25'),(67,5479,'Idamdehe Gamsungi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(68,5479,'Jalan Baru','2020-02-21 16:24:25','2020-02-21 16:24:25'),(69,5479,'Kuripasai','2020-02-21 16:24:25','2020-02-21 16:24:25'),(70,5479,'Loloy (Lolori)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(71,5479,'Marmabati (Marimbati)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(72,5479,'Matui','2020-02-21 16:24:25','2020-02-21 16:24:25'),(73,5479,'Pateng','2020-02-21 16:24:25','2020-02-21 16:24:25'),(74,5479,'Payo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(75,5479,'Pornity (Porniti)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(76,5479,'Saria','2020-02-21 16:24:25','2020-02-21 16:24:25'),(77,5479,'Soakonora','2020-02-21 16:24:25','2020-02-21 16:24:25'),(78,5479,'Taboso (Tobosa/Tabaso)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(79,5479,'Tauro','2020-02-21 16:24:25','2020-02-21 16:24:25'),(80,5479,'Tedeng','2020-02-21 16:24:25','2020-02-21 16:24:25'),(81,5479,'Todowangi (Todowongi)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(82,5479,'Tuada','2020-02-21 16:24:25','2020-02-21 16:24:25'),(83,5479,'Ulo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(84,5480,'Ake Jailolo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(85,5480,'Akeara','2020-02-21 16:24:25','2020-02-21 16:24:25'),(86,5480,'Akelaha','2020-02-21 16:24:25','2020-02-21 16:24:25'),(87,5480,'Bangkit Rahmat','2020-02-21 16:24:25','2020-02-21 16:24:25'),(88,5480,'Biamaahi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(89,5480,'Bobane Dano','2020-02-21 16:24:25','2020-02-21 16:24:25'),(90,5480,'Braha','2020-02-21 16:24:25','2020-02-21 16:24:25'),(91,5480,'Dodinga','2020-02-21 16:24:25','2020-02-21 16:24:25'),(92,5480,'Domato','2020-02-21 16:24:25','2020-02-21 16:24:25'),(93,5480,'Gamlenge','2020-02-21 16:24:25','2020-02-21 16:24:25'),(94,5480,'Hijrah','2020-02-21 16:24:25','2020-02-21 16:24:25'),(95,5480,'Moiso','2020-02-21 16:24:25','2020-02-21 16:24:25'),(96,5480,'Ratem','2020-02-21 16:24:25','2020-02-21 16:24:25'),(97,5480,'Rioribati','2020-02-21 16:24:25','2020-02-21 16:24:25'),(98,5480,'Sidangoli Dehe','2020-02-21 16:24:25','2020-02-21 16:24:25'),(99,5480,'Sidangoli Gam','2020-02-21 16:24:25','2020-02-21 16:24:25'),(100,5480,'Suka Damai','2020-02-21 16:24:25','2020-02-21 16:24:25'),(101,5480,'Taba Damai (Db)','2020-02-21 16:24:25','2020-02-21 16:24:25'),(102,5480,'Tataleka','2020-02-21 16:24:25','2020-02-21 16:24:25'),(103,5480,'Tewe','2020-02-21 16:24:25','2020-02-21 16:24:25'),(104,5480,'Toniku','2020-02-21 16:24:25','2020-02-21 16:24:25'),(105,5480,'Tuguraci','2020-02-21 16:24:25','2020-02-21 16:24:25'),(106,5481,'Aruku','2020-02-21 16:24:25','2020-02-21 16:24:25'),(107,5481,'Baja','2020-02-21 16:24:25','2020-02-21 16:24:25'),(108,5481,'Bakun','2020-02-21 16:24:25','2020-02-21 16:24:25'),(109,5481,'Bakun Pantai','2020-02-21 16:24:25','2020-02-21 16:24:25'),(110,5481,'Bantol','2020-02-21 16:24:25','2020-02-21 16:24:25'),(111,5481,'Barataku','2020-02-21 16:24:25','2020-02-21 16:24:25'),(112,5481,'Bilote','2020-02-21 16:24:25','2020-02-21 16:24:25'),(113,5481,'Bosala','2020-02-21 16:24:25','2020-02-21 16:24:25'),(114,5481,'Buo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(115,5481,'Gamkahe','2020-02-21 16:24:25','2020-02-21 16:24:25'),(116,5481,'Jangailulu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(117,5481,'Jano','2020-02-21 16:24:25','2020-02-21 16:24:25'),(118,5481,'Kahatola','2020-02-21 16:24:25','2020-02-21 16:24:25'),(119,5481,'Kedi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(120,5481,'Laba Besar','2020-02-21 16:24:25','2020-02-21 16:24:25'),(121,5481,'Laba Kecil','2020-02-21 16:24:25','2020-02-21 16:24:25'),(122,5481,'Linggua','2020-02-21 16:24:25','2020-02-21 16:24:25'),(123,5481,'Pumadada','2020-02-21 16:24:25','2020-02-21 16:24:25'),(124,5481,'Salu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(125,5481,'Soa-Sio','2020-02-21 16:24:25','2020-02-21 16:24:25'),(126,5481,'Tasye','2020-02-21 16:24:25','2020-02-21 16:24:25'),(127,5481,'Tolofuo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(128,5481,'Tomodo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(129,5481,'Tosomolo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(130,5481,'Totala','2020-02-21 16:24:25','2020-02-21 16:24:25'),(131,5481,'Totala Jaya','2020-02-21 16:24:25','2020-02-21 16:24:25'),(132,5481,'Tuakara','2020-02-21 16:24:25','2020-02-21 16:24:25'),(133,5481,'Tuguis','2020-02-21 16:24:25','2020-02-21 16:24:25'),(134,5482,'Balisoan','2020-02-21 16:24:25','2020-02-21 16:24:25'),(135,5482,'Balisoan Utara','2020-02-21 16:24:25','2020-02-21 16:24:25'),(136,5482,'Dere','2020-02-21 16:24:25','2020-02-21 16:24:25'),(137,5482,'Golo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(138,5482,'Goro Goro','2020-02-21 16:24:25','2020-02-21 16:24:25'),(139,5482,'Jaraoke','2020-02-21 16:24:25','2020-02-21 16:24:25'),(140,5482,'Lako Akediri','2020-02-21 16:24:25','2020-02-21 16:24:25'),(141,5482,'Lako Akelamo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(142,5482,'Peot','2020-02-21 16:24:25','2020-02-21 16:24:25'),(143,5482,'Ropu Tengah Hulu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(144,5482,'Sasur','2020-02-21 16:24:25','2020-02-21 16:24:25'),(145,5482,'Sasur Pantai','2020-02-21 16:24:25','2020-02-21 16:24:25'),(146,5482,'Susupu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(147,5482,'Tacici','2020-02-21 16:24:25','2020-02-21 16:24:25'),(148,5482,'Tacim','2020-02-21 16:24:25','2020-02-21 16:24:25'),(149,5482,'Taraudu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(150,5482,'Taruba','2020-02-21 16:24:25','2020-02-21 16:24:25'),(151,5482,'Todahe','2020-02-21 16:24:25','2020-02-21 16:24:25'),(152,5482,'Worat Worat','2020-02-21 16:24:25','2020-02-21 16:24:25'),(153,5483,'Air Panas','2020-02-21 16:24:25','2020-02-21 16:24:25'),(154,5483,'Akelamo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(155,5483,'Aketola','2020-02-21 16:24:25','2020-02-21 16:24:25'),(156,5483,'Awer','2020-02-21 16:24:25','2020-02-21 16:24:25'),(157,5483,'Campaka','2020-02-21 16:24:25','2020-02-21 16:24:25'),(158,5483,'Gamnyial','2020-02-21 16:24:25','2020-02-21 16:24:25'),(159,5483,'Gamomeng','2020-02-21 16:24:25','2020-02-21 16:24:25'),(160,5483,'Gamsungi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(161,5483,'Goal','2020-02-21 16:24:25','2020-02-21 16:24:25'),(162,5483,'Golago Kusuma','2020-02-21 16:24:25','2020-02-21 16:24:25'),(163,5483,'Hoku Hoku Gam','2020-02-21 16:24:25','2020-02-21 16:24:25'),(164,5483,'Idamgamlamo','2020-02-21 16:24:25','2020-02-21 16:24:25'),(165,5483,'Loce','2020-02-21 16:24:25','2020-02-21 16:24:25'),(166,5483,'Ngaon','2020-02-21 16:24:25','2020-02-21 16:24:25'),(167,5483,'Sidodadi','2020-02-21 16:24:25','2020-02-21 16:24:25'),(168,5483,'Taba Campaka','2020-02-21 16:24:25','2020-02-21 16:24:25'),(169,5483,'Taraudu Kusu','2020-02-21 16:24:25','2020-02-21 16:24:25'),(170,5483,'Tibobo','2020-02-21 16:24:25','2020-02-21 16:24:25');
/*!40000 ALTER TABLE `sfd_master_desa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_master_kecamatan`
--

DROP TABLE IF EXISTS `sfd_master_kecamatan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_master_kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=5484 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_master_kecamatan`
--

LOCK TABLES `sfd_master_kecamatan` WRITE;
/*!40000 ALTER TABLE `sfd_master_kecamatan` DISABLE KEYS */;
INSERT INTO `sfd_master_kecamatan` VALUES (5476,'Kecamatan Ibu','2020-02-04 16:35:26','2020-02-21 16:18:42'),(5477,'Kecamatan Ibu Selatan','2020-02-04 16:35:26','2020-02-21 16:18:48'),(5478,'Kecamatan Ibu Utara','2020-02-04 16:35:26','2020-02-21 16:18:55'),(5479,'Kecamatan Jailolo','2020-02-04 16:35:26','2020-02-21 16:19:09'),(5480,'Kecamatan Jailolo Selatan','2020-02-04 16:35:26','2020-02-21 16:19:16'),(5481,'Kecamatan Loloda','2020-02-04 16:35:26','2020-02-21 16:19:35'),(5482,'Kecamatan Sahu','2020-02-04 16:35:26','2020-02-21 16:19:25'),(5483,'Kecamatan Sahu Timur','2020-02-04 16:35:26','2020-02-21 16:19:30');
/*!40000 ALTER TABLE `sfd_master_kecamatan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_master_penerimaan`
--

DROP TABLE IF EXISTS `sfd_master_penerimaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_master_penerimaan` (
  `id_penerimaan` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `kode_rekening` varchar(10) DEFAULT NULL,
  `nama_penerimaan` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_penerimaan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_master_penerimaan`
--

LOCK TABLES `sfd_master_penerimaan` WRITE;
/*!40000 ALTER TABLE `sfd_master_penerimaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_master_penerimaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_pemerintahaan`
--

DROP TABLE IF EXISTS `sfd_pemerintahaan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_pemerintahaan` (
  `id_pemerintahan` int(11) NOT NULL AUTO_INCREMENT,
  `jumlah_kecamatan` int(11) DEFAULT NULL,
  `jumlah_kelurahan` int(11) DEFAULT NULL,
  `jumlah_desa` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pemerintahan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_pemerintahaan`
--

LOCK TABLES `sfd_pemerintahaan` WRITE;
/*!40000 ALTER TABLE `sfd_pemerintahaan` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_pemerintahaan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_profil_kabupaten`
--

DROP TABLE IF EXISTS `sfd_profil_kabupaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_profil_kabupaten` (
  `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT,
  `kode_wilayah` varchar(50) DEFAULT NULL,
  `nama_kabupaten` varchar(100) DEFAULT NULL,
  `id_kepala` int(11) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `maps` text,
  `logo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kabupaten`),
  KEY `kode_wilayah` (`kode_wilayah`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_profil_kabupaten`
--

LOCK TABLES `sfd_profil_kabupaten` WRITE;
/*!40000 ALTER TABLE `sfd_profil_kabupaten` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_profil_kabupaten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_realisasi_anggaran_belanja`
--

DROP TABLE IF EXISTS `sfd_realisasi_anggaran_belanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_realisasi_anggaran_belanja` (
  `id_realisasi_belaja` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` date DEFAULT NULL,
  `nobukti` varchar(25) DEFAULT NULL,
  `id_renacana_belanja` int(11) DEFAULT NULL,
  `deskripsi` text,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_realisasi_belaja`),
  KEY `id_renacana_belanja` (`id_renacana_belanja`) USING BTREE,
  CONSTRAINT `realisasi_anggaran_belanja_ibfk_1` FOREIGN KEY (`id_renacana_belanja`) REFERENCES `sfd_rencana_anggaran_belanja` (`id_rencana_belanja`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_realisasi_anggaran_belanja`
--

LOCK TABLES `sfd_realisasi_anggaran_belanja` WRITE;
/*!40000 ALTER TABLE `sfd_realisasi_anggaran_belanja` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_realisasi_anggaran_belanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_realisasi_penerimaan_anggaran`
--

DROP TABLE IF EXISTS `sfd_realisasi_penerimaan_anggaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_realisasi_penerimaan_anggaran` (
  `id_realiasasi_penerimaan` int(11) NOT NULL AUTO_INCREMENT,
  `id_renacan_penerimaan` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `deskripsi` text,
  `nominal_diterima` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_realiasasi_penerimaan`),
  KEY `id_renacan_penerimaan` (`id_renacan_penerimaan`) USING BTREE,
  CONSTRAINT `realisasi_penerimaan_anggaran_ibfk_1` FOREIGN KEY (`id_renacan_penerimaan`) REFERENCES `sfd_rencana_penerimaan_anggaran` (`id_renacan_penerimaan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_realisasi_penerimaan_anggaran`
--

LOCK TABLES `sfd_realisasi_penerimaan_anggaran` WRITE;
/*!40000 ALTER TABLE `sfd_realisasi_penerimaan_anggaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_realisasi_penerimaan_anggaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_rencana_anggaran_belanja`
--

DROP TABLE IF EXISTS `sfd_rencana_anggaran_belanja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_rencana_anggaran_belanja` (
  `id_rencana_belanja` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `nobukti` varchar(20) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `id_anggaran_belanja` int(11) DEFAULT NULL,
  `deskripsi` text,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_rencana_belanja`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `rencana_anggaran_belanja_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_rencana_anggaran_belanja`
--

LOCK TABLES `sfd_rencana_anggaran_belanja` WRITE;
/*!40000 ALTER TABLE `sfd_rencana_anggaran_belanja` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_rencana_anggaran_belanja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_rencana_penerimaan_anggaran`
--

DROP TABLE IF EXISTS `sfd_rencana_penerimaan_anggaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_rencana_penerimaan_anggaran` (
  `id_renacan_penerimaan` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `id_penerimaan` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `deskripsi` text,
  `nominal_rencana` decimal(20,2) DEFAULT NULL,
  `realisasi` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_renacan_penerimaan`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  KEY `id_penerimaan` (`id_penerimaan`) USING BTREE,
  CONSTRAINT `rencana_penerimaan_anggaran_ibfk_1` FOREIGN KEY (`id_penerimaan`) REFERENCES `sfd_master_penerimaan` (`id_penerimaan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `rencana_penerimaan_anggaran_ibfk_2` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_rencana_penerimaan_anggaran`
--

LOCK TABLES `sfd_rencana_penerimaan_anggaran` WRITE;
/*!40000 ALTER TABLE `sfd_rencana_penerimaan_anggaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_rencana_penerimaan_anggaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_roles`
--

DROP TABLE IF EXISTS `sfd_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_roles` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_roles`
--

LOCK TABLES `sfd_roles` WRITE;
/*!40000 ALTER TABLE `sfd_roles` DISABLE KEYS */;
INSERT INTO `sfd_roles` VALUES (1,'Superadmin',NULL,NULL),(2,'Administrator',NULL,NULL),(3,'Staff',NULL,NULL),(4,'Tamu',NULL,NULL);
/*!40000 ALTER TABLE `sfd_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_sejarah_desa`
--

DROP TABLE IF EXISTS `sfd_sejarah_desa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_sejarah_desa` (
  `id_sejarah_desa` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `sejarah` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_sejarah_desa`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `sejarah_desa_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_sejarah_desa`
--

LOCK TABLES `sfd_sejarah_desa` WRITE;
/*!40000 ALTER TABLE `sfd_sejarah_desa` DISABLE KEYS */;
INSERT INTO `sfd_sejarah_desa` VALUES (1,11,'<strong>Sahu Timur</strong> adalah sebuah kecamatan di Kabupaten Halmahera Barat, Maluku Utara, Indonesia.','2020-02-09 08:38:22','2020-07-14 08:51:20');
/*!40000 ALTER TABLE `sfd_sejarah_desa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_sejarah_kabupaten`
--

DROP TABLE IF EXISTS `sfd_sejarah_kabupaten`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_sejarah_kabupaten` (
  `id_sejarah` int(11) NOT NULL AUTO_INCREMENT,
  `sejarah` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_sejarah`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_sejarah_kabupaten`
--

LOCK TABLES `sfd_sejarah_kabupaten` WRITE;
/*!40000 ALTER TABLE `sfd_sejarah_kabupaten` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_sejarah_kabupaten` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_statistik_desa`
--

DROP TABLE IF EXISTS `sfd_statistik_desa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_statistik_desa` (
  `id_statistik_desa` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `jumlah_kk` int(11) DEFAULT NULL,
  `jumlah_penduduk` int(11) DEFAULT NULL,
  `jumlah_laki_laki` int(11) DEFAULT NULL,
  `jumlah_perempuan` int(11) DEFAULT NULL,
  `balita` int(11) DEFAULT NULL,
  `anak` int(11) DEFAULT NULL,
  `remaja` int(11) DEFAULT NULL,
  `dewasa` int(11) DEFAULT NULL,
  `orang_tua` int(11) DEFAULT NULL,
  `tgl_statistik` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_statistik_desa`),
  UNIQUE KEY `id_statistik_desa` (`id_statistik_desa`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `statistik_desa_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_statistik_desa`
--

LOCK TABLES `sfd_statistik_desa` WRITE;
/*!40000 ALTER TABLE `sfd_statistik_desa` DISABLE KEYS */;
INSERT INTO `sfd_statistik_desa` VALUES (2,11,123,3,3,3,3,3,3,3,3,'2020-11-14','2020-11-14 08:58:45','2020-11-14 09:02:29');
/*!40000 ALTER TABLE `sfd_statistik_desa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_tahun_anggaran`
--

DROP TABLE IF EXISTS `sfd_tahun_anggaran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_tahun_anggaran` (
  `id_tahun_anggaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_tahun_anggaran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_tahun_anggaran`
--

LOCK TABLES `sfd_tahun_anggaran` WRITE;
/*!40000 ALTER TABLE `sfd_tahun_anggaran` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_tahun_anggaran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_users`
--

DROP TABLE IF EXISTS `sfd_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_desa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_user`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `FK_sfd_users_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Tabel pengguna web / aplikasi';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_users`
--

LOCK TABLES `sfd_users` WRITE;
/*!40000 ALTER TABLE `sfd_users` DISABLE KEYS */;
INSERT INTO `sfd_users` VALUES (1,'administrator','Administrator','admin@mail.com',NULL,'$2y$10$x0NQnJqJL83Sc.6SP8PfauhBdhpq1YMyRoBfQZhOTRiR3Q7GAwaPO',0,1,'JmAOQCjESrlgMNgPMTazkDxF4dA2USl8J6qKBrhHeS7auekbYd0uLG5Sfezo',0,NULL,'2021-03-25 02:32:48'),(2,'desaloce','Desa Loce','desaloce@mail.com',NULL,'$2y$10$RYkZLd4zjW1khLaR.1IK6uH0ReOkGawgQVaNbaA1.OGfutPLSTvPW',2,1,'3EIbsk4Ba86zmms78OxVjdQIFTqNxxfqJuutcOnGZPA06fmKgU0Ux0P5gews',11,NULL,'2021-06-17 07:00:42'),(4,'desaairpanas@mail.com',NULL,'desaairpanas@mail.com',NULL,'$2y$10$IOdEaX1MoUAejHMTO.5wxuGRUvO8I.qHTiwaQQUHETmNKNyXkqCPm',2,1,NULL,26,'2020-08-23 10:05:27','2020-08-23 10:05:27');
/*!40000 ALTER TABLE `sfd_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sfd_visi_misi`
--

DROP TABLE IF EXISTS `sfd_visi_misi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sfd_visi_misi` (
  `id_visimisi` int(11) NOT NULL AUTO_INCREMENT,
  `visi` text,
  `misi` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_visimisi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sfd_visi_misi`
--

LOCK TABLES `sfd_visi_misi` WRITE;
/*!40000 ALTER TABLE `sfd_visi_misi` DISABLE KEYS */;
/*!40000 ALTER TABLE `sfd_visi_misi` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-24 13:11:02
