<?php

namespace App\Http\Controllers\Backend;

use App\Models\Desa;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		if (\Auth::user()->role_id == 0){
			return view('home.superadmin');
		}elseif( \Auth::user()->role_id == 1){
			return view('home.admin');
		}elseif( \Auth::user()->role_id == 2){
			return view('home.desa');
		}elseif( \Auth::user()->role_id == 3){
			return view('home.tamu');
		}
	}

	public function desa_data(){
		$data = array();
		$desa = \App\Models\Desa::orderby('id_desa', 'desc')->where('id_desa','!=',0)->get();
		foreach ($desa as $list) {
			$row = array();
			$row[] = $list->kode_wilayah;
			$row[] = '<button onclick="view_data(\''.route('manajemen.desa.show',$list->id_desa).'\',\'Detail Desa\',\''.route('manajemen.desa.detail',$list->id_desa).'\',\'Edit\',\'modal-lg\')" class="btn btn-default btn-xs">'.ucwords($list->nama_desa).'</button>';
			$row[] = '<a href="https://'.$list->website.'" class="btn btn-primary btn-xs" target="_blank" data-toggle="tooltip" data-placement="top" title="Open https://'.$list->website.' in new tab.">https://'.$list->website.'</a>';
			$row[] = ($list->status==1)?'<button class="btn btn-success btn-xs">Aktif</button>':'<button class="btn btn-danger btn-sm">Tidak Aktif</button>';
			$row[] = date('Y-m-d H:i:s', strtotime($list->created_at));
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
		// $output = $data;
		// return json_encode($output);
	}
}
