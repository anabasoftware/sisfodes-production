<?php

namespace App\Http\Controllers\Backend;

use App\Models\Berita;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Support\Str;

class BeritaController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('website.berita.index');
	}

	public function berita_data()
	{
		$data = array();
		$berita = Berita::orderby('id_berita', 'desc')->get();
		foreach ($berita as $list) {
			$row = array();
			$row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="location.href=\''.route('berita.detail',$list->id_berita).'\'" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\'' . $list->id_berita .'\',\''.route('website.berita').'\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';   
			$row[] = $list->judul_berita;
			$row[] = $list->get_desa->nama_desa;
			$row[] = $list->get_kategori_berita->nama_kategori;
			// $row[] = $list->tags()->id_berita_tag;
			$row[] = Carbon::parse($list->updated_at)->format('Y-m-d H:i:s');
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
	}

	public function berita_tambah()
	{
		$tag = Tag::get()->pluck('nama_tag', 'id_tag_berita');
        $kategori = \App\Models\KategoriBerita::all();
		return view('website.berita.tambah-berita')
        ->with([
            'kategori' => $kategori,
        ]);
	}

	public function berita_simpan (Request $request)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'judul_berita' => 'required',
				'isi_berita' => 'required',
				'slug_judul_berita' => 'required',
				'id_kategori_berita' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('berita.tambah')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}

		$berita = new Berita;
		$berita->judul_berita = $request['judul_berita'];
		$berita->isi_berita = $request['isi_berita'];
		$berita->id_kategori_berita = $request['id_kategori_berita'];
		$berita->slug_judul_berita = $request['slug_judul_berita'];
		$berita->publikasi = $request['publikasi'];
		$berita->id_desa = $request['id_desa'];
		$berita->save();

		// if($berita){
		// 	$tag_namas = explode(',',$request['tags']);
		// 	$tag_ids = [];
		// 	foreach ($tag_namas as $tag) {
		// 		$slug_tag = str_replace(' ', '-', $tag);
		// 		$tag = Tag::firstOrCreate([
		// 			'id_desa' => $request['id_desa'],
		// 			'nama_tag' => $tag,
		// 			'slug_tag' => $slug_tag,
		// 		]);
				
		// 		if($tag){
		// 			$tag_ids[] = $tag->id_berita_tag;
		// 		}
		// 		$berita->tags()->sync($tag_ids);
		// 	}
		// }

		if($berita){
			return redirect()
			->route('website.berita')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return redirect()
			->route('berita.tambah')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}
	}

	public function berita_detail($id)
	{
		$data = Berita::where('id_berita', $id)->first();
        $kategori = \App\Models\KategoriBerita::all();
        return view('website.berita.tambah-berita')->with([
            'data' => $data,
            'kategori' => $kategori,
        ]);
	}

	public function berita_hapus($id)
	{
		$data = Berita::where('id_berita', $id)->first();
		if (!empty($data->id_berita)) {
			$data->delete();
			if ($data){
				return redirect()
				->route('website.berita')
				->with(['success'=>true, 'message' => 'Data berhasil dihapus.'], 200);
			}else{
				return redirect()
				->route('website.berita')
				->with(['success'=>false, 'message' => 'Gagal menghapus data.'], 204);
			}
		}
	}

	public function berita_update (Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'judul_berita' => 'required',
				'isi_berita' => 'required',
				'slug_judul_berita' => 'required',
				'id_kategori_berita' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('berita.tambah')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}

		$berita = Berita::where('id_berita', $id)->first();
		$berita->judul_berita = $request['judul_berita'];
		$berita->isi_berita = $request['isi_berita'];
		$berita->id_kategori_berita = $request['id_kategori_berita'];
		$berita->slug_judul_berita = $request['slug_judul_berita'];
		$berita->publikasi = $request['publikasi'];
		$berita->id_desa = $request['id_desa'];
		$berita->update();

		// if($berita){
		// 	$tag_namas = explode(',',$request['tags']);
		// 	$tag_ids = [];
		// 	foreach ($tag_namas as $tag) {
		// 		$slug_tag = str_replace(' ', '-', $tag);
		// 		$tag = Tag::firstOrCreate([
		// 			'id_desa' => $request['id_desa'],
		// 			'nama_tag' => $tag,
		// 			'slug_tag' => $slug_tag,
		// 		]);
				
		// 		if($tag){
		// 			$tag_ids[] = $tag->id_berita_tag;
		// 		}
		// 		$berita->tags()->sync($tag_ids);
		// 	}
		// }

		if($berita){
			return redirect()
			->route('website.berita')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return redirect()
			->route('berita.tambah')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}
	}
}
