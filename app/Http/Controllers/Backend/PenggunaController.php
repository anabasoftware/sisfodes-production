<?php

namespace App\Http\Controllers\Backend;

use App\Models\Pengguna;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PenggunaController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('pengaturan.pengguna.index');
	}

	public function pengguna_update (Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
			'email' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()
				->route('identitas.index')
				->withErrors($validate)
				->withInput();
		}
		$pengguna = \App\Models\User::where('id_user', $id)->first();
		$pengguna->email = $request['email'];
        if(!empty($request['password'])){
            $pengguna->password = bcrypt($request['password']);
		}
		$pengguna->user_name = $request['user_name'];
		
		$pengguna->update();
		if($pengguna->update()){
			return redirect()
			->route('identitas.index')
			->with(['success'=>true, 'message' => 'Data berhasil diperbarui.']);
		}else{
			return redirect()
			->route('identitas.index')
			->with(['success'=>false, 'message' => 'Data gagal diperbarui.']);
		}
	}

}
