<?php

namespace App\Http\Controllers\Backend\apbdes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Apbdes\RekeningBank;
use App\Models\Apbdes\ObjekRekening;


class BankDesaController extends Controller
{
    //
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('apbdes.rekeningbank.index');

    }

    /*
    | ---------------------------------------------------
    | Bank Desa Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_rekening($id)
    {
        $akun     = ObjekRekening::join('jenis_rekening','jenis_rekening.id_jenis_rekening','=','objek_rekening.id_jenis_rekening')->
                                   where('kode_jenis_rekening','=','1.1.1')->get();
        $rekening = RekeningBank::where('id_bank', '=', $id)->first();
        return view('apbdes.rekeningbank.form-rekening-bank', ['rekening' => $rekening,'akun' => $akun]);

    }

    public function rekening_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'kode_rincian'   => 'required',
                'norek'          => 'required',
                'nama_bank'      => 'required',
                
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('bank_desa')->UpdateOrInsert(
                ['id_bank' => $request['id_bank']],
                [
                    'id_desa' => \Auth::user()->id_desa,
                    'kode_rincian'  => $request['kode_rincian'],
                    'norek'         => $request['norek'],
                    'nama_bank'     => $request['nama_bank'],
                    'kantor_cabang' => $request['kantor_cabang'],
                    'pemilik'       => $request['pemilik'],
                    'alamat'        => $request['alamat'],
                    'notelepon'     => $request['notelepon'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function rekening_data()
    {
        $data = array();
        $bankdesa = RekeningBank::orderby('id_bank', 'asc')->where('id_desa', \Auth::user()->id_desa)->get();
        foreach ($bankdesa as $list) {
            $row = array();
            $row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="view_form(\'' . route('refbankdesa.showform', $list->id_bank) . '\',\'Edit Data\',\'Simpan\',\'modal-md\',\'add\',\'' . $list->id_bank . '\',\'refbankdesa\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\'' . $list->id_bank . '\',\'refbankdesa\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';
            $row[] = $list->kode_rincian;
            $row[] = $list->norek;
            $row[] = $list->nama_bank;
            $row[] = $list->kantor_cabang;
            $row[] = $list->pemilik;
            $row[] = $list->alamat;
            $row[] = $list->notelepon;
            $data[] = $row;
        }
        
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function rekening_hapus($id)
    {
        $bankdesa = RekeningBank::where('id_sumber_dana', '=', $id)->first();
        if ($bankdesa != null) {
            $bankdesa->delete();
            if (!$sumberdana->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }
}
