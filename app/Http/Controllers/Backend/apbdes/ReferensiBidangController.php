<?php

namespace App\Http\Controllers\Backend\apbdes;

use App\Http\Controllers\Controller;
use App\Models\apbdes\Bidang;
use App\Models\apbdes\SubBidang;
use App\Models\apbdes\Kegiatan;
use DB;
use Illuminate\Http\Request;

class ReferensiBidangController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('apbdes.kegiatan.index');

    }

    /*
    | ---------------------------------------------------
    | Bidang Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_bidang($id)
    {
        $bidang = Bidang::where('id_bidang', '=', $id)->first();
        return view('apbdes.kegiatan.form-bidang-dan-kegiatan', ['bidang' => $bidang]);

    }

    public function bidang_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'kode_bidang' => 'required',
                'nama_bidang' => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('bidang')->UpdateOrInsert(
                ['id_bidang' => $request['id_bidang']],
                [
                    'id_desa' => \Auth::user()->id_desa,
                    'kode_bidang' => $request['kode_bidang'],
                    'nama_bidang' => $request['nama_bidang'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function bidang_data()
    {
        $data = array();
        $bidang = Bidang::orderby('id_bidang', 'desc')->where('id_desa', \Auth::user()->id_desa)->get();
        foreach ($bidang as $list) {
            $row = array();
            $row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="view_form(\'' . route('refbidang.showform', $list->id_bidang) . '\',\'Edit Data\',\'Simpan\',\'modal-md\',\'add\',\'' . $list->id_bidang . '\',\'refbidang\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\'' . $list->id_bidang . '\',\'refbidang\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';
            $row[] = $list->kode_bidang;
            $row[] = $list->nama_bidang;
            $data[] = $row;
        }
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function bidang_hapus($id)
    {
        $bidang = Bidang::where('id_bidang', '=', $id)->first();
        if ($bidang != null) {
            $bidang->delete();
            if (!$bidang->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }

    /*
    | ---------------------------------------------------
    | Sub Bidang Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_subbidang($id)
    {
        
     //   $subbidang = SubBidang::where('id_sub_bidang',$id)->firs();
        $bidang    = Bidang::where('id_desa', \Auth::user()->id_desa)->get();
        $subbidang = Subbidang::where('id_sub_bidang',$id)->first();

        return view('apbdes.kegiatan.form-sub-bidang',['bidang' => $bidang, 'subbidang' => $subbidang]);

    }

    public function subbidang_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'id_bidang'         => 'required',
                'kode_sub_bidang'   => 'required',
                'nama_sub_bidang'   => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('sub_bidang')->UpdateOrInsert(
                ['id_sub_bidang' => $request['id_sub_bidang']],
                [                    
                    'id_bidang'         => $request['id_bidang'],
                    'kode_sub_bidang'   => $request['kode_sub_bidang'],
                    'nama_sub_bidang'   => $request['nama_sub_bidang'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function subbidang_data()
    {
        $data = array();
        $subbidang = SubBidang::with(['bidang'])->get();
        foreach ($subbidang as $list) {
            $row = array();
            $row[] = '
            <div class="btn-group flex-wrap">
                <button type="button" onclick="view_form(\'' . route('refsubbidang.showform', $list->id_sub_bidang) . '\',\'Edit Data\',\'Update\',\'modal-md\',\'add\',\'' . $list->id_sub_bidang . '\',\'refbidang\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                <button type="button" onclick="delete_data(\'' . $list->id_sub_bidang . '\',\'refsubbidang\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
            </div>';
            $row[] = $list->bidang->nama_bidang;
            $row[] = $list->kode_sub_bidang;
            $row[] = $list->nama_sub_bidang;
            $data[] = $row;
        }
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function subbidang_hapus($id)
    {
        $subbidang = SubBidang::where('id_sub_bidang', '=', $id)->first();
        if ($subbidang != null) {
            $subbidang->delete();
            if (!$subbidang->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }



    /*
    | ---------------------------------------------------
    | Kegiatan Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_kegiatan($id)
    {
        
     //   $subbidang = SubBidang::where('id_sub_bidang',$id)->firs();
        $subbidang = SubBidang::get();
        $kegiatan  = Kegiatan::where('id_kegiatan',$id)->first();

        return view('apbdes.kegiatan.form-kegiatan',['kegiatan' => $kegiatan, 'subbidang' => $subbidang]);

    }

    public function kegiatan_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'id_sub_bidang'     => 'required',
                'kode_kegiatan'     => 'required',
                'nama_kegiatan'     => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('kegiatan')->UpdateOrInsert(
                ['id_kegiatan' => $request['id_kegiatan']],
                [                    
                    'id_desa'       => \Auth::user()->id_desa,
                    'id_sub_bidang' => $request['id_sub_bidang'],
                    'kode_kegiatan' => $request['kode_kegiatan'],
                    'nama_kegiatan' => $request['nama_kegiatan'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function kegiatan_data()
    {
        $data = array();
        $kegiatan = Kegiatan::with('subbidang')->get();
        foreach ($kegiatan as $list) {
            $row = array();
            $row[] = '
            <div class="btn-group flex-wrap">
                <button type="button" onclick="view_form(\'' . route('refkegiatan.showform', $list->id_kegiatan) . '\',\'Edit Data\',\'Update\',\'modal-md\',\'add\',\'' . $list->id_kegiatan . '\',\'refkegiatan\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                <button type="button" onclick="delete_data(\'' . $list->id_kegiatan . '\',\'refkegiatan\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
            </div>';
            $row[] = $list->subbidang->nama_sub_bidang;
            $row[] = $list->kode_kegiatan;
            $row[] = $list->nama_kegiatan;
            $data[] = $row;
        }
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function kegiatan_hapus($id)
    {
        $kegiatan = Kegiatan::where('id_kegiatan', '=', $id)->first();
        if ($kegiatan != null) {
            $kegiatan->delete();
            if (!$kegiatan->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }
}
