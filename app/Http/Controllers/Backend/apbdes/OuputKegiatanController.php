<?php

namespace App\Http\Controllers\Backend\apbdes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use App\Models\Apbdes\OutputDD;
use App\Models\Apbdes\Kegiatan;


class OuputKegiatanController extends Controller
{
    //
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('apbdes.outputkegiatan.index');

    }

    /*
    | ---------------------------------------------------
    | Sumber Dana Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_outputkegiatan($id)
    {
        $outputdd = OutputDD::where('id_output_dd', '=', $id)->first();
        $kegiatan = Kegiatan::where('id_desa', '=', \Auth::user()->id_desa)->get();

        return view('apbdes.outputkegiatan.form-output-kegiatan', ['outputdd' => $outputdd,'kegiatan' => $kegiatan]);

    }

    public function outputkegiatan_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'id_kegiatan' => 'required',
                'kode_output' => 'required',
                'uraian'      => 'required',
                'satuan'      => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('output_dd')->UpdateOrInsert(
                ['id_output_dd' => $request['id_output_dd']],
                [
                    'id_kegiatan' => $request['id_kegiatan'],
                    'kode_output' => $request['kode_output'],
                    'uraian'      => $request['uraian'],
                    'satuan'      => $request['satuan'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function outputkegiatan_data()
    {
        $data = array();
        $outputdd = OutputDD::with('kegiatan')->orderby('id_output_dd', 'desc')->get();
        foreach ($outputdd as $list) {
            $row = array();
            $row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="view_form(\'' . route('refoutputkegiatan.showform', $list->id_output_dd) . '\',\'Edit Data\',\'Simpan\',\'modal-md\',\'add\',\'' . $list->id_output_dd . '\',\'refoutputkegiatan\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\'' . $list->id_output_dd . '\',\'refoutputkegiatan\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';
            $row[] = $list->kegiatan->nama_kegiatan;
            $row[] = $list->kode_output;
            $row[] = $list->uraian;
            $row[] = $list->satuan;
            $data[] = $row;
        }
        
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function outputkegiatan_hapus($id)
    {
        $outputdd = OutputDD::where('id_output_dd', '=', $id)->first();
        if ($outputdd != null) {
            $outputdd->delete();
            if (!$outputdd->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }
}
