<?php

namespace App\Http\Controllers\Backend\apbdes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Apbdes\Akun;
use App\Models\Apbdes\Kelompok;
use App\Models\Apbdes\JenisRekening;
use App\Models\Apbdes\ObjekRekening;
use DB;

class RekeningApbdesController extends Controller
{
    //
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('apbdes.rekeningapb.index');

    }

    /*
    | ---------------------------------------------------
    | akun Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_akun($id)
    {
        $akun = Akun::where('id_akun_rekening', '=', $id)->first();
        return view('apbdes.rekeningapb.form-akun', ['akun' => $akun]);

    }

    public function akun_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'nomer_akun' => 'required',
                'nama_akun' => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('akun_rekening')->UpdateOrInsert(
                ['id_akun_rekening' => $request['id_akun_rekening']],
                [
                    'id_desa' => \Auth::user()->id_desa,
                    'nomer_akun' => $request['nomer_akun'],
                    'nama_akun' => $request['nama_akun'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function akun_data()
    {
        $data = array();
        $akun = Akun::orderby('id_akun_rekening', 'asc')->where('id_desa', \Auth::user()->id_desa)->get();
        foreach ($akun as $list) {
            $row = array();
            $row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="view_form(\'' . route('refakun.showform', $list->id_akun_rekening) . '\',\'Edit Data\',\'Simpan\',\'modal-md\',\'add\',\'' . $list->id_akun_rekening . '\',\'refakun\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\'' . $list->id_akun_rekening . '\',\'refakun\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';
            $row[] = $list->nomer_akun;
            $row[] = $list->nama_akun;
            $data[] = $row;
        }
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function akun_hapus($id)
    {
        $akun = Akun::where('id_akun_rekening', '=', $id)->first();
        if ($akun != null) {
            $akun->delete();
            if (!$akun->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }

    /*
    | ---------------------------------------------------
    | Kelompok akun Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_kelompok($id)
    {
        
     //   $subakun = Subakun::where('id_sub_akun',$id)->firs();
        $akun    = Akun::where('id_desa', \Auth::user()->id_desa)->get();
        $kelompok = Kelompok::where('id_kelompok_rekening',$id)->first();

        return view('apbdes.rekeningapb.form-kelompok',['akun' => $akun, 'kelompok' => $kelompok]);

    }

    public function kelompok_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'id_akun'         => 'required',
                'kode_kelompok'   => 'required',
                'nama_kelompok'   => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('kelompok_rekening')->UpdateOrInsert(
                ['id_kelompok_rekening' => $request['id_kelompok_rekening']],
                [                    
                    'id_akun'         => $request['id_akun'],
                    'kode_kelompok'   => $request['kode_kelompok'],
                    'nama_kelompok'   => $request['nama_kelompok'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function kelompok_data()
    {
        $data = array();
        $kelompok = Kelompok::with(['akun'])->get();
        foreach ($kelompok as $list) {
            $row = array();
            $row[] = '
            <div class="btn-group flex-wrap">
                <button type="button" onclick="view_form(\'' . route('refkelompok.showform', $list->id_kelompok_rekening) . '\',\'Edit Data\',\'Update\',\'modal-md\',\'add\',\'' . $list->id_kelompok_rekening . '\',\'refkelompok\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                <button type="button" onclick="delete_data(\'' . $list->id_kelompok_rekening . '\',\'refkelompok\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
            </div>';
            $row[] = $list->akun->nama_akun;
            $row[] = $list->kode_kelompok;
            $row[] = $list->nama_kelompok;
            $data[] = $row;
        }
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function kelompok_hapus($id)
    {
        $kelompok = Kelompok::where('id_sub_akun', '=', $id)->first();
        if ($kelompok != null) {
            $kelompok->delete();
            if (!$kelompok->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }



    /*
    | ---------------------------------------------------
    | Jenis Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_jenis($id)
    {
        
     //   $subakun = Subakun::where('id_sub_akun',$id)->firs();
        $kelompok = Kelompok::whereHas('akun', function($query){
            $query->where('id_desa','=',\Auth::user()->id_desa);
        })->get();
        $jenis  = JenisRekening::where('id_jenis_rekening',$id)->first();

        return view('apbdes.rekeningapb.form-jenis',['kelompok' => $kelompok, 'jenis' => $jenis]);

    }

    public function jenis_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'id_kelompok_rekening'     => 'required',
                'kode_jenis_rekening'     => 'required',
                'nama_jenis_rekening'     => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('jenis_rekening')->UpdateOrInsert(
                ['id_jenis_rekening' => $request['id_jenis_rekening']],
                [                    
                    'id_kelompok_rekening' => $request['id_kelompok_rekening'],
                    'kode_jenis_rekening' => $request['kode_jenis_rekening'],
                    'nama_jenis_rekening' => $request['nama_jenis_rekening'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function jenis_data()
    {
        $data = array();
        $jenisrekening = JenisRekening::with('kelompok')->get();
        foreach ($jenisrekening as $list) {
            $row = array();
            $row[] = '
            <div class="btn-group flex-wrap">
                <button type="button" onclick="view_form(\'' . route('refjenisrekening.showform', $list->id_jenis_rekening) . '\',\'Edit Data\',\'Update\',\'modal-md\',\'add\',\'' . $list->id_jenis_rekening . '\',\'refjenisrekening\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                <button type="button" onclick="delete_data(\'' . $list->id_jenis_rekening . '\',\'refjenisrekening\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
            </div>';
            $row[] = $list->kelompok->nama_kelompok;
            $row[] = $list->kode_jenis_rekening;
            $row[] = $list->nama_jenis_rekening;
            $data[] = $row;
        }
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function jenis_hapus($id)
    {
        $jenisrekening = JenisRekening::where('id_jenis_rekening', '=', $id)->first();
        if ($jenisrekening != null) {
            $jenisrekening->delete();
            if (!$jenisrekening->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    } 


    /*
    | ---------------------------------------------------
    | Objek Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */


    public function show_form_objek($id)
    {
        
     //   $subakun = Subakun::where('id_sub_akun',$id)->firs();
        $jenisrekening = JenisRekening::join('kelompok_rekening','jenis_rekening.id_kelompok_rekening','=','kelompok_rekening.id_kelompok_rekening')
                                        ->join('akun_rekening','akun_rekening.id_akun_rekening','=','kelompok_rekening.id_akun')
                                        ->where('akun_rekening.id_desa','=',\Auth::user()->id_desa)->get();
        $objekrekening  = ObjekRekening::where('id_objek_rekening',$id)->first();

        return view('apbdes.rekeningapb.form-objek',['jenisrekening' => $jenisrekening, 'objekrekening' => $objekrekening]);

    }

    public function objek_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'id_jenis_rekening'       => 'required',
                'kode_objek_rekening'     => 'required',
                'nama_objek_rekening'     => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('objek_rekening')->UpdateOrInsert(
                ['id_objek_rekening' => $request['id_objek_rekening']],
                [                    
                    'id_jenis_rekening' => $request['id_jenis_rekening'],
                    'kode_objek_rekening' => $request['kode_objek_rekening'],
                    'nama_objek_rekening' => $request['nama_objek_rekening'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function objek_data()
    {
        $data = array();
        $objekrekening = ObjekRekening::with('jenis')->get();
        foreach ($objekrekening as $list) {
            $row = array();
            $row[] = '
            <div class="btn-group flex-wrap">
                <button type="button" onclick="view_form(\'' . route('refobjekrekening.showform', $list->id_objek_rekening) . '\',\'Edit Data\',\'Update\',\'modal-md\',\'add\',\'' . $list->id_objek_rekening . '\',\'refobjekrekening\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                <button type="button" onclick="delete_data(\'' . $list->id_objek_rekening . '\',\'refobjekrekening\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
            </div>';
            $row[] = $list->jenis->nama_jenis_rekening;
            $row[] = $list->kode_objek_rekening;
            $row[] = $list->nama_objek_rekening;
            $data[] = $row;
        }
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function objek_hapus($id)
    {
        $objekrekening = ObjekRekening::where('id_objek_rekening', '=', $id)->first();
        if ($objekrekening != null) {
            $objekrekening->delete();
            if (!$objekrekening->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    } 

}
