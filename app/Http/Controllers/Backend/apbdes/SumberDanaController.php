<?php

namespace App\Http\Controllers\Backend\apbdes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Apbdes\SumberDana;
use DB;

class SumberDanaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('apbdes.sumberdana.index');

    }

    /*
    | ---------------------------------------------------
    | Sumber Dana Controller *
    | -  Author : Joko Purnomo
    | -  Modul  : Apbdes
    | ---------------------------------------------------
     */

    public function show_form_sumberdana($id)
    {
        $sumberdana = SumberDana::where('id_sumber_dana', '=', $id)->first();
        return view('apbdes.sumberdana.form-sumber-dana', ['sumberdana' => $sumberdana]);

    }

    public function sumberdana_simpan(Request $request)
    {

        $message = [
            'required' => '::attribute harus diisi',
        ];

        $validate = \Validator::make($request->all(),
            [
                'kode_sumber' => 'required',
                'nama_sumber' => 'required',
            ], $message);

        if ($validate->fails()) {
            return response()->json(['success' => false, 'msg' => $validate->errors()], 422);
        }

        try {
            DB::table('sumber_dana')->UpdateOrInsert(
                ['id_sumber_dana' => $request['id_sumber_dana']],
                [
                    'id_desa' => \Auth::user()->id_desa,
                    'kode_sumber' => $request['kode_sumber'],
                    'nama_sumber' => $request['nama_sumber'],
                ]
            );

            return response()->json(['succes' => true, 'msg' => 'Data berhasil disimpan'], 200);
        } catch (QueryException $e) {
            return response()->json(['success' => false,
                'error' => $e->getMessage(),
            ], 422);
        }
    }

    public function sumberdana_data()
    {
        $data = array();
        $sumberdana = SumberDana::orderby('id_sumber_dana', 'desc')->where('id_desa', \Auth::user()->id_desa)->get();
        foreach ($sumberdana as $list) {
            $row = array();
            $row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="view_form(\'' . route('refsumberdana.showform', $list->id_sumber_dana) . '\',\'Edit Data\',\'Simpan\',\'modal-md\',\'add\',\'' . $list->id_sumber_dana . '\',\'refbidang\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\'' . $list->id_sumber_dana . '\',\'refsumberdana\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';
            $row[] = $list->kode_sumber;
            $row[] = $list->nama_sumber;
            $data[] = $row;
        }
        
        $output = array('data' => $data);
        return response()->json($output);
    }

    public function sumberdana_hapus($id)
    {
        $sumberdana = SumberDana::where('id_sumber_dana', '=', $id)->first();
        if ($sumberdana != null) {
            $sumberdana->delete();
            if (!$sumberdana->delete()) {
                return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
            } else {
                return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
            }
        }
        return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
    }
}
