<?php

namespace App\Http\Controllers\Backend;

use App\Models\Sejarah;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SejarahController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('desa.sejarah.index');
	}

	public function sejarah_simpan(Request $request)
	{
		$message = [ 'required' => ':attribute harus diisi tidak boleh kosong!'];
		$validate = \Validator::make($request->all(), [
			'id_desa' => 'required',
			'sejarah' => 'required',
		], $message);
		if ($validate->fails() ) {
		  return redirect()->route('sejarah.index')->withErrors($validate)->withInput();
		}
		$sejarah = new \App\Models\Sejarah;
		$sejarah->id_desa = $request['id_desa'];
		$sejarah->sejarah = $request['sejarah'];
		$sejarah->save();
		return redirect()->route('sejarah.index')->with([ 'message' => 'Data berhasil disimpan.', 'status' => 'ok' ], 200);
	}

	public function sejarah_update(Request $request, $id)
	{
		$message = [ 'required' => ':attribute harus diisi tidak boleh kosong!'];
		$validate = \Validator::make($request->all(), [
			'id_desa' => 'required',
			'sejarah' => 'required',
		], $message);
		if ($validate->fails() ) {
		  return redirect()->route('sejarah.index')->withErrors($validate)->withInput();
		}
		$sejarah = \App\Models\Sejarah::where('id_sejarah_desa', $id)->first();
		$sejarah->id_desa = $request['id_desa'];
		$sejarah->sejarah = $request['sejarah'];
		$sejarah->update();
		return redirect()->route('sejarah.index')->with([ 'message' => 'Data berhasil diperbarui.', 'status' => 'ok' ], 200);
	}

	public function sejarah_detail(Request $request, $id)
	{
		$sejarah = \App\Models\Sejarah::where('id_sejarah_desa', $id)->first();
		return json_encode($sejarah);
	}
}
