<?php

namespace App\Http\Controllers\Backend;

use App\Models\Kategoriberita;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KategoriberitaController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('website.berita.kategori');
	}

    public function kategori_data()
    {
		$data = array();
		$banner = \App\Models\KategoriBerita::orderby('id_kategori_berita', 'desc')->where('id_desa', \Auth::user()->id_desa)->get();
		foreach ($banner as $list) {
			$row = array();
			$row[] = '<div class="btn-group flex-wrap">
				<button onclick="window.location.href=\''.route('website.berita.kategori.detail', $list->id_kategori_berita).'\'" class="btn btn-primary btn-xs"><i class="fas fa-pen"></i></button>
				<button onclick="delete_data(\''.$list->id_kategori_berita.'\',\''.route('website.berita.kategori').'\')" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt"></i></button>
				</div>';   
			$row[] = ($list->status==1)?'Aktif':'Tidak Aktif';
			$row[] = $list->nama_kategori;
			$row[] = $list->slug_kategori;
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
    }

	public function kategori_simpan (Request $request)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_kategori' => 'required',
				'slug_kategori' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('website.berita.kategori')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}

		$kategori = new \App\Models\KategoriBerita;
		$kategori->nama_kategori = $request['nama_kategori'];
		$kategori->slug_kategori = $request['slug_kategori'];
        $kategori->status = $request['status'];
        $kategori->id_desa = $request['id_desa'];
		$kategori->save();
		if($kategori){
			return redirect()
			->route('website.berita.kategori')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return redirect()
			->route('website.berita.kategori')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}
	}

	public function kategori_detail ($id)
	{
		$data = \App\Models\KategoriBerita::where('id_kategori_berita', $id)->first();
        return view('website.berita.kategori')->with([
            'data' => $data,
        ]);
	}

	public function kategori_update (Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_kategori' => 'required',
				'slug_kategori' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('website.berita.kategori')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 204);
		}

		$kategori = \App\Models\KategoriBerita::find($id);
		$kategori->nama_kategori = $request['nama_kategori'];
		$kategori->slug_kategori = $request['slug_kategori'];
        $kategori->status = $request['status'];
        $kategori->id_desa = $request['id_desa'];
		$kategori->update();
		if($kategori){
			return redirect()
			->route('website.berita.kategori')
			->with(['success'=>true, 'message' => 'Data berhasil diperbarui.'], 200);
		}else{
			return redirect()
			->route('website.berita.kategori')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 204);
		}
	}

	public function kategori_hapus($id)
	{
		$data = \App\Models\KategoriBerita::where('id_kategori_berita', $id)->first();
		if (!empty($data->id_kategori_berita)) {
			$data->delete();
			if ($data){
				return redirect()
				->route('website.berita.kategori')
				->with(['success'=>true, 'message' => 'Data berhasil dihapus.'], 200);
			}else{
				return redirect()
				->route('website.berita.kategori')
				->with(['success'=>false, 'message' => 'Gagal menghapus data.'], 204);
			}
		}
	}
}
