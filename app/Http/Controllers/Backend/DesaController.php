<?php

namespace App\Http\Controllers\Backend;

use App\Models\Desa;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DesaController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('pengaturan.desa.index');
	}

	public function desa_tambah()
	{
		return view('pengaturan.desa.form');
    }
    
    public function desa_data()
	{
		$data = array();
		$desa = \App\Models\Desa::orderby('id_desa', 'desc')->where('id_desa','!=',0)->get();
		foreach ($desa as $list) {
			$row = array();
			$row[] = '
				<div class="btn-group flex-wrap">
					<button onclick="window.location.href=\''.route('manajemen.desa.detail',$list->id_desa).'\'" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></button>
					<button onclick="delete_data(\''.$list->id_desa.'\')" class="btn btn-danger btn-sm"><i class="fas fa-trash"></i></button>
					<button onclick="view_data(\''.route('manajemen.desa.show',$list->id_desa).'\',\'Detail Desa\',\''.route('manajemen.desa.detail',$list->id_desa).'\',\'Edit\',\'modal-lg\')" class="btn btn-secondary btn-sm"><i class="fas fa-eye"></i></button>
				</div>';   
			$row[] = $list->kode_wilayah;
			$row[] = ucwords($list->nama_desa);
			$row[] = '<a href="https://'.$list->website.'" class="btn btn-primary btn-xs" target="_blank" data-toggle="tooltip" data-placement="top" data-original-title="Open https://'.$list->website.' in new tab.">https://'.$list->website.'</a>';
			$row[] = ($list->status==1)?'<button class="btn btn-success btn-xs">Aktif</button>':'<button class="btn btn-danger btn-sm">Tidak Aktif</button>';
			$row[] = date('Y-m-d H:i:s', strtotime($list->created_at));
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
	}

	public function desa_simpan(Request $request)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_desa' => 'required',
				'id_kecamatan' => 'required',
				'kode_pos' => 'required',
				'alamat' => 'required',
				'website' => 'required',
				'email' => 'required',
				'user_name' => 'required',
				'password' => 'required',
				're_password' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()
				->route('manajemen.desa.tambah')
				->withErrors($validate)
				->withInput();
		}

		$desa = new Desa;
		$desa->kode_wilayah = $request['kode_wilayah'];
		$desa->nama_desa = $request['nama_desa'];
		$desa->id_kecamatan = $request['id_kecamatan'];
		$desa->kode_pos = $request['kode_pos'];
		$desa->alamat = $request['alamat'];
		$desa->website = $request['website'];
		$desa->save();

		if($desa){
			$user = new User;
			$user->email = $request['email'];
			$user->user_name = $request['user_name'];
			if(!empty($request['password'])){
				$user->password = bcrypt($request['password']);
			}
			$user->status = $request['status'];
			$user->role_id = '2';
			$user->id_desa = $desa->getKey();
			$user->save();
			if($user){
				return redirect()
				->route('manajemen.desa.index')
				->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
			}
			return redirect()
			->route('manajemen.desa.index')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}
	}

	public function desa_update(Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_desa' => 'required',
				'id_kecamatan' => 'required',
				'kode_pos' => 'required',
				'alamat' => 'required',
				'website' => 'required',
				'email' => 'required',
				'user_name' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()
				->route('manajemen.desa.detail', $id)
				->withErrors($validate)
				->withInput();
		}

		$desa = \App\Models\Desa::where('id_desa', $id)->first();
		$desa->kode_wilayah = $request['kode_wilayah'];
		$desa->nama_desa = $request['nama_desa'];
		$desa->id_kecamatan = $request['id_kecamatan'];
		$desa->kode_pos = $request['kode_pos'];
		$desa->alamat = $request['alamat'];
		$desa->website = $request['website'];
		$desa->update();

		if($desa){
			$user = \App\Models\User::where('id_desa', $id)->first();;
			$user->email = $request['email'];
			$user->user_name = $request['user_name'];
			if(!empty($request['password'])){
				$user->password = bcrypt($request['password']);
			}
			$user->status = $request['status'];
			$user->update();
			if($user){
				return redirect()
				->route('manajemen.desa.detail', $id)
				->with(['success'=>true, 'message' => 'Data berhasil diperbarui.'], 200);
			}
			return redirect()
			->route('manajemen.desa.detail', $id)
			->with(['success'=>true, 'message' => 'Data berhasil diperbarui.'], 200);
		}
	}

	public static function desa_detail($id){
		$data = \App\Models\Desa::where('id_desa', $id)->first();
        return view('pengaturan.desa.form')->with([
            'data' => $data,
        ]);
	}

	public static function desa_show($id){
		$data = \App\Models\Desa::where('id_desa', $id)->first();
        return view('pengaturan.desa.show')->with([
            'data' => $data,
        ]);
	}
}
