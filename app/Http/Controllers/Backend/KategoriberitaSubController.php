<?php

namespace App\Http\Controllers\Backend;

use App\Models\KategoriBeritaSub;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class KategoriberitaSubController extends Controller
{

    public function index()
    {
        $kategori = \App\Models\KategoriBerita::all();
        $sub_kategori = \App\Models\KategoriBeritaSub::all();
        return view('website.berita.sub-kategori')
        ->with([
            'kategori' => $kategori,
            'sub_kategori' => $sub_kategori,
        ]);
    }

    public function sub_kategori_data()
    {
		$data = array();
		$sub_kategori = \App\Models\KategoriBeritaSub::orderby('id_sub_kategori', 'desc')->where('id_desa', \Auth::user()->id_desa)->get();
		foreach ($sub_kategori as $list) {
			$row = array();
			$row[] = '<div class="btn-group flex-wrap">
				<button onclick="window.location.href=\''.route('website.berita.kategori.sub.detail', $list->id_sub_kategori).'\'" class="btn btn-primary btn-xs"><i class="fas fa-pen"></i></button>
				<button onclick="delete_data(\'sub/hapus/\',\''.$list->id_sub_kategori.'\',\''.route('website.berita.kategori.sub').'\')" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt"></i></button>
				</div>';   
			$row[] = ($list->status==1)?'Aktif':'Tidak Aktif';
			$row[] = $list->kategori->first()->nama_kategori;
			$row[] = $list->nama_sub_kategori;
			$row[] = $list->slug_sub_kategori;
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
    }

	public function sub_kategori_simpan (Request $request)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_sub_kategori' => 'required',
				'slug_sub_kategori' => 'required',
				'id_kategori_berita' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('website.berita.kategori.sub')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}

		$sub_kategori = new \App\Models\KategoriBeritaSub;
		$sub_kategori->nama_sub_kategori = $request['nama_sub_kategori'];
		$sub_kategori->slug_sub_kategori = $request['slug_sub_kategori'];
		$sub_kategori->id_kategori_berita = $request['id_kategori_berita'];
        $sub_kategori->status = $request['status'];
        $sub_kategori->id_desa = $request['id_desa'];
		$sub_kategori->save();
		if($sub_kategori){
			return redirect()
			->route('website.berita.kategori.sub')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return redirect()
			->route('website.berita.kategori.sub')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}
    }

    public function sub_kategori_detail ($id)
	{
        $kategori = \App\Models\KategoriBerita::all();
		$data = \App\Models\KategoriBeritaSub::where('id_sub_kategori', $id)->first();
        return view('website.berita.sub-kategori')->with([
            'data' => $data,
            'kategori' => $kategori,
        ]);
    }

	public function sub_kategori_update (Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_sub_kategori' => 'required',
				'slug_sub_kategori' => 'required',
				'id_kategori_berita' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('website.berita.kategori.sub')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 204);
		}

		$sub_kategori = \App\Models\KategoriBeritaSub::find($id);
		$sub_kategori->nama_sub_kategori = $request['nama_sub_kategori'];
		$sub_kategori->slug_sub_kategori = $request['slug_sub_kategori'];
		$sub_kategori->id_kategori_berita = $request['id_kategori_berita'];
        $sub_kategori->status = $request['status'];
        $sub_kategori->id_desa = $request['id_desa'];
		$sub_kategori->update();
		if($sub_kategori){
			return redirect()
			->route('website.berita.kategori.sub')
			->with(['success'=>true, 'message' => 'Data berhasil diperbarui.'], 200);
		}else{
			return redirect()
			->route('website.berita.kategori.sub')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 204);
		}
    }


	public function sub_kategori_hapus($id)
	{
		$data = \App\Models\KategoriBeritaSub::where('id_sub_kategori', $id)->first();
		if (!empty($data->id_sub_kategori)) {
			$data->delete();
			if ($data){
				return redirect()
				->route('website.berita.kategori.sub')
				->with(['success'=>true, 'message' => 'Data berhasil dihapus.'], 200);
			}else{
				return redirect()
				->route('website.berita.kategori.sub')
				->with(['success'=>false, 'message' => 'Gagal menghapus data.'], 204);
			}
		}
	}
}
