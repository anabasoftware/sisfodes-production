<?php

namespace App\Http\Controllers\Backend;

use App\Models\Layanan;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LayananController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('fasilitas.layanan.index');
	}

	public function layanan_tambah()
	{
		return view('fasilitas.layanan.form-layanan');
	}


	public function layanan_data()
	{
		$data = array();
		$layanan = \App\Models\Layanan::orderby('id_layanan', 'desc')->get();
		foreach ($layanan as $list) {
			$row = array();
			$row[] = '
				<div class="btn-group flex-wrap">
					<button onclick="window.location.href=\''.route('fasilitas.layanan.detail', $list->id_layanan).'\'" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button onclick="delete_data(\''.$list->id_layanan.'\',\''.route('fasilitas.layanan.index').'\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';   
			$row[] = $list->nama_layanan;
			$row[] = htmlentities(substr($list->deskripsi_layanan,0,50)) . '...';
			$row[] = $list->get_desa['nama_desa'];
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
	}

	public function layanan_simpan(Request $request)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_layanan' => 'required',
				'deskripsi_layanan' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('fasilitas.layanan.tambah')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}

		$layanan = new \App\Models\Layanan;
		$layanan->nama_layanan = $request['nama_layanan'];
		$layanan->deskripsi_layanan = $request['deskripsi_layanan'];
		$layanan->id_desa = $request['id_desa'];
		$layanan->kode_wilayah = $request['kode_wilayah'];
		$layanan->slug_layanan = $request['slug_layanan'];

		// echo "<pre>"; print_r($layanan);echo "</pre>";exit;

		$layanan->save();
		if($layanan){
			return redirect()
			->route('fasilitas.layanan.index')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return redirect()
			->route('fasilitas.layanan.tambah')
			->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}
		// if($layanan->save()){
		// 	return response()->json([ 'success' => true, 'message' => 'Data berhasil disimpan.'], 200);
		// }else{
		// 	return response()->json([ 'success' => false, 'error' => $layanan]);
		// }
	}

	public function layanan_detail($id){
		$layanan = \App\Models\Layanan::where('id_layanan', '=', $id)->first();
		// echo json_encode($layanan);
		return view('fasilitas.layanan.form-layanan')->with([
			'layanan' => $layanan
		]);
	}

	public function layanan_update(Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_layanan' => 'required',
				'deskripsi_layanan' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('fasilitas.layanan.detail', $id)
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 422);
		}

		$layanan = \App\Models\Layanan::where('id_layanan','=', $id)->first();
		$layanan->nama_layanan = $request['nama_layanan'];
		$layanan->deskripsi_layanan = $request['deskripsi_layanan'];
		$layanan->id_desa = $request['id_desa'];
		$layanan->kode_wilayah = $request['kode_wilayah'];
		$layanan->slug_layanan = $request['slug_layanan'];
		$layanan->update();
		if($layanan){
			return redirect()
			->route('fasilitas.layanan.index')
			->with(['success'=>true, 'message' => 'Data berhasil diperbarui.'], 200);
		}else{
			return redirect()
			->route('fasilitas.layanan.tambah')
			->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 204);
		}
	}

	public function layanan_hapus($id){
		$layanan = \App\Models\Layanan::where('id_layanan', '=', $id)->first();
		if ($layanan != null) {
			$layanan->delete();
			if(!$layanan->delete()){
				return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
			}else{
				return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
			}
		}
		return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
	}

}
