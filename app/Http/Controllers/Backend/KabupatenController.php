<?php

namespace App\Http\Controllers\Backend;

use App\Models\Kabupaten;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class KabupatenController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		$kabupaten = \App\Models\Kabupaten::where('id_kabupaten', '=', '1')->first();
		return view('kabupaten.index',compact('kabupaten'));
	}

	
	public function kabupaten_store(Request $request){
		
		$id_kabupaten      = $request->input('id_kabupaten');
        $kabupaten         = \App\Models\Kabupaten::find($id_kabupaten);

        if (is_null($kabupaten)) {         
            $kabupaten    = new \App\Models\Kabupaten;
        }
		$kabupaten->id_kabupaten    		= $request['id_kabupaten'];
		$kabupaten->tentang_pemerintahan    = $request['tentang_pemerintahan'];
		$kabupaten->nama_pemerintahan    	= $request['nama_pemerintahan'];
		$kabupaten->kode_wilayah            = $request['kode_wilayah'];
		$kabupaten->kepala_pemerintahan     = $request['kepala_pemerintahan'];
		$kabupaten->email                   = $request['email'];
		$kabupaten->logo                    = $request['logo'];
		$kabupaten->save();

		
		if($kabupaten->save()){
			return response()->json([ 'success' => true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return response()->json([ 'success' => false, 'error' => $kabupaten]);
		}
	}

}
