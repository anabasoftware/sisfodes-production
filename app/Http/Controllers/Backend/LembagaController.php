<?php

namespace App\Http\Controllers\Backend;

use App\Models\Lembaga;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LembagaController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('desa.lembaga.index');
	}

	public function lembaga_tambah()
	{
		return view('desa.lembaga.form');
	}

	public function lembaga_data()
	{
		$data = array();
		$lembaga = \App\Models\Lembaga::orderby('id_lembaga', 'desc')->get();
		foreach ($lembaga as $list) {
			$row = array();
			$row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="view_form(\''.route('lembaga.detail',$list->id_lembaga).'\',\'Edit Data\',\'Update\',\'modal-lg\',\'edit\',\''.$list->id_lembaga.'\',\'lembaga\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\'' . $list->id_lembaga .'\',\'lembaga\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';   
			$row[] = $list->nama_lembaga;
			$row[] = htmlentities(substr($list->deskripsi_lembaga,0,50)) . '...';
			$row[] = $list->get_desa->nama_desa;
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
	}

	public function lembaga_simpan(Request $request)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_lembaga' => 'required',
				'deskripsi_lembaga' => 'required',
		], $message );

		if ($validate->fails()) {
			return response()->json(['success' => false, 'message' => $validate->errors()], 422);
		}

		$lembaga = new \App\Models\Lembaga;
		$lembaga->nama_lembaga = $request['nama_lembaga'];
		$lembaga->deskripsi_lembaga = $request['deskripsi_lembaga'];
		$lembaga->id_desa = $request['id_desa'];
		$lembaga->kode_wilayah = $request['kode_wilayah'];

		// echo "<pre>"; print_r($lembaga);echo "</pre>";exit;

		$lembaga->save();
		if($lembaga->save()){
			return response()->json([ 'success' => true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return response()->json([ 'success' => false, 'error' => $lembaga]);
		}
	}

	public function lembaga_detail($id){
		$lembaga = \App\Models\Lembaga::where('id_lembaga', '=', $id)->first();
		return view('desa.lembaga.form')->with(['lembaga'=>$lembaga]);
	}

	public function lembaga_detail_json($id){
		$lembaga = \App\Models\Lembaga::where('id_lembaga', '=', $id)->first();
		echo json_encode($lembaga);
	}

	public function lembaga_hapus($id){
		$lembaga = \App\Models\Lembaga::where('id_lembaga', '=', $id)->first();
		if ($lembaga != null) {
			$lembaga->delete();
			if(!$lembaga->delete()){
				return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
			}else{
				return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
			}
		}
		return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
	}

	public function lembaga_update(Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_lembaga' => 'required',
				'deskripsi_lembaga' => 'required',
		], $message );

		if ($validate->fails()) {
			return response()->json(['success' => false, 'message' => $validate->errors()], 422);
		}

		$lembaga = \App\Models\Lembaga::where('id_lembaga', '=', $id)->first();
		$lembaga->nama_lembaga = $request['nama_lembaga'];
		$lembaga->deskripsi_lembaga = $request['deskripsi_lembaga'];
		$lembaga->update();
		if($lembaga){
			return response()->json([ 'success' => true, 'message' => 'Data berhasil diperbarui.'], 200);
		}else{
			return response()->json([ 'success' => false, 'error' => $lembaga]);
		}
	}
	
}
