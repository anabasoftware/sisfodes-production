<?php

namespace App\Http\Controllers\Backend;

use App\Models\Banner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BannerController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
	
    public function banner_index()
    {
        return view('website.berita.banner')->with([
			'route' => 'dashboard/website/berita/banner',
		]);
	}
	
    public function banner_data()
    {
		$data = array();
		$img_path = '/'.env('IMAGES_PATH').'banners/'.\Auth::user()->id_desa.'/';
		$banner = \App\Models\Banner::orderby('id_banner', 'desc')->where('id_desa', \Auth::user()->id_desa)->get();
		foreach ($banner as $list) {
			$row = array();
			$row[] = '<div class="btn-group flex-wrap">
				<button onclick="window.location.href=\''.route('website.berita.banner.detail', $list->id_banner).'\'" class="btn btn-primary btn-xs"><i class="fas fa-pen"></i></button>
				<button onclick="delete_data(\''.$list->id_banner.'\',\''.route('website.berita.banner.hapus', $list->id_banner).'\',\'banner/\')" class="btn btn-danger btn-xs"><i class="fas fa-trash-alt"></i></button>
				</div>';   
			$row[] = ($list->status==1)?'Aktif':'Tidak Aktif';
			$row[] = $list->nama_banner;
			$row[] = \Str::substr($list->deskripsi_banner,0,50) . ' ...';
			$row[] = '
				<a class="image-popup-banner" href="'.$img_path.$list->gambar.'">
					<img class="img-fluid" src="'.$img_path.$list->gambar.'" width="50" height="50">
				</a>
				<script>
					$(\'.image-popup-banner\').magnificPopup({
						type: \'image\',
						closeOnContentClick: true,
						closeBtnInside: false,
						fixedContentPos: true,
						mainClass: \'mfp-no-margins mfp-with-zoom\',
						image: {
							verticalFit: true
						},
						zoom: {
							enabled: true,
							duration: 600
						}
					});
				</script>';
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
    }

    public function banner_simpan(Request $request)
    {
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_banner' => 'required',
				'deskripsi_banner' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('website.berita.banner')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}

		$banner = new \App\Models\Banner;
		$banner->nama_banner = $request['nama_banner'];
		$banner->deskripsi_banner = $request['deskripsi_banner'];
        $banner->status = $request['status'];
		$get_id = \DB::select('SELECT AUTO_INCREMENT as id FROM information_schema.tables WHERE TABLE_SCHEMA = "'.env('DB_DATABASE').'" AND TABLE_NAME = "sfd_berita_banner"');
		if($request->hasFile('gambar')){
            $gambar = $request->file('gambar');
			$filename = "banner__".\Auth::user()->id_desa."_".$get_id[0]->id."_".date('YmdHis',time()).".".$gambar->getClientOriginalExtension();
			$path = env('IMAGES_PATH').'banners/'.\Auth::user()->id_desa;
			$gambar->move($path, $filename);
			$banner->gambar = $filename;
        }
        $banner->id_desa = $request['id_desa'];
		$banner->save();
		if($banner){
			return redirect()
			->route('website.berita.banner')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return redirect()
			->route('website.berita.banner')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}
	}

	public function banner_update(Request $request, $id)
    {
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
				'nama_banner' => 'required',
				'deskripsi_banner' => 'required',
				'status' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()->route('website.berita.banner')
				->withErrors($validate)
				->withInput()
				->with(['success'=>false, 'message' => 'Data gagal disimpan.'], 204);
		}

		$banner = \App\Models\Banner::where('id_banner', $id)->first();
		$banner->nama_banner = $request['nama_banner'];
		$banner->deskripsi_banner = $request['deskripsi_banner'];
        $banner->status = $request['status'];
        if($request->hasFile('gambar')){
			$path = env('IMAGES_PATH').'banners/'.\Auth::user()->id_desa.'/';
			\File::delete($path.$banner->gambar);
            $gambar = $request->file('gambar');
            $filename = "banner__".\Auth::user()->id_desa."_".$id."_".date('YmdHis',time()).".".$gambar->getClientOriginalExtension();            
			$gambar->move($path, $filename);
			$banner->gambar = $filename;
        }
        $banner->id_desa = $request['id_desa'];
		$banner->update();
		if($banner){
			return redirect()
			->route('website.berita.banner')
			->with(['success'=>true, 'message' => 'Data berhasil diperbarui.'], 200);
		}else{
			return redirect()
			->route('website.berita.banner')
			->withInput()->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 204);
		}
	}

	public function banner_detail($id)
	{
		$data = \App\Models\Banner::where('id_banner', $id)->first();
        return view('website.berita.banner')->with([
            'data' => $data,
        ]);
	}

	public function banner_hapus($id)
	{
		$data = \App\Models\Banner::where('id_banner', $id)->first();
		if (!empty($data->id_banner)) {
			$data->delete();
			if ($data){
				\File::delete(env('IMAGES_PATH').'banners/'.\Auth::user()->id_desa.'/'.$data->gambar);
				return redirect()
				->route('website.berita.banner')
				->with(['success'=>true, 'message' => 'Data berhasil dihapus.'], 200);
			}else{
				return redirect()
				->route('website.berita.banner')
				->with(['success'=>false, 'message' => 'Gagal menghapus data.'], 204);
			}
		}
	}
}
