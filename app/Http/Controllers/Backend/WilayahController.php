<?php

namespace App\Http\Controllers\Backend;

use App\Models\Wilayah;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WilayahController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()
	{
		return view('desa.wilayah.index');
	}

	public function wilayah_simpan(Request $request)
	{
		$message = [ 'required' => ':attribute harus diisi tidak boleh kosong!'];
		$validate = \Validator::make($request->all(), [
			'id_desa' => 'required',
			'luas_wilayah_tanah' => 'required',
			'luas_daratan' => 'required',
			'luas_persawahan' => 'required',
			'luas_tegalan' => 'required',
			'luas_tanah_desa' => 'required',
			'luas_lain_lain' => 'required',
			'jumlah_dusun' => 'required',
			'jumlah_rt' => 'required',
			'jumlah_rw' => 'required'
		], $message);
		if ($validate->fails() ) {
		  return redirect()->route('wilayah.index')->withErrors($validate)->withInput()->with(['message'=>'Data gagal disimpan.','status'=>false], 422);
		}
		$wilayah = new \App\Models\Wilayah;
		$wilayah->id_desa = $request['id_desa'];
		$wilayah->luas_wilayah_tanah = $request['luas_wilayah_tanah'];
		$wilayah->luas_daratan = $request['luas_daratan'];
		$wilayah->luas_persawahan = $request['luas_persawahan'];
		$wilayah->luas_tegalan = $request['luas_tegalan'];
		$wilayah->luas_tanah_desa = $request['luas_tanah_desa'];
		$wilayah->luas_lain_lain = $request['luas_lain_lain'];
		$wilayah->jumlah_dusun = $request['jumlah_dusun'];
		$wilayah->jumlah_rt = $request['jumlah_rt'];
		$wilayah->jumlah_rw = $request['jumlah_rw'];
		$wilayah->save();
		return redirect()->route('wilayah.index')->with(['message'=>'Data berhasil disimpan.','status'=>true], 200);
	}

	public function wilayah_update(Request $request, $id)
	{
		$message = [ 'required' => ':attribute harus diisi tidak boleh kosong!'];
		$validate = \Validator::make($request->all(), [
			'id_desa' => 'required',
			'luas_wilayah_tanah' => 'required',
			'luas_daratan' => 'required',
			'luas_persawahan' => 'required',
			'luas_tegalan' => 'required',
			'luas_tanah_desa' => 'required',
			'luas_lain_lain' => 'required',
			'jumlah_dusun' => 'required',
			'jumlah_rt' => 'required',
			'jumlah_rw' => 'required'
		], $message);
		if ($validate->fails() ) {
		  return redirect()->route('wilayah.index')->withErrors($validate)->withInput()->with(['message'=>'Perubahan gagal disimpan','status'=>false], 422);
		}
		$wilayah = \App\Models\Wilayah::where('id_wilayah', $id)->first();
		$wilayah->luas_wilayah_tanah = $request['luas_wilayah_tanah'];
		$wilayah->luas_daratan = $request['luas_daratan'];
		$wilayah->luas_persawahan = $request['luas_persawahan'];
		$wilayah->luas_tegalan = $request['luas_tegalan'];
		$wilayah->luas_tanah_desa = $request['luas_tanah_desa'];
		$wilayah->luas_lain_lain = $request['luas_lain_lain'];
		$wilayah->jumlah_dusun = $request['jumlah_dusun'];
		$wilayah->jumlah_rt = $request['jumlah_rt'];
		$wilayah->jumlah_rw = $request['jumlah_rw'];
		$wilayah->update();
		if($wilayah->update()){
			return response()->json([ 'success' => true, 'message' => 'Perubahan berhasil disimpan.'], 200);
		}else{
			return response()->json([ 'success' => false, 'error' => $wilayah]);
		}
	}

	public function wilayah_detail(Request $request, $id)
	{
		$wilayah = \App\Models\Wilayah::where('id_wilayah', $id)->first();
		return json_encode($wilayah);
	}

	public function wilayah_delete (Request $request, $id) 
	{
		$wilayah = \App\Models\Wilayah::where('id_wilayah', $id)->first();
		if ($wilayah != null) {
			$wilayah->delete();
			if ($wilayah->delete()){
				return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
			}else{
				return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
			}
		}
		return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
	}
}
