<?php

namespace App\Http\Controllers\Backend;

use App\Models\Statistik;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatistikController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('desa.statistik.index');
	}

	public function statistik_tambah()
	{
		return view('desa.statistik.form');
	}

	public function statistik_data()
	{
		$data = array();
		$statistik = \App\Models\Statistik::orderby('id_statistik_desa', 'desc')->get();
		foreach ($statistik as $list) {
			$row = array();
			$row[] = '
				<div class="btn-group flex-wrap">
					<button type="button" onclick="view_form(\''.route('statistik.detail',$list->id_statistik_desa).'\',\'Edit Data\',\'Update\',\'modal-lg\',\'edit\',\''.$list->id_statistik_desa.'\',\'statistik_desa\')" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
					<button type="button" onclick="delete_data(\''.$list->id_statistik_desa.'\',\'statistik\')" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
				</div>';
			$row[] = date('d F Y', strtotime($list->tgl_statistik));
			$row[] = $list->jumlah_kk;
			$row[] = $list->jumlah_penduduk;
			$row[] = $list->jumlah_laki_laki;
			$row[] = $list->jumlah_perempuan;
			$row[] = $list->balita;
			$row[] = $list->anak;
			$row[] = $list->remaja;
			$row[] = $list->dewasa;
			$row[] = $list->orang_tua;
			$row[] = $list->users->user_name;
			$data[] = $row;
		}
		$output = array('data' => $data);
		return response()->json($output);
	}

	public function statistik_simpan(Request $request)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
			'tgl_statistik' => 'required',
			'id_desa' => 'required',
			'jumlah_kk' => 'required',
			'jumlah_penduduk' => 'required',
			'jumlah_laki_laki' => 'required',
			'jumlah_perempuan' => 'required',
			'balita' => 'required',
			'anak' => 'required',
			'remaja' => 'required',
			'dewasa' => 'required',
			'orang_tua' => 'required',
		], $message );

		if ($validate->fails()) {
			return response()->json(['success' => false, 'message' => $validate->errors()], 422);
		}
		$statistik = new \App\Models\Statistik;
		$statistik->id_desa = $request['id_desa'];
		$statistik->tgl_statistik = date('Y-m-d', strtotime($request['tgl_statistik']));
		$statistik->jumlah_kk = $request['jumlah_kk'];
		$statistik->jumlah_penduduk = $request['jumlah_penduduk'];
		$statistik->jumlah_laki_laki = $request['jumlah_laki_laki'];
		$statistik->jumlah_perempuan = $request['jumlah_perempuan'];
		$statistik->balita = $request['balita'];
		$statistik->anak = $request['anak'];
		$statistik->remaja = $request['remaja'];
		$statistik->dewasa = $request['dewasa'];
		$statistik->orang_tua = $request['orang_tua'];
		// echo "<pre>"; print_r($statistik);echo "</pre>";exit;

		$statistik->save();
		if($statistik->save()){
			return response()->json([ 'success' => true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return response()->json([ 'success' => false, 'error' => $statistik]);
		}
	}

	public function statistik_detail($id){
		$statistik = \App\Models\Statistik::where('id_statistik_desa', '=', $id)->first();
		return view('desa.statistik.form')->with(['statistik'=>$statistik]);
	}

	public function statistik_detail_json($id){
		$statistik = \App\Models\Statistik::where('id_statistik_desa', '=', $id)->first();
		echo json_encode($statistik);
	}

	public function statistik_hapus($id){
		$statistik = \App\Models\Statistik::where('id_statistik_desa', '=', $id)->first();
		if ($statistik != null) {
			$statistik->delete();
			if(!$statistik->delete()){
				return response()->json(['success' => true, 'message' => 'Data telah dihapus.'], 201);
			}else{
				return response()->json(['success' => false, 'message' => 'Data gagal dihapus.'], 204);
			}
		}
		return response()->json(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
	}

	public function statistik_update(Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
			'tgl_statistik' => 'required',
			'id_desa' => 'required',
			'jumlah_kk' => 'required',
			'jumlah_penduduk' => 'required',
			'jumlah_laki_laki' => 'required',
			'jumlah_perempuan' => 'required',
			'balita' => 'required',
			'anak' => 'required',
			'remaja' => 'required',
			'dewasa' => 'required',
			'orang_tua' => 'required',
		], $message );

		if ($validate->fails()) {
			return response()->json(['success' => false, 'message' => $validate->errors()], 422);
		}

		$statistik = \App\Models\Statistik::where('id_statistik_desa', '=', $id)->first();
		$statistik->id_desa = $request['id_desa'];
		$statistik->tgl_statistik = date('Y-m-d', strtotime($request['tgl_statistik']));
		$statistik->jumlah_kk = $request['jumlah_kk'];
		$statistik->jumlah_penduduk = $request['jumlah_penduduk'];
		$statistik->jumlah_laki_laki = $request['jumlah_laki_laki'];
		$statistik->jumlah_perempuan = $request['jumlah_perempuan'];
		$statistik->balita = $request['balita'];
		$statistik->anak = $request['anak'];
		$statistik->remaja = $request['remaja'];
		$statistik->dewasa = $request['dewasa'];
		$statistik->orang_tua = $request['orang_tua'];

		$statistik->update();
		if($statistik->update()){
			return response()->json([ 'success' => true, 'message' => 'Data berhasil diperbarui.'], 200);
		}else{
			return response()->json([ 'success' => false, 'error' => $statistik]);
		}
	}
}
