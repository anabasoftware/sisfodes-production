<?php

namespace App\Http\Controllers\Backend;

use App\Models\Desa;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IdentitasController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
	
	public function index()
	{
		return view('desa.identitas.index');
	}

	public function identitas_desa_simpan (Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
			'id_desa' => 'required',
			'kode_desa' => 'required',
			'nama_desa' => 'required',
			'id_kecamatan' => 'required',
			'desa_id' => 'required',
			'kepala_desa' => 'required',
			'alamat' => 'required',
			'kode_pos' => 'required',
			'fax' => 'required',
			'website' => 'required',
			'visi' => 'required',
			'misi' => 'required',
			'peta_wilayah' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()
				->route('identitas.index')
				->withErrors($validate)
				->withInput();
		}
		$desa = new \App\Models\Desa;
		$desa->id_desa = $request['id_desa'];
		$desa->kode_desa = $request['kode_desa'];
		$desa->nama_desa = $request['nama_desa'];
		$desa->id_kecamatan = $request['id_kecamatan'];
		$desa->desa_id = $request['desa_id'];
		$desa->kepala_desa = $request['kepala_desa'];
		$desa->alamat = $request['alamat'];
		$desa->kode_pos = $request['kode_pos'];
		$desa->fax = $request['fax'];
		$desa->website = $request['website'];
		$desa->visi = $request['visi'];
		$desa->misi = $request['misi'];
		$desa->peta_wilayah = $request['peta_wilayah'];

        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $logo = "logo__".time().".".$file->getClientOriginalExtension();
			$path = storage_path('app/public/images/logo/');
            
            $file->move($path, $logo);
            $desa->logo = $logo;
            $logos = $logo;
        }else{
            $logos = $desa->logo;
        }

		// $desa->logo = $request['logo'];

		$desa->save();
		if($desa->save()){
			return redirect()
			->route('identitas.index')
			->with(['success'=>true, 'message' => 'Data berhasil disimpan.'], 200);
		}else{
			return redirect()
			->route('identitas.index')
			->with(['success'=>false, 'message' => 'Data berhasil disimpan.'], 204);
		}
	}

	public function identitas_desa_update (Request $request, $id)
	{
		$message = [
			'required' => ':attribute harus diisi, tidak boleh kosong.',
		];
		$validate = \Validator::make( $request->all(), [
			'id_desa' => 'required',
			'kode_desa' => 'required',
			'nama_desa' => 'required',
			'id_kecamatan' => 'required',
			'desa_id' => 'required',
			'kepala_desa' => 'required',
			'alamat' => 'required',
			'kode_pos' => 'required',
			'fax' => 'required',
			'website' => 'required',
			'visi' => 'required',
			'misi' => 'required',
			'peta_wilayah' => 'required',
		], $message );

		if ($validate->fails()) {
			return redirect()
				->route('identitas.index')
				->withErrors($validate)
				->withInput();
		}
		$desa = \App\Models\Desa::where('id_desa', $id)->first();
		$desa->id_desa = $request['id_desa'];
		$desa->kode_desa = $request['kode_desa'];
		$desa->nama_desa = $request['nama_desa'];
		$desa->id_kecamatan = $request['id_kecamatan'];
		$desa->desa_id = $request['desa_id'];
		$desa->kepala_desa = $request['kepala_desa'];
		$desa->alamat = $request['alamat'];
		$desa->kode_pos = $request['kode_pos'];
		$desa->fax = $request['fax'];
		$desa->website = $request['website'];
		$desa->visi = $request['visi'];
		$desa->misi = $request['misi'];
		$desa->peta_wilayah = $request['peta_wilayah'];
		
        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $logo = "logo__".time().".".$file->getClientOriginalExtension();
			$path = storage_path('app/public/images/logo/');
            
            $file->move($path, $logo);
            $desa->logo = $logo;
            $logos = $logo;
        }else{
            $logos = $desa->logo;
        }

		// $desa->logo = $request['logo'];
		
		$desa->update();
		if($desa->update()){
			return redirect()
			->route('identitas.index')
			->with(['success'=>true, 'message' => 'Data berhasil diperbarui.'], 200);
		}else{
			return redirect()
			->route('identitas.index')
			->with(['success'=>false, 'message' => 'Data gagal diperbarui.'], 204);
		}
	}

	public function identitas_desa_detail ($id)
	{
		$desa = \App\Models\Desa::where('id_desa', $id)->first();
		return \json_encode($desa);
	}

	public function identitas_desa_delete ($id) 
	{
		$desa = \App\Models\Desa::where('id_desa', $id)->first();
		if ($desa != null) {
			$desa->delete();
			if ($desa->delete()){
				return redirect()
				->route('identitas.index')
				->with(['success'=>true, 'message' => 'Data berhasil dihapus.'], 200);
			}else{
				return redirect()
				->route('identitas.index')
				->with(['success'=>false, 'message' => 'Gagal menghapus data.'], 204);
			}
		}
		return redirect()->route('identitas.index')->with(['success' => false, 'message' => 'Terjadi kesalahan sistem.'], 406);
	}

	public function get_desa ($id)
	{
		$desa = \App\Models\LokasiDesa::where('id_kecamatan', $id)->get();
		return response()->json($desa);
	}
}
