<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Desa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function frontpage_index ()
    {
        return view('frontend.index');
    }

    public function frontpage_profil()
    {
        return view('frontend.profil');
    }

    public function frontpage_potensi()
    {
        return view('frontend.potensi');
    }

    public function detail_potensi($slug)
    {
        // $data = \App\Models\Potensi::
        // leftjoin('desa','potensi_desa.id_desa','desa.id_desa')
        // ->where('potensi_desa.slug_potensi', '=', $slug)
        // ->first();
        $data = DB::table('potensi_desa AS p_desa')
            ->select('p_desa.*')
            ->leftjoin('desa','p_desa.id_desa','desa.id_desa')
            ->where('p_desa.slug_potensi', '=', $slug)
            ->first();
        return view('frontend.potensi-detail')->with([
            'detail' => $data
        ]);
    }
    
    public function frontpage_layanan()
    {
        return view('frontend.layanan');
    }

    public function frontpage_lapor()
    {
        return view('frontend.lapor');
    }    
    
    public function frontpage_info()
    {
        return view('frontend.informasi');
    }

    public function frontpage_info_detail()
    {
        return view('frontend.informasi-detail');
    }

    public function frontpage_berita ()
    {
        // $desa = Desa::
        return view('frontend.informasi');
    }

    public function frontpage_berita_detail ()
    {
        return view('frontend.informasi');
    }
}
