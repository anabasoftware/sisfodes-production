<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Desa extends Model
{
	protected $table = 'desa';
	protected $primaryKey = 'id_desa';
	public $incrementing = true;

	public function users ()
	{
		return $this->belongsTo('App\Models\User');
	}

	public function kecamatan ()
	{
		return $this->belongsTo('App\Models\LokasiKec', 'id_kecamatan', 'id_kecamatan');
	}

	public function statistiks ()
	{
		return $this->belongsTo('App\Models\Statistik');
	}

	public function get_user ()
	{
	  return $this->hasOne('App\Models\User', 'id_desa', 'id_desa');
	}

	public function get_sejarah ()
	{
	  return $this->hasOne('App\Models\Sejarah', 'id_desa', 'id_desa');
	}

	public function get_banner ()
	{
	  return $this->hasMany('App\Models\Banner', 'id_desa', 'id_desa');
	}

	public function get_potensi ()
	{
		return $this->hasMany('App\Models\Potensi', 'id_desa', 'id_desa');
	}

	public function get_berita ()
	{
		return $this->hasMany('App\Models\Berita', 'id_desa', 'id_desa');
	}

	public function potensi ()
	{
		return $this->belongsTo('App\Models\Potensi');
	}

	public function lembaga ()
	{
		return $this->belongsTo('App\Models\Lembaga');
	}
}
