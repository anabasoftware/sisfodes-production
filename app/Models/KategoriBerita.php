<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriBerita extends Model
{
    protected $table = 'berita_kategori';
    protected $primaryKey = 'id_kategori_berita';
    
    public function sub_kategori ()
	{
		return $this->belongsTo('App\Models\KategoriBeritaSub');
	}
}
