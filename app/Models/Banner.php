<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
	protected $table = 'berita_banner';
	protected $primaryKey = 'id_banner';
	
	public function desa ()
	{
		return $this->belongsTo('App\Models\Desa');
	}
}
