<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lembaga extends Model
{
	protected $table = 'desa_lembaga';
	protected $primaryKey = 'id_lembaga';

	public function get_desa ()
	{
	  return $this->hasOne('App\Models\Desa', 'id_desa', 'id_desa');
	}
}
