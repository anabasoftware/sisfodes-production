<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wilayah extends Model
{
	protected $table = 'desa_wilayah';
	protected $primaryKey = 'id_wilayah';
}
