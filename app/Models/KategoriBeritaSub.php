<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KategoriBeritaSub extends Model
{
    protected $table = 'berita_kategori_sub';
    protected $primaryKey = 'id_sub_kategori';

    public function kategori ()
	{
		return $this->hasMany('App\Models\KategoriBerita', 'id_kategori_berita', 'id_kategori_berita');
	}
}
