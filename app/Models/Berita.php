<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
	protected $table = 'berita';
	protected $primaryKey = 'id_berita';
	public $incrementing = false;
	public function get_desa ()
	{
		return $this->hasOne(Desa::class, 'id_desa', 'id_desa');
	}

	public function tags ()
    {
        return $this->belongsToMany(Tag::class, 'berita_detail', 'id_berita', 'id_berita');
    }

	public function get_kategori_berita ()
	{
		return $this->hasOne(KategoriBerita::class, 'id_kategori_berita', 'id_kategori_berita');
	}


}
