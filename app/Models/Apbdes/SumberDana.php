<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;

class SumberDana extends Model
{
    //
    protected $table      = 'sumber_dana';
    protected $primaryKey = 'id_sumber_dana';
}
