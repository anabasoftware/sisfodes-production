<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\Kelompok;

class Akun extends Model
{
    //
    protected $table = 'akun_rekening';
    protected $primaryKey = 'id_akun_rekening';

    public function kelompok(){
        return $this->hasMany(Kelompok::class,'id_akun','id_akun_rekening')->where('id_desa',\Auth::user()->id_desa);
    }
}
