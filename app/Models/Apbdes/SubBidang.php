<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\Bidang;
use App\Models\Apbdes\Kegiatan;

class SubBidang extends Model
{
    //
    protected $table='sub_bidang';
    protected $primaryKey='kode_sub_bidang';

    function bidang(){
        return $this->belongsTo(Bidang::class,'id_bidang','id_bidang');
    }

}
