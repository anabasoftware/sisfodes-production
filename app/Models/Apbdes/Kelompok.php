<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\Akun;

class Kelompok extends Model
{
    //
    protected $table =  'kelompok_rekening';
    protected $primaryKey = 'id_kelompok_rekening';

    public function akun(){
        return $this->belongsTo(Akun::class,'id_akun','id_akun_rekening');
    }
}
