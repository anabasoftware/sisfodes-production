<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\SubBidang;
use App\Models\Apbdes\OutputDD;

class Kegiatan extends Model
{
    //
    protected $table = 'kegiatan';
    protected $primaryKey = 'id_kegiatan';

    public function subbidang(){
        return $this->belongsTo(SubBidang::class,'id_sub_bidang','id_sub_bidang');
    }

    public function outputdd(){
        return $this->hasMany(OutputDD::class,'id_kegiatan','id_kegiatan');
    }

}
