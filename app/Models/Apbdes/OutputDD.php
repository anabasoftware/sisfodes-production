<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\Kegiatan;

class OutputDD extends Model
{
    //
    protected $table = 'output_dd';
    protected $primaryKey = 'id_output_dd';

    public function kegiatan(){
        return $this->belongsTo(Kegiatan::class,'id_kegiatan','id_kegiatan');
    }
}
