<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\JenisRekening;

class ObjekRekening extends Model
{
    //
    protected $table        = 'objek_rekening';
    protected $primaryKey   = 'id_objek_rekekning';

    public function jenis(){
        return $this->belongsTo(JenisRekening::class,'id_jenis_rekening', 'id_jenis_rekening');
    }
}
