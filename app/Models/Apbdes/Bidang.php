<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\SubBidang;

class Bidang extends Model
{
    //
    protected $table        = 'bidang';
    protected $primaryKey   = 'id_bidang';

    function subbidang(){
        return $this->hashMany(SubBidang::class,'id_bidang')->where('id_desa',\Auth::user()->id_desa);

    }

}
