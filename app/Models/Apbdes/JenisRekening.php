<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;
use App\Models\Apbdes\Kelompok;
use App\Models\Apbdes\ObjekRekening;
class JenisRekening extends Model
{
    //
    protected $table = 'jenis_rekening';
    protected $primaryKey = 'id_jenis_rekening';

    public function kelompok(){
        return $this->belongsTo(Kelompok::class,'id_kelompok_rekening','id_kelompok_rekening');
    }

    public function objek(){
        return $this->hasMany(ObjekRekening::class,'id_jenis_rekening','id_jenis_rekening');
    }
}
