<?php

namespace App\Models\Apbdes;

use Illuminate\Database\Eloquent\Model;

class RekeningBank extends Model
{
    //
    protected $table = 'bank_desa';
    protected $primaryKey = 'id_bank';
}
