<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Statistik extends Model
{
	protected $table = 'statistik_desa';
	protected $primaryKey = 'id_statistik_desa';

	public function desa ()
	{
		return $this->hasOne('App\Models\Desa', 'id_desa', 'id_desa');
	}

	public function users () 
	{
		return $this->hasOne('App\Models\User', 'id_desa', 'id_desa');
	}
}
