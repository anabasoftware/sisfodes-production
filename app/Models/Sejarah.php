<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sejarah extends Model
{
	protected $table = 'sejarah_desa';
	protected $primaryKey = 'id_sejarah_desa';

	public function desa ()
	{
		return $this->belongsTo('App\Models\Desa');
	}
}
