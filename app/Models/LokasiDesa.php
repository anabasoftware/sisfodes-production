<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LokasiDesa extends Model
{
    protected $table = 'master_desa';
    protected $primaryKey = 'desa_id';
}
