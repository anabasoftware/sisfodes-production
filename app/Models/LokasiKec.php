<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LokasiKec extends Model
{
    protected $table = 'master_kecamatan';
    protected $primaryKey = 'id_kecamatan';

    public function desa ()
	{
	  return $this->hasOne('App\Models\Desa', 'id_desa', 'id_desa');
	}

}
