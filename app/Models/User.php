<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use Notifiable;
	protected $table = 'users';
	protected $primaryKey = 'id_user';
	public $incrementing = false;
	protected $fillable = [
		'id_user', 'user_name', 'email', 'password', 'status', 'role_id', 'user_img',
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	protected $casts = [
		'email_verified_at' => 'datetime',
	];

	public function desa ()
	{
		return $this->hasOne('App\Models\Desa', 'id_desa', 'id_desa');
	}

	public function statistiks ()
	{
		return $this->belongsTo('App\Models\Statistik');
	}

	public function get_desa ()
	{
		return $this->belongsTo('App\Models\Desa');
	}

	public function anggaran ()
	{
		return $this->belongsTo('App\Models\Anggaran');
	}

	public function get_role ()
	{
		return $this->hasOne('App\Models\Role', 'id', 'role_id');
	}
}
