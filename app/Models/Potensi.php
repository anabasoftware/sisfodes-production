<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Potensi extends Model
{
	protected $table = 'desa_potensi';
	protected $primaryKey = 'id_potensi';

	public function desa ()
	{
		return $this->belongsTo('App\Models\Desa');
	}

	public function get_desa ()
	{
	  return $this->hasOne('App\Models\Desa', 'id_desa', 'id_desa');
	}
}
