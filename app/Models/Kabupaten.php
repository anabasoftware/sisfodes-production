<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
	protected $table = 'profil_kabupaten';
	protected $primaryKey = 'id_kabupaten';
}
