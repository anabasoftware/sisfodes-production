<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
	protected $table = 'desa_layanan';
	protected $primaryKey = 'id_layanan';
	public $incrementing = false;

	public function get_desa ()
	{
	  return $this->hasOne('App\Models\Desa', 'id_desa', 'id_desa');
	}
}
