<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class Helpers
{
    public static function topbar_content($domain = ''){
        $content = '';
        $desa = \App\Models\Desa::where('website', $domain)->first();
        if(!empty($desa)){
            $content .= '
            <li class="nav-item nav-item-borders py-2 d-none d-sm-inline-flex">
                <span class="pl-0"><i class="far fa-dot-circle text-4 text-color-primary" style="top: 1px;"></i> 
                ';
            $content .= $desa->website;
            $content .='
                </span>
            </li>
            <li class="nav-item nav-item-borders py-2">
                <a href=""><i class="fab fa-whatsapp text-4 text-color-primary" style="top: 0;"></i> '.$desa->no_telp.'</a>
            </li>
            <li class="nav-item nav-item-borders py-2 d-none d-md-inline-flex">
                <a href=""><i class="far fa-envelope text-4 text-color-primary" style="top: 1px;"></i> '.$desa->get_user->email.'</a>
            </li>';
        }
        return $content;
    }

    public static function navbar_menu(){
        $menu = '';
        $menu .= '
        <div class="header-nav header-nav-stripe order-2 order-lg-1">
            <div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1">
                <nav class="collapse">
                    <ul class="nav nav-pills" id="mainNav">
                        <li class="dropdown"><a class="dropdown-item m-beranda" href="'.route('frontpage.index').'"> Beranda </a></li>
                        <li class="dropdown"><a class="dropdown-item m-profil" href="'.route('frontpage.profil').'">Profil</a></li>
                        <li class="dropdown"><a class="dropdown-item m-info" href="'.route('frontpage.info').'">Informasi</a></li>
                        <li class="dropdown"><a class="dropdown-item m-potensi" href="'.route('frontpage.potensi').'">Potensi</a></li>
                        <li class="dropdown"><a class="dropdown-item m-layanan" href="'.route('frontpage.layanan').'">Layanan</a></li>
                        <li class="dropdown"><a class="dropdown-item m-lapor" href="'.route('frontpage.lapor').'">Warga melapor</a></li>
                    </ul>
                </nav>
            </div>
            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                <i class="fas fa-bars"></i>
            </button>
        </div>
        ';
        return $menu;
    }

    public static function jumbotron($domain){
        $data = \App\Models\Banner::
            leftjoin('desa','berita_banner.id_desa','desa.id_desa')
            ->where('desa.website', $domain)->first();
            // where('id_desa','=',(\DB::select('SELECT id_desa FROM '.env('DB_PREFIX_TABLE').'desa')))->first();
        if(!empty($data)){
            $content = '';
            $content .= '
            <div class="slider-container rev_slider_wrapper" style="height: 700px;">
                <div id="revolutionSlider" class="slider rev_slider" data-version="5.4.8" data-plugin-revolution-slider data-plugin-options="{\'delay\': 9000, \'gridwidth\': 1170, \'gridheight\': 700, \'responsiveLevels\': [4096,1200,992,500], \'navigation\' : {\arrows\': { \'enable\': false }, \'bullets\': {\'enable\': true, \'style\': \'bullets-style-1\', \'h_align\': \'center\', \'v_align\': \'bottom\', \'space\': 7, \'v_offset\': 70, \'h_offset\': 0}}}">
                    <ul>
                        <li class="slide-overlay" data-transition="fade">
                            <img src="'.asset(env('IMAGES_PATH').'banners/'.$data->id_desa.'/'.$data->gambar).'"  
                                alt=""
                                data-bgposition="center center" 
                                data-bgfit="cover" 
                                data-bgrepeat="no-repeat" 
                                class="rev-slidebg">

                            <h1 class="tp-caption font-weight-extra-bold text-color-light negative-ls-2"
                                data-frames=\'[{"delay":1000,"speed":2000,"frame":"0","from":"sX:1.5;opacity:0;fb:20px;","to":"o:1;fb:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]\'
                                data-x="center"
                                data-y="center" data-voffset="[\'-55\',\'-55\',\'-55\',\'-55\']"
                                data-fontsize="[\'50\',\'50\',\'50\',\'90\']"
                                data-lineheight="[\'55\',\'55\',\'55\',\'95\']"
                                data-letterspacing="-1">
                                '.$data->nama_banner.'
                            </h1>

                            <div class="tp-caption font-weight-light text-center"
                                data-frames=\'[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":2000,"split":"chars","splitdelay":0.05,"ease":"Power2.easeInOut"},{"delay":"wait","speed":1000,"to":"y:[100%];","mask":"x:inherit;y:inherit;s:inherit;e:inherit;","ease":"Power2.easeInOut"}]\'
                                data-x="center"
                                data-y="center" data-voffset="[\'-5\',\'-5\',\'-5\',\'15\']"
                                data-fontsize="[\'18\',\'18\',\'18\',\'35\']"
                                data-lineheight="[\'20\',\'20\',\'20\',\'40\']"
                                style="color: #b5b5b5;">
                                '.$data->deskripsi_banner.'
                            </div>

                            <a class="tp-caption btn btn-light-2 btn-outline font-weight-semibold"
                                data-frames=\'[{"delay":2500,"speed":2000,"frame":"0","from":"opacity:0;y:50%;","to":"o:1;y:0;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"}]\'
                                data-hash
                                data-hash-offset="85"
                                href="#main"
                                data-x="center" data-hoffset="0"
                                data-y="center" data-voffset="[\'65\',\'65\',\'65\',\'105\']"
                                data-whitespace="nowrap"	
                                data-fontsize="[\'15\',\'15\',\'15\',\'33\']"	
                                data-paddingtop="[\'15\',\'15\',\'15\',\'40\']"
                                data-paddingright="[\'45\',\'45\',\'45\',\'110\']"
                                data-paddingbottom="[\'15\',\'15\',\'15\',\'40\']"				 
                                data-paddingleft="[\'45\',\'45\',\'45\',\'110\']">GET STARTED NOW!
                            </a>
                        
                        </li>
                    </ul>
                </div>
            </div>';
            return $content;
        }
    }

    public static function whoami($domain){
        $content = '';
        $desa = \App\Models\Desa::where('website', $domain)->first();
        $content .= '
        <section class="section section-secondary border-0 py-0 mb-5 appear-animation" data-appear-animation="fadeIn">
            <div class="container">
                <div class="row align-items-center justify-content-center justify-content-lg-between pb-5 pb-lg-0">
                    <div class="col-lg-5 order-2 order-lg-1 pt-4 pt-lg-0 pb-5 pb-lg-0 mt-5 mt-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                        <h2 class="font-weight-bold text-color-light text-7 mb-2">'.$desa->nama_desa.'</h2>
                        <p class="lead font-weight-light text-color-light text-4">'.$desa->get_sejarah->sejarah.'</p>
                        <p class="font-weight-light text-color-light text-2 mb-4 opacity-7">'.$desa->kepala_desa.'</p>
                        <a href="#" class="btn btn-dark-scale-2 btn-px-5 btn-py-2 text-2">SELENGKAPNYA</a>
                    </div>
                    <div class="col-9 offset-lg-1 col-lg-5 order-1 order-lg-2 scale-2">
                        <img class="img-fluid box-shadow-3 my-2 border-radius" src="'.asset('storage/images/logo/' . $desa->logo).'" alt="">
                    </div>
                </div>
            </div>
        </section>
        ';
        return $content;
    }

    public static function dana_desa(){
        $content = '';
        $content .= '
        <div class="container">
            <div class="row justify-content-center py-4 my-5">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 text-center appear-animation" data-appear-animation="fadeInUpShorter">
                            <h2 class="font-weight-normal text-6 pb-2 mb-4">
                                <strong class="font-weight-extra-bold">Transparansi Dana Desa</strong></h2>
                        </div>
                    </div>
                </div>
                <div class="col-lg-10">
                    <div class="tabs tabs-bottom tabs-center tabs-simple">
                        <ul class="nav nav-tabs mb-3">
                            <li class="nav-item active appear-animation" data-appear-animation="fadeInLeftShorter"
                                data-appear-animation-delay="200">
                                <a class="nav-link" href="#tabsNavigationAnggaranDesa" data-toggle="tab">
                                    <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                        <span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
                                            <span class="box-content p-0 m-0">
                                                <i class="icon-featured fas fa-user"></i>
                                            </span>
                                        </span>
                                    </span>
                                    <h4 class="font-weight-bold text-color-dark text-3 mb-0 pb-0 mt-2">Grafik Pendapatan Desa</h4>
                                </a>
                            </li>
                            <li class="nav-item appear-animation" data-appear-animation="fadeInLeftShorter">
                                <a class="nav-link" href="#tabsNavigationBelanjaDesa" data-toggle="tab">
                                    <span class="featured-boxes featured-boxes-style-6 p-0 m-0">
                                        <span class="featured-box featured-box-primary featured-box-effect-6 p-0 m-0">
                                            <span class="box-content p-0 m-0">
                                                <i class="icon-featured fas fa-file"></i>
                                            </span>
                                        </span>
                                    </span>
                                    <h4 class="font-weight-bold text-color-dark text-3 mb-0 pb-0 mt-2">Rencana Anggaran</h4>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content appear-animation" data-appear-animation="fadeInUpShorter"
                            data-appear-animation-delay="300">
                            <div class="tab-pane active" id="tabsNavigationAnggaranDesa">
                                <div class="text-center">
                                    <div class="col-lg-12">
                                        <section class="card">
                                            <div class="card-header">
                                                Grafik pendapatan
                                                Berikut ini adalah daftar master perndapatan yang telah tersimpan.
                                            </div>
                                            <div class="card-body">
                                                <canvas id="chart_anggaran_desa"></canvas>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabsNavigationBelanjaDesa">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card border-primary mb-3">
                                                <div class="card-header bg-primary text-white">APBDes Tahun 2019</div>
                                                <div class="card-body">
                                                    <div class="progress-group">
                                                        Jumlah Rencana Penerimaan TA 2019
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 8,617,690</b></span>
                                                            <small class="pull-right"><b>10.09%&nbsp;</b></small>
                                                        </div>
                                                            <div class="progress" align="right">
                                                            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width: 43.73%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Alokasi Dana Desa (ADD)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 20,617,690</b></span>
                                                            <small class="pull-right"><b>7.09%&nbsp;</b></small>
                                                        </div>
                                                            <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width: 43.73%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Dana Desa (DD)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 10,617,690</b></span>
                                                            <small class="pull-right"><b>30.09%&nbsp;</b></small>
                                                        </div>
                                                            <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" style="width: 48.79%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Bagi Hasil Pajak & Retribusi Daerah (PBH)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 18,617,690</b></span>
                                                            <small class="pull-right"><b>9.09%&nbsp;</b></small>
                                                        </div>
                                                            <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: 6.91%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Pendapatan Asli Desa (PAD)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 18,617,690</b></span>
                                                            <small class="pull-right"><b>8.09%&nbsp;</b></small>
                                                        </div>
                                                            <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 0.57%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Penerimaan Pembiayaan<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 329,324,456</b></span>
                                                            <small class="pull-right"><b>100.00%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: 100%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card border-primary mb-3">
                                                <div class="card-header bg-success text-white">APBDes Tahun 2018</div>
                                                <div class="card-body">
                                                    <div class="progress-group">
                                                        Realisasi Pendapatan Desa TA 2018<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 329,324,456</b></span>
                                                            <small class="pull-right"><b>100.00%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: 100%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Alokasi Dana Desa (ADD)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 682,242,000</b></span>
                                                            <small class="pull-right"><b>98.23%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width: 98.23%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Dana Desa (DD)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 737,089,000</b></span>
                                                            <small class="pull-right"><b>100.00%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" style="width: 100%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Bagi Hasil Pajak & Retribusi Daerah (PBH)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 90,391,200</b></span>
                                                            <small class="pull-right"><b>79.95%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: 79.95%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Pendapatan Lain-Lain (DLL)<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 8,617,690</b></span>
                                                            <small class="pull-right"><b>73.09%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 73.09%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Penerimaan Pembiayaan<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 254,380,746</b></span>
                                                            <small class="pull-right"><b>100.00%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: 100%"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card border-primary">
                                                <div class="card-header bg-info text-white">Belanja Tahun 2018</div>
                                                <div class="card-body">
                                                    <div class="progress-group">
                                                        Realisasi Belanja Desa TA 2018
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 258.568.000</b></span>
                                                            <small class="pull-right"><b>100.00%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: 100%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Penyelenggaraan Pemerintahan Desa<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 502,790,673</b></span>
                                                            <small class="pull-right"><b>86.80%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width: 86.8%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Pembangunan Desa<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 720,427,689</b></span>
                                                            <small class="pull-right"><b>77.62%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" style="width: 77.62%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Pembinaan Kemasyarakatan Desa<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 52,724,000</b></span>
                                                            <small class="pull-right"><b>94.71%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: 94.71%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Pemberdayaan Masyarakat Desa<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 167,453,818</b></span>
                                                            <small class="pull-right"><b>67.08%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress" align="right">
                                                            <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 67.08%"></div>
                                                        </div>
                                                    </div>
                                                    <hr/>
                                                    <div class="progress-group">
                                                        Bidang Tak Terduga<br>
                                                        <div class="clearfix">
                                                            <span class="pull-left"><b>Rp. 512,740</b></span>
                                                            <small class="pull-right"><b>5%&nbsp;</b></small>
                                                        </div>
                                                        <div class="progress sm" align="right">
                                                            <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: 5%"></div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ';
        return $content;
    }

    public static function blog_post_popular($domain){
        // ------------------------- Deprecated 16-06-2021 ------------------------
            // $datas = \App\Models\Berita::
                //     leftjoin('desa','berita.id_desa','desa.id_desa')
                //     ->where('desa.website', $domain)->get();
                // $datas = \App\Models\Desa::where('website', $domain)->get();
                // foreach($datas as $data){
                //     // return $data->get_berita;
                //     return $data;
                // }
                // exit;

                // if(!empty($datas)){
                //     $content = '';
                //     foreach ($datas as $data) {
                //         foreach ($data->get_berita as $val) {
                //         $content .= '
                //             <ul class="simple-post-list">
                //                 <li>
                //                     <article>
                //                         <div class="post-image">
                //                             <div class="img-thumbnail img-thumbnail-no-borders d-block">
                //                                 <a href="#">
                //                                     <img src="img/blog/square/blog-55.jpg" class="border-radius-0" width="50" height="50" alt="Simple Ways to Have a Pretty Face">
                //                                 </a>
                //                             </div>
                //                         </div>
                //                         <div class="post-info">
                //                             <h4 class="font-weight-normal text-3 line-height-4 mb-0">
                //                                 <a href="'.route('frontpage.berita.post.detail', array("id_desa" => $val->id_desa, "slug" => $val->slug_judul_berita)).'" class="text-dark">'.$val->judul_berita.'</a>
                //                             </h4>
                //                             <div class="post-meta">
                //                                 '.Carbon::parse($val->updated_at)->format('d F Y').' &mdash; '.$val->nama_desa.'
                //                             </div>
                //                         </div>
                //                     </article>
                //                 </li>
                //             </ul>
                //             ';
                //             return $content;
                //         }
                //     }
                // }else{
                //     $content = 'Blog tidak ditemukan!';
                //     return $content;
            // }
        // ------------------------------------------------------------------------
        $limit = 5;
        $id_desa = \App\Models\Desa::where('website', $domain)->first(['id_desa']);
        $data = \App\Models\Berita::where('id_desa', $id_desa['id_desa'])->limit($limit)->orderby('dilihat', 'DESC')->get();
        $content = '';
        foreach ($data as $value) {
            $content .= '
            <ul class="simple-post-list">
                <li>
                    <article>
                        <div class="post-image">
                            <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                <a href="#">
                                    <img src="img/blog/square/blog-55.jpg" class="border-radius-0" width="50" height="50" alt="Simple Ways to Have a Pretty Face">
                                </a>
                            </div>
                        </div>
                        <div class="post-info">
                            <h4 class="font-weight-normal text-3 line-height-4 mb-0">
                                <a href="'.route('frontpage.berita.post.detail', array("id_desa" => $value->id_desa, "slug" => $value->slug_judul_berita)).'" class="text-dark">'.$value->judul_berita.'</a>
                            </h4>
                            <div class="post-meta">
                                <span><i class="far fa-user"></i> By <a href="#">'.$value->get_desa->nama_desa.'</a> </span> &mdash;
                                <span><i class="far fa-newspaper"></i> '.(empty($value->dilihat)?'0':$value->dilihat).' dilihat</span>
                            </div>
                        </div>
                    </article>
                </li>
            </ul>
            ';
            // '.Carbon::parse($value->created_at)->format('d F Y').' &mdash; '.$value->get_desa->nama_desa.'
        }
        return $content;
    }

    public static function blog_post_recent($domain){
        $limit = 5;
        $id_desa = \App\Models\Desa::where('website', $domain)->first(['id_desa']);
        $data = \App\Models\Berita::where('id_desa', $id_desa['id_desa'])->limit($limit)->orderby('id_berita', 'DESC')->get();
        $content = '';
        foreach ($data as $value) {
            $content .= '
            <ul class="simple-post-list">
                <li>
                    <article>
                        <div class="post-image">
                            <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                <a href="#">
                                    <img src="img/blog/square/blog-55.jpg" class="border-radius-0" width="50" height="50" alt="Simple Ways to Have a Pretty Face">
                                </a>
                            </div>
                        </div>
                        <div class="post-info">
                            <h4 class="font-weight-normal text-3 line-height-4 mb-0">
                                <a href="'.route('frontpage.berita.post.detail', array("id_desa" => $value->id_desa, "slug" => $value->slug_judul_berita)).'" class="text-dark">'.$value->judul_berita.'</a>
                            </h4>
                            <div class="post-meta">
                                <span><i class="far fa-user"></i> By <a href="#">'.$value->get_desa->nama_desa.'</a> </span> &mdash;
                                <span><i class="far fa-newspaper"></i> '.(empty($value->dilihat)?'0':$value->dilihat).' dilihat</span>
                            </div>
                        </div>
                    </article>
                </li>
            </ul>
            ';
        }
        return $content;
    }

    public static function blog_post_featured($domain){
        $limit = 5;
        $id_desa = \App\Models\Desa::where('website', $domain)->first(['id_desa']);
        $data = \App\Models\Berita::where('id_desa', $id_desa['id_desa'])
            ->where('pin_berita', '1')
            ->limit($limit)->get();
        // dd($data);
        $content = '';
        $content .= '
            <div class="owl-carousel owl-theme" data-plugin-options="{\'items\': 1, \'margin\': 10, \'loop\': true, \'nav\': false, \'dots\': false, \'autoplay\': true, \'autoplayTimeout\': 5000}">
        ';
        foreach ( $data as $value ) {
            $content .= '
                <div>
                    <a href="#">
                        <article>
                            <div class="thumb-info thumb-info-no-borders thumb-info-bottom-info thumb-info-bottom-info-dark thumb-info-bottom-info-show-more thumb-info-no-zoom border-radius-0">
                                <div class="thumb-info-wrapper thumb-info-wrapper-opacity-6">
                                    <img src="'.(!empty($value->post_img)?asset('storage/images/berita/'.$value->id_desa.'/'.$value->post_img):asset('storage/images/berita/default-blog-wide.jpg')).'" class="img-fluid" alt="'.$value->judul_berita.'">
                                    <div class="thumb-info-title bg-transparent p-4">
                                        <div class="thumb-info-type bg-color-primary px-2 mb-1"></div>
                                        <div class="thumb-info-inner mt-1">
                                            <h2 class="text-color-light line-height-2 text-4 font-weight-bold mb-0">'.$value->judul_berita.'</h2>
                                        </div>
                                        <div class="thumb-info-show-more-content">
                                            <p class="mb-0 text-1 line-height-9 mb-1 mt-2 text-light opacity-5">'.\Str::substr(\Str::replaceFirst('<p style="text-align:justify">', '', $value->isi_berita), 0, 100).' ...</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </a>
                </div>
            ';
        }
        $content .='
            </div>';
            
        $content .= '
            <h3 class="font-weight-bold text-3 mt-4 pt-2 mb-2 mt-4 mt-md-0">Author</h3>

            <div class="post-block post-author pt-2">
                <div class="img-thumbnail img-thumbnail-no-borders d-block pb-3">
                    <a href="#">
                        <img class="border-radius-0" src="'.(!empty($value->get_desa->logo)?asset('storage/images/logo/'.$value->get_desa->logo):asset('storage/images/avatars/default-avatar.jpg')).'" alt="The post author image" style="height: 112px; max-height: 112px; width: auto; max-width: 100%;">
                    </a>
                </div>
                <p><strong class="name"><a href="#" class="text-4 pb-2 pt-2 d-block text-dark">Desa '.!empty($value->get_desa->nama_desa).'</a></strong></p>
                '.!empty($value->get_desa->visi).'
            </div>
        ';
        return $content;
    }

    public static function blog_post($domain){
        $paginate = 2;
        // $post_desa = \App\Models\Desa::where('website', $domain)->get();
        $id_desa = \App\Models\Desa::where('website', $domain)->first();
        $post_desa = \App\Models\Berita::where('id_desa', $id_desa['id_desa'])->paginate($paginate);
        $content = '';
        // dd($post_desa);

        $content .= '
            <div class="row px-3">
        ';

        foreach ($post_desa as $value) {
            $content .= '
                <div class="col-sm-6">
                    <article class="post post-medium border-0 pb-0 mb-5">
                        <div class="post-image">
                            <a href="'.route('frontpage.berita.post.detail',[$value->id_desa, $value->slug_judul_berita]).'">
                                <img src="'.(!empty($value->post_img)?asset('storage/images/berita/'.$value->id_desa.'/'.$value->post_img):asset('storage/images/berita/default.jpg')).'" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
                            </a>
                        </div>
                        <div class="post-content">
                            <h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2">
                                <a href="'.route('frontpage.berita.post.detail',[$value->id_desa, $value->slug_judul_berita]).'">
                                    '.$value->judul_berita.'
                                </a>
                            </h2>
                            '.\Str::substr($value->isi_berita, 0, 100).' ...
                            <div class="post-meta">
                                <span><i class="far fa-user"></i> By <a href="#">'.$value->get_desa->nama_desa.'</a> </span>
                                <span><i class="far fa-calendar-check"></i> '.date('d F Y H:i', strtotime($value->created_at)).'</span>
                                <span><i class="far fa-newspaper"></i> '.(empty($value->dilihat)?'0':$value->dilihat).' dilihat</span>
                                <span class="d-block mt-2">
                                    <a href="'.route('frontpage.berita.post.detail',[$value->id_desa, $value->slug_judul_berita]).'" 
                                        class="btn btn-xs btn-primary text-1 text-uppercase">Baca selengkapnya ...
                                    </a>
                                </span>
                            </div>
                        </div>
                    </article>
                </div>
            ';
        }
        
        $content .= '
            </div>
            <div class="row px-3">
                <div class="col text-center">
                    '.$post_desa->links().'
                </div>
            </div>';
        // $page->links('vendor.pagination.bootstrap-4')
        return $content;
    }

    public static function services ($domain){
        $paginate = 4;
        // $post_desa = \App\Models\Desa::where('website', $domain)->get();
        $id_desa = \App\Models\Desa::where('website', $domain)->first();
        $layanan = \App\Models\Layanan::where('id_desa', $id_desa['id_desa'])->paginate($paginate);
        $content = '';
        $content .= '
        <div class="row">
        ';

        foreach ($layanan as $value) {
            $content .= '
            <div class="col-md-3">
                <article class="post post-medium border-0 pb-0 mb-5">
                    <div class="post-image">
                        <a href="'.route('frontpage.layanan.detail', [$value->id_desa, $value->slug_layanan]).'">
                            <img src="'.(!empty($value->img)?'storage/images/services/'.$value->id_desa.'/'.$value->img:'storage/images/services/default-service.jpg').'" class="img-fluid img-thumbnail img-thumbnail-no-borders rounded-0" alt="" />
                        </a>
                    </div>

                    <div class="post-content">
                        <h2 class="font-weight-semibold text-5 line-height-6 mt-3 mb-2">
                            <a href="'.route('frontpage.layanan.detail', [$value->id_desa, $value->slug_layanan]).'">'.$value->nama_layanan.'</a>
                        </h2>
                        '.\Str::substr(\Str::replaceFirst('<p>', '', $value->deskripsi_layanan), 0, 50).' ...
                        <div class="post-meta">
                            <span><i class="far fa-user"></i> By <a href="#">'.$value->get_desa->nama_desa.'</a> </span>
                            <span><i class="far fa-calendar-check"></i> '.date('d F Y H:i', strtotime($value->created_at)).'</span>
                            <span><i class="far fa-newspaper"></i> '.(empty($value->dilihat)?'0':$value->dilihat).' dilihat</span>
                            <span class="d-block mt-2">
                                <a href="'.route('frontpage.layanan.detail', [$value->id_desa, $value->slug_layanan]).'" 
                                    class="btn btn-xs btn-primary text-1 text-uppercase">Baca selengkapnya ...
                                </a>
                            </span>
                        </div>
                    </div>
                </article>
            </div>';
        }

        $content .= '
        </div>
        <div class="row">
            <div class="col">
                '.$layanan->links().'
            </div>
        </div>
        ';
        return $content;
    }

    public static function potensi_desa($domain){
        $desa = \App\Models\Desa::where('website', $domain)->get();
        $content = '';
        foreach ($desa as $d) {
            // $content .= $d->get_potensi;
            foreach ($d->get_potensi as $val) {
                // $content .= $val->nama_potensi;
                $content .= '
                <div class="isotope-item">
                    <div class="image-gallery-item mb-0 appear-animation" data-appear-animation="fadeInLeftShorter"
                        data-appear-animation-delay="700">
                        <a href="'.route('frontpage.detail_potensi',$val->slug_potensi).'">
                            <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                                <span class="thumb-info-wrapper">
                                    <img src="'.asset(env('IMAGES_PATH').'potentions/'.$d->id_desa.'/'.$val->gambar).'" class="img-fluid" style="width:300px;height:200px;">
                                    <span class="thumb-info-title">
                                        <span class="thumb-info-inner">'.strtoupper($val->nama_potensi).'</span>
                                        <span class="thumb-info-type">Baca selengkapnya</span>
                                    </span>
                                    <span class="thumb-info-action">
                                        <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                                    </span>
                                </span>
                            </span>
                        </a>
                    </div>
                </div>
                ';
            }
        }
        return $content;
    }

    public static function footer_address($domain = ''){
        $content = '';
        $desa = \App\Models\Desa::where('website', $domain)->first();   
        $content .= '
        <ul class="list list-icons list-icons-lg">
            <li class="mb-1"><i class="far fa-dot-circle text-color-primary"></i><p class="m-0">'.$desa->alamat.'</p></li>
            <li class="mb-1"><i class="fab fa-whatsapp text-color-primary"></i><p class="m-0"><a href="tel:'.$desa->no_telp.'">'.$desa->no_telp.'</a></p></li>
            <li class="mb-1"><i class="far fa-envelope text-color-primary"></i><p class="m-0"><a href="mailto:'.$desa->get_user->email.'">'.$desa->get_user->email.'</a></p></li>
        </ul>
        ';
        return $content;
    }
    
    public static function footer_page_menu($domain = ''){
        $content = '';
        $desa = \App\Models\Desa::where('website', $domain)->first();   
        $content .= '
        <ul class="list list-icons list-icons-sm mb-0">
            <li><i class="fas fa-angle-right top-8"></i> <a class="link-hover-style-1 text-uppercase" href="'.route('frontpage.layanan').'">Layanan</a></li>
            <li><i class="fas fa-angle-right top-8"></i> <a class="link-hover-style-1 text-uppercase" href="'.route('frontpage.potensi').'">Potensi</a></li>
            <li><i class="fas fa-angle-right top-8"></i> <a class="link-hover-style-1 text-uppercase" href="'.route('frontpage.info').'">Informasi</a></li>
            <li><i class="fas fa-angle-right top-8"></i> <a class="link-hover-style-1 text-uppercase" href="'.route('frontpage.lapor').'">Warga Melapor</a></li>
        </ul>
        ';
        return $content;
    }

    public static function footer_latest_news ($domain){
        $limit = 2;
        $id_desa = \App\Models\Desa::where('website', $domain)->first(['id_desa']);
        $data = \App\Models\Berita::where('id_desa', $id_desa['id_desa'])->limit($limit)->orderby('id_berita', 'DESC')->get();
        $content = '';
        $content .= '
        <ul class="list-unstyled mb-0">
        ';
        foreach ($data as $value){
        $content .= '
            <li class="media mb-3 pb-1">
                <article class="d-flex">
                    <a href="'.route('frontpage.berita.post.detail',[$value->id_desa, $value->slug_judul_berita]).'">
                        <img class="mr-3 rounded-circle" src="'.(!empty($value->post_img)?asset('storage/images/berita/'.$value->id_desa.'/'.$value->post_img):asset('storage/images/berita/default-blog-square.jpg')).'" alt="" style="max-width: 70px;">
                    </a>
                    <div class="media-body">
                        <a href="'.route('frontpage.berita.post.detail',[$value->id_desa, $value->slug_judul_berita]).'">
                            <h6 class="text-3 text-color-light opacity-8 ls-0 mb-1">'.$value->judul_berita.'</h6>
                            <p class="text-2 mb-0">'.date('d M Y', strtotime($value->created_at)).'</p>
                        </a>
                    </div>
                </article>
            </li>
        ';
        }
        $content .='
        </ul>
        ';
        return $content;
    }

    public static function footer_reports ($domain) {
        $content = '';
        $content .= '
        <ul class="list-unstyled mb-0">
        ';
        $content .= '
            <li class="mb-3 pb-1">
                <a href="#">
                    <p class="text-3 text-color-light opacity-8 mb-1"><i class="fas fa-angle-right text-color-primary"></i><strong class="ml-2">John Doe</strong> commented on <strong class="text-color-primary">lorem ipsum dolor sit amet.</strong></p>
                    <p class="text-2 mb-0">12:55 AM Dec 19th</p>
                </a>
            </li>
            <li>
                <a href="#">
                    <p class="text-3 text-color-light opacity-8 mb-1"><i class="fas fa-angle-right text-color-primary"></i><strong class="ml-2">John Doe</strong> commented on <strong class="text-color-primary">lorem ipsum dolor sit amet.</strong></p>
                    <p class="text-2 mb-0">12:55 AM Dec 19th</p>
                </a>
            </li>
        ';
        $content .= '
        </ul>        
        ';
        return $content;
    }

	public static function autonumber($table,$primary,$prefix=''){
        $q = \DB::table($table)->select(\DB::raw('MAX(RIGHT('.$primary.',3)) as kd_max'));
        $prx = $prefix;
        if($q->count()>0){
            foreach($q->get() as $k){
                $tmp = ((int)$k->kd_max)+1;
                $kd = $prx.sprintf("%04s", $tmp);
            }
        }else{$kd = $prx."0001";}
        return $kd;
    }

    public static function visitor_counter ($id, $id_desa) {
        $berita = \App\Models\Berita::where('id_berita', $id)
            ->where('id_desa', $id_desa)->first();
        $berita->dilihat = $berita->dilihat + 1;
        $berita->update();
        return $berita->dilihat;
    }
}
