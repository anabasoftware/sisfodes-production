-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE TABLE "sfd_berita" -----------------------------------
CREATE TABLE `sfd_berita`( 
	`id_berita` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NOT NULL,
	`id_kategori_berita` Int( 11 ) NULL DEFAULT NULL,
	`judul_berita` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`slug_judul_berita` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`isi_berita` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`dilihat` Int( 11 ) NULL DEFAULT NULL,
	`publikasi` TinyInt( 1 ) NULL DEFAULT 0,
	`post_img` VarChar( 191 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'img/blog/medium/blog-1.jpg',
	`pin_berita` Int( 11 ) NULL DEFAULT 0,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_berita` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 48;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_berita_banner" ----------------------------
CREATE TABLE `sfd_berita_banner`( 
	`id_banner` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NOT NULL,
	`nama_banner` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`deskripsi_banner` Text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`status` Int( 2 ) NULL DEFAULT 1,
	`gambar` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_banner` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 12;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_berita_detail" ----------------------------
CREATE TABLE `sfd_berita_detail`( 
	`id_berita_detail` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_berita` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`id_tag_berita` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`id_desa` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_berita_detail` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 22;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_berita_kategori" --------------------------
CREATE TABLE `sfd_berita_kategori`( 
	`id_kategori_berita` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NOT NULL,
	`nama_kategori` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`slug_kategori` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`status` Int( 11 ) NOT NULL DEFAULT 1,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_kategori_berita` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 10;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_berita_kategori_sub" ----------------------
CREATE TABLE `sfd_berita_kategori_sub`( 
	`id_sub_kategori` BigInt( 20 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NOT NULL,
	`id_kategori_berita` Int( 11 ) NOT NULL,
	`nama_sub_kategori` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`slug_sub_kategori` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`status` Int( 1 ) NOT NULL,
	`created_at` Timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id_sub_kategori` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_berita_tags" ------------------------------
CREATE TABLE `sfd_berita_tags`( 
	`id_tag_berita` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NOT NULL,
	`nama_tag` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`slug_tag` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`status` Int( 1 ) NOT NULL DEFAULT 1,
	`created_at` Timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` Timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id_tag_berita` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 51;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_desa" -------------------------------------
CREATE TABLE `sfd_desa`( 
	`id_desa` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`kode_wilayah` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`kepala_desa` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`id_kecamatan` Int( 11 ) NULL DEFAULT NULL,
	`nama_desa` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`alamat` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`desa_id` Int( 11 ) NULL DEFAULT NULL,
	`no_telp` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`fax` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`website` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`peta_wilayah` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`logo` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`kode_pos` VarChar( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`visi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`misi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`status` TinyInt( 1 ) NULL DEFAULT 1,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	PRIMARY KEY ( `id_desa` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 27;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_desa_dokumen" -----------------------------
CREATE TABLE `sfd_desa_dokumen`( 
	`id_dokumen_desa` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`judul_dokumen` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`file` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_dokumen_desa` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_desa_layanan" -----------------------------
CREATE TABLE `sfd_desa_layanan`( 
	`id_layanan` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`kode_wilayah` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`slug_layanan` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`nama_layanan` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`deskripsi_layanan` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`post_img` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`dilihat` Int( 11 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `id_layanan` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 13;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_desa_lembaga" -----------------------------
CREATE TABLE `sfd_desa_lembaga`( 
	`id_lembaga` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NOT NULL,
	`kode_wilayah` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`nama_lembaga` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`deskripsi_lembaga` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_lembaga` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 4;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_desa_potensi" -----------------------------
CREATE TABLE `sfd_desa_potensi`( 
	`id_potensi` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`nama_potensi` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`deskripsi_potensi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`gambar` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`slug_potensi` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_potensi` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 6;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_desa_wilayah" -----------------------------
CREATE TABLE `sfd_desa_wilayah`( 
	`id_wilayah` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`luas_wilayah_tanah` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`luas_daratan` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`luas_persawahan` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`luas_tegalan` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`luas_lain_lain` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`luas_tanah_desa` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`jumlah_dusun` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_rt` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_rw` Int( 11 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_wilayah` ),
	CONSTRAINT `id_wilayah` UNIQUE( `id_wilayah` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_detail_realisasi_belanja" -----------------
CREATE TABLE `sfd_detail_realisasi_belanja`( 
	`id_detail_realisasi` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_realisasi_belanja` Int( 11 ) NULL DEFAULT NULL,
	`id_rencana_belanja` Int( 11 ) NULL DEFAULT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`jumlah` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`satuan` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`harga` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`subtotal` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`file_foto` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_detail_realisasi` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_detail_rencana_anggaran_biaya" ------------
CREATE TABLE `sfd_detail_rencana_anggaran_biaya`( 
	`id_detail_rab` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_rencana_belaja` Int( 11 ) NULL DEFAULT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`jumlah` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`satuan` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`harga` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`subtotal` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_detail_rab` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_geografi" ---------------------------------
CREATE TABLE `sfd_geografi`( 
	`id_geografi` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`barat` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`utara` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`selatan` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`timur` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_geografi` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_keadaan_statistik" ------------------------
CREATE TABLE `sfd_keadaan_statistik`( 
	`id_statistik` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`kategori` Enum( 'LAHAN SAWAH', 'LAHAN KERING', 'LAHAN LAINYA' ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`keterangan` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`luas` Decimal( 10, 0 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_statistik` ),
	CONSTRAINT `id_statistik` UNIQUE( `id_statistik` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_lok_kabupaten" ----------------------------
CREATE TABLE `sfd_lok_kabupaten`( 
	`id_kabupaten` Int( 10 ) AUTO_INCREMENT NOT NULL,
	`provinsi_id` Int( 10 ) NULL DEFAULT NULL,
	`nama` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	PRIMARY KEY ( `id_kabupaten` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 8273;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_master_anggaran_belanja" ------------------
CREATE TABLE `sfd_master_anggaran_belanja`( 
	`id_anggaran_belanja` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`tahun_anggaran` Int( 11 ) NULL DEFAULT NULL,
	`kode_rekening` VarChar( 8 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`nama_angggaran_belanja` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_anggaran_belanja` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_master_desa" ------------------------------
CREATE TABLE `sfd_master_desa`( 
	`desa_id` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_kecamatan` Int( 11 ) NULL DEFAULT NULL,
	`nama_desa` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT CURRENT_TIMESTAMP,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	PRIMARY KEY ( `desa_id` ),
	CONSTRAINT `desa_id` UNIQUE( `desa_id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 171;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_master_kecamatan" -------------------------
CREATE TABLE `sfd_master_kecamatan`( 
	`id_kecamatan` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`nama_kecamatan` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_kecamatan` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5484;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_master_penerimaan" ------------------------
CREATE TABLE `sfd_master_penerimaan`( 
	`id_penerimaan` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`tahun_anggaran` Int( 11 ) NULL DEFAULT NULL,
	`kode_rekening` VarChar( 10 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`nama_penerimaan` VarChar( 200 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_penerimaan` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_pemerintahaan" ----------------------------
CREATE TABLE `sfd_pemerintahaan`( 
	`id_pemerintahan` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`jumlah_kecamatan` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_kelurahan` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_desa` Int( 11 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_pemerintahan` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_profil_kabupaten" -------------------------
CREATE TABLE `sfd_profil_kabupaten`( 
	`id_kabupaten` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`kode_wilayah` VarChar( 50 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`nama_kabupaten` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`id_kepala` Int( 11 ) NULL DEFAULT NULL,
	`email` VarChar( 150 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`maps` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`logo` VarChar( 100 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_kabupaten` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_realisasi_anggaran_belanja" ---------------
CREATE TABLE `sfd_realisasi_anggaran_belanja`( 
	`id_realisasi_belaja` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`tgl` Date NULL DEFAULT NULL,
	`nobukti` VarChar( 25 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`id_renacana_belanja` Int( 11 ) NULL DEFAULT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`total` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_realisasi_belaja` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_realisasi_penerimaan_anggaran" ------------
CREATE TABLE `sfd_realisasi_penerimaan_anggaran`( 
	`id_realiasasi_penerimaan` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_renacan_penerimaan` Int( 11 ) NULL DEFAULT NULL,
	`tgl` Date NULL DEFAULT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`nominal_diterima` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_realiasasi_penerimaan` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_rencana_anggaran_belanja" -----------------
CREATE TABLE `sfd_rencana_anggaran_belanja`( 
	`id_rencana_belanja` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`tahun_anggaran` Int( 11 ) NULL DEFAULT NULL,
	`nobukti` VarChar( 20 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`tgl` Date NULL DEFAULT NULL,
	`id_anggaran_belanja` Int( 11 ) NULL DEFAULT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`total` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_rencana_belanja` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_rencana_penerimaan_anggaran" --------------
CREATE TABLE `sfd_rencana_penerimaan_anggaran`( 
	`id_renacan_penerimaan` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`id_penerimaan` Int( 11 ) NULL DEFAULT NULL,
	`tgl` Date NULL DEFAULT NULL,
	`tahun_anggaran` Int( 11 ) NULL DEFAULT NULL,
	`deskripsi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`nominal_rencana` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`realisasi` Decimal( 20, 2 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_renacan_penerimaan` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_roles" ------------------------------------
CREATE TABLE `sfd_roles`( 
	`id` BigInt( 11 ) AUTO_INCREMENT NOT NULL,
	`role_name` VarChar( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL DEFAULT NULL,
	PRIMARY KEY ( `id` ),
	CONSTRAINT `unique_id` UNIQUE( `id` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_sejarah_desa" -----------------------------
CREATE TABLE `sfd_sejarah_desa`( 
	`id_sejarah_desa` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`sejarah` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_sejarah_desa` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 2;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_sejarah_kabupaten" ------------------------
CREATE TABLE `sfd_sejarah_kabupaten`( 
	`id_sejarah` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`sejarah` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_sejarah` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_statistik_desa" ---------------------------
CREATE TABLE `sfd_statistik_desa`( 
	`id_statistik_desa` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_kk` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_penduduk` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_laki_laki` Int( 11 ) NULL DEFAULT NULL,
	`jumlah_perempuan` Int( 11 ) NULL DEFAULT NULL,
	`balita` Int( 11 ) NULL DEFAULT NULL,
	`anak` Int( 11 ) NULL DEFAULT NULL,
	`remaja` Int( 11 ) NULL DEFAULT NULL,
	`dewasa` Int( 11 ) NULL DEFAULT NULL,
	`orang_tua` Int( 11 ) NULL DEFAULT NULL,
	`tgl_statistik` Date NOT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_statistik_desa` ),
	CONSTRAINT `id_statistik_desa` UNIQUE( `id_statistik_desa` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 3;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_tahun_anggaran" ---------------------------
CREATE TABLE `sfd_tahun_anggaran`( 
	`id_tahun_anggaran` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`id_desa` Int( 11 ) NULL DEFAULT NULL,
	`tahun_anggaran` Int( 11 ) NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_tahun_anggaran` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_users" ------------------------------------
CREATE TABLE `sfd_users`( 
	`id_user` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`user_name` VarChar( 191 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`fullname` VarChar( 191 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
	`email` VarChar( 191 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`email_verified_at` Timestamp NULL DEFAULT NULL,
	`password` VarChar( 191 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
	`role_id` Int( 11 ) NOT NULL,
	`status` TinyInt( 1 ) NOT NULL DEFAULT 1,
	`remember_token` VarChar( 100 ) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
	`id_desa` Int( 11 ) NOT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_user` ),
	CONSTRAINT `users_email_unique` UNIQUE( `email` ) )
CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci
COMMENT 'Tabel pengguna web / aplikasi'
ENGINE = InnoDB
AUTO_INCREMENT = 5;
-- -------------------------------------------------------------


-- CREATE TABLE "sfd_visi_misi" --------------------------------
CREATE TABLE `sfd_visi_misi`( 
	`id_visimisi` Int( 11 ) AUTO_INCREMENT NOT NULL,
	`visi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`misi` Text CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
	`created_at` Timestamp NULL DEFAULT NULL,
	`updated_at` Timestamp NULL ON UPDATE CURRENT_TIMESTAMP DEFAULT NULL,
	PRIMARY KEY ( `id_visimisi` ) )
CHARACTER SET = utf8
COLLATE = utf8_general_ci
ENGINE = InnoDB
AUTO_INCREMENT = 1;
-- -------------------------------------------------------------


-- Dump data of "sfd_berita" -------------------------------
BEGIN;

INSERT INTO `sfd_berita`(`id_berita`,`id_desa`,`id_kategori_berita`,`judul_berita`,`slug_judul_berita`,`isi_berita`,`dilihat`,`publikasi`,`post_img`,`pin_berita`,`created_at`,`updated_at`) VALUES 
( '43', '11', '3', 'Lorem ipsum dolor sit amet', 'lorem-ipsum-dolor-sit-amet', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>

<p style="text-align:justify">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>

<p style="text-align:justify">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>

<p style="text-align:justify">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>

<p style="text-align:justify">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>', '2', '1', NULL, '0', '2021-01-11 14:39:24', '2021-06-17 11:15:03' ),
( '44', '11', '1', 'ini adalah judul beritassssss nya', 'ini-adalah-judul-beritassssss-nya', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>

<p style="text-align:justify">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>

<p style="text-align:justify">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>

<p style="text-align:justify">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>

<p style="text-align:justify">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>', '7', '1', NULL, '1', '2021-02-02 07:30:23', '2021-06-17 11:15:17' ),
( '45', '11', '2', 'nihil molestias vero aspernatur', 'nihil-molestias-vero-aspernatur', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>

<p style="text-align:justify">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>

<p style="text-align:justify">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>

<p style="text-align:justify">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>

<p style="text-align:justify">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>', '0', '1', NULL, '0', '2021-03-16 07:30:23', '2021-06-17 11:15:07' ),
( '46', '11', '4', 'Consequatur id quasi at optio', 'consequatur-id-quasi-at-optio', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>

<p style="text-align:justify">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>

<p style="text-align:justify">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>

<p style="text-align:justify">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>

<p style="text-align:justify">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>', '1', '1', NULL, '1', '2021-04-19 07:30:23', '2021-06-17 11:15:18' ),
( '47', '11', '3', 'amet consectetur adipisicing elit', 'amet-consectetur-adipisicing-elit', '<p style="text-align:justify">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>

<p style="text-align:justify">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>

<p style="text-align:justify">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>

<p style="text-align:justify">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>

<p style="text-align:justify">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>', '2', '1', NULL, '1', '2021-05-26 07:30:23', '2021-06-21 09:52:16' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_berita_banner" ------------------------
BEGIN;

INSERT INTO `sfd_berita_banner`(`id_banner`,`id_desa`,`nama_banner`,`deskripsi_banner`,`status`,`gambar`,`created_at`,`updated_at`) VALUES 
( '11', '11', 'Portal Desa Loce', 'Sahu Timur adalah sebuah kecamatan di Kabupaten Halmahera Barat, Maluku Utara, Indonesia.', '1', 'banner__11_11_20200714152338.jpg', '2020-07-09 14:39:45', '2020-07-14 15:34:42' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_berita_detail" ------------------------
BEGIN;

INSERT INTO `sfd_berita_detail`(`id_berita_detail`,`id_berita`,`id_tag_berita`,`id_desa`,`created_at`,`updated_at`) VALUES 
( '21', NULL, NULL, NULL, '2021-04-06 00:37:34', NULL );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_berita_kategori" ----------------------
BEGIN;

INSERT INTO `sfd_berita_kategori`(`id_kategori_berita`,`id_desa`,`nama_kategori`,`slug_kategori`,`status`,`created_at`,`updated_at`) VALUES 
( '1', '11', 'Kategori Berita 1', 'sluk-kategori-1', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '2', '11', 'Kategori Berita 2', 'sluk-kategori-2', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '3', '11', 'Kategori Berita 3', 'sluk-kategori-3', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '4', '11', 'Kategori Berita 4', 'sluk-kategori-4', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '5', '11', 'Kategori Berita 5', 'sluk-kategori-5', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '6', '11', 'Kategori Berita 6', 'sluk-kategori-6', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '7', '11', 'Kategori Berita 7', 'sluk-kategori-7', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '8', '11', 'Kategori Berita 8', 'sluk-kategori-8', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' ),
( '9', '11', 'Kategori Berita 9', 'sluk-kategori-9', '1', '2021-04-04 04:49:01', '2021-04-04 04:49:01' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_berita_kategori_sub" ------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_berita_tags" --------------------------
BEGIN;

INSERT INTO `sfd_berita_tags`(`id_tag_berita`,`id_desa`,`nama_tag`,`slug_tag`,`status`,`created_at`,`updated_at`) VALUES 
( '42', '11', 'eko sos bud', NULL, '1', '2021-04-05 17:33:14', '2021-04-05 17:33:14' ),
( '43', '11', 'eko sos bud', NULL, '1', '2021-04-05 17:33:37', '2021-04-05 17:33:37' ),
( '44', '11', 'eko sos bud', NULL, '1', '2021-04-05 17:34:07', '2021-04-05 17:34:07' ),
( '45', '11', 'eko sos bud', NULL, '1', '2021-04-05 17:35:25', '2021-04-05 17:35:25' ),
( '46', '11', 'sosial budaya', NULL, '1', '2021-04-05 17:35:25', '2021-04-05 17:35:25' ),
( '47', '11', 'perekonomian', NULL, '1', '2021-04-05 17:35:25', '2021-04-05 17:35:25' ),
( '48', '11', 'eko sos bud', NULL, '1', '2021-04-05 17:37:16', '2021-04-05 17:37:16' ),
( '50', '11', 'sosial budaya', NULL, '1', '2021-04-05 17:37:34', '2021-04-05 17:37:34' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_desa" ---------------------------------
BEGIN;

INSERT INTO `sfd_desa`(`id_desa`,`kode_wilayah`,`kepala_desa`,`id_kecamatan`,`nama_desa`,`alamat`,`desa_id`,`no_telp`,`fax`,`website`,`peta_wilayah`,`logo`,`kode_pos`,`visi`,`misi`,`status`,`updated_at`,`created_at`) VALUES 
( '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '1', '2020-08-24 00:01:54', NULL ),
( '11', '82.01.09.2001', 'Drs. Joko Purnomo, M.Pd, M.Si, M.Kom, MH', '5483', 'Loce', 'Jl. Loce, Kecamatan Sahu Timur, Kabupaten Halmahera Barat, Maluku Utara', '165', '0853-4004-6724', '0853-4004-6724', 'desaloce.desa.dev', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15956.47836281679!2d127.4550362767079!3d1.0723140374848803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x329b500beeadbcfb%3A0xfe9e3f84c455a46e!2sJl.%20Baru%2C%20Jailolo%2C%20Kabupaten%20Halmahera%20Barat%2C%20Maluku%20Utara!5e0!3m2!1sid!2sid!4v1582555104368!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>', 'logo__1594741540.png', '97758', '<p><em>&quot;Terwujudnya kehidupan masyarakat desa loce yang religius, aman, harmonis, maju, adil dan berkah&quot;</em></p>', '<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto cupiditate ex ab tenetur laudantium ducimus tempora nulla quo unde distinctio doloribus eos, amet in et vero repellendus vel expedita praesentium?</p>', '1', '2020-08-23 23:38:54', '2020-07-27 22:48:38' ),
( '26', '82.01.09.2018', 'Drs. Eko Nugroho, M.Pd, M.Si, M.Kom, MH', '5483', 'Air Panas', 'Jl. Air Panas, Kecamatan Sahu Timur, Kabupaten Halmahera Barat, Maluku Utara', NULL, NULL, NULL, 'airpanas.desa.dev', NULL, NULL, '97753', NULL, NULL, '1', '2021-06-17 14:02:13', '2020-08-23 17:05:27' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_desa_dokumen" -------------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_desa_layanan" -------------------------
BEGIN;

INSERT INTO `sfd_desa_layanan`(`id_layanan`,`id_desa`,`kode_wilayah`,`slug_layanan`,`nama_layanan`,`deskripsi_layanan`,`post_img`,`dilihat`,`created_at`,`updated_at`) VALUES 
( '3', '11', NULL, 'surat-keterangan-lahir', 'Surat Keterangan Lahir', 'Salah satu tugas atau layanan desa ialah melakukan pengurusan akte lahir atau surat keterangan lahir.&nbsp;<br />
Urus akte kelahiran&nbsp;di kantor desa harus membawa persyaratan, berupa fotokopi KK dan KTP, fotokopi surat nikah, surat pengantar dari Rt/Rw, fotokopi lunas PBB, fotokopi surat keterangan lahir yang diberikan oleh pihak Rumah sakit atau Bidan Desa.', NULL, NULL, '2021-06-21 21:16:59', '2021-06-21 15:11:03' ),
( '4', '11', NULL, 'pelatihan-desa', 'Pelatihan Desa', 'Pelatihan desa&nbsp;biasanya dilakukan untuk aparatur desa, dalam rangka meningkatkan kapasitas dari pihak pemerintah desa sekaligus BPD, dengan tujuan agar para peserta yang mengikuti pelatihan bisa mengetahui fungsi kerja dan tugas-tugas dalam pemerintah desa. Selain itu, para peserta juga bisa menguraikan tanggung jawab dan tugas kerja masing-masing. Seperti tugas-tugas dari pihak aparatur desa berikut ini :<br />
Sekretaris desa<br />
Tugasnya ialah membantu Kepala Desa untuk melaksanakan dan mempersiapkan pengelolaan administrasi desa sekaligus mempersiapkan penyusunan laporan mengenai penyelenggaraan pemerintah.<br />
Adapun fungsinya ialah untuk penyelenggaraan kegiatan administrasi serta mempersiapkan bahan laporan guna kelancaran tugas Kades (Kepala Desa).<br />
Kaur Umum<br />
Bertugas untuk membantu sekretaris desa melalui pelaksanaan pengelolaan sumber APBD, mempersiapkan bahan laporan penyusunan APBDes dan pengelolaan administrasi untuk keuangan desa.&nbsp;<br />
Adapun fungsinya yaitu untuk melaksanakan persiapan penyusunan APBDes, pengelolaan administrasi pada keuangan desa,&nbsp; dan sebagainya.<br />
Kaur Kesos<br />
Bertugas untuk mempersiapkan bahan perumusan tentang kebijakan teknis dari penyusunan keagamaan dan melaksanakan program berkaitan dengan pemberdayaan masyarakat.<br />
Adapun fungsinya mempersiapkan bahan dalam program pemberdayaan masyarakat dan persiapan bahan pelaksanaan program yang berkaitan dengan kegiatan keagamaan.<br />
Kaur Ekbang<br />
Bertugas untuk membantu desa melaksanakan persiapan bahan perumusan tentang kebijakan teknis dalam pengembangan potensi desa dan ekonomi masyarakat, serta pengelolaan administrasi pembangunan.<br />
Fungsinya ialah mempersiapkan bantuan kajian perkembangan ekonomi masyarakat dan membantu pelaksanaan administrasi pembangunan serta pengelolaan tugas pembangunan yang diberikan kepala desa.', NULL, NULL, '2021-06-21 21:16:43', '2021-06-21 15:10:59' ),
( '5', '11', NULL, 'daftar-ktp', 'Daftar KTP', 'Daftar&nbsp;KTP&nbsp;online&nbsp;atau biasa disebut sebagai KTP elektronik maupun&nbsp;e-KTP juga dilakukan di kantor desa. KTP elektronik ialah kartu tanda penduduk dilengkapi oleh cip sebagai identitas resmi dari penduduk yang terdaftar. Jika tak ada perubahan berdasarkan kebijakan pemerintah, maka KTP dapat berlaku hingga seumur hidup.<br />
Untuk persyaratan pengajuan e-KTP diantaranya meliputi perekaman, fotokopi KK, surat pengantar dari Kepala&nbsp; Desa yang diketahui camat, dan fotokopi akta nikah untuk pemohon yang usianya belum genap 17 tahun. Sesudah Anda merekam data, maka selanjutnya petugas akan mencetak KTP elektronik dengan syarat form permohonan cetak KTP-elektronik, surat keterangan atau KTP lama dan fotokopi KK.<br />
jika KTP Anda hilang, bisa mengajukan pencetakan KTP pengganti langsung di kantor kecamatan dan harus melampirkan fotokopi KK, surat keterangan KTP hilang dari pihak kepolisian, dan surat pernyataan kehilangan dengan materai.', NULL, NULL, '2021-06-21 21:15:49', '2021-06-21 15:10:54' ),
( '7', '11', NULL, 'pembuatan-ktp', 'Pembuatan KTP', 'Adapun syarat keterangan untuk pembuatan KTP para pemula, diwajibkan membawa surat pengantar dari RT atau RW, menyertakan fotokopi KK, fotokopi lunas PBB dan pas foto berukuran 3 x 4 sebanyak 3 lembar. Ketentuannya usia pemohon harus berusia 17 tahun minimal.<br />
Sementara untuk membuat surat keterangan perpanjangan KTP, pemohon harus menyertakan surat pengantar RT/RW, KTP asli, pas foto berukuran 3 x 4 sebanyak 3 lembar, fotokopi lunas PBB, dan fotokopi KK.', NULL, NULL, '2021-06-21 21:15:34', '2021-06-21 15:10:50' ),
( '8', '11', NULL, 'perubahan-data-kartu-keluarga', 'Perubahan Data Kartu Keluarga', 'Pelayanan birokrasi desa/administrasi lainnya yaitu untuk pembuatan kartu keluarga. Untuk urus kartu keluarga di desa, Anda harus membawa surat pengantar dari Rt/Rw dan KK asli.', NULL, NULL, '2021-06-21 21:15:20', '2021-06-21 15:10:45' ),
( '9', '11', NULL, 'pengurusan-surat-kematian', 'Pengurusan Surat Kematian', 'Urus surat kematian&nbsp;sangat penting untuk mencegah agar data-data almarhum/almarhumah disalahgunakan pihak tertentu yang tidak bertanggung jawab. Sementara tujuan pemerintah agar dapat memastikan keakuratan informasi dan data penduduk yang potensial untuk dijadikan pemilih dalam Pilkada atau Pemilu.&nbsp;<br />
Adapun persyaratan yang harus dipenuhi saat mengurus surat keterangan kematian, ialah surat keterangan kematian dari Rumah Sakit jika yang meninggal di rumah sakit, fotokopi KTP yang meninggal dan surat pengantar RT/RW.', NULL, NULL, '2021-06-21 21:15:09', '2021-06-21 15:10:40' ),
( '10', '11', NULL, 'apbd-desa', 'APBD Desa', 'Anggaran Pendapatan dan Belanja Desa atau APBDesa, yaitu rencana keuangan pemerintah desa tahunan yang ditetapkan dan dibahas Kepala Desa dengan BPD atau Badan Permusyawaratan Desa lewat Peraturan Desa.<br />
APB Desa merupakan dokumen dengan kekuatan hukum untuk menjamin kepastian terhadap rencana kegiatan, artinya mengikat aparatur dan pemerintah desa terkait, dalam melaksanakan kegiatan berdasarkan rencana yang sudah ditetapkan sekaligus untuk menjamin ketersediaan anggaran dengan jumlah tertentu. APB Desa sendiri menjamin kelayakan kegiatan dalam hal pendanaan, karenanya bisa dipastikan kelayakan pada hasil aktivitas secara teknis.', NULL, NULL, '2021-06-21 21:14:55', '2021-06-21 15:10:35' ),
( '11', '11', NULL, 'jaringan-aspirasi-rakyat', 'Jaringan Aspirasi Rakyat', 'Badan Permusyawaratan Desa disingkat BPD ialah lembaga perwujudan dari demokrasi penyelenggaraan pemerintah desa. BPD inilah yang berfungsi untuk menetapkan semua peraturan desa dengan kepala desa, sekaligus dapat menyalurkan dan menampung aspirasi masyarakat sesuai peraturan Undang-Undang No. 6 Tahun 2014.&nbsp;', NULL, NULL, '2021-06-21 21:14:44', '2021-06-21 15:10:31' ),
( '12', '11', NULL, 'lapor-pemerintah-desa', 'Lapor Pemerintah Desa', 'Salah satu pelayanan desa yaitu dapat mempermudah masyarakat atau warga untuk melaporkan keluhan sekaligus aspirasi Anda mengenai pemerintah desa terkait. Masyarakat bisa melaporkan secara langsung ke Badan Permusyawaratan Desa. Selain itu, kini pemerintah juga menyediakan layanan berupa&nbsp;call center&nbsp;di 1500040 untuk masyarakat agar bisa melaporkan berbagai macam masalah yang berhubungan dengan desa. Itulah beberapa jenis dan bentuk pelayanan desa dalam mewadahi aktivitas masyarakat sekaligus sebagai jaringan aspirasi warga terkait hal-hal yang berkaitan dengan kegiatan desa. Sehingga semua fungsi dan bentuk layanan desa dapat berjalan dengan baik dalam lingkungan masyarakat, sebagaimana mestinya.', NULL, NULL, '2021-06-21 21:14:33', '2021-06-21 15:07:49' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_desa_lembaga" -------------------------
BEGIN;

INSERT INTO `sfd_desa_lembaga`(`id_lembaga`,`id_desa`,`kode_wilayah`,`nama_lembaga`,`deskripsi_lembaga`,`created_at`,`updated_at`) VALUES 
( '3', '11', '82.01.09.2001', 'zxczxc', 'ccccccccccccccccccccccccccccc', '2020-11-08 13:30:34', '2020-11-14 16:03:11' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_desa_potensi" -------------------------
BEGIN;

INSERT INTO `sfd_desa_potensi`(`id_potensi`,`id_desa`,`nama_potensi`,`deskripsi_potensi`,`gambar`,`slug_potensi`,`created_at`,`updated_at`) VALUES 
( '1', '11', 'Pertanian Tanaman Pangan', 'Pertanian tanaman pangan memiliki andil besar dalam perekonomian di Kabupaten Wonosobo dengan komoditasnya meliputi tanaman padi dan palawija, tanaman sayuran dan tanaman buah-buahan. Pada tahun 2011, luas panen padi sawah mengalami penurunan sebesar 978 Ha (3,21%) diikuti penurunan produksi padi sawah sebesar 12.115 ton, begitu pula padi gogo mengalami penurunan sebesar 126 ton dari 1.225 ton di tahun 2010 yang disebabkan turunnya luas panen sebesar 335 Ha. Produksi jagung juga turun sebesar 8,99% sementara ubi kayu mengalami kenaikan 7,63%.
<p>&nbsp;</p>

<p>Sedangkan produksi sayuran mengalami peningkatan yang cukup signifikan kecuali kacang panjang, tomat, kobis dan kacang merah. Sementara produksi buah-buahan sebagian besar mengalami penurunan.</p>', 'potensi__11_1_20200723154252.jpeg', 'pertanian-tanaman-pangan', '2020-07-21 15:32:26', '2020-07-23 15:42:52' ),
( '2', '11', 'Perkebunan', 'Tanaman perkebunan yang potensial di Kabupaten Wonosobo adalah kelapa dalam, kelapa deres, kopi dan tembakau.&nbsp;
<p>&nbsp;</p>

<p>Pada tahun 2011 produksi kelapa dalam naik 26,98%, produksi kelapa deres naik 34,24%, produksi tembakau naik 56,69% dan produksi kopi turun sangat drastis yaitu mencapai 99%.</p>', 'potensi__11_2_20200723154239.jpeg', 'perkebunan', '2020-06-29 15:33:45', '2020-07-24 21:27:07' ),
( '3', '11', 'Peternakan', 'Jenis ternak yang banyak diusahakan di Kabupaten Wonosobo tahun 2010 dan populasinya cukup besar adalah sapi potong (27.687 ekor), kambing (136.706 ekor), domba (90.267 ekor) dan ayam buras (665.238 ekor).
<p>&nbsp;</p>

<p>Hasil produksi peternakan tahun 2011 yaitu daging naik menjadi 47.452 kw dari 42.873 kw pada tahun 2010, produksi susu meningkat dari tahun 2010 sebesar 556.316 lt menjadi 559.676 lt pada tahun 2011 dan produksi telor menurun dari 25.321 kw tahun 2010 menjadi 23.758 kw tahun 2011.</p>', 'potensi__11_3_20200723154110.jpeg', 'peternakan', '2020-05-02 15:34:17', '2020-07-24 20:47:17' ),
( '4', '11', 'Perikananan', 'Usaha perikanan di Kabupaten Wonosobo banyak diusahakan di kolam, karamba, waduk, telaga dan sungai dengan produksi pada tahun 2011 mencapai 5.830,24 ton. Di samping itu juga ada usaha pembenihan yang dilakukan di pembenihan rakyat, balai benih ikan maupun pembenihan sawah.', 'potensi__11_4_20200723154034.jpeg', 'perikananan', '2020-04-10 15:34:36', '2020-07-24 20:47:26' ),
( '5', '11', 'Kehutanan', 'Hutan yang ada di Kabupaten Wonosobo terdiri dari hutan rakyat dan hutan negara. Luas hutan rakyat sebesar 18.982 Ha dengan sebagian besar tanaman berupa sengon/albasia, mahoni, suren d an jemitri. Sedangkan hasil dari hutan negara adalah kayu pertukangan, kayu bakar, kopal, getah pinus, gondorukem dan terpenting.', 'potensi__11_5_20200723135659.jpg', 'kehutanan', '2020-03-17 15:35:05', '2020-07-24 20:47:39' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_desa_wilayah" -------------------------
BEGIN;

INSERT INTO `sfd_desa_wilayah`(`id_wilayah`,`id_desa`,`luas_wilayah_tanah`,`luas_daratan`,`luas_persawahan`,`luas_tegalan`,`luas_lain_lain`,`luas_tanah_desa`,`jumlah_dusun`,`jumlah_rt`,`jumlah_rw`,`created_at`,`updated_at`) VALUES 
( '1', '11', '120.00', '123.00', '127.00', '123.00', '123.00', '123.00', '123', '123', '123', '2020-02-11 15:41:22', '2020-02-11 15:54:11' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_detail_realisasi_belanja" -------------
-- ---------------------------------------------------------


-- Dump data of "sfd_detail_rencana_anggaran_biaya" --------
-- ---------------------------------------------------------


-- Dump data of "sfd_geografi" -----------------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_keadaan_statistik" --------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_lok_kabupaten" ------------------------
BEGIN;

INSERT INTO `sfd_lok_kabupaten`(`id_kabupaten`,`provinsi_id`,`nama`) VALUES 
( '8201', '82', 'Kabupaten Halmahera Barat' ),
( '8202', '82', 'Kabupaten Halmahera Tengah' ),
( '8203', '82', 'Kabupaten Kepulauan Sula' ),
( '8204', '82', 'Kabupaten Halmahera Selatan' ),
( '8205', '82', 'Kabupaten Halmahera Utara' ),
( '8206', '82', 'Kabupaten Halmahera Timur' ),
( '8207', '82', 'Kabupaten Pulau Morotai' ),
( '8208', '82', 'Kabupaten Pulau Taliabu' ),
( '8271', '82', 'Kota Ternate' ),
( '8272', '82', 'Kota Tidore Kepulauan' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_master_anggaran_belanja" --------------
-- ---------------------------------------------------------


-- Dump data of "sfd_master_desa" --------------------------
BEGIN;

INSERT INTO `sfd_master_desa`(`desa_id`,`id_kecamatan`,`nama_desa`,`created_at`,`updated_at`) VALUES 
( '1', '5476', 'Ake Boso', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '2', '5476', 'Akesibu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '3', '5476', 'Gam Ici', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '4', '5476', 'Gam Lamo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '5', '5476', 'Kampung Baru', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '6', '5476', 'Kie Lei (Kie Ici)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '7', '5476', 'Maritango', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '8', '5476', 'Naga', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '9', '5476', 'Soana Masungi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '10', '5476', 'Tahafo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '11', '5476', 'Tobaol', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '12', '5476', 'Togola Sanger/Sangir', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '13', '5476', 'Togola Wayolo/Wayoli', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '14', '5476', 'Tongute Goin', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '15', '5476', 'Tongute Sungi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '16', '5476', 'Tongute Ternate', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '17', '5476', 'Tongute Ternate Selatan', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '18', '5477', 'Adu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '19', '5477', 'Baru', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '20', '5477', 'Bataka', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '21', '5477', 'Gamkonora', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '22', '5477', 'Gamsida', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '23', '5477', 'Gamsungi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '24', '5477', 'Jere', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '25', '5477', 'Nanas', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '26', '5477', 'Ngalo Ngalo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '27', '5477', 'Ngawet', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '28', '5477', 'Sarau', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '29', '5477', 'Talaga', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '30', '5477', 'Tobelos', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '31', '5477', 'Tobobol (Tabobol)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '32', '5477', 'Tosoa', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '33', '5477', 'Tuguaer', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '34', '5478', 'Aru Jaya', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '35', '5478', 'Borona', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '36', '5478', 'Duono', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '37', '5478', 'Goin', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '38', '5478', 'Pasalulu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '39', '5478', 'Podol', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '40', '5478', 'Sangaji Nyeku', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '41', '5478', 'Soasangaji', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '42', '5478', 'Tengowango (Teongowango)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '43', '5478', 'Todoke', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '44', '5478', 'Togoreba Sungi (Tugureba Sungi)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '45', '5478', 'Togoreba Tua', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '46', '5478', 'Togowo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '47', '5478', 'Tolisaor', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '48', '5478', 'Tuguis', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '49', '5478', 'Tukuoku', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '50', '5479', 'Acango', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '51', '5479', 'Akediri', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '52', '5479', 'Bobanehena', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '53', '5479', 'Bobo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '54', '5479', 'Bobo Jiko', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '55', '5479', 'Buku Bualawa', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '56', '5479', 'Buku Maadu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '57', '5479', 'Bukumatiti', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '58', '5479', 'Galala', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '59', '5479', 'Gamlamo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '60', '5479', 'Gamtala', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '61', '5479', 'Guaemaadu (Guaimaadu)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '62', '5479', 'Guaeria', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '63', '5479', 'Gufasa', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '64', '5479', 'Hate Bicara', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '65', '5479', 'Hoku Hoku Kie (Huku-Huku Kie)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '66', '5479', 'Idamdehe', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '67', '5479', 'Idamdehe Gamsungi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '68', '5479', 'Jalan Baru', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '69', '5479', 'Kuripasai', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '70', '5479', 'Loloy (Lolori)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '71', '5479', 'Marmabati (Marimbati)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '72', '5479', 'Matui', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '73', '5479', 'Pateng', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '74', '5479', 'Payo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '75', '5479', 'Pornity (Porniti)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '76', '5479', 'Saria', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '77', '5479', 'Soakonora', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '78', '5479', 'Taboso (Tobosa/Tabaso)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '79', '5479', 'Tauro', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '80', '5479', 'Tedeng', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '81', '5479', 'Todowangi (Todowongi)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '82', '5479', 'Tuada', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '83', '5479', 'Ulo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '84', '5480', 'Ake Jailolo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '85', '5480', 'Akeara', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '86', '5480', 'Akelaha', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '87', '5480', 'Bangkit Rahmat', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '88', '5480', 'Biamaahi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '89', '5480', 'Bobane Dano', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '90', '5480', 'Braha', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '91', '5480', 'Dodinga', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '92', '5480', 'Domato', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '93', '5480', 'Gamlenge', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '94', '5480', 'Hijrah', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '95', '5480', 'Moiso', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '96', '5480', 'Ratem', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '97', '5480', 'Rioribati', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '98', '5480', 'Sidangoli Dehe', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '99', '5480', 'Sidangoli Gam', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '100', '5480', 'Suka Damai', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '101', '5480', 'Taba Damai (Db)', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '102', '5480', 'Tataleka', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '103', '5480', 'Tewe', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '104', '5480', 'Toniku', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '105', '5480', 'Tuguraci', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '106', '5481', 'Aruku', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '107', '5481', 'Baja', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '108', '5481', 'Bakun', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '109', '5481', 'Bakun Pantai', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '110', '5481', 'Bantol', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '111', '5481', 'Barataku', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '112', '5481', 'Bilote', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '113', '5481', 'Bosala', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '114', '5481', 'Buo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '115', '5481', 'Gamkahe', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '116', '5481', 'Jangailulu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '117', '5481', 'Jano', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '118', '5481', 'Kahatola', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '119', '5481', 'Kedi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '120', '5481', 'Laba Besar', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '121', '5481', 'Laba Kecil', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '122', '5481', 'Linggua', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '123', '5481', 'Pumadada', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '124', '5481', 'Salu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '125', '5481', 'Soa-Sio', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '126', '5481', 'Tasye', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '127', '5481', 'Tolofuo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '128', '5481', 'Tomodo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '129', '5481', 'Tosomolo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '130', '5481', 'Totala', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '131', '5481', 'Totala Jaya', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '132', '5481', 'Tuakara', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '133', '5481', 'Tuguis', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '134', '5482', 'Balisoan', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '135', '5482', 'Balisoan Utara', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '136', '5482', 'Dere', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '137', '5482', 'Golo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '138', '5482', 'Goro Goro', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '139', '5482', 'Jaraoke', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '140', '5482', 'Lako Akediri', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '141', '5482', 'Lako Akelamo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '142', '5482', 'Peot', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '143', '5482', 'Ropu Tengah Hulu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '144', '5482', 'Sasur', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '145', '5482', 'Sasur Pantai', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '146', '5482', 'Susupu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '147', '5482', 'Tacici', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '148', '5482', 'Tacim', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '149', '5482', 'Taraudu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '150', '5482', 'Taruba', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '151', '5482', 'Todahe', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '152', '5482', 'Worat Worat', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '153', '5483', 'Air Panas', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '154', '5483', 'Akelamo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '155', '5483', 'Aketola', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '156', '5483', 'Awer', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '157', '5483', 'Campaka', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '158', '5483', 'Gamnyial', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '159', '5483', 'Gamomeng', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '160', '5483', 'Gamsungi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '161', '5483', 'Goal', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '162', '5483', 'Golago Kusuma', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '163', '5483', 'Hoku Hoku Gam', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '164', '5483', 'Idamgamlamo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '165', '5483', 'Loce', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '166', '5483', 'Ngaon', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '167', '5483', 'Sidodadi', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '168', '5483', 'Taba Campaka', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '169', '5483', 'Taraudu Kusu', '2020-02-21 23:24:25', '2020-02-21 23:24:25' ),
( '170', '5483', 'Tibobo', '2020-02-21 23:24:25', '2020-02-21 23:24:25' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_master_kecamatan" ---------------------
BEGIN;

INSERT INTO `sfd_master_kecamatan`(`id_kecamatan`,`nama_kecamatan`,`created_at`,`updated_at`) VALUES 
( '5476', 'Kecamatan Ibu', '2020-02-04 23:35:26', '2020-02-21 23:18:42' ),
( '5477', 'Kecamatan Ibu Selatan', '2020-02-04 23:35:26', '2020-02-21 23:18:48' ),
( '5478', 'Kecamatan Ibu Utara', '2020-02-04 23:35:26', '2020-02-21 23:18:55' ),
( '5479', 'Kecamatan Jailolo', '2020-02-04 23:35:26', '2020-02-21 23:19:09' ),
( '5480', 'Kecamatan Jailolo Selatan', '2020-02-04 23:35:26', '2020-02-21 23:19:16' ),
( '5481', 'Kecamatan Loloda', '2020-02-04 23:35:26', '2020-02-21 23:19:35' ),
( '5482', 'Kecamatan Sahu', '2020-02-04 23:35:26', '2020-02-21 23:19:25' ),
( '5483', 'Kecamatan Sahu Timur', '2020-02-04 23:35:26', '2020-02-21 23:19:30' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_master_penerimaan" --------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_pemerintahaan" ------------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_profil_kabupaten" ---------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_realisasi_anggaran_belanja" -----------
-- ---------------------------------------------------------


-- Dump data of "sfd_realisasi_penerimaan_anggaran" --------
-- ---------------------------------------------------------


-- Dump data of "sfd_rencana_anggaran_belanja" -------------
-- ---------------------------------------------------------


-- Dump data of "sfd_rencana_penerimaan_anggaran" ----------
-- ---------------------------------------------------------


-- Dump data of "sfd_roles" --------------------------------
BEGIN;

INSERT INTO `sfd_roles`(`id`,`role_name`,`created_at`,`updated_at`) VALUES 
( '1', 'Superadmin', NULL, NULL ),
( '2', 'Administrator', NULL, NULL ),
( '3', 'Staff', NULL, NULL ),
( '4', 'Tamu', NULL, NULL );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_sejarah_desa" -------------------------
BEGIN;

INSERT INTO `sfd_sejarah_desa`(`id_sejarah_desa`,`id_desa`,`sejarah`,`created_at`,`updated_at`) VALUES 
( '1', '11', '<strong>Sahu Timur</strong> adalah sebuah kecamatan di Kabupaten Halmahera Barat, Maluku Utara, Indonesia.', '2020-02-09 15:38:22', '2020-07-14 15:51:20' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_sejarah_kabupaten" --------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_statistik_desa" -----------------------
BEGIN;

INSERT INTO `sfd_statistik_desa`(`id_statistik_desa`,`id_desa`,`jumlah_kk`,`jumlah_penduduk`,`jumlah_laki_laki`,`jumlah_perempuan`,`balita`,`anak`,`remaja`,`dewasa`,`orang_tua`,`tgl_statistik`,`created_at`,`updated_at`) VALUES 
( '2', '11', '123', '3', '3', '3', '3', '3', '3', '3', '3', '2020-11-14', '2020-11-14 15:58:45', '2020-11-14 16:02:29' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_tahun_anggaran" -----------------------
-- ---------------------------------------------------------


-- Dump data of "sfd_users" --------------------------------
BEGIN;

INSERT INTO `sfd_users`(`id_user`,`user_name`,`fullname`,`email`,`email_verified_at`,`password`,`role_id`,`status`,`remember_token`,`id_desa`,`created_at`,`updated_at`) VALUES 
( '1', 'administrator', 'Administrator', 'admin@mail.com', NULL, '$2y$10$x0NQnJqJL83Sc.6SP8PfauhBdhpq1YMyRoBfQZhOTRiR3Q7GAwaPO', '0', '1', 'JmAOQCjESrlgMNgPMTazkDxF4dA2USl8J6qKBrhHeS7auekbYd0uLG5Sfezo', '0', NULL, '2021-03-25 09:32:48' ),
( '2', 'desaloce', 'Desa Loce', 'desaloce@mail.com', NULL, '$2y$10$RYkZLd4zjW1khLaR.1IK6uH0ReOkGawgQVaNbaA1.OGfutPLSTvPW', '2', '1', '3EIbsk4Ba86zmms78OxVjdQIFTqNxxfqJuutcOnGZPA06fmKgU0Ux0P5gews', '11', NULL, '2021-06-17 14:00:42' ),
( '4', 'desaairpanas@mail.com', NULL, 'desaairpanas@mail.com', NULL, '$2y$10$IOdEaX1MoUAejHMTO.5wxuGRUvO8I.qHTiwaQQUHETmNKNyXkqCPm', '2', '1', NULL, '26', '2020-08-23 17:05:27', '2020-08-23 17:05:27' );
COMMIT;
-- ---------------------------------------------------------


-- Dump data of "sfd_visi_misi" ----------------------------
-- ---------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_berita`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_kategori" ----------------------------------
CREATE INDEX `id_kategori` USING BTREE ON `sfd_berita`( `id_kategori_berita` );
-- -------------------------------------------------------------


-- CREATE INDEX "lnk_sfd_desa_sfd_berita_banner" ---------------
CREATE INDEX `lnk_sfd_desa_sfd_berita_banner` USING BTREE ON `sfd_berita_banner`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "lnk_sfd_desa_sfd_berita_kategori" -------------
CREATE INDEX `lnk_sfd_desa_sfd_berita_kategori` USING BTREE ON `sfd_berita_kategori`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "lnk_sfd_berita_kategori_sfd_berita_kategori_sub" 
CREATE INDEX `lnk_sfd_berita_kategori_sfd_berita_kategori_sub` USING BTREE ON `sfd_berita_kategori_sub`( `id_kategori_berita` );
-- -------------------------------------------------------------


-- CREATE INDEX "lnk_sfd_desa_sfd_berita_kategori_sub" ---------
CREATE INDEX `lnk_sfd_desa_sfd_berita_kategori_sub` USING BTREE ON `sfd_berita_kategori_sub`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_berita_tags`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_kecamatan" ---------------------------------
CREATE INDEX `id_kecamatan` USING BTREE ON `sfd_desa`( `id_kecamatan` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_kepala_desa" -------------------------------
CREATE INDEX `id_kepala_desa` USING BTREE ON `sfd_desa`( `kepala_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "kode_wilayah" ---------------------------------
CREATE INDEX `kode_wilayah` USING BTREE ON `sfd_desa`( `kode_wilayah` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_desa_dokumen`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_desa_layanan`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "index_id_desa" --------------------------------
CREATE INDEX `index_id_desa` USING BTREE ON `sfd_desa_lembaga`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_desa_potensi`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_desa_wilayah`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_realisasi_belanja" -------------------------
CREATE INDEX `id_realisasi_belanja` USING BTREE ON `sfd_detail_realisasi_belanja`( `id_realisasi_belanja` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_rencana_belanja" ---------------------------
CREATE INDEX `id_rencana_belanja` USING BTREE ON `sfd_detail_realisasi_belanja`( `id_rencana_belanja` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_rencana_belaja" ----------------------------
CREATE INDEX `id_rencana_belaja` USING BTREE ON `sfd_detail_rencana_anggaran_biaya`( `id_rencana_belaja` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_geografi`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_kecamatan" ---------------------------------
CREATE INDEX `id_kecamatan` USING BTREE ON `sfd_master_desa`( `id_kecamatan` );
-- -------------------------------------------------------------


-- CREATE INDEX "kode_wilayah" ---------------------------------
CREATE INDEX `kode_wilayah` USING BTREE ON `sfd_profil_kabupaten`( `kode_wilayah` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_renacana_belanja" --------------------------
CREATE INDEX `id_renacana_belanja` USING BTREE ON `sfd_realisasi_anggaran_belanja`( `id_renacana_belanja` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_renacan_penerimaan" ------------------------
CREATE INDEX `id_renacan_penerimaan` USING BTREE ON `sfd_realisasi_penerimaan_anggaran`( `id_renacan_penerimaan` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_rencana_anggaran_belanja`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_rencana_penerimaan_anggaran`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_penerimaan" --------------------------------
CREATE INDEX `id_penerimaan` USING BTREE ON `sfd_rencana_penerimaan_anggaran`( `id_penerimaan` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_sejarah_desa`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_statistik_desa`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE INDEX "id_desa" --------------------------------------
CREATE INDEX `id_desa` USING BTREE ON `sfd_users`( `id_desa` );
-- -------------------------------------------------------------


-- CREATE LINK "lnk_sfd_berita_sfd_berita_kategori" ------------
ALTER TABLE `sfd_berita`
	ADD CONSTRAINT `lnk_sfd_berita_sfd_berita_kategori` FOREIGN KEY ( `id_kategori_berita` )
	REFERENCES `sfd_berita_kategori`( `id_kategori_berita` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "lnk_sfd_desa_sfd_berita" -----------------------
ALTER TABLE `sfd_berita`
	ADD CONSTRAINT `lnk_sfd_desa_sfd_berita` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "lnk_sfd_desa_sfd_berita_banner" ----------------
ALTER TABLE `sfd_berita_banner`
	ADD CONSTRAINT `lnk_sfd_desa_sfd_berita_banner` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "fk_sfd_desa_sfd_berita_kategori" ---------------
ALTER TABLE `sfd_berita_kategori`
	ADD CONSTRAINT `fk_sfd_desa_sfd_berita_kategori` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "fk_sfd_berita_kategori_sfd_berita_kategori_sub" 
ALTER TABLE `sfd_berita_kategori_sub`
	ADD CONSTRAINT `fk_sfd_berita_kategori_sfd_berita_kategori_sub` FOREIGN KEY ( `id_kategori_berita` )
	REFERENCES `sfd_berita_kategori`( `id_kategori_berita` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "fk_sfd_desa_sfd_berita_kategori_sub" -----------
ALTER TABLE `sfd_berita_kategori_sub`
	ADD CONSTRAINT `fk_sfd_desa_sfd_berita_kategori_sub` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "fk_sfd_berita_tags_sfd_desa" -------------------
ALTER TABLE `sfd_berita_tags`
	ADD CONSTRAINT `fk_sfd_berita_tags_sfd_desa` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "lnk_sfd_desa_kecamatan" ------------------------
ALTER TABLE `sfd_desa`
	ADD CONSTRAINT `lnk_sfd_desa_kecamatan` FOREIGN KEY ( `id_kecamatan` )
	REFERENCES `sfd_master_kecamatan`( `id_kecamatan` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "fk_sfd_dokumen_desa_sfd_desa" ------------------
ALTER TABLE `sfd_desa_dokumen`
	ADD CONSTRAINT `fk_sfd_dokumen_desa_sfd_desa` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "FK_sfd_layanan_desa_sfd_desa" ------------------
ALTER TABLE `sfd_desa_layanan`
	ADD CONSTRAINT `FK_sfd_layanan_desa_sfd_desa` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "FK_sfd_lembaga_desa_sfd_desa" ------------------
ALTER TABLE `sfd_desa_lembaga`
	ADD CONSTRAINT `FK_sfd_lembaga_desa_sfd_desa` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "potensi_desa_ibfk_1" ---------------------------
ALTER TABLE `sfd_desa_potensi`
	ADD CONSTRAINT `potensi_desa_ibfk_1` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "fk_sfd_desa_wilayah_sfd_desa" ------------------
ALTER TABLE `sfd_desa_wilayah`
	ADD CONSTRAINT `fk_sfd_desa_wilayah_sfd_desa` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE No Action
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "detail_realisasi_belanja_ibfk_1" ---------------
ALTER TABLE `sfd_detail_realisasi_belanja`
	ADD CONSTRAINT `detail_realisasi_belanja_ibfk_1` FOREIGN KEY ( `id_realisasi_belanja` )
	REFERENCES `sfd_realisasi_anggaran_belanja`( `id_realisasi_belaja` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "detail_realisasi_belanja_ibfk_2" ---------------
ALTER TABLE `sfd_detail_realisasi_belanja`
	ADD CONSTRAINT `detail_realisasi_belanja_ibfk_2` FOREIGN KEY ( `id_rencana_belanja` )
	REFERENCES `sfd_detail_rencana_anggaran_biaya`( `id_rencana_belaja` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "detail_rencana_anggaran_biaya_ibfk_1" ----------
ALTER TABLE `sfd_detail_rencana_anggaran_biaya`
	ADD CONSTRAINT `detail_rencana_anggaran_biaya_ibfk_1` FOREIGN KEY ( `id_rencana_belaja` )
	REFERENCES `sfd_rencana_anggaran_belanja`( `id_rencana_belanja` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "sfd_masterdesa_ibfk_1" -------------------------
ALTER TABLE `sfd_master_desa`
	ADD CONSTRAINT `sfd_masterdesa_ibfk_1` FOREIGN KEY ( `id_kecamatan` )
	REFERENCES `sfd_master_kecamatan`( `id_kecamatan` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "realisasi_anggaran_belanja_ibfk_1" -------------
ALTER TABLE `sfd_realisasi_anggaran_belanja`
	ADD CONSTRAINT `realisasi_anggaran_belanja_ibfk_1` FOREIGN KEY ( `id_renacana_belanja` )
	REFERENCES `sfd_rencana_anggaran_belanja`( `id_rencana_belanja` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "realisasi_penerimaan_anggaran_ibfk_1" ----------
ALTER TABLE `sfd_realisasi_penerimaan_anggaran`
	ADD CONSTRAINT `realisasi_penerimaan_anggaran_ibfk_1` FOREIGN KEY ( `id_renacan_penerimaan` )
	REFERENCES `sfd_rencana_penerimaan_anggaran`( `id_renacan_penerimaan` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "rencana_anggaran_belanja_ibfk_1" ---------------
ALTER TABLE `sfd_rencana_anggaran_belanja`
	ADD CONSTRAINT `rencana_anggaran_belanja_ibfk_1` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "rencana_penerimaan_anggaran_ibfk_1" ------------
ALTER TABLE `sfd_rencana_penerimaan_anggaran`
	ADD CONSTRAINT `rencana_penerimaan_anggaran_ibfk_1` FOREIGN KEY ( `id_penerimaan` )
	REFERENCES `sfd_master_penerimaan`( `id_penerimaan` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "rencana_penerimaan_anggaran_ibfk_2" ------------
ALTER TABLE `sfd_rencana_penerimaan_anggaran`
	ADD CONSTRAINT `rencana_penerimaan_anggaran_ibfk_2` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "sejarah_desa_ibfk_1" ---------------------------
ALTER TABLE `sfd_sejarah_desa`
	ADD CONSTRAINT `sejarah_desa_ibfk_1` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "statistik_desa_ibfk_1" -------------------------
ALTER TABLE `sfd_statistik_desa`
	ADD CONSTRAINT `statistik_desa_ibfk_1` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------


-- CREATE LINK "FK_sfd_users_sfd_desa" -------------------------
ALTER TABLE `sfd_users`
	ADD CONSTRAINT `FK_sfd_users_sfd_desa` FOREIGN KEY ( `id_desa` )
	REFERENCES `sfd_desa`( `id_desa` )
	ON DELETE Cascade
	ON UPDATE Cascade;
-- -------------------------------------------------------------



delimiter $$$ 
-- CREATE FUNCTION "FListBerita" -------------------------------
CREATE DEFINER=`root`@`%` PROCEDURE `FListBerita`(PID VARCHAR(100))
BEGIN
	SELECT * from sfd_users 
	LEFT JOIN sfd_desa ON
		sfd_users.id_desa = sfd_desa.id_desa
	LEFT JOIN sfd_berita ON
		sfd_berita.id_desa = sfd_desa.id_desa
	WHERE sfd_desa.website = PID;
END;
-- -------------------------------------------------------------

$$$ delimiter ;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


