USE db_sisfodes2;

CREATE TABLE `sfd_bidang` (
  `id_bidang` INT(11) NOT NULL AUTO_INCREMENT,
  `id_desa` INT(11) DEFAULT NULL,
  `kode_bidang` VARCHAR(10) DEFAULT NULL,
  `nama_bidang` VARCHAR(150) DEFAULT NULL,
  PRIMARY KEY (`id_bidang`)
) ENGINE=INNODB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1

CREATE TABLE `sfd_sub_bidang` (
  `id_sub_bidang` INT(11) NOT NULL AUTO_INCREMENT,
  `id_bidang` INT(11) DEFAULT NULL,
  `kode_sub_bidang` VARCHAR(10) DEFAULT NULL,
  `nama_sub_bidang` VARCHAR(150) DEFAULT NULL,
  PRIMARY KEY (`id_sub_bidang`),
  KEY `id_bidang` (`id_bidang`),
  CONSTRAINT `sfd_sub_bidang_ibfk_1` FOREIGN KEY (`id_bidang`) REFERENCES `sfd_bidang` (`id_bidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1


CREATE TABLE `sfd_kegiatan` (
  `id_kegiatan` INT(11) NOT NULL AUTO_INCREMENT,
  `id_sub_bidang` INT(11) DEFAULT NULL,
  `kode_kegiatan` VARCHAR(10) DEFAULT NULL,
  `nama_kegiatan` VARCHAR(150) DEFAULT NULL,
  PRIMARY KEY (`id_kegiatan`),
  KEY `id_sub_bidang` (`id_sub_bidang`),
  KEY `kode_kegiatan` (`kode_kegiatan`),
  CONSTRAINT `sfd_kegiatan_ibfk_1` FOREIGN KEY (`id_sub_bidang`) REFERENCES `sfd_sub_bidang` (`id_sub_bidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=latin1