@extends('frontend.layouts.app')
@section('page_title')
Potensi Desa
@endsection
@section('content')
<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md ">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <div class="overflow-hidden pb-2">
                            <h1 class="text-dark font-weight-bold text-9 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="100">Produk & Potensi Desa</h1>
                            <span class="sub-title text-dark" data-appear-animation="maskUp" data-appear-animation-delay="100">Periksa potensi yang ada di Desa kami!</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<div class="container py-2">
    <ul class="nav nav-pills sort-source sort-source-style-3 justify-content-center" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'fitRows', 'filter': '*'}">
        <li class="nav-item active" data-option-value="*"><a class="nav-link text-1 text-uppercase active" href="#">Semua</a></li>
        <li class="nav-item" data-option-value=".pertanian"><a class="nav-link text-1 text-uppercase" href="#">Pertanian</a></li>
        <li class="nav-item" data-option-value=".perikanan"><a class="nav-link text-1 text-uppercase" href="#">Perikanan</a></li>
        <li class="nav-item" data-option-value=".perkebunan"><a class="nav-link text-1 text-uppercase" href="#">Perkebunan</a></li>
        <li class="nav-item" data-option-value=".wisata"><a class="nav-link text-1 text-uppercase" href="#">Wisata</a></li>
    </ul>

    <div class="sort-destination-loader sort-destination-loader-showing mt-3">
        <div class="row portfolio-list sort-destination" data-sort-id="portfolio">
            

            <div class="col-lg-12 isotope-item mt-4 perkebunan">
                <div class="row">
                    
                    <div class="col-lg-6">
                        <div class="portfolio-item">
                            <a href="#">
                                <span class="thumb-info thumb-info-no-zoom thumb-info-lighten border-radius-0 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="100">
                                    <span class="thumb-info-wrapper border-radius-0">
                                        <img src="{{asset('img/office/our-office-1.jpg')}}" class="img-fluid border-radius-0" alt="">

                                        <span class="thumb-info-action">
                                            <span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">

                        <div class="overflow-hidden">
                            <h2 class="text-color-dark font-weight-bold text-5 mb-2 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="600">Presentation</h2>
                        </div>

                        <p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti.</p>

                        <ul class="list list-icons list-primary list-borders text-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1200">
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Client:</strong> Okler Themes</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Date:</strong> Nov 2018</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Skills:</strong> <a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">DESIGN</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">BRAND</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">WEBSITES</a></li>
                        </ul>

                    </div>
                    
                </div>
            </div>

            <div class="col-lg-12 isotope-item mt-4 wisata">
                <div class="row">
                    
                    <div class="col-lg-6">
                        <div class="portfolio-item">
                            <a href="#">
                                <span class="thumb-info thumb-info-no-zoom thumb-info-lighten border-radius-0 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="100">
                                    <span class="thumb-info-wrapper border-radius-0">
                                        <span class="owl-carousel owl-theme dots-inside m-0" data-plugin-options="{'items': 1, 'margin': 20, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 3000}">
                                            <span>
                                                <img src="{{asset('img/office/our-office-1.jpg')}}" class="img-fluid border-radius-0" alt="">
                                            </span>
                                            <span>
                                                <img src="{{asset('img/office/our-office-1.jpg')}}" class="img-fluid border-radius-0" alt="">
                                            </span>
                                        </span>

                                        <span class="thumb-info-action">
                                            <span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">

                        <div class="overflow-hidden">
                            <h2 class="text-color-dark font-weight-bold text-5 mb-2 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="600">Porto Watch</h2>
                        </div>

                        <p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti.</p>

                        <ul class="list list-icons list-primary list-borders text-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1200">
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Client:</strong> Okler Themes</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Date:</strong> Jan 2018</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Skills:</strong> <a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">DESIGN</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">BRAND</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">WEBSITES</a></li>
                        </ul>

                    </div>
                    
                </div>
            </div>

            <div class="col-lg-12 isotope-item mt-4 perikanan">
                <div class="row">
                    
                    <div class="col-lg-6">
                        <div class="portfolio-item">
                            <a href="#">
                                <span class="thumb-info thumb-info-no-zoom thumb-info-lighten border-radius-0 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="100">
                                    <span class="thumb-info-wrapper border-radius-0">
                                        <img src="{{asset('img/office/our-office-1.jpg')}}" class="img-fluid border-radius-0" alt="">

                                        <span class="thumb-info-action">
                                            <span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">

                        <div class="overflow-hidden">
                            <h2 class="text-color-dark font-weight-bold text-5 mb-2 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="600">Identity</h2>
                        </div>

                        <p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti.</p>

                        <ul class="list list-icons list-primary list-borders text-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1200">
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Client:</strong> Okler Themes</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Date:</strong> July 2018</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Skills:</strong> <a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">DESIGN</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">BRAND</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">WEBSITES</a></li>
                        </ul>

                    </div>
                    
                </div>
            </div>

            <div class="col-lg-12 isotope-item mt-4 pertanian">
                <div class="row">
                    
                    <div class="col-lg-6">
                        <div class="portfolio-item">
                            <a href="#">
                                <span class="thumb-info thumb-info-no-zoom thumb-info-lighten border-radius-0 appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="100">
                                    <span class="thumb-info-wrapper border-radius-0">
                                        <img src="{{asset('img/office/our-office-1.jpg')}}" class="img-fluid border-radius-0" alt="">

                                        <span class="thumb-info-action">
                                            <span class="thumb-info-action-icon bg-dark opacity-8"><i class="fas fa-plus"></i></span>
                                        </span>
                                    </span>
                                </span>
                            </a>
                        </div>
                    </div>
                    
                    <div class="col-lg-6">

                        <div class="overflow-hidden">
                            <h2 class="text-color-dark font-weight-bold text-5 mb-2 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="600">Porto Screens</h2>
                        </div>

                        <p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="800">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed tempus nibh sed elimttis adipiscing. Fusce in hendrerit purus. Suspendisse potenti.</p>

                        <ul class="list list-icons list-primary list-borders text-2 appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1200">
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Client:</strong> Okler Themes</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Date:</strong> February 2018</li>
                            <li><i class="fas fa-caret-right left-10"></i> <strong class="text-color-primary">Skills:</strong> <a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">DESIGN</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">BRAND</a><a href="#" class="badge badge-dark badge-sm badge-pill px-2 py-1 ml-1">WEBSITES</a></li>
                        </ul>

                    </div>
                    
                </div>
            </div>
            
        </div>
    </div>
</div>
@endsection
@section('css')

@endsection
@section('javascript')
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-potensi').addClass('active')
    });
</script>
@endsection
