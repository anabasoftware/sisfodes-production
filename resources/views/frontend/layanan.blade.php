@extends('frontend.layouts.app')
@section('page_title')
Layanan Desa
@endsection
@section('content')

<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md ">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <div class="overflow-hidden pb-2">
                            <h1 class="text-dark font-weight-bold text-9 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="100">Layanan</h1>
                            <span class="sub-title text-dark" data-appear-animation="maskUp" data-appear-animation-delay="100">Periksa layanan yang ada di Desa kami!</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<div class="container py-4">
    <div class="row">
        <div class="col">
            <div class="blog-posts">
                {!! \Helpers::services (\Request::getHttpHost()) !!}
                {{-- <div class="row">
                    <div class="col">
                        <ul class="pagination float-right">
                            <li class="page-item"><a class="page-link" href="#"><i class="fas fa-angle-left"></i></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <a class="page-link" href="#"><i class="fas fa-angle-right"></i></a>
                        </ul>
                    </div>
                </div> --}}

            </div>
        </div>
    </div>
</div>

@endsection
@section('css')

@endsection
@section('javascript')
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-layanan').addClass('active')
    });
</script>
@endsection
