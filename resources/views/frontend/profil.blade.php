@extends('frontend.layouts.app')
@section('page_title')
Profil
@endsection
@section('content')
<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md ">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <div class="overflow-hidden pb-2">
                            <h1 class="text-dark font-weight-bold text-9 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="100">Profil Desa</h2>
                            <span class="sub-title text-dark" data-appear-animation="maskUp" data-appear-animation-delay="100">Tentang kami!</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<div id="googlemaps" class="google-map" style="height: 500px;"></div>
<div class="container py-4">
    <div class="row align-items-center">
        <div class="col-md-6 appear-animation" data-appear-animation="fadeInLeftShorter" data-appear-animation-delay="1000">
            <div class="owl-carousel owl-theme nav-inside mb-0" data-plugin-options="{'items': 1, 'margin': 10, 'animateOut': 'fadeOut', 'autoplay': true, 'autoplayTimeout': 6000, 'loop': true}">
                <div>
                    <img alt="" class="img-fluid" src="{{asset('img/office/our-office-1.jpg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid" src="{{asset('img/office/our-office-1.jpg')}}">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="overflow-hidden mb-2">
                <h2 class="text-color-dark font-weight-normal text-5 mb-0 pt-0 mt-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="1200">
                    Desa <strong class="font-weight-extra-bold">
                    {!! \App\Models\Desa::where('website', \Request::getHttpHost())->first()->nama_desa !!}
                    </strong>
                </h2>
            </div>
            <p class="appear-animation" data-appear-animation="fadeInUpShorter" data-appear-animation-delay="1400">
                {!! \App\Models\Desa::where('website', \Request::getHttpHost())->first()->get_sejarah->sejarah !!}
            </p>
        </div>
    </div>
</div>
<div class="container">

    <div class="row mt-5 py-3">
        <div class="col-md-6">
            <div class="toggle toggle-primary toggle-simple m-0" data-plugin-toggle>
                <section class="toggle active mt-0">
                    <label>Visi</label>
                    <div class="toggle-content">
                        {!! \App\Models\Desa::where('website', \Request::getHttpHost())->first()->visi !!}
                    </div>
                </section>
                <section class="toggle active">
                    <label>Misi</label>
                    <div class="toggle-content">
                        {!! \App\Models\Desa::where('website', \Request::getHttpHost())->first()->misi !!}
                    </div>
                </section>
            </div>
            <hr/>
            <div class="overflow-hidden mb-1">
                <h2 class="font-weight-normal text-7 mt-2 mb-0">
                    <strong class="font-weight-extra-bold">Contact</strong> Us</h2>
            </div>
            <div class="overflow-hidden mb-4 pb-3">
                <p class="mb-0">Feel free to ask for details, don't save any questions!</p>
            </div>

            <form id="contactForm" class="contact-form" action="" method="POST">
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label class="required font-weight-bold text-dark text-2">Full Name</label>
                        <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="required font-weight-bold text-dark text-2">Email Address</label>
                        <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label class="font-weight-bold text-dark text-2">Subject</label>
                        <input type="text" value="" data-msg-required="Please enter the subject." maxlength="100" class="form-control" name="subject" id="subject" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label class="required font-weight-bold text-dark text-2">Message</label>
                        <textarea maxlength="5000" data-msg-required="Please enter your message." rows="5" class="form-control" name="message" id="message" required></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <input type="submit" value="Send Message" class="btn btn-primary btn-modern" data-loading-text="Loading...">
                    </div>
                </div>
            </form>

        </div>
        <div class="col-md-6">
            <div class="progress-bars">
                <div class="progress-group">
                    Jumlah Rencana Penerimaan TA 2019
                    <div class="clearfix">
                        <span class="pull-left"><b>Rp. 8,617,690</b></span>
                        <small class="pull-right"><b>10.09%&nbsp;</b></small>
                    </div>
                        <div class="progress" align="right">
                        <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width: 10.09%"></div>
                    </div>
                </div>
                <hr/>
                <div class="progress-group">
                    Alokasi Dana Desa (ADD)<br>
                    <div class="clearfix">
                        <span class="pull-left"><b>Rp. 20,617,690</b></span>
                        <small class="pull-right"><b>7.09%&nbsp;</b></small>
                    </div>
                        <div class="progress sm" align="right">
                        <div class="progress-bar progress-bar-danger progress-bar-striped" role="progressbar" style="width: 7.09%"></div>
                    </div>
                </div>
                <hr/>
                <div class="progress-group">
                    Dana Desa (DD)<br>
                    <div class="clearfix">
                        <span class="pull-left"><b>Rp. 10,617,690</b></span>
                        <small class="pull-right"><b>30.09%&nbsp;</b></small>
                    </div>
                        <div class="progress sm" align="right">
                        <div class="progress-bar progress-bar-warning progress-bar-striped" role="progressbar" style="width: 30.09%"></div>
                    </div>
                </div>
                <hr/>
                <div class="progress-group">
                    Bagi Hasil Pajak & Retribusi Daerah (PBH)<br>
                    <div class="clearfix">
                        <span class="pull-left"><b>Rp. 19,334,129</b></span>
                        <small class="pull-right"><b>9.09%&nbsp;</b></small>
                    </div>
                        <div class="progress sm" align="right">
                        <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: 9.09%"></div>
                    </div>
                </div>
                <hr/>
                <div class="progress-group">
                    Pendapatan Asli Desa (PAD)<br>
                    <div class="clearfix">
                        <span class="pull-left"><b>Rp. 18,617,690</b></span>
                        <small class="pull-right"><b>8.09%&nbsp;</b></small>
                    </div>
                        <div class="progress sm" align="right">
                        <div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" style="width: 8.09%"></div>
                    </div>
                </div>
                <hr/>
                <div class="progress-group">
                    Penerimaan Pembiayaan<br>
                    <div class="clearfix">
                        <span class="pull-left"><b>Rp. 329,324,456</b></span>
                        <small class="pull-right"><b>47.06%&nbsp;</b></small>
                    </div>
                    <div class="progress sm" align="right">
                        <div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" style="width: 47.06%"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    {{-- <div class="row">
        <div class="col py-4">
            <hr class="solid">
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 mx-md-auto text-center">

            <h2 class="text-color-dark font-weight-normal text-5 mb-0 pt-2">Warga <strong class="font-weight-extra-bold">Melapor</strong></h2>
            <p>How we started, lorem ipsum dolor sit amet, consectetur adipiscing elit ac laoreet libero.</p>

            <section class="timeline" id="timeline">
                <div class="timeline-body">
                    <div class="timeline-date">
                        <h3 class="text-primary font-weight-bold">2018</h3>
                    </div>

                    <article class="timeline-box left text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="200">
                        <div class="timeline-box-arrow"></div>
                        <div class="p-2">
                            <img alt="" class="img-fluid" src="{{asset('img/history/history-3.jpg')}}" />
                            <h3 class="font-weight-bold text-3 mt-3 mb-1">New Office</h3>
                            <p class="mb-0 text-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat, ante.</p>
                        </div>
                    </article>

                    <div class="timeline-date">
                        <h3 class="text-primary font-weight-bold">2012</h3>
                    </div>

                    <article class="timeline-box right text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="400">
                        <div class="timeline-box-arrow"></div>
                        <div class="p-2">
                            <img alt="" class="img-fluid" src="{{asset('img/history/history-2.jpg')}}" />
                            <h3 class="font-weight-bold text-3 mt-3 mb-1">New Partners</h3>
                            <p class="mb-0 text-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel pellentesque consequat.</p>
                        </div>
                    </article>

                    <div class="timeline-date">
                        <h3 class="text-primary font-weight-bold">2006</h3>
                    </div>

                    <article class="timeline-box left text-left appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="600">
                        <div class="timeline-box-arrow"></div>
                        <div class="p-2">
                            <img alt="" class="img-fluid" src="{{asset('img/history/history-1.jpg')}}" />
                            <h3 class="font-weight-bold text-3 mt-3 mb-1">Foundation</h3>
                            <p class="mb-0 text-2">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce elementum, nulla vel consequat, ante.</p>
                        </div>
                    </article>
                </div>
            </section>

        </div>
    </div> --}}

</div>

<section class="section section-default border-0 my-5">
    <div class="container py-4">

        <div class="row">
            <div class="col pb-4 text-center">
                <h2 class="text-color-dark font-weight-normal text-5 mb-0 pt-2">Pemerintah Desa <strong class="font-weight-extra-bold"></strong></h2>
                <p>Rockstars lorem ipsum dolor sit amet, consectetur adipiscing elit ac laoreet libero.</p>
            </div>
        </div>
        <div class="row pb-4 mb-2">
            <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter">
                <span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
                    <span class="thumb-info-wrapper border-radius-0">
                        <a href="about-me.html">
                            <img src="{{asset('img/clients/client-1.jpg')}}" class="img-fluid border-radius-0" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Jon Doe</span>
                                <span class="thumb-info-type">Kepala Desa</span>
                            </span>
                        </a>
                    </span>
                    <span class="thumb-info-caption">
                        <span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
                        <span class="thumb-info-social-icons">
                            <a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                            <a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
                            <a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                        </span>
                    </span>
                </span>
            </div>
            <div class="col-sm-6 col-lg-3 mb-4 mb-lg-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="200">
                <span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
                    <span class="thumb-info-wrapper border-radius-0">
                        <a href="about-me.html">
                            <img src="{{asset('img/clients/client-2.jpg')}}" class="img-fluid border-radius-0" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Jessica Doe</span>
                                <span class="thumb-info-type">Sekretaris Desa</span>
                            </span>
                        </a>
                    </span>
                    <span class="thumb-info-caption">
                        <span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
                        <span class="thumb-info-social-icons">
                            <a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                            <a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
                            <a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                        </span>
                    </span>
                </span>
            </div>
            <div class="col-sm-6 col-lg-3 mb-4 mb-sm-0 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="400">
                <span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
                    <span class="thumb-info-wrapper border-radius-0">
                        <a href="about-me.html">
                            <img src="{{asset('img/clients/client-3.jpg')}}" class="img-fluid border-radius-0" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Rick Edward Doe</span>
                                <span class="thumb-info-type">Kaur Pembangunan</span>
                            </span>
                        </a>
                    </span>
                    <span class="thumb-info-caption">
                        <span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
                        <span class="thumb-info-social-icons">
                            <a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                            <a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
                            <a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                        </span>
                    </span>
                </span>
            </div>
            <div class="col-sm-6 col-lg-3 appear-animation" data-appear-animation="fadeInRightShorter" data-appear-animation-delay="600">
                <span class="thumb-info thumb-info-hide-wrapper-bg bg-transparent border-radius-0">
                    <span class="thumb-info-wrapper border-radius-0">
                        <a href="about-me.html">
                            <img src="{{asset('img/clients/client-4.jpg')}}" class="img-fluid border-radius-0" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Melinda Wolosky</span>
                                <span class="thumb-info-type">Bayan</span>
                            </span>
                        </a>
                    </span>
                    <span class="thumb-info-caption">
                        <span class="thumb-info-caption-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac ligula mi, non suscipitaccumsan</span>
                        <span class="thumb-info-social-icons">
                            <a target="_blank" href="http://www.facebook.com"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
                            <a href="http://www.twitter.com"><i class="fab fa-twitter"></i><span>Twitter</span></a>
                            <a href="http://www.linkedin.com"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
                        </span>
                    </span>
                </span>
            </div>
        </div>  
    </div>
</section>

@endsection
@section('css')
<style type="text/css">
.page-header {
    background-color: #212529;
}
</style>
@endsection
@section('javascript')
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-profil').addClass('active')
    });
</script>
@endsection
