@extends('frontend.layouts.app')
@section('page_title')
Layanan Pemerintah Desa
@endsection
@section('content')
<div class="container py-4">
    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">
                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-date ml-0">
                        <span class="day">{{ date('d', strtotime($detail->layananCreatedAt)) }}</span>
                        <span class="month">{{ date('M', strtotime($detail->layananCreatedAt)) }}</span>
                    </div>
            
                    <div class="post-content ml-0">
                        <h2 class="font-weight-bold"><a href="">{{ $detail->nama_layanan }}</a></h2>
                        <div class="post-meta">
                            <span><i class="far fa-user"></i> Penulis <a href="#">{{ $detail->get_desa->nama_desa}}</a> </span>
                            <span><i class="far fa-calendar-check"></i> {{ date('d F Y H:i', strtotime($detail->layananCreatedAt)) }}</span>
                        </div>
                        <img src="{{ (!empty($detail->post_img)?asset('storage/images/services/'.$detail->id_desa.'/'.$detail->post_img):asset('storage/images/services/default-service.jpg')) }}" class="img-fluid float-left mr-4 mt-2" alt="">
                        {!! $detail->deskripsi_layanan !!}
                        <hr>
                        <div class="post-block mt-5 post-share">
                            <h4 class="mb-3">Bagikan artikel ini</h4>
                                        
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_button_pinterest_pinit"></a>
                                <a class="addthis_counter addthis_pill_style"></a>
                            </div>
                            <!-- AddThis Button END -->
                           
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')

@endsection
@section('javascript')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-info').addClass('active')
    });
</script>
@endsection
