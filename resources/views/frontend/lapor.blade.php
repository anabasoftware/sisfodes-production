@extends('frontend.layouts.app')
@section('page_title')
Warga Melapor
@endsection
@section('content')

<div class="container">
    <div class="row py-4">
        <div class="col-lg-6">

            <div class="overflow-hidden mb-1">
                <h2 class="font-weight-normal text-7 mt-2 mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="200"><strong class="font-weight-extra-bold">Lapor</strong> kepada kami</h2>
            </div>
            <div class="overflow-hidden mb-4 pb-3">
                <p class="mb-0 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="400">Kirimkan laporan terkait temuan masyarakat, tentang hasil bumi, wisata, atau bentuk keresahan masyarakat!</p>
            </div>

            <form id="contactForm" class="contact-form" action="" method="POST">
                <div class="form-row">
                    <div class="form-group col-lg-6">
                        <label class="required font-weight-bold text-dark text-2">Nama Lengkap</label>
                        <input type="text" value="" data-msg-required="isi nama lengkap anda." maxlength="100" class="form-control" name="name" id="name" required>
                    </div>
                    <div class="form-group col-lg-6">
                        <label class="required font-weight-bold text-dark text-2">Email</label>
                        <input type="email" value="" data-msg-required="Isi alamat email anda" data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label class="font-weight-bold text-dark text-2">Judul</label>
                        <input type="text" value="" data-msg-required="Judul laporan." maxlength="100" class="form-control" name="subject" id="subject" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label class="required font-weight-bold text-dark text-2">Pesan</label>
                        <textarea maxlength="5000" data-msg-required="Uraian terkait laporan." rows="8" class="form-control" name="message" id="message" required></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <input type="submit" value="Kirim laporan" class="btn btn-primary btn-modern" data-loading-text="Loading...">
                    </div>
                </div>
            </form>

        </div>
        <div class="col-lg-6">

            <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="800">
                <h4 class="mt-2 mb-1">Kantor <strong>Balai Desa</strong></h4>
                <ul class="list list-icons list-icons-style-2 mt-2">
                    <li><i class="fas fa-map-marker-alt top-6"></i> <strong class="text-dark">Address:</strong> {{\App\Models\Desa::where('website', \Request::getHttpHost())->first()->alamat}}</li>
                    <li><i class="fas fa-phone top-6"></i> <strong class="text-dark">Phone:</strong> {{\App\Models\Desa::where('website', \Request::getHttpHost())->first()->no_telp}}</li>
                    <li><i class="fas fa-envelope top-6"></i> <strong class="text-dark">Email:</strong> <a href="mailto:mail@example.com">{{\App\Models\Desa::where('website', \Request::getHttpHost())->first()->get_user->email}}</a></li>
                </ul>
            </div>

            <div class="appear-animation" data-appear-animation="fadeIn" data-appear-animation-delay="950">
                <h4 class="pt-5">Pelayanan <strong>dibuka</strong></h4>
                <ul class="list list-icons list-dark mt-2">
                    <li><i class="far fa-clock top-6"></i> Senin - Jum'at - 09:00 to 14:00</li>
                    <li><i class="far fa-clock top-6"></i> Sabtu - 09:00 to 12:00</li>
                    <li><i class="far fa-clock top-6"></i> Minggu - Tutup</li>
                </ul>
            </div>

        </div>
    </div>
</div>

@endsection
@section('css')

@endsection
@section('javascript')
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-lapor').addClass('active')
    });
</script>
@endsection
