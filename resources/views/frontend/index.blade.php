<script>
function redirect(domain) {
    window.location.href = domain;
}
</script>
@php
    // if(\Request::segment(1)=="login"){
        $get_domain = \Request::getSchemeAndHttpHost();
        // print_r($get_domain);
        // print_r(explode("://",$get_domain));
        // print_r(explode("://",$get_domain)[1]);
        $explode = explode("://",$get_domain);
        $current = $explode[1];

        $check = \App\Models\Desa::where('website', $current)->first();

        // dd($check);
        if(empty($check)){
            echo "<script>redirect('".$get_domain."/login');</script>";
        }
    // }
@endphp
@extends('frontend.layouts.app')
@section('page_title')
Halaman Depan
@endsection
@section('content')
{!! \Helpers::jumbotron(\Request::getHttpHost()) !!}
<div class="home-intro home-intro-quaternary" id="home-intro">
    <div class="container">
        <div class="row text-center">
            <div class="col">
                <p class="mb-0">
                        Transparansi dana desa merupakan bentuk pengawasan penggunaan dan 
                        pengolalaan anggaran pendapatan dan belanja desa, 
                        serta sebagai sosialisasi dan wadah penerapan program "Padat Karya".
                        {{-- <span class="highlighted-word highlighted-word-animation-1 highlighted-word-animation-1-light text-color-light font-weight-semibold text-5">
                            
                        </span> --}}
                    <span>Yuk! Kita periksa transparansi dana desa.</span>
                </p>
            </div>
        </div>
    </div>
</div>

{{-- Section transparansi dana desa--}}
{!! \Helpers::dana_desa() !!}

{{-- Section blog post --}}

<div class="container container-lg">
    <div class="row py-5">
        <div class="col-md-6 col-lg-4">
            <h3 class="font-weight-bold text-3 mb-0">Populer</h3>
            {!! \Helpers::blog_post_popular(\Request::getHttpHost()) !!}
        </div>
        <div class="col-md-6 col-lg-4">
            <h3 class="font-weight-bold text-3 mb-0">Terbaru</h3>
            {!! \Helpers::blog_post_recent(\Request::getHttpHost()) !!}
        </div>
        <div class="col-lg-4">
            <h3 class="font-weight-bold text-3 mt-4 mt-md-0">Unggulan</h3>
            {!! \Helpers::blog_post_featured(\Request::getHttpHost()) !!}
        </div>
    </div>    
</div>

{{-- Section galery & potention --}}
<section class="section bg-color-light border-0 pb-0 m-0">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center appear-animation" data-appear-animation="fadeInUpShorter">
                <h2 class="font-weight-normal text-6 pb-2 mb-4"><strong class="font-weight-extra-bold">Produk</strong>
                    &amp; <strong class="font-weight-extra-bold">Potensi Desa</strong></h2>
            </div>
        </div>
    </div>
    <div class="image-gallery sort-destination full-width mb-0">
        {!! \Helpers::potensi_desa(\Request::getHttpHost()) !!}
        
        {{-- <div class="isotope-item">
            <div class="image-gallery-item mb-0 appear-animation" data-appear-animation="fadeInLeftShorter"
                data-appear-animation-delay="700">
                <a href="#">
                    <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                            <img src="{{asset('img/projects/project-30.jpg')}}" class="img-fluid" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Project Title</span>
                                <span class="thumb-info-type">Project Type</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
        <div class="isotope-item">
            <div class="image-gallery-item mb-0 appear-animation" data-appear-animation="fadeInLeftShorter"
                data-appear-animation-delay="500">
                <a href="#">
                    <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                            <img src="{{asset('img/projects/project-31.jpg')}}" class="img-fluid" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Project Title</span>
                                <span class="thumb-info-type">Project Type</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
        <div class="isotope-item">
            <div class="image-gallery-item mb-0 appear-animation" data-appear-animation="fadeIn"
                data-appear-animation-delay="300">
                <a href="#">
                    <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                            <img src="{{asset('img/projects/project-32.jpg')}}" class="img-fluid" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Project Title</span>
                                <span class="thumb-info-type">Project Type</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
        <div class="isotope-item">
            <div class="image-gallery-item mb-0 appear-animation" data-appear-animation="fadeInRightShorter"
                data-appear-animation-delay="500">
                <a href="#">
                    <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                            <img src="{{asset('img/projects/project-33.jpg')}}" class="img-fluid" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Project Title</span>
                                <span class="thumb-info-type">Project Type</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div>
        <div class="isotope-item">
            <div class="image-gallery-item mb-0 appear-animation" data-appear-animation="fadeInRightShorter"
                data-appear-animation-delay="700">
                <a href="#">
                    <span class="thumb-info thumb-info-centered-info thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                            <img src="{{asset('img/projects/project-34.jpg')}}" class="img-fluid" alt="">
                            <span class="thumb-info-title">
                                <span class="thumb-info-inner">Project Title</span>
                                <span class="thumb-info-type">Project Type</span>
                            </span>
                            <span class="thumb-info-action">
                                <span class="thumb-info-action-icon"><i class="fas fa-plus"></i></span>
                            </span>
                        </span>
                    </span>
                </a>
            </div>
        </div> --}}
    </div>
</section>

{{-- Section supporter --}}
<div class="container">
    <div class="row py-5 my-5">
        <div class="col">
            <div class="owl-carousel owl-theme mb-0"
                data-plugin-options="{'responsive': {'0': {'items': 1}, '476': {'items': 1}, '768': {'items': 5}, '992': {'items': 7}, '1200': {'items': 7}}, 'autoplay': true, 'autoplayTimeout': 3000, 'dots': false}">
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-1.png')}}" alt="">
                </div>
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-2.png')}}" alt="">
                </div>
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-3.png')}}" alt="">
                </div>
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-4.png')}}" alt="">
                </div>
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-5.png')}}" alt="">
                </div>
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-6.png')}}" alt="">
                </div>
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-4.png')}}" alt="">
                </div>
                <div>
                    <img class="img-fluid opacity-2" src="{{asset('img/logos/logo-2.png')}}" alt="">
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
@section('css')
<link rel="stylesheet" href="{{asset('vendor/chartjs/Chart.min.css')}}">
@endsection
@section('javascript')
<script src="{{asset('vendor/chartjs/Chart.min.js')}}"></script>
<script type="text/javascript">
	$(function () { 
        var ctx = document.getElementById('chart_anggaran_desa').getContext('2d');
		var month = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var config = new Chart(ctx, {
			type: 'line',
			data: {
				labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
				datasets: [{
					label: 'Pendapatan Pertanian',
					backgroundColor: "red",
					borderColor: "red",
					data: [12, 19, 3, 5, 2, 3],
					fill: false,
				}, {
					label: 'Pamsimas',
					fill: false,
					backgroundColor: "blue",
					borderColor: "blue",
					data: [1,2,4,6,5,10,12,16],
				}]
			},
			options: {
				responsive: true,
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Bulan'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Ratusan Ribu'
						}
					}]
				}
			}
        })
        $('.m-beranda').addClass('active')
    });
</script>
@endsection