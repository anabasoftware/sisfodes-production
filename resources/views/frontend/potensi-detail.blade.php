@extends('frontend.layouts.app')
@section('page_title')
Potensi Desa
@endsection
@section('content')
<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md ">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <div class="overflow-hidden pb-2">
                            <h1 class="text-dark font-weight-bold text-9 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="100">{{$detail->nama_potensi}}</h1>
                            <span class="sub-title text-dark" data-appear-animation="maskUp" data-appear-animation-delay="100">{{Str::substr($detail->deskripsi_potensi,0,86)}} ...</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<div class="container py-2">
    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">           
                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-date ml-0">
                        <span class="day">{{date('d',strtotime($detail->created_at))}}</span>
                        <span class="month">{{date('M',strtotime($detail->created_at))}}</span>
                    </div>
            
                    <div class="post-content ml-0">
            
                        <h2 class="font-weight-bold">
                            <a href="{{route('frontpage.detail_potensi',$detail->slug_potensi)}}">{{$detail->nama_potensi}}</a>
                        </h2>
            
                        <div class="post-meta">
                            <span><i class="far fa-user"></i> By <a href="#">John Doe</a></span>
                        </div>
                        <img src="/{{env('IMAGES_PATH')}}/potentions/{{$detail->id_desa}}/{{$detail->gambar}}" class="img-fluid float-left mr-4 mt-2" style="width:300px;height:200px;"/>
                        {{-- Content start --}}
                        {!! $detail->deskripsi_potensi !!}
                        {{-- End of content --}}
                        <div class="post-block mt-5 post-share">
                            <h4 class="mb-3">Bagikan</h4>
            
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_counter addthis_pill_style"></a>
                            </div>
                            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
                            <!-- AddThis Button END -->
            
                        </div>
            
                        <div class="post-block mt-4 pt-2 post-author">
                            <h4 class="mb-3">Author</h4>
                            <div class="img-thumbnail img-thumbnail-no-borders d-block pb-3">
                                <a href="">
                                    <img src="{{asset('img/!logged-user.jpg')}}" alt="">
                                </a>
                            </div>
                            <p><strong class="name"><a href="#" class="text-4 pb-2 pt-2 d-block">John Doe</a></strong></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim ornare nisi, vitae mattis nulla ante id dui. </p>
                        </div>
                    </div>
                </article>
            
            </div>
        </div>
    </div>

</div>
@endsection
@section('css')

@endsection
@section('javascript')
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-potensi').addClass('active')
    });
</script>
@endsection
