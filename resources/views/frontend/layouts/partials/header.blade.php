<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">	

    <title>{{env('APP_NAME')}} &mdash; @yield('page_title')</title>

    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.ico')}}" type="image/x-icon" />
    <link rel="apple-touch-icon" href="{{asset('img/apple-touch-icon.png')}}">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts  -->
    {{-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light%7CPlayfair+Display:400" rel="stylesheet" type="text/css"> --}}

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/font-awesome/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/animate/animate.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/simple-line-icons/css/simple-line-icons.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl.carousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/magnific-popup/magnific-popup.css')}}">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="{{asset('css/frontpage/theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/frontpage/theme-elements.css')}}">
    <link rel="stylesheet" href="{{asset('css/frontpage/theme-blog.css')}}">
    <link rel="stylesheet" href="{{asset('css/frontpage/theme-shop.css')}}">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="{{asset('vendor/rs-plugin/css/settings.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/rs-plugin/css/layers.css')}}">
    <link rel="stylesheet" href="{{asset('vendor/rs-plugin/css/navigation.css')}}">
    
    <!-- Demo CSS -->
    @yield('css')

    <!-- Skin CSS -->
    <link rel="stylesheet" href="{{asset('css/frontpage/skins/skin-corporate-5.css')}}">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="{{asset('css/frontpage/custom.css')}}">

    <!-- Head Libs -->
    <script src="{{asset('vendor/modernizr/modernizr.js')}}"></script>

</head>