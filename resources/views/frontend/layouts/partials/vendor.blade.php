
		<!-- Vendor -->
		<script src="{{asset('vendor/jquery/jquery.js')}}"></script>
		<script src="{{asset('vendor/jquery-appear/jquery.appear.js')}}"></script>
		<script src="{{asset('vendor/jquery-easing/jquery.easing.min.js')}}"></script>
		<script src="{{asset('vendor/jquery-cookie/jquery.cookie.js')}}"></script>
		<script src="{{asset('vendor/popper/umd/popper.min.js')}}"></script>
		<script src="{{asset('vendor/bootstrap/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('vendor/common/common.js')}}"></script>
		<script src="{{asset('vendor/jquery-lazyload/jquery.lazyload.min.js')}}"></script>
		<script src="{{asset('vendor/isotope/isotope.js')}}"></script>
		<script src="{{asset('vendor/owl.carousel/owl.carousel.js')}}"></script>
		<script src="{{asset('vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('vendor/vide/jquery.vide.min.js')}}"></script>
		<script src="{{asset('vendor/vivus/vivus.min.js')}}"></script>
		
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('js/frontpage/theme.js')}}"></script>
		<!-- Theme Initialization Files -->
		<script src="{{asset('js/frontpage/theme.init.js')}}"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="{{asset('vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
		<script src="{{asset('vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
		
		<!-- Theme Custom -->
		<script src="{{asset('js/frontpage/custom.js')}}"></script>
		
		@yield('javascript')
		
		<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
			ga('create', 'UA-12345678-1', 'auto');
			ga('send', 'pageview');
		</script>
		 -->