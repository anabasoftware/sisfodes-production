<footer id="footer">
    <div class="container">
        <div class="row py-5 my-4">
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <h5 class="text-3 mb-3 text-uppercase">Tentang</h5>
                <p>{!! !empty(\App\Models\Desa::where('website',\Request::getHttpHost())->first())?\App\Models\Desa::where('website',\Request::getHttpHost())->first()->get_sejarah->sejarah:''!!}</p>
                <p class="mb-0">{!! !empty(\App\Models\Desa::where('website',\Request::getHttpHost())->first())?\App\Models\Desa::where('website',\Request::getHttpHost())->first()->visi:''!!}</p>
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-lg-0">
                <h5 class="text-3 mb-3">BERITA TERIKINI</h5>
                {!! \Helpers::footer_latest_news(\Request::getHttpHost())!!}
            </div>
            <div class="col-md-6 col-lg-3 mb-5 mb-md-0">
                <h5 class="text-3 mb-3">LAPORAN WARGA</h5>
                {!! \Helpers::footer_reports(\Request::getHttpHost())!!}
            </div>
            <div class="col-md-6 col-lg-1 mb-4 mb-lg-0">
                <h5 class="text-3 mb-3 text-uppercase">HALAMAN</h5>
                {!!\Helpers::footer_page_menu()!!}
            </div>
            <div class="col-md-6 col-lg-2">
                <h5 class="text-3 mb-3">KATEGORI</h5>
                <p>
                    <a href="#"><span class="badge badge-dark bg-color-black badge-sm py-2 mr-1 mb-2 text-uppercase">Travel</span></a>
                    <a href="#"><span class="badge badge-dark bg-color-black badge-sm py-2 mr-1 mb-2 text-uppercase">Business</span></a>
                    <a href="#"><span class="badge badge-dark bg-color-black badge-sm py-2 mr-1 mb-2 text-uppercase">Architecture</span></a>
                    <a href="#"><span class="badge badge-dark bg-color-black badge-sm py-2 mr-1 mb-2 text-uppercase">Sports</span></a>
                    <a href="#"><span class="badge badge-dark bg-color-black badge-sm py-2 mr-1 mb-2 text-uppercase">Videos</span></a>
                    <a href="#"><span class="badge badge-dark bg-color-black badge-sm py-2 mr-1 mb-2 text-uppercase">Technology</span></a>
                </p>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container py-2">
            <div class="row py-4">
                <div class="col-lg-2 d-flex align-items-center justify-content-center justify-content-lg-start mb-2 mb-lg-0">
                    <a href="" class="logo pr-0 pr-lg-3">
                        <img alt="Porto Website Template" src="{{asset('img/isifodes-logo.png')}}" class="opacity-5" height="33">
                    </a>
                </div>
                <div class="col-lg-7 d-flex align-items-center justify-content-center justify-content-lg-start mb-4 mb-lg-0">
                    <p>&copy; Copyright 2019 &mdash; {{date('Y')}} . Hak Cipta dilindungi oleh Undang-undang.</p>
                </div>
                <div class="col-lg-3 d-flex align-items-center justify-content-center justify-content-lg-end">
                    <nav id="sub-menu">
                        <ul>
                            <li><i class="fas fa-angle-right"></i><a href="" class="ml-1 text-decoration-none"> FAQ's</a></li>
                            <li><i class="fas fa-angle-right"></i><a href="" class="ml-1 text-decoration-none"> Sitemap</a></li>
                            <li><i class="fas fa-angle-right"></i><a href="" class="ml-1 text-decoration-none"> Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</footer>
{{-- 
<footer id="footer">
    <div class="container">
        <div class="row py-5 my-4">
            <div class="col-md-6 mb-4 mb-lg-0">
                <a href="index.html" class="logo pr-0 pr-lg-3">
                    <img alt="Porto Website Template" src="{{asset('img/isifodes-logo.png')}}" class="opacity-7 bottom-4" height="33">
                </a>
                <p class="mt-2 mb-2">
                    {!! \App\Models\Desa::where('website',\Request::getHttpHost())->first()->get_sejarah->sejarah!!}
                </p>
                <p class="mb-0"><a href="#" class="btn-flat btn-xs text-color-light"><strong class="text-2">SELENGKAPNYA</strong><i class="fas fa-angle-right p-relative top-1 pl-2"></i></a></p>
            </div>
            <div class="col-md-6">
                <h5 class="text-3 mb-3">CONTACT US</h5>
                <div class="row">
                    <div class="col-md-6">
                        {!! \Helpers::footer_menu(\Request::getHttpHost()) !!}
                    </div>
                    <div class="col-md-6">
                        <ul class="list list-icons list-icons-sm">
                            <li><i class="fas fa-angle-right"></i><a href="#" class="link-hover-style-1 ml-1"> FAQ's</a></li>
                            <li><i class="fas fa-angle-right"></i><a href="#" class="link-hover-style-1 ml-1"> Sitemap</a></li>
                            <li><i class="fas fa-angle-right"></i><a href="{{route('frontpage.profil')}}" class="link-hover-style-1 ml-1"> Hubungi Kami</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright footer-copyright-style-2">
        <div class="container py-2">
            <div class="row py-4">
                <div class="col d-flex align-items-center justify-content-center">
                    <p>&copy; Copyright 2019 &mdash; {{date('Y')}} . Hak Cipta dilindungi oleh Undang-undang.</p>
                </div>
            </div>
        </div>
    </div>
</footer> 
--}}