<header id="header" class="header-effect-shrink" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 120, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body border-top-0">
        <div class="header-top header-top-borders">
            <div class="container h-100">
                <div class="header-row h-100">
                    <div class="header-column justify-content-start">
                        <div class="header-row">
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills">
                                    {!! \Helpers::topbar_content(\Request::getHttpHost()) !!}
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="header-column justify-content-end">
                        <div class="header-row">
                            <nav class="header-nav-top">
                                <ul class="nav nav-pills">
                                    {{-- <li class="nav-item nav-item-borders py-2 d-none d-lg-inline-flex">
                                        <a href="#">Blog</a>
                                    </li> --}}
                                    <li class="nav-item nav-item-borders py-2 pr-0 dropdown">
                                        <a class="nav-link" href="#" role="button" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <img src="{{asset('img/blank.gif')}}" class="flag flag-id" alt="Indonesia" /> Bahasa Indonesia
                                            <i class="fas fa-angle-down"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
                                            <a class="dropdown-item" href="#">
                                                <img src="{{asset('img/blank.gif')}}" class="flag flag-id" alt="Bahasa Indonesia" /> 
                                                Bahasa Indonesia
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="/">
                                <img alt="Sisfodes" 
                                    width="100" 
                                    height="32" 
                                    data-sticky-width="82" 
                                    data-sticky-height="26" 
                                    src="{{asset('img/isifodes-logo.png')}}">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        
                        {!! \Helpers::navbar_menu() !!}

                        <div class="header-nav-features header-nav-features-no-border header-nav-features-lg-show-border order-1 order-lg-2">
                            <div class="header-nav-feature header-nav-features-cart d-inline-flex ml-2">
                                <a href="#" class="header-nav-features-toggle"><i class="fas fa-sign-in-alt"></i></a>
                                @auth
                                <div class="header-nav-features-dropdown" id="headerTopCartDropdown">
                                    <ol class="mini-products-list">
                                        <li class="item">
                                            <a href="#" title="@if(!empty(\Auth::user()->count())){{\Auth::user()->email}}@endif" class="product-image">
                                                <img 
                                                src="@if(\Auth::user()->role_id != 0 && !empty(\Auth::user()->count())){{ asset('storage/images/logo/' . \Auth::user()->desa()->first()->logo) }}@endif" 
                                                alt="@if(\Auth::user()->role_id != 0 && !empty(\Auth::user()->count())){{\Auth::user()->user_name}}@endif">
                                            </a>
                                            <div class="product-details">
                                                <p class="product-name"><a href="#">@if(\Auth::user()->role_id != 0 && !empty(\Auth::user()->count())){{\Auth::user()->email}}@endif</a></p>
                                                <p class="qty-price">@if(\Auth::user()->role_id != 0 && !empty(\Auth::user()->count())){{\Auth::user()->user_name}}@endif</p>
                                            </div>
                                        </li>
                                    </ol>
                                    <div class="actions">
                                        <a class="btn btn-dark" href="{{route('home')}}">Dashboard</a>
                                        <a class="btn btn-primary" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                                            <i class="fas fa-power-off"></i> {{ __('Logout') }}
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
                                    </div>
                                </div>
                                @else
                                <div class="header-nav-features-dropdown" id="headerTopCartDropdown">
                                    <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                        <div class="form-group mb-3">
                                            <label>Nama pengguna</label>
                                            <div class="input-group">
                                                <input type="email" 
                                                    class="form-control 
                                                    @error('email') is-invalid @enderror"
                                                    name="email" 
                                                    value="{{ old('email') }}" 
                                                    required 
                                                    autocomplete="email" 
                                                    autofocus 
                                                    class="form-control form-control-lg" />
                                                <span class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-user"></i>
                                                    </span>
                                                </span>
                                                @error('email')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group mb-3">
                                            <div class="clearfix">
                                                <label class="float-left">Kata sandi</label>
                                                @if (Route::has('password.request'))
                                                <a href="{{ route('password.request') }}" class="float-right">Lupa kata sandi ?</a>
                                                @endif
                                            </div>
                                            <div class="input-group">
                                                <input id="password" 
                                                    type="password" 
                                                    class="form-control 
                                                    @error('password') is-invalid @enderror" 
                                                    name="password" 
                                                    required 
                                                    autocomplete="current-password" 
                                                    class="form-control form-control-lg" />
                                                <span class="input-group-append">
                                                    <span class="input-group-text">
                                                        <i class="fas fa-lock"></i>
                                                    </span>
                                                </span>
                                                @error('password')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="actions">
                                            <div class="text-right">
                                                <button type="submit" class="btn btn-primary mt-2">Masuk</button>
                                            </div>
                                        </div>
                                    </form> 
                                </div>
                                @endauth
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>