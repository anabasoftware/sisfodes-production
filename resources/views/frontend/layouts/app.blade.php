<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	@include('frontend.layouts.partials.header')
	<body>
		<div class="body">
			@include('frontend.layouts.partials.navbar')

			<div role="main" class="main">
				@yield('content')
			</div>
 
			@include('frontend.layouts.partials.footer')
		</div>

	@include('frontend.layouts.partials.vendor')
	</body>
</html>