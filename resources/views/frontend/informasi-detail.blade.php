@extends('frontend.layouts.app')
@section('page_title')
Papan Informasi
@endsection
@section('content')
<div class="container py-4">
    <div class="row">
        <div class="col">
            <div class="blog-posts single-post">
                <article class="post post-large blog-single-post border-0 m-0 p-0">
                    <div class="post-date ml-0">
                        <span class="day">{{ date('d', strtotime($detail->beritaCreatedAt)) }}</span>
                        <span class="month">{{ date('M', strtotime($detail->beritaCreatedAt)) }}</span>
                    </div>
            
                    <div class="post-content ml-0">
                        <h2 class="font-weight-bold"><a href="">{{ $detail->judul_berita }}</a></h2>
                        <div class="post-meta">
                            <span><i class="far fa-user"></i> Penulis <a href="#">{{ $detail->get_desa->nama_desa}}</a> </span>
                            <span><i class="far fa-calendar-check"></i> {{ date('d F Y H:i', strtotime($detail->beritaCreatedAt)) }}</span>
                            <span><i class="far fa-folder"></i> <a href="kategori/{{$detail->get_kategori_berita->slug_kategori}}">{{ $detail->get_kategori_berita->nama_kategori }}</a> </span>
                        </div>
                        <img src="{{ (!empty($detail->post_img)?asset('storage/images/berita/'.$detail->id_desa.'/'.$detail->post_img):asset('storage/images/berita/default.jpg')) }}" class="img-fluid float-left mr-4 mt-2" alt="">
                        {!! $detail->isi_berita !!}
                        <hr>
                        <div class="post-block mt-5 post-share">
                            <h4 class="mb-3">Bagikan artikel ini</h4>
                                        
                            <!-- AddThis Button BEGIN -->
                            <div class="addthis_toolbox addthis_default_style ">
                                <a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
                                <a class="addthis_button_tweet"></a>
                                <a class="addthis_button_pinterest_pinit"></a>
                                <a class="addthis_counter addthis_pill_style"></a>
                            </div>
                            <!-- AddThis Button END -->
                           
                        </div>
                        {{-- <div class="post-block mt-4 pt-2 post-author">
                            <h4 class="mb-3">Author</h4>
                            <div class="img-thumbnail img-thumbnail-no-borders d-block pb-3">
                                <a href="blog-post.html">
                                    <img src="img/avatars/avatar.jpg" alt="">
                                </a>
                            </div>
                            <p><strong class="name"><a href="#" class="text-4 pb-2 pt-2 d-block">John Doe</a></strong></p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae. Sed dui lorem, adipiscing in adipiscing et, interdum nec metus. Mauris ultricies, justo eu convallis placerat, felis enim ornare nisi, vitae mattis nulla ante id dui. </p>
                        </div> --}}
                    </div>
                </article>
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')

@endsection
@section('javascript')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=xa-50faf75173aadc53"></script>
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-info').addClass('active')
    });
</script>
@endsection
