@extends('frontend.layouts.app')
@section('page_title')
Papan Informasi
@endsection
@section('content')


<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md ">
    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col">
                <div class="row">
                    <div class="col-md-12 align-self-center p-static order-2 text-center">
                        <div class="overflow-hidden pb-2">
                            <h1 class="text-dark font-weight-bold text-9 appear-animation" data-appear-animation="maskUp" data-appear-animation-delay="100">Informasi dan Berita</h1>
                            <span class="sub-title text-dark" data-appear-animation="maskUp" data-appear-animation-delay="100">Berita terbaru kami!</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<div class="container py-4">
    <div class="row">
        <div class="col-lg-3">
            <aside class="sidebar">
                <form action="#" method="get">
                    <div class="input-group mb-3 pb-1">
                        <input class="form-control text-1" placeholder="Search..." name="s" id="s" type="text">
                        <span class="input-group-append">
                            <button type="submit" class="btn btn-dark text-1 p-2"><i class="fas fa-search m-2"></i></button>
                        </span>
                    </div>
                </form>
                <h5 class="font-weight-bold pt-4">Kategori</h5>
                <ul class="nav nav-list flex-column mb-5">
                    <li class="nav-item"><a class="nav-link" href="#">Design (2)</a></li>
                    <li class="nav-item">
                        <a class="nav-link active" href="#">Photos (4)</a>
                        <ul>
                            <li class="nav-item"><a class="nav-link" href="#">Animals</a></li>
                            <li class="nav-item"><a class="nav-link active" href="#">Business</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">Sports</a></li>
                            <li class="nav-item"><a class="nav-link" href="#">People</a></li>
                        </ul>
                    </li>
                    <li class="nav-item"><a class="nav-link" href="#">Videos (3)</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Lifestyle (2)</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Technology (1)</a></li>
                </ul>
                <div class="tabs tabs-dark mb-4 pb-2">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active"><a class="nav-link show active text-1 font-weight-bold text-uppercase" href="#popularPosts" data-toggle="tab">Popular</a></li>
                        <li class="nav-item"><a class="nav-link text-1 font-weight-bold text-uppercase" href="#recentPosts" data-toggle="tab">Recent</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="popularPosts">
                            <ul class="simple-post-list">
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                            <a href="{{route('frontpage.info-detail')}}">
                                                <img src="{{asset('img/blog/square/blog-11.jpg')}}" width="50" height="50" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="{{route('frontpage.info-detail')}}">Nullam Vitae Nibh Un Odiosters</a>
                                        <div class="post-meta">
                                             Nov 10, 2018
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                            <a href="{{route('frontpage.info-detail')}}">
                                                <img src="{{asset('img/blog/square/blog-24.jpg')}}" width="50" height="50" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="{{route('frontpage.info-detail')}}">Vitae Nibh Un Odiosters</a>
                                        <div class="post-meta">
                                             Nov 10, 2018
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                            <a href="{{route('frontpage.info-detail')}}">
                                                <img src="{{asset('img/blog/square/blog-42.jpg')}}" width="50" height="50" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="{{route('frontpage.info-detail')}}">Odiosters Nullam Vitae</a>
                                        <div class="post-meta">
                                             Nov 10, 2018
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-pane" id="recentPosts">
                            <ul class="simple-post-list">
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                            <a href="{{route('frontpage.info-detail')}}">
                                                <img src="{{asset('img/blog/square/blog-24.jpg')}}" width="50" height="50" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="{{route('frontpage.info-detail')}}">Vitae Nibh Un Odiosters</a>
                                        <div class="post-meta">
                                             Nov 10, 2018
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                            <a href="{{route('frontpage.info-detail')}}">
                                                <img src="{{asset('img/blog/square/blog-42.jpg')}}" width="50" height="50" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="{{route('frontpage.info-detail')}}">Odiosters Nullam Vitae</a>
                                        <div class="post-meta">
                                             Nov 10, 2018
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="post-image">
                                        <div class="img-thumbnail img-thumbnail-no-borders d-block">
                                            <a href="{{route('frontpage.info-detail')}}">
                                                <img src="{{asset('img/blog/square/blog-11.jpg')}}" width="50" height="50" alt="">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="post-info">
                                        <a href="{{route('frontpage.info-detail')}}">Nullam Vitae Nibh Un Odiosters</a>
                                        <div class="post-meta">
                                             Nov 10, 2018
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <h5 class="font-weight-bold">Latest from Twitter</h5>
                <div id="tweet" class="twitter mb-4" data-plugin-tweets data-plugin-options="{'username': 'oklerthemes', 'count': 2}">
                    <p>Please wait...</p>
                </div>
                <h5 class="font-weight-bold pt-4">Photos from Instagram</h5>
                <div id="instafeedNoMargins" class="mb-4 pb-1"></div>
                <h5 class="font-weight-bold pt-4 mb-2">Tags</h5>
                <div class="mb-3 pb-1">
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">design</span></a>
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">brands</span></a>
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">video</span></a>
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">business</span></a>
                    <a href="#"><span class="badge badge-dark badge-sm badge-pill text-uppercase px-2 py-1 mr-1">travel</span></a>
                </div>
            </aside>
        </div>
        <div class="col-lg-9">
            <div class="blog-posts">
                {!! \Helpers::blog_post(\Request::getHttpHost()) !!}
            </div>
        </div>
    </div>
</div>
@endsection
@section('css')

@endsection
@section('javascript')
<script type="text/javascript">
	$(function () { 
        $('.m-beranda').removeClass('active')
        $('.m-info').addClass('active')
    });
</script>
@endsection
