@extends('layouts.app')
@section('title') Profil Kabupaten
@endsection
@section('breadcrumbs')
<li><span>Profil Kabupaten</span></li>
@endsection
@section('content')
<div class="row">
	<div class="col-lg-4 col-xl-4 mb-4 mb-xl-0">

		<section class="card">
			<div class="card-body">
				<div class="thumb-info mb-3">
					<img src="{{ asset('img/logo-halbar.png') }}" class="rounded img-fluid" alt={{ $kabupaten->nama_kabupaten }}>
					<div class="thumb-info-title">
						<span class="thumb-info-inner">Halmahera Barat</span>
					</div>
				</div>

				<div class="widget-toggle-expand mb-3 widget-collapsed">
					<div class="widget-header">
						<h5 class="mb-2">Profil Pemerintah</h5>						
					</div>					
					<div class="widget-content-expanded" style="">
					</div>
				</div>

				<hr class="dotted short">
						<p class="text-2" id="tentang">{{ $kabupaten->tentang_pemerintahan }}</p>
						<div class="col-md-12 text-right mt-3">                   
							
								<button class="btn btn-primary modal-confirm" onclick="add_form()">Edit Tentang</button>
								
							</div>
							<!-- Form -->
							
						<div class="clearfix">
						
						<hr class="dotted short">

						<div class="social-icons-list">
							<a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com" data-original-title="Facebook"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
							<a rel="tooltip" data-placement="bottom" href="http://www.twitter.com" data-original-title="Twitter"><i class="fab fa-twitter"></i><span>Twitter</span></a>
							<a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com" data-original-title="Linkedin"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
						</div>

					</div>
		</section>

	</div>
	<div class="col-lg-8 col-xl-8">

		<div class="tabs">
			<ul class="nav nav-tabs tabs-primary">
				<li class="nav-item active">
					<a class="nav-link active" href="#edit" data-toggle="tab">Profil Kabupaten</a>
					<p class="text-2">{{ $kabupaten->tentang_pemerintah }}</p>
				</li>
			</ul>
			<div class="tab-content">
				<div id="edit" class="tab-pane active">

					<form class="form form-horizontal" 
					data-toggle="validator"
					role="form" 
					enctype="multipart/form-data" id="form-kabupaten">
					@csrf @method('POST')
					<input type="hidden" name="id_kabupaten" id="id_kabupaten" value="">
					<input type="hidden" name="tentang_pemerintahan" id="tentang_pemerintahan" value="">
						
						<div class="form-group">
							<label for="kode_wilayah">Kode Wilayah</label>
							<input type="text" class="form-control" id="kode_wilayah" name="kode_wilayah" value="{{ $kabupaten->kode_wilayah }}">
                        </div>
                        <div class="form-group">
							<label for="nama_pemerintahan">Pemerintahan</label>
							<input type="text" class="form-control" id="nama_pemerintahan" name="nama_pemerintahan" value="{{ $kabupaten->nama_pemerintahan }}">
						</div>
						<div class="form-group">
							<label for="kepala_pemerintahan">Kepala Pemerintahan</label>
							<input type="text" class="form-control" id="kepala_pemerintahan" name="kepala_pemerintahan" value="{{ $kabupaten->kepala_pemerintahan }}">
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="email">Email</label>
								<input type="text" class="form-control" id="email" name="email" value="{{ $kabupaten->email }}">
							</div>							
						</div>

						<hr class="dotted tall">				

						<div class="form-row">
							<div class="col-md-12 text-right mt-3">
								<button class="btn btn-primary modal-confirm">Save</button>
							</div>
						</div>

					</form>

				</div>
			</div>
		</div>
	</div>
</div>


@include('kabupaten.form')
@endsection
@section('javascript')

<!-- Examples -->
<script src="{{ asset('js/examples/examples.lightbox.js') }} "></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$(function () {
        CKEDITOR.replace('edit_tentang',{
			language: 'en',
            toolbar: [
                [ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
                [ 'Link', 'Unlink' ],
                [ 'EasyImageUpload', 'Table', 'HorizontalRule' ],
                [ 'Bold', 'Italic', 'Strike', '-', 'NumberedList', 'BulletedList' ],
                [ 'Source', 'Maximize'],
                [ 'About' ]
            ],
            cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
            cloudServices_uploadUrl: "{{ asset('img/') }}",
            autoParagraph: false,
            width: '100%',
        });

	

		$('#modal-form').on('submit', function(e){
			if(!e.isDefaultPrevented()){
				// alert('simpan');
				
				var content = CKEDITOR.instances['edit_tentang'].getData();
				for ( instance in CKEDITOR.instances ) {
					CKEDITOR.instances[instance].updateElement();
				}
				$('#tentang').html(content);
				CKEDITOR.instances['edit_tentang'].setData('');
				$('#modal-form').modal('hide');
				$('.alert-success').css('display', 'block').delay(5000).fadeOut()
				$('.msg-success').empty().append(' Tentang kabupaten berhasil disimpan.');
						
				
				return false;
			}
		});

		$('#form-kabupaten').on('submit', function(e){
			if(!e.isDefaultPrevented()){
				// alert('simpan');
				$('#id_kabupaten').val('1');
				$('#tentang_pemerintahan').val($('#tentang').html());
			
				 url = "{{ route('kabupaten.store') }}";
				$.ajax({
					headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					url: url,
					type: 'post',
					data:  new FormData($("#form-kabupaten")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( response ){						
						$('.msg-success').empty().append(' Profil kabupaten berhasil disimpan.');
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Profil kabupaten gagal disimpan.</strong>');
					}
				});
				return false;
			}
		});

	});

	
	function add_form(){
        save_method = 'add';
        $('.card-title').html('Tentang Pemerintahan');
        $('input[name=_method]').val('POST');
        $('.modal').modal('show');
		$('.modal-open').removeAttr('style');
        $('#modal-form form')[0].reset();
    }
	
</script>
@endsection
