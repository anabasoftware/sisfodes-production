<div class="modal" 
    id="modal-form" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="donation" 
    aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
			<header class="card-header">
				<h2 class="card-title">Tentang Pemerintahan</h2>
			</header>
            <!-- StartForm -->
            <form class="form form-horizontal" 
                data-toggle="validator" 
                role="form" 
                method="post" 
                enctype="multipart/form-data">
                <div class="modal-body">
                    <div class="row">
                        
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="edit_tentang">Deskripsi</label>
                                <textarea class="form-control" id="edit_tentang" name="edit_tentang"></textarea>
                            </div>
						</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary simpan-lembaga" type="submit">
                                <i class="fas fa-save"></i>
                                Simpan
                            </button>
                            <button class="btn btn-default text-right" type="button" data-dismiss="modal">
                                <i class="fas fa-window-close"></i>
                                Keluar
                            </button>
                        </div>
                    </div>
                </div>
            </form>
            <!-- EndForm -->
        </div>
    </div>
</div>