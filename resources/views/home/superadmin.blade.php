@extends('layouts.app')

@section('title')
Dashboard [{{ Auth::user()->user_name }}]
@endsection
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-xl-3">
				<section class="card card-featured-left card-featured-primary mb-3">
					<div class="card-body">
						<div class="widget-summary widget-summary-sm">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-primary">
									<i class="fas fa-globe"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Total Desa</h4>
									<div class="info">
										<strong class="amount">
											{{ \App\Models\User::where('status', 1)->where('role_id','=',2)->count()}}
										</strong><br/>
										<a class="text-muted text-uppercase" href="{{route('manajemen.desa.index')}}">(lihat semua)</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			{{-- <div class="col-xl-3">
				<section class="card card-featured-left card-featured-tertiary mb-3">
					<div class="card-body">
						<div class="widget-summary widget-summary-sm">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-tertiary">
									<i class="fas fa-user"></i>
								</div>
							</div>
						<div class="widget-summary-col">
							<div class="summary">
								<h4 class="title">Desa</h4>
								<div class="info">
									<strong class="amount">RP. 12,312</strong><br/>
									<a class="text-muted text-uppercase" href="#">(lihat detil)</a>
								</div>
							</div>
							</div>
						</div>
					</div>
				</section>
			</div> --}}
		</div>
	</div>
</div>
<div class="row pt-0">
	<div class="col-lg-6">
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>
				<h2 class="card-title">Trafik</h2>
			</header>
			<div class="card-body">
				<div id="ChartistSimpleLineChart" class="ct-chart ct-perfect-fourth ct-golden-section h-25"></div>
			</div>
		</section>
	</div>
	<div class="col-lg-6">
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>
				<h2 class="card-title">Daftar Desa</h2>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-desa">
					<thead>
						<tr>
							<th>Kode</th>
							<th>Nama Desa</th>
							<th>Website</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/chartist/chartist.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
@endsection
@section('javascript')
<script src="{{ asset('vendor/chartist/chartist.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
$(function () {
	if($('#ChartistSimpleLineChart').get(0) ) {
		new Chartist.Line('#ChartistSimpleLineChart', {
			labels: ['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Agu','Sep','Okt','Nov','Des'],
			series: [
				[1,2,1,3,4,5,6,5,4,4,5,6]
			]
		},
		{
			low: 1,
			high: 6,
			showArea: true,
			showLine: true,
			showPoint: true,
			fullWidth: true, 
			chartPadding: {
				right: 60
			},
			axisX: {
			onlyInteger: true,
				showLabel: true,
				showGrid: false
			}
		});
	}
	var myTable = $('#tabel-desa').dataTable({
		"responsive": true,
		language: {
			paginate: {
				previous: "<i class='fas fa-arrow-left'></i>",
				next: "<i class='fas fa-arrow-right'></i>"
			}
		},
		"autoWidth": false,
		"columnDefs": [
			{ "width": "20%", "targets": 0, "orderable": false},
			{ "width": "20%", "targets": 1, "orderable": false},
			{ "width": "20%", "targets": 2, "orderable": false},
			{ "width": "10%", "targets": 3, "orderable": false},
		],
		"ajax": {
			"url": "/dashboard/home/desa/json",
			"type": "GET",
			"serverSide": true
		},
	});
	$('.li-dashboard').addClass('nav-expanded');
	$('.li-home').addClass('nav-active');
});
</script>
@endsection