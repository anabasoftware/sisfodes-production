@extends('layouts.app')

@section('title')
Dashboard [{{ Auth::user()->user_name }}]
@endsection
@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-xl-3">
				<section class="card card-featured-left card-featured-primary mb-3">
					<div class="card-body">
						<div class="widget-summary widget-summary-sm">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-primary">
									<i class="fas fa-life-ring"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Pengaduan</h4>
									<div class="info">
										<strong class="amount">1281</strong><br/>
										<a class="text-muted text-uppercase" href="#">(lihat semua)</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-xl-3">
				<section class="card card-featured-left card-featured-secondary mb-3">
					<div class="card-body">
						<div class="widget-summary widget-summary-sm">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-secondary">
									<i class="fas fa-dollar-sign"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">RAPBD</h4>
									<div class="info">
										<strong class="amount">RP 123,123</strong><br/>
										<a class="text-muted text-uppercase" href="#">(lihat detil)</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-xl-3">
					<section class="card card-featured-left card-featured-tertiary mb-3">
						<div class="card-body">
							<div class="widget-summary widget-summary-sm">
								<div class="widget-summary-col widget-summary-col-icon">
									<div class="summary-icon bg-tertiary">
										<i class="fas fa-shopping-cart"></i>
									</div>
								</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Realisasi</h4>
									<div class="info">
										<strong class="amount">RP. 12,312</strong><br/>
										<a class="text-muted text-uppercase" href="#">(lihat detil)</a>
									</div>
								</div>
								</div>
							</div>
						</div>
					</section>
			</div>
			<div class="col-xl-3">
				<section class="card card-featured-left card-featured-quaternary mb-3">
					<div class="card-body">
						<div class="widget-summary widget-summary-sm">
							<div class="widget-summary-col widget-summary-col-icon">
								<div class="summary-icon bg-quaternary">
									<i class="fas fa-user"></i>
								</div>
							</div>
							<div class="widget-summary-col">
								<div class="summary">
									<h4 class="title">Pendapatan</h4>
									<div class="info">
											<strong class="amount">RP. 123,123</strong><br/>
											<a class="text-muted text-uppercase" href="#">(lihat detil)</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
<div class="row pt-0">
	<div class="col-lg-12">
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss></a>
				</div>
				<h2 class="card-title">Grafik Pendapatan Desa</h2>
			</header>
			<div class="card-body">
				<div id="ChartistSimpleLineChart" class="ct-chart ct-perfect-fourth ct-golden-section h-25"></div>
			</div>
		</section>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/chartist/chartist.min.css') }}"/>
@endsection
@section('javascript')
<script src="{{ asset('vendor/chartist/chartist.js') }}"></script>
<script type="text/javascript">
$(function () {
	if($('#ChartistSimpleLineChart').get(0) ) {
		new Chartist.Line('#ChartistSimpleLineChart', {
			labels: ['Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'],
			series: [
				[1,2,1,3,4,2,5,1,2,4,3,4]
			]
		},
		{
			low: 1,
			high: 6,
			showArea: true,
			showLine: true,
			showPoint: true,
			fullWidth: true, 
			chartPadding: {
				right: 60
			},
			axisX: {
			onlyInteger: true,
				showLabel: true,
				showGrid: false
			}
		});
	}
	$('.li-dashboard').addClass('nav-expanded nav-active');
	$('.li-home').addClass('nav-active');
});
</script>
@endsection