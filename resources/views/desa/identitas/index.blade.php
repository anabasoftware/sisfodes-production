@extends('layouts.app')
@section('title') Identitas Desa @endsection
@section('content_title') Identitas Desa @endsection
@section('breadcrumbs') <li><span>Identitas Desa</span></li> @endsection
@section('content')
<div class="row">
	<div class="col-lg-5 col-xl-5 mb-5 mb-xl-0">
		<section class="card">
			<form class="form form-horizontal" 
				data-toggle="validator" 
				role="form" 
				method="post"
				enctype="multipart/form-data">
				@csrf @method('POST')

			<div class="card-body">
				<label class="label mb-0" data-toggle="tooltip" data-placement="top" title="Ganti foto profil">
					<div class="thumb-info mb-3" style="cursor:pointer">
					 <img src="{{ asset('storage/images/logo/' . Auth::user()->desa->logo) }}" class="rounded img-fluid" alt="John Doe"> 
						<div class="thumb-info-title">
							<span class="thumb-info-inner">Desa {{ Auth::user()->desa->nama_desa}}</span>
							<span class="thumb-info-type">{{ Auth::user()->desa->kecamatan->nama_kecamatan}}</span>
						</div>
					</div>
				</label>
				<div class="form-group row">
					<div class="col-lg-12">
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<div class="input-append">
								<div class="uneditable-input" style="width:70%">
									<i class="fas fa-file fileupload-exists"></i>
									<span class="fileupload-preview"></span>
								</div>
								<span class="btn btn-default btn-file">
									<span class="fileupload-exists"><i class="fas fa-reply"></i></span>
									<span class="fileupload-new"><i class="fas fa-upload"></i></span>
									<input type="file" name="logo" id="logo"/>
								</span>
								<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload"><i class="fas fa-trash"></i></a>
							</div>
						</div>
					</div>
				</div>
				<hr class="dotted short">
				<h5 class="mb-2 mt-3">Tentang</h5>
				<p class="text-2">{!! \App\Models\Sejarah::where('id_desa', Auth::user()->id_desa)->first()->sejarah !!}</p>
				<div class="clearfix">
					<a class="text-uppercase text-muted float-right" href="{{ route('sejarah.index')}}">Detail</a>
				</div>
				<hr class="solid short">
				{{-- <div class="social-icons-list">
					<a rel="tooltip" data-placement="bottom" target="_blank" href="http://www.facebook.com"
						data-original-title="Facebook"><i class="fab fa-facebook-f"></i><span>Facebook</span></a>
					<a rel="tooltip" data-placement="bottom" href="http://www.twitter.com"
						data-original-title="Twitter"><i class="fab fa-twitter"></i><span>Twitter</span></a>
					<a rel="tooltip" data-placement="bottom" href="http://www.linkedin.com"
						data-original-title="Linkedin"><i class="fab fa-linkedin-in"></i><span>Linkedin</span></a>
				</div> --}}

				<div class="form-group">
					<label for="kepala_desa">Kepala Desa</label>
					<input type="text" class="form-control" id="kepala_desa" name="kepala_desa">
					<input type="hidden" class="form-control" id="id_kepala_desa" name="id_kepala_desa">
				</div>
				<div class="form-group">
					<label for="visi">Visi</label>
					<textarea class="form-control" name="visi" id="visi" rows="5"></textarea>
				</div>
				<div class="form-group">
					<label for="misi">Misi</label>							
					<textarea class="form-control" name="misi" id="misi" rows="5"></textarea>
				</div>
			</div>
		</section>
	</div>
	<div class="col-lg-7 col-xl-7">
		@if(session('success'))
		<div class="alert alert-success" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
		</div>
		@endif
		@if ($errors->any())
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>{{ __('Gagal!') }}</strong><br />{{ session('message') }}
			Perbaiki kolom isian berikut ini
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<div class="alert alert-warning alr" style="display:none">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Peringatan!</strong><span class="msg"></span>
		</div>
		<div class="tabs">
			<ul class="nav nav-tabs tabs-primary">
				<li class="nav-item active">
					<a class="nav-link active" href="#identitas" data-toggle="tab">Data</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#akun" data-toggle="tab">Akun</a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="identitas" class="tab-pane active">
					{{-- ================================ --}}
					<input type="hidden" name="id_desa" id="id_desa">
					<input type="hidden" name="kode_desa" id="kode_desa">
					<h4>Detail Desa</h4>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="id_kecamatan">Kecamatan</label>
							<select id="id_kecamatan" name="id_kecamatan" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									$kec = \App\Models\LokasiKec::get();
									foreach ($kec as $key => $val) {
										echo "<option value=\"".$val->id_kecamatan."\">".$val->nama_kecamatan."</option>";
									}
								@endphp
							</select>
						</div>
						<div class="form-group col-md-5">
							<label for="desa_id">Desa</label>
							<select id="desa_id" name="desa_id" data-plugin-selectTwo class="form-control kec">
								<option selected disabled value="">- Pilih -</option>
								@php
									$des = \App\Models\LokasiDesa::get();
									foreach ($des as $key => $val) {
										echo "<option value=\"".$val->desa_id."\">".ucwords($val->nama_desa)."</option>";
									}
								@endphp
							</select>
						</div>
						<div class="form-group col-md-2">
							<label for="kode_pos">Kode pos</label>
							<input type="text" class="form-control" id="kode_pos" name="kode_pos">
						</div>
					</div>
					<div class="form-group">
						<label for="nama_desa">Nama Desa</label>
						<input type="text" class="form-control" id="nama_desa" name="nama_desa">
					</div>
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<input type="text" class="form-control" id="alamat" name="alamat">
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="no_telp">No. Telp</label>
							<input type="text" class="form-control" id="no_telp" name="no_telp">
						</div>
						<div class="form-group col-md-6">
							<label for="fax">Fax</label>
							<input type="text" class="form-control" id="fax" name="fax">
						</div>
					</div>
					<div class="form-group">
						<label for="website">Website</label>
						<input type="text" class="form-control" id="website" name="website">
					</div>
					<div class="form-group">
						<label for="peta_wilayah">Peta Wilayah</label>
						<textarea class="form-control" name="peta_wilayah" id="peta_wilayah" rows="5"></textarea>
					</div>
					<div class="form-group">
						<label for="peta_wilayah_view">Pratinjau Peta</label>
						<div id="peta_wilayah_view"></div>
					</div>
					<hr class="dotted">
					<div class="form-row">
						<div class="col-md-12">
							<button class="btn btn-primary modal-confirm"><i class="fas fa-save"></i>
								Simpan</button>
						</div>
					</div>
				</div>
				</form>
				<div id="akun" class="tab-pane">
					<form class="form-akun form-horizontal" data-toggle="validator" role="form" method="post"
					enctype="multipart/form-data" 
					oninput="re_password.setCustomValidity(re_password.value != password.value ? 'Password tidak sama. Silahkan periksa kembali':'')">
					@csrf @method('POST')
						<input type="hidden" name="id_desa" id="id_desa_pass" value="{{ Auth::user()->id_desa }}">
						<input type="hidden" name="id_user" id="id_user" value="{{ Auth::user()->id_user }}">
						<h4>Login Aplikasi</h4>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="email">Alamat Email</label>
								<input type="email" 
									class="form-control" 
									id="email" 
									name="email" 
									placeholder="Email" 
									value="{{ Auth::user()->email }}">
							</div>
							<div class="form-group col-md-6">
								<label for="user_name">Nama Pengguna</label>
								<input type="text" 
									class="form-control" 
									id="user_name" 
									name="user_name" 
									placeholder="Nama Pengguna" 
									readonly value="{{ Auth::user()->user_name }}">
							</div>
						</div>
						<div class="form-row mb-0">
							<div class="form-group col-md-6">
								<label for="password">Kata sandi baru</label>
								<div class="input-group mb-3">
									<input type="password" 
										id="password"
										name="password"
										class="form-control" 
										placeholder="Password">
								</div>
							</div>
							<div class="form-group col-md-6">
								<label for="re_password">Ulangi kata sandi</label>
								<div class="input-group mb-3">
									<input type="password" 
										id="re_password"
										name="re_password"
										class="form-control" 
										placeholder="Ulangi password">
									<span class="eye input-group-append" style="cursor:pointer" >
										<span class="input-group-text">
											<i class="fa fa-eye-slash lihat_aku" aria-hidden="true"></i>
										</span>
									</span>
								</div>
							</div>
						</div>
						@if (Auth::user()->role_id == '99')
							<em><i class="fas fa-exclamation-circle"></i> Anda dapat menonakttifkan akun jika terjadi masalah.</em>
							<hr class="divider">
							<div class="form-group mb-2">
								<label for="status">Status</label>
								<div class="row">
									<div class="col-lg-12">
										<div class="radio-custom radio-primary form-check-inline mr-4">
											<input type="radio" name="status" id="active" value="Aktif" checked="">
											<label for="active" class=""> Aktif</label>
										</div>
										<div class="radio-custom radio-primary form-check-inline">
											<input type="radio" name="status" id="deactive" value="Tidak Aktif">
											<label for="deactive" class=""> Nonaktifkan</label>
										</div>
									</div>
								</div>
							</div>	
						@endif
						<hr class="dotted mt-0">
						<div class="form-row">
							<div class="col-md-12">
								<button class="btn btn-primary modal-confirm"><i class="fas fa-save"></i> Simpan</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
<style type="text/css">
</style>
@endsection
@section('javascript')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/autosize/autosize.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('visi',{
			language: 'en',
			toolbar: [
					[ 'Undo', 'Redo'],
					[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
					[ 'Bold', 'Italic', 'Strike', '-']
			],
			cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
			cloudServices_uploadUrl: "{{ asset('img/') }}",
			autoParagraph: false,
			width: '100%',
			height: '100',
		})
		CKEDITOR.replace('misi',{
			language: 'en',
			toolbar: [
					[ 'Undo', 'Redo'],
					[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
					[ 'Bold', 'Italic', 'Strike', '-']
			],
			cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
			cloudServices_uploadUrl: "{{ asset('img/') }}",
			autoParagraph: false,
			width: '100%',
			height: '100',
		})
		$('.li-info').addClass('nav-expanded nav-active');
		$('.li-identitas').addClass('nav-active');
		@php 
			$c = \App\Models\Desa::where('id_desa', Auth::user()->id_desa)->count();
			$d = \App\Models\Desa::where('id_desa', Auth::user()->id_desa)->first();
		@endphp
		@if(empty($c))
		$('.form').attr('action', "{{ route('identitas.simpan')}}")
		$('.form-akun').attr('action', "{{ route('pengguna.update', Auth::user()->id_user) }}")
		@else
		$.ajax({
			url: "{{ route('identitas.detail', $d->id_desa) }}",
			type: "get",
			dataType: "json",
			success: function (data) {
				$('#id_desa').val(data.id_desa)
				$('#kode_desa').val(data.kode_desa)
				$('#kepala_desa').val(data.kepala_desa)
				$('#nama_desa').val(data.nama_desa)
				$('#id_kecamatan').val(data.id_kecamatan).trigger("change.select2")
				$('#desa_id').val(data.desa_id).trigger("change.select2")
				$('#id_kepala_desa').val(data.id_kepala_desa)
				$('#alamat').val(data.alamat)
				$('#kode_pos').val(data.kode_pos)
				$('#fax').val(data.fax)
				$('#no_telp').val(data.no_telp)
				$('#website').val(data.website)
				CKEDITOR.instances.visi.setData(data.visi);
				CKEDITOR.instances.misi.setData(data.misi);
				$('#peta_wilayah').val(data.peta_wilayah)
				$('#peta_wilayah_view').html(data.peta_wilayah)
				$('.fileupload-preview').html(data.logo)
			},
			error: function (e) {
				console.log(e)
			}
		});
		$('.form').attr('action', "{{ route('identitas.update', $d->id_desa) }}")
		$('.form-akun').attr('action', "{{ route('pengguna.update', Auth::user()->id_user) }}")
		@endif
		
		$('#id_kecamatan').on('change', function(e){
			// console.log(e);
			var id_kec = e.target.value;
			$.get('identitas/kecamatan/' + id_kec, function(data){
				// console.log(data);
				$('#desa_id').empty();
				$('#desa_id').append('<option value="0" disabled selected>- Pilih -</option>');
				$.each(data, function(index, obj){
					$('#desa_id').append('<option value="'+ obj.desa_id +'">'+ toTitleCase(obj.nama_desa) +'</option>');
				});
			});
		});

		$(".lihat_aku").on('click', function(event) {
			event.preventDefault();
			if($('#password, #re_password').attr("type") == "text"){
				$('#password').attr('type', 'password');
				$('#re_password').attr('type', 'password');
				$('.lihat_aku').addClass( "fa-eye-slash" );
				$('.lihat_aku').removeClass( "fa-eye" );
			}else if($('#password, #re_password').attr("type") == "password"){
				$('#password').attr('type', 'text');
				$('#re_password').attr('type', 'text');
				$('.lihat_aku').removeClass( "fa-eye-slash" );
				$('.lihat_aku').addClass( "fa-eye" );
			}
		});

		$('.alert-success').css('display', 'block').delay(5000).fadeOut()
		$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
	});

	function toTitleCase(str) {
		return str.replace(/\w\S*/g, function(txt) {
			return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
		});
	}

</script>
@endsection
