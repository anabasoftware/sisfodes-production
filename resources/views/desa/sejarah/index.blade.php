@extends('layouts.app')
@section('title') Sejarah Desa @endsection
@section('content_title') Sejarah Desa @endsection
@section('breadcrumbs') <li><span>Sejarah Desa</span></li> @endsection
@section('content')
<form class="form form-horizontal"
	data-toggle="validator"
	role="form"
	method="post"
	enctype="multipart/form-data">
	@csrf @method('POST')
	<input type="hidden" name="id_desa" id="id_desa" value="{{ Auth::user()->id_desa }}">
	<input type="hidden" name="id_sejarah_desa" id="id_sejarah_desa">
	<div class="card">
		<header class="card-header">
			<div class="card-actions">
					<a href="#" class="card-action card-action-toggle" data-card-toggle=""></a>
					<a href="#" class="card-action card-action-dismiss" data-card-dismiss=""></a>
			</div>
			<p class="card-subtitle">
					Berikut ini adalah Form sejarah desa.
			</p>
		</header>
		<div class="card-body">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<textarea class="form-control" id="sejarah" name="sejarah"></textarea>
					</div>
				</div>
			</div>
		</div>
		<footer class="card-footer">
			<div class="row">
				<div class="col-md-12">
					<button class="btn btn-primary simpan-lembaga" type="submit">
						<i class="fas fa-save"></i>
						Simpan
					</button>
					<a class="btn btn-default text-right" href="{{ route('identitas.index') }}">
						<i class="fas fa-window-close"></i>
						Tutup
					</a>
				</div>
			</div>
		</footer>
</div>
</form>
@endsection
@section('javascript')
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$(function () {
		CKEDITOR.replace('sejarah',{
			language: 'en',
			toolbar: [
					[ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
					[ 'Link', 'Unlink' ],
					[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
					[ 'Table', 'HorizontalRule' ],
					[ 'Bold', 'Italic', 'Strike', '-', 'NumberedList', 'BulletedList' ],
					[ 'Source', '-', 'Maximize']
			],
			cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
			cloudServices_uploadUrl: "{{ asset('img/') }}",
			autoParagraph: false,
			width: '100%',
			height: '260',
		})

		$('.li-info').addClass('nav-expanded nav-active')
		$('.li-sejarah').addClass('nav-active')

		"{{ $c = \App\Models\Sejarah::count() }}"
		@if (empty($c))
			// alert('kosong')
			$('.form').attr('action', "{{ route('sejarah.simpan') }}")
		@else
			// alert('isi')
			"{{ $d = \App\Models\Sejarah::first(['id_sejarah_desa']) }}"
			$.ajax({
				url: "{{ route('sejarah.detail', $d->id_sejarah_desa) }}",
				type: 'get',
				dataType: 'json',
				success: function (data) {
					$('#id_desa').val(data.id_desa);
					$('#sejarah').val(data.sejarah);
				},
				error: function (e) {
					console.log(e);
				}
			});
			$('.form').attr('action', "{{ route('sejarah.update', $d->id_sejarah_desa) }}")
		@endif
	});
</script>
@endsection
