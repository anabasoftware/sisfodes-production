@extends('layouts.app')
@section('title') Wilayah Desa @endsection
@section('content_title') Wilayah Desa @endsection
@section('breadcrumbs') <li><span>Wilayah Desa</span></li> @endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Gagal!') }}</strong><br />{{ session('message') }}. Perbaiki kolom isian berikut ini
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <section class="card">
            <div class="card-header">Berikut ini adalah formulir isian tentang detail wilayah desa.</div>
            <div class="card-body">
                <form class="form form-horizontal" data-toggle="validator" role="form" method="post"
                    enctype="multipart/form-data">
                    @csrf @method('POST')
                    <div class="form-group">
                        <label for="nama_desa">Nama Desa</label>
                        <input type="hidden" class="form-control" id="id_desa" name="id_desa"
                            value="{{ Auth::user()->id_desa }}">
                        <input type="hidden" class="form-control" id="id_wilayah" name="id_wilayah">
                        <input type="text" class="form-control" id="nama_desa" name="nama_desa" disabled
                            value="{{ Auth::user()->desa->nama_desa }}">
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="luas_wilayah_tanah">Luas Wilayah Tanah</label>
                            <input type="number" class="form-control" id="luas_wilayah_tanah" name="luas_wilayah_tanah">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="luas_daratan">Luas Dataran</label>
                            <input type="number" class="form-control" id="luas_daratan" name="luas_daratan">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="luas_persawahan">Luas Persawahan</label>
                            <input type="number" class="form-control" id="luas_persawahan" name="luas_persawahan">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="luas_tegalan">Luas Tegalan</label>
                            <input type="number" class="form-control" id="luas_tegalan" name="luas_tegalan">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="luas_tanah_desa">Luas Tanah Desa</label>
                            <input type="number" class="form-control" id="luas_tanah_desa" name="luas_tanah_desa">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="luas_lain_lain">Luas Lain-lain</label>
                            <input type="number" class="form-control" id="luas_lain_lain" name="luas_lain_lain">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="jumlah_dusun">Jumlah Dusun</label>
                            <input type="number" class="form-control" id="jumlah_dusun" name="jumlah_dusun">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="jumlah_rt">Jumlah RT</label>
                            <input type="number" class="form-control" id="jumlah_rt" name="jumlah_rt">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="jumlah_rw">Jumlah RW</label>
                            <input type="number" class="form-control" id="jumlah_rw" name="jumlah_rw">
                        </div>
                    </div>
                    <hr class="dotted">
                    <div class="form-row">
                        <div class="col-md-12 text-left mt-3">
                            <button class="btn btn-primary modal-confirm"><i class="fas fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
    $(function () {
        $('.li-info').addClass('nav-expanded nav-active');
        $('.li-wilayah').addClass('nav-active');

        "{{ $c = \App\Models\Wilayah::where('id_desa', Auth::user()->id_desa)->count() }}"
        @if(empty($c))
        // var c = "$c"
        // alert(c)
        $('.form').attr('action', "{{ route('wilayah.simpan') }}")
        @else
		// alert('isi')
		"{{ $d = \App\Models\Wilayah::first(['id_wilayah']) }}"
        $.ajax({
            url: "{{ route('wilayah.detail', $d->id_wilayah) }}",
            type: 'get',
            dataType: 'json',
            success: function (data) {
                $('#luas_wilayah_tanah').val(data.luas_wilayah_tanah)
                $('#luas_daratan').val(data.luas_daratan)
                $('#luas_persawahan').val(data.luas_persawahan)
                $('#luas_tegalan').val(data.luas_tegalan)
                $('#luas_tanah_desa').val(data.luas_tanah_desa)
                $('#luas_lain_lain').val(data.luas_lain_lain)
                $('#jumlah_dusun').val(data.jumlah_dusun)
                $('#jumlah_rt').val(data.jumlah_rt)
                $('#jumlah_rw').val(data.jumlah_rw)
            },
            error: function (e) {
                console.log(e);
            }
        });
        $('.form').attr('action', "{{ route('wilayah.update', $d->id_wilayah) }}")
        @endif
        $('.alert-success').css('display', 'block').delay(5000).fadeOut()
        $('.alert-danger').css('display', 'block').delay(5000).fadeOut()
    });

</script>
@endsection
