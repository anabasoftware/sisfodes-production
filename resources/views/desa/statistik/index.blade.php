@extends('layouts.app')
@section('title') Statistik Desa @endsection
@section('content_title') Statistik Desa @endsection
@section('breadcrumbs')<li><span>Statistik Desa</span></li>@endsection
@section('content')
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<style type="text/css">
	th.dt-center, td.dt-center, td.dt-body-center { text-align: center; }
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<section class="card">

			<div class="alert alert-success" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
				<strong>Berhasil!</strong><span class="msg-success"></span>
			</div>

			<div class="alert alert-danger" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
				<strong>Gagal!</strong><span class="msg-danger"></span>
			</div>

			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" 
							class="btn btn-success btn-sm" 
							data-toggle="tooltip" 
							data-placement="top"
							onclick="view_form('{{route('statistik.tambah')}}','Tambah Data','Simpan','modal-lg','add')" 
							data-original-title="Tambah statistik desa">
							<i class="fas fa-plus"></i>
						</button>
						<button type="button" 
							class="btn btn-success btn-sm card-action-toggle"
							data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
				<p class="card-subtitle">Berikut ini adalah daftar statistik desa atau daerah yang telah tersimpan dan ditampilkan di halaman website.</p>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-statistik">
					<thead>
						<tr>
							<th rowspan="2" class="align-middle text-center">Aksi</th>
							<th rowspan="2" class="align-middle">Tanggal</th>
							<th colspan="9" class="text-center">Jumlah</th>
							<th rowspan="2" class="align-middle">Operator</th>
						</tr>
						<tr>
							<th style="width:60px">Kk</th>
							<th style="width:60px">Penduduk</th>
							<th style="width:60px">Pria</th>
							<th style="width:60px">Wanita</th>
							<th style="width:60px">Balita</th>
							<th style="width:60px">Anak</th>
							<th style="width:60px">Remaja</th>
							<th style="width:60px">Dewasa</th>
							<th style="width:60px">Ortu</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
	$(function () {
		$('#tabel-statistik').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "10%", "targets": 0, "orderable": false, "className": "dt-body-center" },
				{ "targets": 1, "orderable": false, "className": "dt-body-right" },
			],
			"ajax": {
				"url": "{{ route('statistik.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});

		$('.li-info').addClass('nav-expanded nav-active');
		$('.li-statistik').addClass('nav-active');
		$('html').addClass('sidebar-left-collapsed');
	});
	
</script>
@endsection