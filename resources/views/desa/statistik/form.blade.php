<form class="form form-horizontal" data-toggle="validator" role="form" method="post" enctype="multipart/form-data">
    <div class="modal-body">
        @csrf @method('POST')
        <input type="text" name="id_statistik_desa" id="id_statistik_desa">
        <input type="text" name="save_method" id="save_method">
        <input type="text" name="kode_wilayah" id="kode_wilayah" value="{{ Auth::user()->desa->kode_wilayah }}">
        <input type="text" name="id_desa" id="id_desa" value="{{ Auth::user()->id_desa }}">
        <div class="row">
            <div class="form-group col-md-4">
                <label for="tgl_statistik">Tanggal Statistik</label>
                <input class="form-control" data-plugin-datepicker id="tgl_statistik" name="tgl_statistik" value="{{date('m/d/Y')}}">
            </div>
            <div class="form-group col-md-8">
                <label for="nama_desa">Nama Desa</label>
                @php
                $id = Auth::user()->id_desa;
                $desa = \App\Models\Desa::where('id_desa', $id)->first(['nama_desa', 'id_desa']) @endphp
                @if (!empty($desa))
                    <input type="text" class="form-control" id="nama_desa" name="nama_desa" disabled
                        value="{{ $desa->nama_desa }}">
                @else
                    <input type="text" class="form-control" id="nama_desa" name="nama_desa" disabled
                        value="{{ Auth::user()->desa->nama_desa }}">
                @endif
            </div>
            <div class="form-group col-md-6">
                <label for="jumlah_kk">Jumlah Kepala Keluarga</label>
                <input type="number" class="form-control" id="jumlah_kk" name="jumlah_kk">
            </div>
            <div class="form-group col-md-6">
                <label for="jumlah_penduduk">Jumlah Penduduk</label>
                <input type="number" class="form-control" id="jumlah_penduduk" name="jumlah_penduduk">
            </div>
            <div class="form-group col-md-6">
                <label for="jumlah_laki_laki">Jumlah Laki-laki</label>
                <input type="number" class="form-control" id="jumlah_laki_laki" name="jumlah_laki_laki">
            </div>
            <div class="form-group col-md-6">
                <label for="jumlah_perempuan">Jumlah Perempuan</label>
                <input type="number" class="form-control" id="jumlah_perempuan" name="jumlah_perempuan">
            </div>
            <div class="form-group col-md-4">
                <label for="balita">Balita</label>
                <input type="number" class="form-control" id="balita" name="balita">
            </div>
            <div class="form-group col-md-4">
                <label for="anak">Anak-anak</label>
                <input type="number" class="form-control" id="anak" name="anak">
            </div>
            <div class="form-group col-md-4">
                <label for="remaja">Remaja</label>
                <input type="number" class="form-control" id="remaja" name="remaja">
            </div>
            <div class="form-group col-md-6">
                <label for="dewasa">Dewasa</label>
                <input type="number" class="form-control" id="dewasa" name="dewasa">
            </div>
            <div class="form-group col-md-6">
                <label for="orang_tua">Orang Tua</label>
                <input type="number" class="form-control" id="orang_tua" name="orang_tua">
            </div>
        </div>
    </div>
</form>
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}"/>
<script type="text/javascript">
    $(document).ready(function(id) {
        var id = $('#id_statistik_desa').val()
        $.ajax({
            url: 'statistik/detail/json/' + id,
            type: 'get',
            dataType: 'json',
            success: function(data) {
                $('#id_statistik_desa').val(data.id_statistik_desa)
                $('#tgl_statistik').val(data.tgl_statistik)
                $('#jumlah_kk').val(data.jumlah_kk)
                $('#jumlah_penduduk').val(data.jumlah_penduduk)
                $('#jumlah_laki_laki').val(data.jumlah_laki_laki)
                $('#jumlah_perempuan').val(data.jumlah_perempuan)
                $('#balita').val(data.balita)
                $('#anak').val(data.anak)
                $('#remaja').val(data.remaja)
                $('#dewasa').val(data.dewasa)
                $('#orang_tua').val(data.orang_tua)
            },
            error: function(data) {
                // alert('gagal\n' + data.responseText)
                console.log(data)
            }
        });
        $('.b-action').on('click', function(e) {
            if (!e.isDefaultPrevented()) {
                var id = $('#id_statistik_desa').val();
                var save_method = $('#save_method').val();
                if (save_method == "add") url = "{{ route('statistik.simpan') }}";
                else url = "statistik/update/" + id;
                $.ajax({
                    url: url,
                    type: 'post',
                    data: new FormData($(".form")[0]),
                    async: true,
                    processData: false,
                    contentType: false,
                    success: function() {
                        $('#modal-form').modal('hide');
                        $('.alert-success').css('display', 'block').delay(6000).fadeOut()
                        $('.msg-success').empty().append(' Statistik Desa berhasil disimpan.');
						$('#tabel-statistik').DataTable().ajax.reload();
                    },
                    error: function(response) {
                        var errors = $.parseJSON(response.responseText);
                        var errorMessage = '';
                        $.each(errors.msg, function(key, value) {
                            errorMessage += '<br/> - ' + value;
                        });
                        $('.alert-danger').css('display', 'block').delay(6000).fadeOut()
                        $('.msg-danger').empty().append(errorMessage + '<br/> Statistik Desa gagal disimpan.');
                        $('#modal-form').modal('hide');
                    }
                });
                return false;
            }
        });
    });
    if ($.isFunction($.fn['bootstrapDP'])) {
        $(function() {
            $('[data-plugin-datepicker]').each(function() {
                var $this = $(this),
                    opts = {};
                var pluginOptions = $this.data('plugin-options');
                if (pluginOptions)
                    opts = pluginOptions;
                $this.themePluginDatePicker(opts);
            });
        });
    }
</script>
