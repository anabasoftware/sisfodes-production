<form class="form form-horizontal"
    data-toggle="validator"
    role="form"
    method="post"
    enctype="multipart/form-data">
    @csrf @method('POST')
    <input type="text" name="id_potensi" id="id_potensi">
    <input type="text" name="save_method" id="save_method">
    <input type="text" name="id_desa" id="id_desa" value="{{ Auth::user()->id_desa }}">
    <div class="form-input">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="form-control-label" for="nama_potensi">Nama Potensi</label>
                    <input class="form-control" type="text" id="nama_potensi" name="nama_potensi" value="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="slug_potensi">Slug</label>
                    <input type="text" class="form-control" id="slug_potensi" name="slug_potensi" value="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="form-control-label" for="deskripsi_potensi">Deskripsi</label>
                    <textarea class="form-control" id="deskripsi_potensi" name="deskripsi_potensi"></textarea>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="form-control-label" for="gambar">Gambar</label>
                    <input class="form-control" type="file" name="gambar"/>
                    <small>[Ukuran 300x200]</small>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label class="form-control-label">Preview</label><br/>
                    <img src="{{asset('img/no-image.png')}}" class="img-fluid img-thumbnail img-preview" style="width:150px;height:150px">
                </div>
            </div>
        </div>
    </div>
</form>
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<script>
    $(document).ready(function(id){
		slugify('#nama_potensi', '#slug_potensi');
		var editor = CKEDITOR.replace('deskripsi_potensi',{
			language: 'en',
			toolbar: [
				['Undo','Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
				['Link','Unlink' ],
				['EasyImageUpload','Table', 'HorizontalRule' ],
				['Bold','Italic', 'Strike', '-', 'NumberedList', 'BulletedList' ],
				['Source','Maximize'],
				['About']
			],
			cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
			cloudServices_uploadUrl: "{{ asset('img/') }}",
			autoParagraph: false,
			width: '100%',
		});
		var id = $('#id_potensi').val();
		$.ajax({
			url: 'potensi/detail/json/' + id ,
			type: 'get',
			dataType: 'json',
			success: function( data ){
				$('#id_potensi').val(data.id_potensi);
				$('#slug_potensi').val(data.slug_potensi);
				$('#nama_potensi').val(data.nama_potensi);
				editor.on( "instanceReady", function( event ){
					editor.setData(data.deskripsi_potensi);
				});
				// CKEDITOR.instances.deskripsi_potensi.setData(data.deskripsi_potensi);
				if(data.gambar != null && data.gambar != 'default.png'){
					$('.img-preview').attr('src','/{{env("IMAGES_PATH")}}potentions/{{\Auth::user()->id_desa}}/'+data.gambar)
				}else{
					$('.img-preview').attr('src','{{asset("img/no-image.png")}}')
				}
			},
			error: function( data ){
				console.log(data)
			}
		});
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_potensi').val();
				var save_method = $('#save_method').val();
				if(save_method == "add") url = "{{ route('potensi.simpan') }}";
				else url = "potensi/update/"+id;
				for ( instance in CKEDITOR.instances ) {
					CKEDITOR.instances[instance].updateElement();
				}
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append(' Potensi Desa berhasil disimpan.');
						CKEDITOR.instances.deskripsi_potensi.setData('');
						$('#tabel-potensi').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/>Potensi Desa gagal disimpan.');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});
    });
</script>