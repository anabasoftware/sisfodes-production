@extends('layouts.app')
@section('title') Potensi Desa @endsection
@section('content_title') Potensi Desa @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<style type="text/css">
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<section class="card">

			<div class="alert alert-success" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
				<strong>Berhasil!</strong><span class="msg-success"></span>
			</div>

			<div class="alert alert-danger" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
				<strong>Gagal!</strong><span class="msg-danger"></span>
			</div>

			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" 
							class="btn btn-success btn-sm" 
							data-toggle="tooltip" 
							data-placement="top"
							onclick="view_form('{{route('potensi.tambah')}}','Tambah Data','Simpan','modal-lg','add')" 
							data-original-title="Tambah Potensi Daerah">
							<i class="fas fa-plus"></i>
						</button>
						<button type="button" 
							class="btn btn-success btn-sm card-action-toggle"
							data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
				<p class="card-subtitle">Berikut ini adalah daftar potensi desa atau daerah yang telah tersimpan dan ditampilkan di halaman website.</p>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-potensi">
					<thead>
						<tr>
							<th class="text-center">Aksi</th>
							<th>Nama Potensi</th>
							<th>Deskripsi</th>
							<th>Images</th>
							<th>Penulis</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center">
								<div class="btn-group flex-wrap">
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-edit"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-trash"></i></button>
								</div>
							</td>
							<td>Taman Berseri</td>
							<td>Taman bermain alami, lengkap dengan sungai bebatuan khas ...</td>
							{{-- <td><button class="btn btn-primary btn-sm">Ya</button></td> --}}
							<td>Administrator</td>
						</tr>
						<tr>
							<td class="text-center">
								<div class="btn-group flex-wrap">
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-edit"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-trash"></i></button>
								</div>
							</td>
							<td>Taman Berseri</td>
							<td>Taman bermain alami, lengkap dengan sungai bebatuan khas ...</td>
							{{-- <td><button class="btn btn-secondary btn-sm">Tidak</button></td> --}}
							<td>Administrator</td>
						</tr>
						<tr>
							<td class="text-center">
								<div class="btn-group flex-wrap">
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-edit"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-trash"></i></button>
								</div>
							</td>
							<td>Taman Berseri</td>
							<td>Taman bermain alami, lengkap dengan sungai bebatuan khas ...</td>
							{{-- <td><button class="btn btn-primary btn-sm">Ya</button></td> --}}
							<td>Administrator</td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>
	</div>
</div>

@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
@endsection
@section('javascript')
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$(function () {
		$('#tabel-potensi').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "10%", "targets": 0, "className": "text-center" },
				{ "width": "26%", "targets": 1},
				{ "width": "5%", "targets": 3},
				{ "orderable": false, target:[0,1,3]}
			],
			"ajax": {
				"url": "{{ route('potensi.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});
		$('.li-info').addClass('nav-expanded nav-active');
		$('.li-potensi').addClass('nav-active');
	});
	
	// function delete_data(id) {
	// 	if (confirm("Apakah yakin akan menghapus Data?")) {
	// 		$.ajax({
	// 			url: "potensi/hapus/"+id,
	// 			type: "POST",
	// 			data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
	// 			success: function ( data ) {
	// 				$('.alert-success').css('display', 'block').delay(5000).fadeOut()
	// 				$('.msg-success').empty().append(' ' + data.msg);
	// 				CKEDITOR.instances['deskripsi_potensi'].setData('');
	// 				$('.table').DataTable().ajax.reload();
	// 			},
	// 			error: function ( response ) {
	// 				var errors = $.parseJSON(response.responseText);
	// 				$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
	// 				$('.msg-danger').empty().append(response.msg);
	// 				$('#modal-form').modal('hide');
	// 			}
	// 		});
	// 	}
	// }

	// function detail_data(id){
	// 	save_method = 'detail';
	// 	$('.card-title').html('Detail Data');
	// 	$('input[name=_method]').val('PATCH');
	// 	$('#modal-form').modal('show');
	// 	$('.modal-open').removeAttr('style');
	// }
</script>
@endsection
