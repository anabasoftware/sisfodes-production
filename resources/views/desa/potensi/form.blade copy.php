{{-- <div class="modal"
    id="modal-form"
    tabindex="-1"
    role="dialog"
    aria-labelledby="donation"
    aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document"> --}}
        {{-- <div class="modal-content"> --}}
			{{-- <header class="card-header">
				<h2 class="card-title">Data Baru</h2>
			</header> --}}
            <!-- StartForm -->
            <form class="form form-horizontal"
                data-toggle="validator"
                role="form"
                method="post"
                enctype="multipart/form-data">
                @csrf @method('POST')
                <input type="hidden" name="id_potensi" id="id_potensi">
                <input type="hidden" name="id_desa" id="id_desa" value="{{ Auth::user()->id_desa }}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="nama_potensi">Nama Potensi</label>
								<input class="form-control" type="text" id="nama_potensi" name="nama_potensi">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="slug_potensi">Slug</label>
                                <input type="text" class="form-control" id="slug_potensi" name="slug_potensi">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="deskripsi_potensi">Deskripsi</label>
                                <textarea class="form-control" id="deskripsi_potensi" name="deskripsi_potensi"></textarea>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label" for="gambar">Gambar</label>
                                <input class="form-control" type="file" name="gambar"/>
                                <small>[Ukuran 300x200]</small>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-control-label">Preview</label><br/>
                                <img src="{{asset('img/no-image.png')}}" class="img-fluid img-thumbnail img-preview" style="width:150px;height:150px">
                            </div>
                        </div>
                        {{--
                        <div class="col-md-4">
                            <div class="form-group">
								<label class="form-control-label" for="status">Terbitkan</label>
								<select data-plugin-selectTwo class="form-control select2">
									<option value="true">Ya</option>
									<option value="false">Tidak</option>
								</select>
                            </div>
                        </div>
                        --}}
                    </div>
                </div>
                {{-- <div class="modal-footer">
                    <div class="row">
                        <div class="col-md-12">
                            <button class="btn btn-primary simpan-potensi" type="submit">
                                <i class="fas fa-save"></i>
                                Simpan
                            </button>
                            <button class="btn btn-default text-right" type="button" data-dismiss="modal">
                                <i class="fas fa-window-close"></i>
                                Keluar
                            </button>
                        </div>
                    </div>
                </div> --}}
            </form>
            <!-- EndForm -->
        {{-- </div> --}}
    {{-- </div>
</div> --}}