<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<input type="text" name="id_lembaga" id="id_lembaga">
    <input type="text" name="save_method" id="save_method">
	<input type="text" name="kode_wilayah" id="kode_wilayah" value="{{ Auth::user()->desa->kode_wilayah }}">
	<input type="text" name="id_desa" id="id_desa" value="{{ Auth::user()->id_desa }}">
	<div class="form-input">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-control-label" for="nama_lembaga">Nama Lembaga</label>
					<input class="form-control" type="text" id="nama_lembaga" name="nama_lembaga">
				</div>
			</div>
			
			<div class="col-md-12">
				<div class="form-group">
					<label class="form-control-label" for="deskripsi_lembaga">Deskripsi</label>
					<textarea class="form-control" id="deskripsi_lembaga" name="deskripsi_lembaga"></textarea>
				</div>
			</div>
		</div>
	</div>
</form>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script>
    $(document).ready(function(id){
		var editor = CKEDITOR.replace('deskripsi_lembaga',{
			language: 'en',
			toolbar: [
				['Undo','Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
				['Link','Unlink' ],
				['EasyImageUpload','Table', 'HorizontalRule' ],
				['Bold','Italic', 'Strike', '-', 'NumberedList', 'BulletedList' ],
				['Source','Maximize'],
				['About']
			],
			cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
			cloudServices_uploadUrl: "{{ asset('img/') }}",
			autoParagraph: false,
			width: '100%',
		});
		var id = $('#id_lembaga').val();
		// alert(id)
		$.ajax({
			url: 'lembaga/detail/json/' + id ,
			type: 'get',
			dataType: 'json',
			success: function( data ){
				$('#id_lembaga').val(data.id_lembaga);
				$('#nama_lembaga').val(data.nama_lembaga);
				$('#nama_lembaga').val(data.nama_lembaga);
				editor.on( "instanceReady", function( event ){
					editor.setData(data.deskripsi_lembaga);
				});
			},
			error: function( data ){
				console.log(data)
			}
		});
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_lembaga').val();
				var save_method = $('#save_method').val();
				if(save_method == "add") url = "{{ route('lembaga.simpan') }}";
				else url = "lembaga/update/"+id;
				for ( instance in CKEDITOR.instances ) {
					CKEDITOR.instances[instance].updateElement();
				}
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Lembaga Desa berhasil disimpan.');
						CKEDITOR.instances.deskripsi_lembaga.setData('');
						$('#tabel-lembaga').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Lembaga Desa gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});
    });
</script>