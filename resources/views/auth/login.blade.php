@extends('layouts.app')
@section('title') Halaman Login @endsection
@section('content')
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="/" class="logo float-left">
            <img src="img/isifodes-logo.png" height="54" alt="Sistem Informasi Desa" />
        </a>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="panel card-sign">
                <div class="card-title-sign mt-3 text-right">
                    <h2 class="title text-uppercase font-weight-bold m-0"><i class="fas fa-user mr-1"></i> Masuk
                        Aplikasi</h2>
                </div>
                <div class="card-body">
                    <div class="form-group mb-3">
                        <label>Nama pengguna</label>
                        <div class="input-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email"
                                autofocus class="form-control form-control-lg" />
                            <span class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fas fa-user"></i>
                                </span>
                            </span>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group mb-3">
                        <div class="clearfix">
                            <label class="float-left">Kata sandi</label>
                            {{-- @if (Route::has('password.request'))
                            <a href="{{ route('password.request') }}" class="float-right">Lupa kata sandi ?</a>
                            @endif --}}
                        </div>
                        <div class="input-group">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"
                                class="form-control form-control-lg" />
                            <span class="input-group-append">
                                <span class="input-group-text">
                                    <i class="fas fa-lock"></i>
                                </span>
                            </span>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember"
                                    {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember">Ingatkan saya</label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary mt-2">Masuk</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <p class="text-center text-muted mt-3 mb-3">&copy; Copyright 2019 - {{ date('Y') }}. Hak cipta dilindungi oleh
            Undang-undang.</p>
    </div>
</section>
<!-- end: page -->
@endsection
