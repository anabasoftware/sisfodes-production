@extends('layouts.app')
@section('title') Berita @endsection
@section('breadcrumbs') <li><a href="{{ route('website.berita') }}">Berita</a></li> @endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <section class="card">
				@if(session('status'))
				<div class="alert alert-success" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
				</div>
				@endif
				@if ($errors->any())
				<div class="alert alert-danger">
					<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					<strong>{{ __('Gagal!') }}</strong><br />{{ session('message') }}. Perbaiki kolom isian berikut ini
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
                <header class="card-header">
                    <div class="card-actions">
                        <div class="btn-group flex-wrap">
                            <button type="button" class="btn btn-success btn-sm" data-toggle="tooltip" data-placement="left"
                                onclick="location.href = '{{ route('berita.tambah') }}'" data-original-title="Tambah Berita">
                                <i class="fas fa-plus"></i>
                            </button>
                            <button type="button" class="btn btn-success btn-sm card-action-toggle" data-card-toggle>
                            </button>
                        </div>
                    </div>
                    <h3 class="card-title">Daftar Berita</h3>
                    <p class="card-subtitle">Berikut ini adalah daftar berita / artikel yang telah tersimpan dan ditampilkan
                        di halaman website.</p>
                </header>
                <div class="card-body">
                    <table class="table table-bordered table-striped table-sm mb-0" id="tabel-berita">
                        <thead>
                            <tr>
                                <th class="text-center">Aksi</th>
                                <th>Judul</th>
                                <th>Penulis</th>
                                <th>Kategori</th>
                                {{-- <th>Tag</th> --}}
                                <th>Dibuat</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    <div class="btn-group flex-wrap">
                                        <button type="button" class="btn btn-default btn-sm">
											<i class="fas fa-edit"></i></button>
                                        <button type="button" class="btn btn-default btn-sm">
											<i class="fas fa-trash"></i></button>
                                    </div>
                                </td>
                                <td>Rapat koordinasi BUMDES 2020</td>
                                <td>Admin</td>
                                <td>Ekonomi</td>
                                {{-- <td>
									<span class="badge badge-secondary">ekonomi</span>
									<span class="badge badge-secondary">sosial</span>
									<span class="badge badge-secondary">budaya</span>
								</td> --}}
                                <td>Senin, 23 November 2020</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </section>
        </div>
    </div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
@endsection
@section('javascript')
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$(function() {
		$('#tabel-berita').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "10%", "targets": 0, "className": "text-center" },
				{ "width": "40%", "targets": 1},
				{ "width": "10%", "targets": 3},
				{ "width": "15%", "targets": 4},
				{ "orderable": false, "target":[0,1,3]}
			],
			"ajax": {
				"url": "{{ route('berita.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});
		$('.li-website').addClass('nav-expanded nav-active');
		$('.li-berita').addClass('nav-expanded nav-active');
		$('.li--berita').addClass('nav-active');
	});
</script>
@endsection
