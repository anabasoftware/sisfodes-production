@extends('layouts.app')
@section('title') Banner @endsection
@section('breadcrumbs') 
<li><a href="{{route('website.berita')}}">Berita</a></li> 
<li><span>Banner</span></li> 
@endsection
@section('content')
<div class="row">
	<div class="col-md-4">
        @if(session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
        </div>
		@endif
        <section class="card">
            <div class="card-body">
				<form class="form form-horizontal" data-toggle="validator" role="form" method="post" enctype="multipart/form-data" action="@if(empty($data->id_banner)){{route('website.berita.banner.simpan')}}@else{{route('website.berita.banner.update',$data->id_banner)}} @endif">
					@csrf @method('POST')
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="nama_desa">Nama Desa</label>
							<input type="hidden" class="form-control" id="id_desa" name="id_desa"
								value="{{ Auth::user()->id_desa }}">
							<input type="text" class="form-control" id="nama_desa" name="nama_desa" disabled
								value="{{ Auth::user()->desa->nama_desa }}">
						</div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nama_banner">Nama Banner</label>
							<input type="text" class="form-control" id="nama_banner" name="nama_banner" value="@if(!empty($data->id_banner)){{$data->nama_banner}}@elseif(!empty(old('nama_banner'))){{old('nama_banner')}}@else{{old('nama_banner')}}@endif">
							@if($errors->has('nama_banner'))
							<div class="text-danger">
								{{ $errors->first('nama_banner')}}
							</div>
							@endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
							<label for="deskripsi_banner">Deskripsi Layanan</label>
							<textarea class="form-control" name="deskripsi_banner" id="deskripsi_banner">@if(!empty($data->id_banner)){{$data->deskripsi_banner}}@elseif(!empty(old('deskripsi_banner'))){{old('deskripsi_banner')}}@else{{old('deskripsi_banner')}}@endif</textarea>
							@if($errors->has('deskripsi_banner'))
							<div class="text-danger">
								{{ $errors->first('deskripsi_banner')}}
							</div>
							@endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="status">Status</label>
							<div class="row">
								<div class="col-lg-12">
									<div class="radio-custom radio-primary form-check-inline mr-5">
										<input type="radio" id="aktif" name="status" value="1" @if(!empty($data->id_banner)){{($data->status == 1)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif checked>
										<label for="aktif">Aktif</label>
									</div>
									<div class="radio-custom radio-primary form-check-inline">
										<input type="radio" id="tidak_aktif" name="status" value="0" @if(!empty($data->id_banner)){{($data->status == 0)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif>
										<label for="tidak_aktif">Tidak Aktif</label>
									</div>
								</div>
							</div>
                        </div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-12 col-12">
							<label for="gambar">Gambar</label>
							<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="input-append">
									<div class="uneditable-input">
										<i class="fas fa-file fileupload-exists"></i>
										<span class="fileupload-preview"></span>
									</div>
									<span class="btn btn-default btn-file">
										<span class="fileupload-exists"><i class="fas fa-sync-alt"></i></span>
										<span class="fileupload-new"><i class="fas fa-file-image"></i></span>
										<input type="file" name="gambar"/>

										@if($errors->has('gambar'))
										<div class="text-danger">
											{{ $errors->first('gambar')}}
										</div>
										@endif
									</span>
									<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">
										<i class="fas fa-trash"></i>
									</a>
								</div>
							</div>
						</div>
					</div>
                    <hr class="solid">
                    <div class="form-row">
                        <div class="col-md-12 text-left">
							@if(empty($data->id_banner)) 
							<button type="button" class="btn btn-default" onclick="history.back(-1)"><i class="fas fa-reply"></i> Kembali</button>
							@else
							<button type="reset" class="btn btn-default"><i class="fas fa-window-close"></i> Batal</button>
							@endif
                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
	</div>
	<div class="col-md-8">
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" 
							class="btn btn-success btn-xs card-action-toggle"
							data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-banner">
					<thead>
						<tr>
							<th class="text-center">Aksi</th>
							<th>Status</th>
							<th nowrap>Judul Banner</th>
							<th>Keterangan</th>
							<th>Gambar</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
@endsection
@section('javascript')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<script type="text/javascript">
	$(function () {
		$('#tabel-banner').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "15%", "targets": 0, "orderable": false, "className": "text-center dt-body-right" },
				{ "width": "9%", "targets": 1, "orderable": false},
				{ "width": "20%", "targets": 2, "orderable": false},
				{ "width": "30%", "targets": 3, "orderable": false},
				{ "width": "15%", "targets": 4, "orderable": false}	
			],
			"ajax": {
				"url": "{{ route('website.berita.banner.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});
		
		$('.li-website').addClass('nav-expanded nav-active');
		$('.li-banner').addClass('nav-active');
	});

    function delete_data(id) {
        if (confirm("Apakah yakin akan menghapus Data?")) {
            $.ajax({
                url: "banner/hapus/"+id,
                type: "GET",
                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success: function (data) {
                    window.location.href = '{{ route('website.berita.banner') }}'
                },
                error: function () {    
                    alert("Error: Tidak dapat menghapus Data!");
                }
            });
        }
    }
</script>
@endsection