@extends('layouts.app')
@section('title') Sub Kategori @endsection
@section('breadcrumbs') 
<li><span>Berita</span></li> 
<li><span>Kategori</span></li> 
<li><span>Sub</span></li> 
@endsection
@section('content')
<div class="row">
	<div class="col-md-4">
        @if(session('success'))
        <div class="alert alert-{{(session('success'))?'success':'danger!'}}" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{(session('success'))?'Berhasil':'Gagal!'}}</strong><br />{{ session('message') }}
        </div>
		@endif
        <section class="card shadow-sm">
            <div class="card-body">
				<form class="form form-horizontal" data-toggle="validator" role="form" method="post" enctype="multipart/form-data" action="@if(empty($data->id_sub_kategori)){{route('website.berita.kategori.sub.simpan')}}@else{{route('website.berita.kategori.sub.update',$data->id_sub_kategori)}} @endif">
					@csrf @method('POST')
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="nama_desa">Nama Desa</label>
							<input type="hidden" class="form-control" id="id_desa" name="id_desa"
								value="{{ Auth::user()->id_desa }}">
							<input type="text" class="form-control" id="nama_desa" name="nama_desa" disabled
								value="{{ Auth::user()->desa->nama_desa }}">
						</div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
							<label for="id_kategori_berita">Nama Kategori</label>
							<select name="id_kategori_berita" id="id_kategori_berita" class="form-control">
								<option value="" selected disabled>&mdash; Select &mdash;</option>
							@foreach ($kategori as $kat)
                            	<option value="{{$kat->id_kategori_berita}}" @if(!$errors->isEmpty()) {{($kat->id_kategori_berita==old('id_kategori_berita'))?'selected':''}} @elseif(!empty($data->id_kategori_berita)) {{($kat->id_kategori_berita==$data->id_kategori_berita)?'selected':''}} @endif> {{$kat->nama_kategori}}</option>
							@endforeach
							</select>
							@if($errors->has('id_kategori_berita'))
							<div class="text-danger">
								{{ $errors->first('id_kategori_berita')}}
							</div>
							@endif
                        </div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nama_sub_kategori">Nama Sub Kategori</label>
							<input type="text" class="form-control" id="nama_sub_kategori" name="nama_sub_kategori" value="@if(!empty($data->id_kategori_berita)){{$data->nama_sub_kategori}}@elseif(!empty(old('nama_sub_kategori'))){{old('nama_sub_kategori')}}@else{{old('nama_sub_kategori')}}@endif">
							@if($errors->has('nama_sub_kategori'))
							<div class="text-danger">
								{{ $errors->first('nama_sub_kategori')}}
							</div>
							@endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="slug_sub_kategori">Slug Kategori</label>
							<input type="text" class="form-control" id="slug_sub_kategori" name="slug_sub_kategori" value="@if(!empty($data->id_kategori_berita)){{$data->slug_sub_kategori}}@elseif(!empty(old('slug_sub_kategori'))){{old('slug_sub_kategori')}}@else{{old('slug_sub_kategori')}}@endif">
							@if($errors->has('slug_sub_kategori'))
							<div class="text-danger">
								{{ $errors->first('slug_sub_kategori')}}
							</div>
							@endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="status">Status</label>
							<div class="row">
								<div class="col-lg-12">
									<div class="radio-custom radio-primary form-check-inline mr-5">
										<input type="radio" id="aktif" name="status" value="1" @if(!empty($data->id_sub_kategori)){{($data->status == 1)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif checked>
										<label for="aktif">Aktif</label>
									</div>
									<div class="radio-custom radio-primary form-check-inline">
										<input type="radio" id="tidak_aktif" name="status" value="0" @if(!empty($data->id_sub_kategori)){{($data->status == 0)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif>
										<label for="tidak_aktif">Tidak Aktif</label>
									</div>
								</div>
							</div>
                        </div>
					</div>
                    <hr class="solid">
                    <div class="form-row">
                        <div class="col-md-12 text-left">
							@if(empty($data->id_sub_kategori)) 
							<button type="button" class="btn btn-default" onclick="history.back(-1)"><i class="fas fa-reply"></i> Kembali</button>
							@else
							<button type="reset" class="btn btn-default" onclick="window.location.href='{{route('website.berita.kategori.sub')}}'"><i class="fas fa-window-close"></i> Batal</button>
							@endif
                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{(empty($data->id_sub_kategori))?'Simpan':'Update'}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
	</div>
	<div class="col-md-8">
		<section class="card shadow-sm">
			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" 
							class="btn btn-success btn-xs card-action-toggle"
							data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-sub-kategori">
					<thead>
						<tr>
							<th class="text-center">Aksi</th>
							<th>Status</th>
							<th nowrap>Kategori</th>
							<th nowrap>Sub Kategori</th>
							<th>Slug</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}"/>
@endsection
@section('javascript')
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script type="text/javascript">
	$(function () {
		select2('#id_kategori_berita');
		slugify('#nama_sub_kategori', '#slug_sub_kategori');
		$('#tabel-sub-kategori').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "13%", "targets": 0, "orderable": false, "className": "text-center dt-body-right" },
				{ "width": "9%", "targets": 1, "orderable": false},
				{ "width": "30%", "targets": 2, "orderable": false},
				{ "width": "30%", "targets": 3, "orderable": false},
			],
			"ajax": {
				"url": "{{ route('website.berita.kategori.sub.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});
		$('.li-website').addClass('nav-expanded nav-active');
		$('.li-kategori').addClass('nav-expanded nav-active');
		$('.li-sub-kategori').addClass('nav-active');
	});
</script>
@endsection