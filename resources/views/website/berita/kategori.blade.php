@extends('layouts.app')
@section('title') Kategori @endsection
@section('breadcrumbs') 
<li><span>Berita</span></li> 
<li><span>Kategori</span></li> 
@endsection
@section('content')
<div class="row">
	<div class="col-md-4">
        @if(session('success'))
        <div class="alert alert-{{(session('success'))?'success':'danger!'}}" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{(session('success'))?'Berhasil':'Gagal!'}}</strong><br />{{ session('message') }}
        </div>
		@endif
        <section class="card">
            <div class="card-body">
				<form class="form form-horizontal" data-toggle="validator" role="form" method="post" enctype="multipart/form-data" action="@if(empty($data->id_kategori_berita)){{route('website.berita.kategori.simpan')}}@else{{route('website.berita.kategori.update',$data->id_kategori_berita)}} @endif">
					@csrf @method('POST')
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="nama_desa">Nama Desa</label>
							<input type="hidden" class="form-control" id="id_desa" name="id_desa"
								value="{{ Auth::user()->id_desa }}">
							<input type="text" class="form-control" id="nama_desa" name="nama_desa" disabled
								value="{{ Auth::user()->desa->nama_desa }}">
						</div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nama_kategori">Nama Kategori</label>
							<input type="text" class="form-control" id="nama_kategori" name="nama_kategori" value="@if(!empty($data->id_kategori_berita)){{$data->nama_kategori}}@elseif(!empty(old('nama_kategori'))){{old('nama_kategori')}}@else{{old('nama_kategori')}}@endif">
							@if($errors->has('nama_kategori'))
							<div class="text-danger">
								{{ $errors->first('nama_kategori')}}
							</div>
							@endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="slug_kategori">Slug Kategori</label>
							<input type="text" class="form-control" id="slug_kategori" name="slug_kategori" value="@if(!empty($data->id_kategori_berita)){{$data->slug_kategori}}@elseif(!empty(old('slug_kategori'))){{old('slug_kategori')}}@else{{old('slug_kategori')}}@endif">
							@if($errors->has('slug_kategori'))
							<div class="text-danger">
								{{ $errors->first('slug_kategori')}}
							</div>
							@endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="status">Status</label>
							<div class="row">
								<div class="col-lg-12">
									<div class="radio-custom radio-primary form-check-inline mr-5">
										<input type="radio" id="aktif" name="status" value="1" @if(!empty($data->id_kategori_berita)){{($data->status == 1)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif checked>
										<label for="aktif">Aktif</label>
									</div>
									<div class="radio-custom radio-primary form-check-inline">
										<input type="radio" id="tidak_aktif" name="status" value="0" @if(!empty($data->id_kategori_berita)){{($data->status == 0)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif>
										<label for="tidak_aktif">Tidak Aktif</label>
									</div>
								</div>
							</div>
                        </div>
					</div>
                    <hr class="solid">
                    <div class="form-row">
                        <div class="col-md-12 text-left">
							@if(empty($data->id_kategori_berita)) 
							<button type="button" class="btn btn-default" onclick="history.back(-1)"><i class="fas fa-reply"></i> Kembali</button>
							@else
							<button type="reset" class="btn btn-default" onclick="window.location.href='{{route('website.berita.kategori')}}'"><i class="fas fa-window-close"></i> Batal</button>
							@endif
                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> {{(empty($data->id_kategori_berita))?'Simpan':'Update'}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
	</div>
	
	<div class="col-md-8">
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" 
							class="btn btn-success btn-xs card-action-toggle"
							data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-kategori-berita">
					<thead>
						<tr>
							<th class="text-center">Aksi</th>
							<th>Status</th>
							<th nowrap>Nama Kategori</th>
							<th>Slug</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
@endsection
@section('javascript')
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script type="text/javascript">
	$(function () {
		slugify('#nama_kategori', '#slug_kategori');
		$('#tabel-kategori-berita').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "9%", "targets": 0, "orderable": false, "className": "text-center dt-body-right" },
				{ "width": "9%", "targets": 1, "orderable": false},
				{ "width": "30%", "targets": 2, "orderable": false},
				{ "width": "30%", "targets": 3, "orderable": false},
			],
			"ajax": {
				"url": "{{ route('website.berita.kategori.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});
		$('.li-website').addClass('nav-expanded nav-active');
		$('.li-kategori').addClass('nav-expanded nav-active');
		$('.li-semua-kategori').addClass('nav-active');
	});
</script>
@endsection