@extends('layouts.app')
@section('title') Kabar Berita @endsection
@section('breadcrumbs') 
<li><span>Berita</span></li>
<li><span>Tambah</span></li>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Gagal!') }}</strong><br />{{ session('message') }}. Perbaiki kolom isian berikut ini
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
		@endif

		<div class="alert alert-secondary">
			<i class="fas fa-info-circle"></i>
			<span class="text-muted">Berikut ini adalah formulir isian tentang layanan pemerintah desa untuk masyarakat.</span>
		</div>
		<form class="form form-horizontal" data-toggle="validator" role="form" method="post" enctype="multipart/form-data" action="@if(empty($data->id_berita)){{route('berita.simpan')}}@else{{route('berita.update', $data->id_berita)}}@endif">
			@csrf @method('POST')
			<div class="row">
				<div class="col-8">
					<div class="card">
						<div class="card-body">
							<input type="hidden" class="form-control" id="id_desa" name="id_desa" value="{{ Auth::user()->id_desa }}">
							<input type="hidden" class="form-control" id="id_wilayah" name="id_wilayah">
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="judul_berita">Judul </label>
									<input type="text" class="form-control" id="judul_berita" name="judul_berita" 
										value="@if(!empty($data->id_berita)){{$data->judul_berita}}@elseif(!empty(old('judul_berita'))){{old('judul_berita')}}@else{{old('judul_berita')}}@endif"
										maxlength="200">
									@if($errors->has('judul_berita'))
									<div class="text-danger">
										{{ $errors->first('judul_berita')}}
									</div>
									@endif
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="isi_berita">Isi Berita</label>
									<textarea class="form-control" name="isi_berita" id="isi_berita">@if(!empty($data->id_berita)){{$data->isi_berita}}@elseif(!empty(old('isi_berita'))){{old('isi_berita')}}@else{{old('isi_berita')}}@endif</textarea>
									@if($errors->has('isi_berita'))
									<div class="text-danger">
										{{ $errors->first('isi_berita')}}
									</div>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-4">
					<div class="card">
						<div class="card-body">
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="id_kategori_berita">Nama Kategori</label>
									<select name="id_kategori_berita" id="id_kategori_berita" class="form-control" style="width:100%">
										<option value="" selected disabled>&mdash; Select &mdash;</option>
									@foreach ($kategori as $kat)
										<option value="{{$kat->id_kategori_berita}}" @if(!$errors->isEmpty()) {{($kat->id_kategori_berita==old('id_kategori_berita'))?'selected':''}} @elseif(!empty($data->id_kategori_berita)) {{($kat->id_kategori_berita==$data->id_kategori_berita)?'selected':''}} @endif> {{$kat->nama_kategori}}</option>
									@endforeach
									</select>
									@if($errors->has('id_kategori_berita'))
									<div class="text-danger">
										{{ $errors->first('id_kategori_berita')}}
									</div>
									@endif
								</div>
								<div class="form-group col-md-12">
									<label for="slug_judul_berita">Judul </label>
									<input type="text" class="form-control" id="slug_judul_berita" name="slug_judul_berita" value="@if(!empty($data->id_berita)){{$data->slug_judul_berita}}@elseif(!empty(old('slug_judul_berita'))){{old('slug_judul_berita')}}@else{{old('slug_judul_berita')}}@endif">
									@if($errors->has('slug_judul_berita'))
									<div class="text-danger">
										{{ $errors->first('slug_judul_berita')}}
									</div>
									@endif
								</div>
								{{-- <div class="form-group col-md-12">
									<label for="tags">Tags</label>
									<input name="tags" id="tags-input" data-role="tagsinput" data-tag-class="badge badge-primary" class="form-control"/>
									<small>Pisahkan tags dengan koma (,)</small>
									@if($errors->has('tags'))
									<div class="text-danger">
										{{ $errors->first('tags')}}
									</div>
									@endif
								</div> --}}
								<div class="form-group col-md-12">
									<label for="publish">Terbitkan</label>
									<div class="row">
										<div class="col-lg-12">
											<div class="radio-custom radio-primary form-check-inline mr-4">
												<input type="radio" name="publish" id="active" value="1" @if(!empty($data->id_desa)){{($data->publish == 1)?'checked':''}} @elseif(!empty(old('publish'))){{old('publish')}}@else{{old('publish')}}@endif checked>
												<label for="active"> Aktif</label>
											</div>
											<div class="radio-custom radio-primary form-check-inline">
												<input type="radio" name="publish" id="inactive" value="0" @if(!empty($data->id_desa)){{($data->publish == 0)?'checked':''}} @elseif(!empty(old('publish'))){{old('publish')}}@else{{old('publish')}}@endif>
												<label for="inactive"> Tidak Aktif</label>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr class="dotted">
			<div class="card">
				<div class="card-body">
					<div class="form-row">
						<div class="col-md-12 text-left">
							@if(!empty($data->id_berita)) 
							<button type="button" class="btn btn-default" onclick="history.back(-1)"><i class="fas fa-reply"></i> Kembali</button>
							@else
							<button type="button" class="btn btn-default" onclick="window.location.href='{{route('website.berita')}}'"><i class="fas fa-window-close"></i> Batal</button>
							@endif
							<button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
						</div>
					</div>
				</div>
			</div>
		</form>
    </div>
</div>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}"/>
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}" />
@endsection
@section('javascript')
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
<script type="text/javascript">
	$(function () {
		select2('#id_kategori_berita');
		slugify('#judul_berita', '#slug_judul_berita');
		CKEDITOR.replace('isi_berita',{
			language: 'en',
			toolbar: [
					[ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
					[ 'Link', 'Unlink' ],
					[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
					[ 'Table', 'HorizontalRule' ],
					[ 'Bold', 'Italic', 'Strike', '-', 'NumberedList', 'BulletedList' ],
					[ 'Source', '-', 'Maximize']
			],
			cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
			cloudServices_uploadUrl: "{{ asset('storage/images/') }}",
			autoParagraph: false,
			width: '100%',
			height: '260',
		})
		$('.li-website').addClass('nav-expanded nav-active');
		$('.li-berita').addClass('nav-expanded nav-active');
		$('.li-berita').addClass('nav-active');
		$('html').addClass('sidebar-left-collapsed');
	});
</script> 
@endsection