@extends('layouts.app')
@section('title') Desa @endsection
@if(empty($desa->id_desa))
@section('content_title') Tambah Desa @endsection
@else
@section('content_title') Sunting Desa @endsection
@endif
@section('breadcrumbs') 
<li><span><a href="{{route('manajemen.desa.index')}}">Manajemen</a></span></li>
<li><span>@if(!empty($data->id_desa))<a href="{{route('manajemen.desa.index')}}">Desa</a>@else Desa @endif</span></li>
@if(!empty($data->id_desa)) 
<li><span>{{$data->id_desa}}</span></li>
<li><span>Edit</span></li>
@else 
<li><span> Tambah</span></li>
@endif
@endsection
@section('content')

<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post"
	enctype="multipart/form-data"
	oninput="re_password.setCustomValidity(re_password.value != password.value ? 'Password tidak sama. Silahkan periksa kembali':'')"
	action="@if(!empty($data->id_desa)){{route('manajemen.desa.update',$data->id_desa)}}@else{{route('manajemen.desa.simpan')}}@endif">
	@csrf @method('POST')
	<div class="row">
		<div class="col-lg-4 col-xl-4 mb-4 mb-xl-0">
			<section class="card">
				<div class="card-body">
					<label class="label mb-0" data-toggle="tooltip" data-placement="right" title="Ganti foto profil">
						<div class="thumb-info mb-3" style="cursor:pointer">
							<img src="{{asset('storage/images/logo/default.png')}}" class="rounded img-fluid">
						</div>
					</label>
					<div class="form-group row">
						<div class="col-lg-12 text-center mb-3">
							<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="input-append">
									<div class="uneditable-input" style="width:70%">
										<i class="fas fa-file fileupload-exists"></i>
										<span class="fileupload-preview"></span>
									</div>
									<span class="btn btn-default btn-file">
										<span class="fileupload-exists"><i class="fas fa-reply"></i></span>
										<span class="fileupload-new"><i class="fas fa-upload"></i></span>
										<input type="file" name="logo" id="logo"/>
									</span>
									<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload"><i class="fas fa-trash"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
		</div>
		<div class="col-lg-8 col-xl-8">
			@if(session('success'))
			<div class="alert alert-success" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>{{__('Berhasil!')}}</strong><br />{{session('message')}}
			</div>
			@endif
			@if ($errors->any())
			<div class="alert alert-danger">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<strong>{{__('Gagal!')}}</strong><br />{{session('message')}}
				Perbaiki kolom isian berikut ini
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
					@endforeach
				</ul>
			</div>
			@endif			
			<section class="card card-primary">
				<div class="card-body">
					<h4>Detail Desa</h4>
					<div class="form-group">
						<label for="kode_wilayah">Kode Wilayah<code>*</code></label>
						<input type="text" class="form-control" id="kode_wilayah" name="kode_wilayah" 
							value="@if(!empty($data->id_desa)){{$data->kode_wilayah}}@elseif(!empty(old('kode_wilayah'))){{old('kode_wilayah')}}@else{{old('kode_wilayah')}}@endif" required>
						<small>Contoh: 82.01.09.2001</small>
					</div>
					<div class="form-group">
						<label for="nama_desa">Nama Desa</label>
						<input type="text" class="form-control" id="nama_desa" name="nama_desa" 
							value="@if(!empty($data->id_desa)){{$data->nama_desa}}@elseif(!empty(old('nama_desa'))){{old('nama_desa')}}@else{{old('nama_desa')}}@endif" required>
					</div>
					<div class="form-row">
						<div class="form-group col-md-5">
							<label for="id_kecamatan">Kecamatan<code>*</code></label>
							<select id="id_kecamatan" name="id_kecamatan" data-plugin-selectTwo class="form-control kab" aria-required="true">
								<option selected disabled value="">- Pilih -</option>
								@php
									$kec = \App\Models\LokasiKec::get();
									foreach ($kec as $key => $val) {
								@endphp
									<option value="{{$val->id_kecamatan}}" {{($val->id_kecamatan==!empty($data->id_kecamatan))?"selected":""}}>{{$val->nama_kecamatan}}</option>
								@php } @endphp
							</select>
						</div>
						<div class="form-group col-md-5">
							<label for="desa_id">Desa<code>*</code></label>
							<select id="desa_id" name="desa_id" data-plugin-selectTwo class="form-control kec" @if(empty($data->id_desa)) disabled @endif required>
								<option selected disabled value="">- Pilih -</option>
								@php
									$des = \App\Models\LokasiDesa::get();
									foreach ($des as $key => $val) {
								@endphp
									<option value="{{$val->desa_id}}" {{($val->desa_id==!empty($data->desa_id))?"selected":""}}>{{ucwords($val->nama_desa)}}</option>
								@php } @endphp
							</select>
						</div>
						<div class="form-group col-md-2">
							<label for="kode_pos">Kode pos</label>
							<input type="text" class="form-control" id="kode_pos" name="kode_pos" value="@if(!empty($data->id_desa)){{$data->kode_pos}}@elseif(!empty(old('kode_pos'))){{old('kode_pos')}}@else{{old('kode_pos')}}@endif">
						</div>
					</div>
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<input type="text" class="form-control" id="alamat" name="alamat" value="@if(!empty($data->id_desa)){{$data->alamat}}@elseif(!empty(old('alamat'))){{old('alamat')}}@else{{old('alamat')}}@endif" >
					</div>
					<div class="form-group">
						<label for="website">Website<code>*</code></label>
						<input type="text" class="form-control" id="website" name="website" 
							value="@if(!empty($data->id_desa)){{$data->website}}@elseif(!empty(old('website'))){{old('website')}}@else{{old('website')}}@endif" required>
					</div>
					<hr class="dotted">
					<h4>Login Aplikasi</h4>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="email">Alamat Email<code>*</code></label>
							<input type="email" 
								class="form-control" 
								id="email" 
								name="email" 
								placeholder="Email" 
								value="@if(!Auth::check()){{Auth::user()->email}}@elseif(!empty($data->id_desa)){{$data->get_user->email}}@elseif(!empty(old('email'))){{old('email')}}@else{{old('email')}}@endif"
								required
								title="Please Provide A Valid Email Address !">
						</div>
						<div class="form-group col-md-6">
							<label for="user_name">Nama Pengguna<code>*</code></label>
							<input type="text" 
								class="form-control" 
								id="user_name" 
								name="user_name" 
								placeholder="Nama Pengguna" 
								value="@if(!Auth::check()){{Auth::user()->user_name}}@elseif(!empty($data->id_desa)){{$data->get_user->user_name}}@elseif(!empty(old('user_name'))){{old('user_name')}}@else{{old('user_name')}}@endif"
								required>
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="password">Kata sandi baru<code>*</code></label>
							<div class="input-group">
								<input type="password" 
									id="password"
									name="password"
									class="form-control" 
									placeholder="Password"
									@if(empty($data->id_desa)) required @endif>
								</div>
								@if(!empty($data->id_desa))<small>Biarkan kosong! Jika tidak ingin mengganti password baru!</small>@endif
							</div>
						<div class="form-group col-md-6">
							<label for="re_password">Ulangi kata sandi<code>*</code></label>
							<div class="input-group">
								<input type="password" 
									id="re_password"
									name="re_password"
									class="form-control" 
									placeholder="Ulangi password"
									@if(empty($data->id_desa)) required @endif>
								<span class="eye input-group-append" style="cursor:pointer" >
									<span class="input-group-text">
										<i class="fa fa-eye-slash lihat_aku" aria-hidden="true"></i>
									</span>
								</span>
							</div>
						</div>
					</div>
					<hr class="dotted">
					<div class="form-group">
						<label for="status">Status</label>
						<div class="row">
							<div class="col-lg-12">
								<div class="radio-custom radio-primary form-check-inline mr-4">
									<input type="radio" name="status" id="active" value="1" @if(!empty($data->id_desa)){{($data->status == 1)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif checked>
									<label for="active"> Aktif</label>
								</div>
								<div class="radio-custom radio-primary form-check-inline">
									<input type="radio" name="status" id="inactive" value="0" @if(!empty($data->id_desa)){{($data->status == 0)?'checked':''}} @elseif(!empty(old('status'))){{old('status')}}@else{{old('status')}}@endif>
									<label for="inactive"> Tidak Aktif</label>
								</div>
							</div>
						</div>
					</div>
					<hr class="divider">
					<div class="form-row">
						<div class="col-md-12">
							@if(empty($data->id_desa))
							<button class="btn btn-primary modal-confirm"><i class="fas fa-save"></i> Simpan</button>
							@else
							<a class="btn btn-default" onclick="window.location.href='{{route('manajemen.desa.index')}}'"><i class="fas fa-reply"></i> Kembali</a>
							<button class="btn btn-primary"><i class="fas fa-save"></i> Update</button>
							@endif
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</form>
@endsection
@section('css')
<link rel="stylesheet" href="{{asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css')}}" />
<link rel="stylesheet" href="{{asset('vendor/select2/css/select2.css')}}" />
<link rel="stylesheet" href="{{asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css')}}" />
<link rel="stylesheet" href="{{asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.css')}}" />
@endsection
@section('javascript')
<script src="{{asset('vendor/select2/js/select2.js')}}"></script>
<script src="{{asset('vendor/autosize/autosize.js')}}"></script>
<script src="{{asset('vendor/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
<script src="{{asset('vendor/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
	$(function () {
		$('.li-manajemen-desa').addClass('nav-active');
		$('#id_kecamatan').on('change', function(e){
			var id_kec = e.target.value;
			$.get('/dashboard/info/identitas/kecamatan/' + id_kec, function(data){
				$('#desa_id').attr('disabled', false)
				$('#desa_id').empty();
				$('#desa_id').append('<option value="0" disabled selected>- Pilih -</option>');
				$.each(data, function(index, obj){
					$('#desa_id').append('<option value="'+ obj.desa_id +'">'+ toTitleCase(obj.nama_desa) +'</option>');
				});
			});
		});

		$(".lihat_aku").on('click', function(event) {
			event.preventDefault();
			if($('#password, #re_password').attr("type") == "text"){
				$('#password').attr('type', 'password');
				$('#re_password').attr('type', 'password');
				$('.lihat_aku').addClass( "fa-eye-slash" );
				$('.lihat_aku').removeClass( "fa-eye" );
			}else if($('#password, #re_password').attr("type") == "password"){
				$('#password').attr('type', 'text');
				$('#re_password').attr('type', 'text');
				$('.lihat_aku').removeClass( "fa-eye-slash" );
				$('.lihat_aku').addClass( "fa-eye" );
			}
		});

		@if(!empty($data->id_desa))
		$.ajax({
			url: "{{ route('manajemen.desa.detail', $data->id_desa) }}",
			type: "get",
			dataType: "json",
			success: function (data) {
				$('#id_kecamatan').val(data.id_kecamatan).trigger("change.select2")
				$('#desa_id').val(data.desa_id).trigger("change.select2")
			},
			error: function (e) {
				console.log(e)
			}
		});
		@endif

		$('.alert-success').css('display', 'block').delay(5000).fadeOut()
		$('.alert-danger').css('display', 'block').delay(5000).fadeOut()

		// $("input[required]").attr("oninvalid", "this.setCustomValidity(toTitleCase(this.name)+' wajib diisi dengan benar!')");
		// $("input[required]").attr("oninput", "setCustomValidity('')");
	});
</script>
@endsection