<div class="row">
	<div class="col-lg-6 col-xl-6 mb-6">
		<section class="card">
			<div class="card-body">
				<label class="label mb-0" data-toggle="tooltip" data-placement="bottom" data-original-title="Logo Desa">
					<div class="thumb-info mb-3" style="cursor:pointer">
						<img src="{{asset('storage/images/logo/default.png')}}" class="rounded img-fluid">
					</div>
				</label>
				<div class="form-group">
					<label for="role_id">Level</label>
					<input type="text" class="form-control" id="role_id" name="role_id" 
						value="@if($data->get_user->role_id==2){{$data->get_user->get_role->role_name}}@endif" readonly>
				</div>
			</div>
		</section>
		<section class="card mt-3">
			<div class="card-body">
				<h4>Informasi Akun</h4>
				<div class="form-group">
					<label for="email">Alamat Email</label>
					<input type="email" class="form-control" id="email" name="email" value="{{$data->get_user->email}}" readonly>
				</div>
				<div class="form-group">
					<label for="user_name">Nama Pengguna</label>
					<input type="text" class="form-control" id="user_name" name="user_name" value="{{$data->get_user->user_name}}" readonly>
				</div>
			</div>
		</section>
	</div>
	<div class="col-lg-6 col-xl-6 mb-6">
		<section class="card">
			<div class="card-body">
				<div class="form-group">
					<label for="kode_wilayah">Kode Wilayah</label>
					<input type="text" class="form-control" id="kode_wilayah" name="kode_wilayah" value="{{$data->kode_wilayah}}" readonly>
				</div>
				<div class="form-group">
					<label for="nama_desa">Nama Desa</label>
					<input type="text" class="form-control" id="nama_desa" name="nama_desa" value="{{$data->nama_desa}}" readonly>
				</div>
				<div class="form-row">
					<div class="form-group col-md-6">
						<label for="id_kecamatan">Kecamatan</label>
						<input type="text" class="form-control" id="id_kecamatan" name="id_kecamatan" value="{{$data->id_kecamatan}}" readonly>
					</div>
					<div class="form-group col-md-6">
						<label for="desa_id">Desa</label>
						<input type="text" class="form-control" id="desa_id" name="desa_id" value="{{ucwords($data->nama_desa)}}" readonly>
					</div>
				</div>
				<div class="form-group">
					<label for="kode_pos">Kode pos</label>
					<input type="text" class="form-control" id="kode_pos" name="kode_pos" value="{{$data->kode_pos}}" readonly>
				</div>
				<div class="form-group">
					<label for="alamat">Alamat</label>
					<input type="text" class="form-control" id="alamat" name="alamat" value="{{$data->alamat}}" readonly>
				</div>
			</div>
		</section>
		<section class="card mt-3">
			<div class="card-body">
				<div class="form-group">
					<label for="website">Website</label>
					<span class="form-control btn btn-primary btn-sm" 
						onclick="window.open('https://{{$data->website}}','_blank')" 
						name="website"
						data-toggle="tooltip" 
						data-placement="bottom"
						title="Buka website https://{{$data->website}}">https://{{$data->website}}</span>
				</div>
				<div class="form-group">
					<label for="status">Status</label>
					@if($data->status == 1)
						<span class="form-control btn btn-success btn-sm">Aktif</span>
					@else
						<span class="form-control btn btn-danger btn-sm">Tidak Aktif</span>
					@endif
				</div>
			</div>
		</section>
	</div>
</div>