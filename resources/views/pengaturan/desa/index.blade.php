@extends('layouts.app')
@section('title') Manajemen Desa @endsection
@section('breadcrumbs') 
<li><span><a href="{{route('manajemen.desa.index')}}">Manajemen</a></span></li>
<li><span>Desa</span></li>
@endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
        @if(session('success'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
        </div>
        @endif
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" 
							class="btn btn-success btn-sm" 
							data-toggle="tooltip" 
							data-placement="left"
							onclick="location.href = '{{ route('manajemen.desa.tambah') }}'" 
							data-original-title="Tambah Website Desa">
							<i class="fas fa-plus"></i>
						</button>
						<button type="button" 
							class="btn btn-success btn-sm card-action-toggle"
							data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
				<p class="card-subtitle">Berikut ini adalah daftar desa atau daerah yang telah terdaftar ke dalam website desa.</p>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-desa">
					<thead>
						<tr>
							<th class="text-center">Aksi</th>
							<th>Kode Wilayah</th>
							<th>Nama Desa</th>
							<th>Website</th>
							<th>Status</th>
							<th>Created</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
	$(function () {
		$('#tabel-desa').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "13%", "targets": 0, "orderable": false, "className": "text-center" },
				{ "width": "15%", "targets": 1, "orderable": false},
				{ "width": "22%", "targets": 2, "orderable": false},
				{ "width": "23%", "targets": 3, "orderable": false},
				{ "width": "12%", "targets": 4, "orderable": false},
			],
			"ajax": {
				"url": "{{ route('manajemen.desa.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});

		$('.li-manajemen-desa').addClass('nav-active');
	});
</script> 
@endsection