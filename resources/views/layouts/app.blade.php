@include('layouts.partials.header')
	<body>
		<section class="body" data-loading-overlay>
			@auth
			@include('layouts.partials.navbar')
			<div class="inner-wrapper">
				@include('layouts.partials.aside')
				<section role="main" class="content-body">
					<header class="page-header">
						<h2>@yield('content_title')</h2>
						<div class="right-wrapper text-right mr-3">
							<ol class="breadcrumbs">
								<li><a href=""><i class="fas fa-home"></i></a></li>
								<li><span>Dashboard</span></li>
								@yield('breadcrumbs')
							</ol>
						</div>
					</header>
			@endauth
					@yield('content')
			@auth
				</section>
		</div>
	@endauth
	</section>
	{{-- Dialog show detail --}}
	<div class="modal"
		id="modal-form"
		tabindex="-1"
		role="dialog"
		aria-hidden="true"
		style="display: none;">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<header class="card-header">
					<h2 class="card-title"></h2>
				</header>
				<div class="loading"><br/>
					<center><img src="{{ asset('img/loading.gif') }}"></center>
				</div>
				<div class="modal-body m-body"></div>
				<div class="modal-footer">
					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-primary b-action" type="submit">
								<i class="fas fa-pen"></i>
								<span class="b-option"></span>
							</button>
							<button class="btn btn-default text-right" type="button" data-dismiss="modal">
								<i class="fas fa-window-close"></i>
								Keluar
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@include('layouts.partials.footer')
