<header class="header">
	<div class="logo-container">
			<a href="{{ route('home') }}" class="logo">
					<img src="{{ asset('img/isifodes-logo.png') }}" height="35" title="Aquila" />
			</a>
			<div class="d-md-none toggle-sidebar-left" 
				data-toggle-class="sidebar-left-opened" 
				data-target="html" 
				data-fire-event="sidebar-left-opened">
				<i class="fas fa-bars" aria-label="Toggle sidebar"></i>
			</div>
	</div>
	<div class="header-right">
		<span class="separator"></span>
		<div id="userbox" class="userbox">
			<a href="#" data-toggle="dropdown">
				<figure class="profile-picture">
					<img src="{{ asset('img/user.png') }}" 
						alt="Joseph Doe" 
						class="rounded-circle" 
						data-lock-picture="{{ asset('img/user.png') }}" />
				</figure>
				<div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
					<span class="name">@if(Auth::user()->role_id==2){{Auth::user()->desa->nama_desa}}@else{{Auth::user()->fullname}}@endif</span>
					<span class="role">
						@if (Auth::user()->role_id == 0)
							Superuser
						@elseif (Auth::user()->role_id == 1)
							Admin
						@elseif (Auth::user()->role_id == 2)
							Desa
						@elseif (Auth::user()->role_id == 3)
							Tamu
						@endif
					</span>
				</div>
				<i class="fa custom-caret"></i>
			</a>

			<div class="dropdown-menu">
				<ul class="list-unstyled mb-2">
					<li class="divider"></li>
					<li>
						<a role="menuitem" tabindex="-1" href=""><i class="fas fa-user"></i> Profil Saya</a>
					</li>
					<li>
						<a class="menuitem" href="{{ route('logout') }}"
							onclick="event.preventDefault();document.getElementById('logout-form').submit();">
							<i class="fas fa-power-off"></i> {{ __('Logout') }}
						</a>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">@csrf</form>
					</li>
				</ul>
			</div>
		</div>
	</div>
</header>