<!-- start: sidebar -->
<aside id="sidebar-left" class="sidebar-left">
	<div class="sidebar-header">
		<div class="sidebar-title">Menu utama</div>
		<div class="sidebar-toggle d-none d-md-block" data-toggle-class="sidebar-left-collapsed" data-target="html"
			data-fire-event="sidebar-left-toggle">
			<i class="fas fa-bars" aria-label="Toggle sidebar"></i>
		</div>
	</div>

	<div class="nano">
		<div class="nano-content">
			<nav id="menu" class="nav-main" role="navigation">
				<ul class="nav nav-main">
					<li class="nav-parent li-dashboard" style="border-bottom: 1px solid #eee">
						<a class="nav-link" href="#">
							<i class="fas fa-home" aria-hidden="true"></i>
							<span>Dashboard</span>
						</a>
						<ul class="nav nav-children">
							<li class="li-home">
								<a class="nav-link" href="{{ url('/dashboard/home') }}">
								<i class="fas fa-desktop" aria-hidden="true"></i>
								<span>Home</span></a>
							</li>
							@if(\Auth::user()->role_id != 0)
							<li class="li-web">
								<a class="nav-link" href="http://{{ Auth::user()->desa->website }}" target="_blank">
								<i class="fas fa-globe" aria-hidden="true"></i>
								<span>Website</span></a>
							</li>
							@endif
						</ul>
					</li>
					@if(\Auth::user()->role_id == 0)
					<li class="li-manajemen-desa">
						<a class="nav-link" href="{{route('manajemen.desa.index')}}">
							<i class="fas fa-city" aria-hidden="true"></i>
							<span>Manajemen Desa</span>
						</a>
					</li>
					<li class="nav-parent li-pengaturan">
						<a class="nav-link" href="#">
							<i class="fas fa-cog" aria-hidden="true"></i>
							<span>Pengaturan</span>
						</a>
						<ul class="nav nav-children">
							<li class="li-pengguna"><a class="nav-link" href="{{ route('pengaturan.pengguna') }}">
								<i class="fas fa-users-cog"></i>Pengguna</a>
							</li>
						</ul>
					</li>	
					@elseif(\Auth::user()->role_id == 1)
					role = 1
					@elseif(\Auth::user()->role_id == 2)
					<li class="li-kabupaten">
						<a class="nav-link" href="{{ route('kabupaten.index') }}">
							<i class="fas fa-city" aria-hidden="true"></i>
							<span>Kabupaten</span>
						</a>
					</li>
					<li class="nav-parent li-info">
						<a class="nav-link" href="#">
							<i class="fas fa-columns" aria-hidden="true"></i>
							<span>Informasi Desa</span>
						</a>
						<ul class="nav nav-children">
							<li class="li-identitas"><a class="nav-link" href="{{ route('identitas.index') }}">
								<i class="fas fa-id-badge"></i>Identitas</a></li>
							<li class="li-sejarah"><a class="nav-link" href="{{ route('sejarah.index') }}">
								<i class="fas fa-history"></i>Sejarah</a></li>
							<li class="li-wilayah"><a class="nav-link" href="{{ route('wilayah.index') }}">
								<i class="fas fa-map"></i>Wilayah</a></li>
							<li class="li-potensi"><a class="nav-link" href="{{ route('potensi.index') }}">
								<i class="fas fa-seedling"></i>Potensi</a></li>
							<li class="li-lembaga"><a class="nav-link" href="{{ route('lembaga.index') }}">
								<i class="fas fa-layer-group"></i>Lembaga</a></li>
							<li class="li-statistik"><a class="nav-link" href="{{ route('statistik.index') }}">
								<i class="fas fa-poll"></i>Statistik</a></li>
						</ul>
					</li>
					<li class="nav-parent li-fasilitas">
						<a class="nav-link" href="#">
							<i class="fas fa-tools" aria-hidden="true"></i>
							<span>Fasilitas</span>
						</a>
						<ul class="nav nav-children">
							<li class="nav-parent li-layanan">
								<a><i class="fas fa-rss-square" aria-hidden="true"></i>Layanan</a>
								<ul class="nav nav-children">
									<li class="li--layanan"><a class="nav-link" href="{{ route('fasilitas.layanan.index') }}">
										<i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i>Semua Layanan</a></li>
									<li class="li-tambah-layanan"><a class="nav-link" href="{{ route('fasilitas.layanan.tambah') }}">
										<i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i>Tambah Layanan</a></li>
								</ul>
							</li>
							<li class="li-pengaduan"><a class="nav-link" href="{{ route('fasilitas.pengaduan') }}">
								<i class="fas fa-question-circle"></i >Pengaduan</a></li>
						</ul>
					</li>
					<li class="nav-parent li-apbdes">
						<a class="nav-link" href="#">
							<i class="fas fa-comment-dollar" aria-hidden="true"></i>
							<span>APBDES</span>
						</a>
						<ul class="nav nav-children">
							<li class="nav-parent li-referensi">
									<a><i class="fas fa-list" aria-hidden="true"></i>Parameters</a>
										<ul class="nav nav-children">
											<li class="li-referensi-kegiatan"><a class="nav-link" href="{{ route('refbidang.index') }}">
												<i class="fas fa-file-invoice-dollar"></i>Referensi Kegiatan</a></li>
											<li class="li-referensi-sumberdana"><a class="nav-link" href="{{ route('refsumberdana.index') }}">
												<i class="fas fa-comments-dollar"></i>Referensi Sumberdana</a></li>
											<li class="li-rekening-desa"><a class="nav-link" href="{{ route('refakun.index') }}">
												<i class="fas fa-hand-holding-usd"></i>Rekening APB Desa</a></li>											
											<li class="li-output-kegiatan-dd"><a class="nav-link" href="{{ route('refoutputkegiatan.index') }}">
												<i class="fas fa-bullhorn"></i>Output Kegiatan DD</a></li>																																	
											<li class="li-rekening-bank"><a class="nav-link" href="{{ route('refbankdesa.index') }}">
												<i class="fas fa-dollar-sign"></i>Rekening Bank Desa</a></li>
										</ul>
							</li>
					
							<li class="nav-parent li-perencanaan">
									<a><i class="fas fa-calendar-alt" aria-hidden="true"></i>Perencanaan</a>
										<ul class="nav nav-children">
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>RPJM Desa</a></li>
										
										</ul>
							</li>
							<li class="nav-parent li-penganggaran">
									<a><i class="fas fa-chart-bar" aria-hidden="true"></i>Penganggaran</a>
										<ul class="nav nav-children">
											<li class="li-anggaran"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Isian Data Anggaran</a></li>
											<li class="li-anggaran-kas-desa"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Anggaran Kas Desa</a></li>
											<li class="li-peraturan-apbdes"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Peraturan APBDes</a></li>
											<li class="li-anggaran-lanjutan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Anggaran Lanjutan</a></li>																				
										</ul>
							</li>
							<li class="nav-parent li-penatausahaan">
									<a><i class="fas fa-dumbbell" aria-hidden="true"></i>Penatausahaan</a>
										<ul class="nav nav-children">
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Penerimaan Desa</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>SPP Kegiatan</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Pencairan SPP</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>SPJ Kegiatan</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Pengembalian</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Penyetoran Pajak</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Mutasi Kas</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Ouput Dana Desa</a></li>
										
										</ul>
							</li>
							<li class="nav-parent li-pembukuan">
									<a><i class="fas fa-list" aria-hidden="true"></i>Pembukuan</a>
										<ul class="nav nav-children">
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Saldo Awal</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Penyesuaian</a></li>
										
										</ul>
							</li>
							<li class="nav-parent li-laporan">
									<a><i class="fas fa-list" aria-hidden="true"></i>Laporan</a>
										<ul class="nav nav-children">
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Parameter</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Perencanaan</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Penganggaran</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Penatausahaan</a></li>
											<li class="li-referensi-kegiatan"><a class="nav-link" href="">
												<i class="fas fa-file-invoice-dollar"></i>Pembukuan</a></li>
										
										</ul>
							</li>
						</ul>
					</li>
					<li class="nav-parent li-website">
						<a class="nav-link" href="#">
							<i class="fas fa-box-open" aria-hidden="true"></i>
							<span>Kabar</span>
						</a>
						<ul class="nav nav-children">
							<li class="li-banner"><a class="nav-link" href="{{ route('website.berita.banner') }}">
								<i class="fas fa-book-open"></i>Banner</a></li>
							<li class="nav-parent li-kategori">
								<a><i class="fas fa-list" aria-hidden="true"></i>Kategori</a>
								<ul class="nav nav-children">
									<li class="li-semua-kategori"><a class="nav-link" href="{{ route('website.berita.kategori') }}">
										<i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i>Semua Kategori</a></li>
									<li class="li-sub-kategori"><a class="nav-link" href="{{ route('website.berita.kategori.sub') }}">
										<i class="fas fa-arrow-alt-circle-right" aria-hidden="true"></i>Sub Kategori</a></li>
								</ul>
							</li>
							{{-- <li class="li-tag"><a class="nav-link" class="nav-link" href="{{ route('website.berita.tag') }}">
								<i class="fas fa-tags"></i>Tag</a></li> --}}
							<li class="li-berita"><a class="nav-link" class="nav-link" href="{{ route('website.berita') }}">
								<i class="fas fa-newspaper"></i>Berita</a></li>
						</ul>
					</li>
					<li class="nav-parent li-pengaturan">
						<a class="nav-link" href="#">
							<i class="fas fa-cog" aria-hidden="true"></i>
							<span>Pengaturan</span>
						</a>
						<ul class="nav nav-children">
							<li class="li-pengguna"><a class="nav-link" href="{{ route('pengaturan.pengguna') }}">
								<i class="fas fa-users-cog"></i>Pengguna</a></li>
							<li class="li-pengaturan-file"><a class="nav-link" href="{{ route('pengaturan.file') }}">
								<i class="fas fa-file-archive"></i>Manajer File</a></li>
							<li class="li-webconfig"><a class="nav-link" href="{{ route('website.webconfig') }}">
								<i class="fas fa-cogs"></i>Konfigurasi</a></li>
						</ul>
					</li>
					@else
					role > 2
					@endif
					<li class="li-logout" style="border-top: 1px solid #eee">
						<a class="nav-link" href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">
							<i class="fas fa-sign-out-alt" aria-hidden="true"></i>
							<span>Logout</span>
						</a>
					</li>
				</ul>
			</nav>
			{{-- 
			<hr class="separator" />
			<div class="sidebar-widget widget-tasks">
				<div class="widget-header">
					<h6>Website</h6>
					<div class="widget-toggle">+</div>
				</div>
				<div class="widget-content">
					<ul class="list-unstyled m-0">
						<li><a href="#">Porto HTML5 Template</a></li>
						<li><a href="#">Tucson Template</a></li>
						<li><a href="#">Porto Admin</a></li>
					</ul>
				</div>
			</div>
			<hr class="separator" />
			<div class="sidebar-widget widget-stats widget-collapsed">
				<div class="widget-header">
					<h6>Company Stats</h6>
					<div class="widget-toggle">+</div>
				</div>
				<div class="widget-content">
					<ul>
						<li>
							<span class="stats-title">Stat 1</span>
							<span class="stats-complete">85%</span>
							<div class="progress">
								<div class="progress-bar progress-bar-primary progress-without-number"
									role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100"
									style="width: 85%;">
									<span class="sr-only">85% Complete</span>
								</div>
							</div>
						</li>
						<li>
							<span class="stats-title">Stat 2</span>
							<span class="stats-complete">70%</span>
							<div class="progress">
								<div class="progress-bar progress-bar-primary progress-without-number"
									role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"
									style="width: 70%;">
									<span class="sr-only">70% Complete</span>
								</div>
							</div>
						</li>
						<li>
							<span class="stats-title">Stat 3</span>
							<span class="stats-complete">2%</span>
							<div class="progress">
								<div class="progress-bar progress-bar-primary progress-without-number"
									role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100"
									style="width: 2%;">
									<span class="sr-only">2% Complete</span>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
			--}}
		</div>

		<script>
			// Maintain Scroll Position
			if (typeof localStorage !== 'undefined') {
				if (localStorage.getItem('sidebar-left-position') !== null) {
					var initialPosition = localStorage.getItem('sidebar-left-position'),
						sidebarLeft = document.querySelector('#sidebar-left .nano-content');

					sidebarLeft.scrollTop = initialPosition;
				}
			}
		</script>
	</div>
</aside>
<!-- end: sidebar -->
