<!doctype html>
<html class="fixed sidebar-light clean-design sidebar-left-sm" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
{{-- sidebar-left-collapsed --}}
<!-- Basic -->
<meta charset="UTF-8">

<title>{{ env('APP_NAME') }} &mdash; @yield('title')</title>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="sistem informasi desa, sisfodes, desaku" />
<meta name="description" content="Sistem informasi desa - Pusat informasi Wilayah Kabupaten, Kecamatan, dan Desa">
<meta name="author" content="anabasoftware.com">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/animate/animate.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/all.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}" />

@yield('css')

<link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/jquery-ui/jquery-ui.theme.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/bootstrap-multiselect/css/bootstrap-multiselect.css') }}" />
<link rel="stylesheet" href="{{ asset('css/theme.css') }}" />
<link rel="stylesheet" href="{{ asset('css/skins/default.css') }}" />
<link rel="stylesheet" href="{{ asset('css/custom.css') }}">
<script src="{{ asset('vendor/modernizr/modernizr.js') }}"></script>

</head>