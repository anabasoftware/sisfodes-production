<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	
		<input type="hidden" id="id_kelompok_rekening" name="id_kelompok_rekening">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
						<label class="form-control-label" for="id_akun">Akun</label>
						<select id="id_akun" name="id_akun" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									
									foreach ($akun as $key => $val) {
										echo "<option value=\"".$val->id_akun_rekening."\">".$val->nama_akun."</option>";
									}
								@endphp
						</select>
						<label class="form-control-label" for="kode_kelompok">Kode Kelompok</label>
						<input class="form-control" type="text" id="kode_kelompok" name="kode_kelompok"}>						
						<label class="form-control-label" for="nama_kelompok">Nama Kelompok</label>
						<input class="form-control" type="text" id="nama_kelompok" name="nama_kelompok"}>
						
					
					

					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>


		var data = {!! json_encode($kelompok) !!};

		if (data != null) {			
			$('#id_kelompok_rekening').val(data.id_kelompok_rekening);
			$('#kode_kelompok').val(data.kode_kelompok);
			$('#nama_kelompok').val(data.nama_kelompok);
			$('#id_akun option[value='+data.id_akun+']').prop('selected',true);
		}

		var id = $('#id_kelompok_rekening').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_kelompok_rekening').val();
				url = "{{ route('refkelompok.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Kelompok Rekening berhasil disimpan.');
						$('#tabel-kelompok').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data Kelompok Rekening gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>