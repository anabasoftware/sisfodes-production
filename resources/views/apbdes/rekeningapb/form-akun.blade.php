<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	<input type="hidden" id="id_akun_rekening" name="id_akun_rekening">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
						<label class="form-control-label" for="nomer_akun">Nomer Akun</label>
						<input class="form-control" type="text" id="nomer_akun" name="nomer_akun">
						<label class="form-control-label" for="nama_akun">Nama Akun</label>
						<input class="form-control" type="text" id="nama_akun" name="nama_akun">
					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>

		var data = {!! json_encode($akun) !!};

		if (data != null){
			$('#id_akun_rekening').val(data.id_akun_rekening);
			$('#nomer_akun').val(data.nomer_akun);
			$('#nama_akun').val(data.nama_akun);
		}

		var id = $('#id_akun_rekening').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_akun_rekening').val();
				url = "{{ route('refakun.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Akun berhasil disimpan.');
						$('#tabel-akun').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data Akun gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>