<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	
		<input type="hidden" id="id_objek_rekening" name="id_objek_rekening">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
                        <label class="form-control-label" for="id_jenis_rekening">Jenis Rekening</label>
						<select id="id_jenis_rekening" name="id_jenis_rekening" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									
									foreach ($jenisrekening as $key => $val) {
										echo "<option value=\"".$val->id_jenis_rekening."\">".$val->nama_jenis_rekening."</option>";
									}
								@endphp
						</select>
						<label class="form-control-label" for="kode_objek_rekening">Kode Objek</label>
						<input class="form-control" type="text" id="kode_objek_rekening" name="kode_objek_rekening"}>						
						<label class="form-control-label" for="nama_objek_rekening">Nama Objek</label>
						<input class="form-control" type="text" id="nama_objek_rekening" name="nama_objek_rekening"}>
						
					
					

					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>


		var data = {!! json_encode($objekrekening) !!};

		if (data != null) {
			$('#id_objek_rekening').val(data.id_objek_rekening);
			$('#kode_objek_rekening').val(data.kode_objek_rekening);
			$('#nama_objek_rekening').val(data.nama_objek_rekening);
			$('#id_jenis_rekening option[value='+data.id_jenis_rekening+']').prop('selected',true);
		}

		var id = $('#id_objek_rekening').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_objek_rekening').val();
				url = "{{ route('refobjekrekening.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Objek Rekening berhasil disimpan.');
						$('#tabel-objek').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data Objek Rekening gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>