<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	
		<input type="hidden" id="id_jenis_rekening" name="id_jenis_rekening">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
                        <label class="form-control-label" for="id_kelompok_rekening">Kelompok Akun</label>
						<select id="id_kelompok_rekening" name="id_kelompok_rekening" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									
									foreach ($kelompok as $key => $val) {
										echo "<option value=\"".$val->id_kelompok_rekening."\">".$val->nama_kelompok."</option>";
									}
								@endphp
						</select>
						<label class="form-control-label" for="kode_jenis_rekening">Kode Jenis AKun</label>
						<input class="form-control" type="text" id="kode_jenis_rekening" name="kode_jenis_rekening"}>						
						<label class="form-control-label" for="nama_jenis_rekening">Nama Jenis Akun</label>
						<input class="form-control" type="text" id="nama_jenis_rekening" name="nama_jenis_rekening"}>
						
					
					

					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>


		var data = {!! json_encode($jenis) !!};

		if (data != null) {
			$('#id_jenis_rekening').val(data.id_jenis_rekening);
			$('#kode_jenis_rekening').val(data.kode_jenis_rekening);
			$('#nama_jenis_rekening').val(data.nama_jenis_rekening);
			$('#id_kelompok_rekening option[value='+data.id_kelompok_rekening+']').prop('selected',true);
		}

		var id = $('#id_jenis_rekening').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_kegiatan').val();
				url = "{{ route('refjenisrekening.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Jenis Rekening berhasil disimpan.');
						$('#tabel-jenis').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data Jenis Rekening gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>