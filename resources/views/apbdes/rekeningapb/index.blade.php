@extends('layouts.app')
@section('title') Referensi Bidang dan Kegiatan @endsection
@section('breadcrumbs') <li><span>Referensi Bidang dan Kegiatan</span></li> @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<style type="text/css">
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        {{--
        @if(session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
    </div>
    @endif
    --}}

    @if(session('status'))
    <div class="alert alert-success" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
    </div>
    @endif
    @if ($errors->any())
    <div class="alert alert-danger">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>{{ __('Gagal!') }}</strong><br />{{ session('message') }}. Perbaiki kolom isian berikut ini
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <section class="card">
        <header class="card-header">

            <h3 class="card-title">Data</h3>
            <p class="card-subtitle">Berikut ini adalah daftar Layanan desa atau daerah yang telah tersimpan dan ditampilkan di halaman website.</p>
        </header>
        <div class="card-body">
            <div class="tabs">
                <ul class="nav nav-tabs tabs-primary">
                    <li class="nav-item active">
                        <a class="nav-link active" href="#akun" data-toggle="tab">Akun</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#kelompok" data-toggle="tab">Kelompok</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#jenis" data-toggle="tab">Jenis</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#objek" data-toggle="tab">Objek</a>
                    </li>
                </ul>
                {{-- Contenct Akun --}}
                <div class="tab-content">
                    <div id="akun" class="tab-pane active">
                        <div class="card-text">
                            <button type="button" style="margin-bottom:10px;" class="btn btn-primary btn-sm"
							onClick="view_form('{{route('refakun.showform',['id' => ' ']) }}','Tambah Data','Simpan','modal-md','add')" ><i class="fas fa-plus"></i> Add Akun</button>

                            <table class="table table-bordered table-striped table-sm mb-0" id="tabel-akun">
                                <thead>
                                    <tr>
                                        <th class="text-center">Aksi</th>
                                        <th>Kode Akun</th>
                                        <th>Nama Akun</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <div class="btn-group flex-wrap">
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <td>101</td>
                                        <td>Akun Rekening penyelenggara pemerintah daerah</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    {{-- Contenct Kelompok --}}

                    <div id="kelompok" class="tab-pane">

                        <div class="card-text">
                            <button type="button" style="margin-bottom:10px;" class="btn btn-primary btn-sm"
                            onClick="view_form('{{route('refkelompok.showform',['id' => ' ']) }}','Tambah Data','Simpan','modal-md','add')" ><i class="fas fa-plus"></i> Add Kelompok</button>

                            <table class="table table-bordered table-striped table-sm mb-0" id="tabel-kelompok">
                                <thead>
                                    <tr>
                                        <th class="text-center">Aksi</th>
                                        <th>Akun</th>
                                        <th>Kode Kelompok</th>
                                        <th>Nama Kelompok</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <div class="btn-group flex-wrap">
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <td>101</td>
                                        <td>101.001</td>
                                        <td>Bidang penyelenggara pemerintah daerah</td>
                                    </tr>

                                    <tr>
                                        <td class="text-center">
                                            <div class="btn-group flex-wrap">
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <td>101</td>
                                        <td>101.002</td>
                                        <td>Bidang penyelenggara pemerintah daerah</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>


                    {{-- Contenct Jenis --}}

                    <div id="jenis" class="tab-pane">

                        <div class="card-text">
                            <button type="button" style="margin-bottom:10px;" class="btn btn-primary btn-sm"
							onClick="view_form('{{route('refjenisrekening.showform',['id' => ' ']) }}','Tambah Data','Simpan','modal-md','add')" ><i class="fas fa-plus"></i> Add Jenis Rekening</button>

                            <table class="table table-bordered table-striped table-sm mb-0" id="tabel-jenis">
                                <thead>
                                    <tr>
                                        <th class="text-center">Aksi</th>
                                        <th>Kelompok</th>
                                        <th>Kode Jenis</th>
                                        <th>Nama Jenis</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <div class="btn-group flex-wrap">
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <td>101.001</td>
                                        <td>101.001.001</td>
                                        <td>Bidang penyelenggara pemerintah daerah</td>
                                    </tr>

                                    <tr>
                                        <td class="text-center">
                                            <div class="btn-group flex-wrap">
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <td>101.002</td>
                                        <td>101.002.001</td>
                                        <td>Bidang penyelenggara pemerintah daerah</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                    {{-- Contenct Objek --}}

                    <div id="objek" class="tab-pane">

                        <div class="card-text">
                            <button type="button" style="margin-bottom:10px;" class="btn btn-primary btn-sm"
							onClick="view_form('{{route('refobjekrekening.showform',['id' => ' ']) }}','Tambah Data','Simpan','modal-md','add')" ><i class="fas fa-plus"></i> Add Objek</button>

                            <table class="table table-bordered table-striped table-sm mb-0" id="tabel-objek">
                                <thead>
                                    <tr>
                                        <th class="text-center">Aksi</th>
                                        <th>Jenis Rekening</th>
                                        <th>Kode Objek</th>
                                        <th>Nama Objek</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center">
                                            <div class="btn-group flex-wrap">
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <td>101.001</td>
                                        <td>101.001.001</td>
                                        <td>Bidang penyelenggara pemerintah daerah</td>
                                    </tr>

                                    <tr>
                                        <td class="text-center">
                                            <div class="btn-group flex-wrap">
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-edit"></i></button>
                                                <button type="button" class="btn btn-default btn-sm"><i class="fas fa-trash"></i></button>
                                            </div>
                                        </td>
                                        <td>101.002</td>
                                        <td>101.002.001</td>
                                        <td>Bidang penyelenggara pemerintah daerah</td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
</div>

@endsection
@section('javascript')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $('#tabel-akun').dataTable({
            "responsive": true
            , language: {
                paginate: {
                    previous: "<i class='fas fa-arrow-left'></i>"
                    , next: "<i class='fas fa-arrow-right'></i>"
                }
            }
            , "autoWidth": false
            , "columnDefs": [{
                    "width": "9%"
                    , "targets": 0
                    , "orderable": false
                    , "className": "dt-body-right"
                }
                , {
                    "width": "26%"
                    , "targets": 1
                    , "orderable": false
                }
                , {
                    "width": "15%"
                    , "targets":2
                    , "orderable": false
                }
            ]
            , "ajax": {
                "url": "{{ route('refakun.data') }}"
                , "type": "GET"
                , "serverSide": true
            }
        , });

        $('#tabel-kelompok').dataTable({
                "responsive": true
                , language: {
                    paginate: {
                        previous: "<i class='fas fa-arrow-left'></i>"
                        , next: "<i class='fas fa-arrow-right'></i>"
                    }
                }
                , "autoWidth": false
                , "columnDefs": [{
                        "width": "9%"
                        , "targets": 0
                        , "orderable": false
                        , "className": "dt-body-right"
                    }
                    , {
                        "width": "26%"
                        , "targets": 1
                        , "orderable": false
                    }
                    , {
                        "width": "15%"
                        , "targets":2
                        , "orderable": false
                    }
                ]
                , "ajax": {
                    "url": "{{ route('refkelompok.data') }}"
                    , "type": "GET"
                    , "serverSide": true
                }
            , }); 


        $('#tabel-jenis').dataTable({
                "responsive": true
                , language: {
                    paginate: {
                        previous: "<i class='fas fa-arrow-left'></i>"
                        , next: "<i class='fas fa-arrow-right'></i>"
                    }
                }
                , "autoWidth": false
                , "columnDefs": [{
                        "width": "9%"
                        , "targets": 0
                        , "orderable": false
                        , "className": "dt-body-right"
                    }
                    , {
                        "width": "26%"
                        , "targets": 1
                        , "orderable": false
                    }
                    , {
                        "width": "15%"
                        , "targets":2
                        , "orderable": false
                    }
                ]
                , "ajax": {
                    "url": "{{ route('refjenisrekening.data') }}"
                    , "type": "GET"
                    , "serverSide": true
                }
            , }); 
            $('#tabel-objek').dataTable({
                "responsive": true
                , language: {
                    paginate: {
                        previous: "<i class='fas fa-arrow-left'></i>"
                        , next: "<i class='fas fa-arrow-right'></i>"
                    }
                }
                , "autoWidth": false
                , "columnDefs": [{
                        "width": "9%"
                        , "targets": 0
                        , "orderable": false
                        , "className": "dt-body-right"
                    }
                    , {
                        "width": "26%"
                        , "targets": 1
                        , "orderable": false
                    }
                    , {
                        "width": "15%"
                        , "targets":2
                        , "orderable": false
                    }
                ]
                , "ajax": {
                    "url": "{{ route('refobjekrekening.data') }}"
                    , "type": "GET"
                    , "serverSide": true
                }
            , }); 
        $('.li-bidang').addClass('nav-active');
        $('.li-apbdes').addClass('nav-expanded nav-active');
        $('.li-referensi').addClass('nav-expanded nav-active');
        $('.li-rekening-desa').addClass('nav-active');
    });

</script>
@endsection
