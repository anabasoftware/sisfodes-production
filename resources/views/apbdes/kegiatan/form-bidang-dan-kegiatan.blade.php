<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	<input type="hidden" id="id_bidang" name="id_bidang">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
						<label class="form-control-label" for="kode_bidang">Kode Bidang</label>
						<input class="form-control" type="text" id="kode_bidang" name="kode_bidang">
						<label class="form-control-label" for="nama_bidang">Nama Bidang</label>
						<input class="form-control" type="text" id="nama_bidang" name="nama_bidang">
					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>

		var data = {!! json_encode($bidang) !!};

		if (data != null){
			$('#id_bidang').val(data.id_bidang);
			$('#kode_bidang').val(data.kode_bidang);
			$('#nama_bidang').val(data.nama_bidang);
		}

		var id = $('#id_bidang').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_bidang').val();
				url = "{{ route('refbidang.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Bidang berhasil disimpan.');
						$('#tabel-bidang').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data Bidang gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>