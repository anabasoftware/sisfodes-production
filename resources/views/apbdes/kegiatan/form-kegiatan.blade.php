<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	
		<input type="hidden" id="id_kegiatan" name="id_kegiatan">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
                        <label class="form-control-label" for="id_sub_bidang">Sub Bidang</label>
						<select id="id_sub_bidang" name="id_sub_bidang" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									
									foreach ($subbidang as $key => $val) {
										echo "<option value=\"".$val->id_sub_bidang."\">".$val->nama_sub_bidang."</option>";
									}
								@endphp
						</select>
						<label class="form-control-label" for="kode_kegiatan">Kode Kegiatan</label>
						<input class="form-control" type="text" id="kode_kegiatan" name="kode_kegiatan"}>						
						<label class="form-control-label" for="nama_kegiatan">Nama Kegiatan</label>
						<input class="form-control" type="text" id="nama_kegiatan" name="nama_kegiatan"}>
						
					
					

					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>


		var data = {!! json_encode($kegiatan) !!};

		if (data != null) {
			$('#id_kegiatan').val(data.id_kegiatan);
			$('#kode_kegiatan').val(data.kode_kegiatan);
			$('#nama_kegiatan').val(data.nama_kegiatan);
			$('#id_sub_bidang option[value='+data.id_sub_bidang+']').prop('selected',true);
		}

		var id = $('#id_kegiatan').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_kegiatan').val();
				url = "{{ route('refkegiatan.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Kegiatan berhasil disimpan.');
						$('#tabel-kegiatan').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data Kegiatan gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>