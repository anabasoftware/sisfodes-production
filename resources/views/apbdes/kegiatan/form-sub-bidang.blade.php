<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	
		<input type="hidden" id="id_sub_bidang" name="id_sub_bidang">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
						<label class="form-control-label" for="id_bidang">Bidang</label>
						<select id="id_bidang" name="id_bidang" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									
									foreach ($bidang as $key => $val) {
										echo "<option value=\"".$val->id_bidang."\">".$val->nama_bidang."</option>";
									}
								@endphp
						</select>
						<label class="form-control-label" for="kode_sub_bidang">Kode Sub Bidang</label>
						<input class="form-control" type="text" id="kode_sub_bidang" name="kode_sub_bidang"}>						
						<label class="form-control-label" for="nama_sub_bidang">Nama Sub Bidang</label>
						<input class="form-control" type="text" id="nama_sub_bidang" name="nama_sub_bidang"}>
						
					
					

					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>


		var data = {!! json_encode($subbidang) !!};

		if (data != null) {			
			$('#id_sub_bidang').val(data.id_sub_bidang);
			$('#kode_sub_bidang').val(data.kode_sub_bidang);
			$('#nama_sub_bidang').val(data.nama_sub_bidang);
			$('#id_bidang option[value='+data.id_bidang+']').prop('selected',true);
		}

		var id = $('#id_sub_bidang').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_sub_bidang').val();
				url = "{{ route('refsubbidang.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Sub Bidang berhasil disimpan.');
						$('#tabel-sub-bidang').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data Sub Bidang gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>