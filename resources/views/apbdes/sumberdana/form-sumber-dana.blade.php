<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	<input type="hidden" id="id_sumber_dana" name="id_sumber_dana">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
						<label class="form-control-label" for="kode_sumber">Kode Sumber Dana</label>
						<input class="form-control" type="text" id="kode_sumber" name="kode_sumber">
						<label class="form-control-label" for="nama_sumber">Nama Sumber Dana</label>
						<input class="form-control" type="text" id="nama_sumber" name="nama_sumber">
					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>

		var data = {!! json_encode($sumberdana) !!};

		if (data != null){
			$('#id_sumber_dana').val(data.id_sumber_dana);
			$('#kode_sumber').val(data.kode_sumber);
			$('#nama_sumber').val(data.nama_sumber);
		}

		var id = $('#id_sumber_dana').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_sumber_dana').val();
				url = "{{ route('refsumberdana.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Sumber Dana berhasil disimpan.');
						$('#tabel-sumber-dana').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data  Sumber Dana gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>