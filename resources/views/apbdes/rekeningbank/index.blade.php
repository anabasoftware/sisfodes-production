@extends('layouts.app')
@section('title') Referensi Sumber Dana dan Kegiatan @endsection
@section('breadcrumbs') <li><span>Referensi Sumber Dana dan Kegiatan</span></li> @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<style type="text/css">
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		<section class="card">
			<div class="alert alert-success" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
				<strong>Berhasil!</strong>&nbsp;
				<span class="msg-success"></span>
			</div>
			<div class="alert alert-danger" style="display:none">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true"><span aria-hidden="true">&times;</span></button>
				<strong>Gagal!</strong>&nbsp;
				<span class="msg-danger"></span>
			</div>
			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" class="btn btn-success btn-sm" 
							data-toggle="tooltip" 
							data-placement="top" 
							onclick="view_form('{{route('refbankdesa.showform',['id' => ' '])}}','Tambah Data','Simpan','modal-lg','add')" 
							data-original-title="Tambah Sumber Dana">
							<i class="fas fa-plus"></i>
						</button>
						<button type="button" class="btn btn-success btn-sm card-action-toggle" data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
				<p class="card-subtitle">Berikut ini adalah daftar sumber dana desa atau daerah yang telah tersimpan dan ditampilkan di halaman website.</p>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-ouput-kegiatan">
					<thead>
						<tr>
							<th class="text-center">Aksi</th>
							<th>Nama Kegiatan</th>
							<th>Kode Output</th>
							<th>Nama Output</th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</section>
	</div>
</div>

@endsection
@section('javascript')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $('#tabel-ouput-kegiatan').dataTable({
            "responsive": true
            , language: {
                paginate: {
                    previous: "<i class='fas fa-arrow-left'></i>"
                    , next: "<i class='fas fa-arrow-right'></i>"
                }
            }
            , "autoWidth": false
            , "columnDefs": [{
                    "width": "9%"
                    , "targets": 0
                    , "orderable": false
                    , "className": "dt-body-right"
                }
                , {
                    "width": "26%"
                    , "targets": 1
                    , "orderable": false
                }
                , {
                    "width": "15%"
                    , "targets": 2
                    , "orderable": false
                }
            ]
            , "ajax": {
                "url": "{{ route('refbankdesa.data') }}"
                , "type": "GET"
                , "serverSide": true
            }
        , });

        $('.li-apbdes').addClass('nav-expanded nav-active');
        $('.li-referensi').addClass('nav-expanded nav-active');
        $('.li-rekening-bank').addClass('nav-active');
    });

</script>
@endsection
