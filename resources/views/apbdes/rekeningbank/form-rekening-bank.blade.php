<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	<input type="hidden" id="id_bank" name="id_bank">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
						<label class="form-control-label" for="id_kegiatan">Pilih Rincian AKun</label>
						<select id="id_kegiatan" name="id_kegiatan" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									
									foreach ($akun as $key => $val) {
										echo "<option value=\"".$val->kode_objek_rekening."\">".$val->nama_objek_rekening."</option>";
									}
								@endphp
						</select>
						<label class="form-control-label" for="kode_rincian">Kode Rincian</label>
						<input class="form-control" type="text" id="kode_rincian" name="kode_rincian">
						<label class="form-control-label" for="norek">No Rekening</label>
						<input class="form-control" type="text" id="norek" name="norek">
						<label class="form-control-label" for="nama_bank">Nama Bank</label>
						<input class="form-control" type="text" id="nama_bank" name="nama_bank">
						<label class="form-control-label" for="kantor_cabang">Kantor Cabang</label>
						<input class="form-control" type="text" id="kantor_cabang" name="kantor_cabang">
						<label class="form-control-label" for="pemilik">Pemilik</label>
						<input class="form-control" type="text" id="pemilik" name="pemilik">
						<label class="form-control-label" for="alamat">Alamat</label>
						<input class="form-control" type="text" id="alamat" name="alamat">
						<label class="form-control-label" for="notelepon">No Telepon</label>
						<input class="form-control" type="text" id="notelepon" name="notelepon">
					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>

		var data = {!! json_encode($rekening) !!};

		if (data != null){
			$('#id_bank').val(data.id_bank);
			$('#kode_rincian').val(data.kode_rincian);
			$('#norek').val(data.norek);
			$('#nama_bank').val(data.nama_bank);
			$('#kantor_cabang').val(data.kantor_cabang);
			$('#pemilik').val(data.pemilik);
			$('#alamat').val(data.alamat);
			$('#notelepon').val(data.notelepon);
		}

		var id = $('#id_output_dd').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_output_dd').val();
				url = "{{ route('refoutputkegiatan.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Output Kegiatan berhasil disimpan.');
						$('#tabel-ouput-kegiatan').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data  Output Kegiatan gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>