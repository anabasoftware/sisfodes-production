<form class="form form-horizontal" 
	data-toggle="validator" 
	role="form" 
	method="post" 
	enctype="multipart/form-data">
	@csrf @method('POST')
	<div class="form-input">
	<input type="hidden" id="id_output_dd" name="id_output_dd">
		<div class="row">
			<div class="col-md-12">
				<div class="form-group">
					
						<label class="form-control-label" for="id_kegiatan">Jenis Kegiatan</label>
						<select id="id_kegiatan" name="id_kegiatan" data-plugin-selectTwo class="form-control kab">
								<option selected disabled value="">- Pilih -</option>
								@php
									
									foreach ($kegiatan as $key => $val) {
										echo "<option value=\"".$val->id_kegiatan."\">".$val->nama_kegiatan."</option>";
									}
								@endphp
						</select>
						<label class="form-control-label" for="kode_output">Kode Output</label>
						<input class="form-control" type="text" id="kode_output" name="kode_output">
						<label class="form-control-label" for="uraian">Uraian</label>
						<input class="form-control" type="text" id="uraian" name="uraian">
						<label class="form-control-label" for="satuan">Satuan</label>
						<input class="form-control" type="text" id="satuan" name="satuan">
					
				</div>
			</div>
			
		</div>
	</div>
</form>

<script>

		var data = {!! json_encode($outputdd) !!};

		if (data != null){
			$('#id_output_dd').val(data.id_ouput_dd);
			$('#kode_output').val(data.kode_output);
			$('#uraian').val(data.uraian);
			$('#satuan').val(data.satuan);
			$('#id_kegiatan option[value='+data.id_kegiatan+']').prop('selected',true);
		}

		var id = $('#id_output_dd').val();
		// alert(id)
		
        $('.b-action').on('click', function(e){
			if(!e.isDefaultPrevented()){
				var id = $('#id_output_dd').val();
				url = "{{ route('refoutputkegiatan.simpan') }}";
				
				$.ajax({
					url: url,
					type: 'post',
					data: new FormData($(".form")[0]),
					async: true,
					processData: false,
					contentType: false,
					success: function( data ){
						$('#modal-form').modal('hide');
						$('.alert-success').css('display', 'block').delay(5000).fadeOut()
						$('.msg-success').empty().append('Data Output Kegiatan berhasil disimpan.');
						$('#tabel-ouput-kegiatan').DataTable().ajax.reload();
					},
					error: function( response ){
						var errors = $.parseJSON(response.responseText);
						var errorMessage = '';
						$.each(errors.msg, function(key, value) {
							errorMessage += '<br/> - ' + value;
						});
						$('.alert-danger').css('display', 'block').delay(5000).fadeOut()
						$('.msg-danger').empty().append(errorMessage + '<br/><strong>Data  Output Kegiatan gagal disimpan.</strong>');
						$('#modal-form').modal('hide');
					}
				});
				return false;
			}
		});

</script>