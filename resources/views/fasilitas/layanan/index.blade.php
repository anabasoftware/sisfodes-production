@extends('layouts.app')
@section('title') Layanan Desa @endsection
@section('breadcrumbs') <li><span>Layanan Desa</span></li> @endsection
@section('css')
<link rel="stylesheet" href="{{ asset('vendor/datatables/media/css/dataTables.bootstrap4.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2-bootstrap-theme/select2-bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.css') }}" />
<style type="text/css">
</style>
@endsection
@section('content')
<div class="row">
	<div class="col-md-12">
		{{-- 
        @if(session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
        </div>
        @endif
		 --}}

		 @if(session('status'))
		 <div class="alert alert-success" role="alert">
			 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			 <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
		 </div>
		 @endif
		 @if ($errors->any())
		 <div class="alert alert-danger">
			 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			 <strong>{{ __('Gagal!') }}</strong><br />{{ session('message') }}. Perbaiki kolom isian berikut ini
			 <ul>
				 @foreach ($errors->all() as $error)
				 <li>{{ $error }}</li>
				 @endforeach
			 </ul>
		 </div>
		 @endif
		<section class="card">
			<header class="card-header">
				<div class="card-actions">
					<div class="btn-group flex-wrap">
						<button type="button" 
							class="btn btn-success btn-sm" 
							data-toggle="tooltip" 
							data-placement="top"
							onclick="location.href = '{{ route('fasilitas.layanan.tambah') }}'" 
							data-original-title="Tambah Layanan Daerah">
							<i class="fas fa-plus"></i>
						</button>
						<button type="button" 
							class="btn btn-success btn-sm card-action-toggle"
							data-card-toggle>
						</button>
					</div>
				</div>
				<h3 class="card-title">Data</h3>
				<p class="card-subtitle">Berikut ini adalah daftar Layanan desa atau daerah yang telah tersimpan dan ditampilkan di halaman website.</p>
			</header>
			<div class="card-body">
				<table class="table table-bordered table-striped table-sm mb-0" id="tabel-layanan">
					<thead>
						<tr>
							<th class="text-center">Aksi</th>
							<th>Nama Layanan</th>
							<th>Deskripsi</th>
							{{-- <th>Diterbitkan</th> --}}
							<th>Penulis</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="text-center">
								<div class="btn-group flex-wrap">
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-edit"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-trash"></i></button>
								</div>
							</td>
							<td>Kartu Keluarga</td>
							<td>Layanan pembuatan atau pembaharuan data kartu keluarga ...</td>
							{{-- <td><button class="btn btn-primary btn-sm">Ya</button></td> --}}
							<td>Desa Jalan Baru</td>
						</tr>
						<tr>
							<td class="text-center">
								<div class="btn-group flex-wrap">
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-edit"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-trash"></i></button>
								</div>
							</td>
							<td>Kartu Keluarga</td>
							<td>Layanan pembuatan atau pembaharuan data kartu keluarga ...</td>
							{{-- <td><button class="btn btn-primary btn-sm">Ya</button></td> --}}
							<td>Desa Jalan Baru</td>
						</tr>
						<tr>
							<td class="text-center">
								<div class="btn-group flex-wrap">
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-edit"></i></button>
									<button type="button" class="btn btn-default btn-sm"><i
											class="fas fa-trash"></i></button>
								</div>
							</td>
							<td>Kartu Keluarga</td>
							<td>Layanan pembuatan atau pembaharuan data kartu keluarga ...</td>
							{{-- <td><button class="btn btn-primary btn-sm">Ya</button></td> --}}
							<td>Desa Jalan Baru</td>
						</tr>
					</tbody>
				</table>
			</div>
		</section>
	</div>
</div>
@endsection
@section('javascript')
<script src="{{ asset('vendor/select2/js/select2.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/media/js/dataTables.bootstrap4.min.js') }}"></script>
<script type="text/javascript">
	$(function () {
		$('#tabel-layanan').dataTable({
			"responsive": true,
			language: {
				paginate: {
					previous: "<i class='fas fa-arrow-left'></i>",
					next: "<i class='fas fa-arrow-right'></i>"
				}
			},
			"autoWidth": false,
			"columnDefs": [
				{ "width": "9%", "targets": 0, "orderable": false, "className": "dt-body-right" },
				{ "width": "26%", "targets": 1, "orderable": false},
				{ "width": "15%", "targets": 3, "orderable": false}
			],
			"ajax": {
				"url": "{{ route('fasilitas.layanan.data') }}",
				"type": "GET",
				"serverSide": true
			},
		});

		$('.li-fasilitas').addClass('nav-expanded nav-active');
		$('.li-layanan').addClass('nav-expanded nav-active');
		$('.li--layanan').addClass('nav-active');
	});
</script> 
@endsection