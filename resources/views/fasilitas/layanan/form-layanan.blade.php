@extends('layouts.app')
@section('title') Layanan Desa @endsection
@if(empty($layanan->id_layanan))
@section('content_title') Tambah Layanan Desa @endsection
@else
@section('content_title') Sunting Layanan Desa @endsection
@endif
@section('breadcrumbs') 
<li><span>Layanan Desa</span></li> 
<li><span>Tambah</span></li> 
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('status'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Berhasil!') }}</strong><br />{{ session('message') }}
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong>{{ __('Gagal!') }}</strong><br />{{ session('message') }}. Perbaiki kolom isian berikut ini
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
		@endif
		<div class="alert alert-secondary">
			<i class="fas fa-info-circle"></i>
			<span class="text-muted">Berikut ini adalah formulir isian tentang layanan pemerintah desa untuk masyarakat.</span>
		</div>
        <section class="card">
            <div class="card-body">
				<form class="form form-horizontal" data-toggle="validator" role="form" method="post" enctype="multipart/form-data" action="@if(empty($layanan->id_layanan)){{route('fasilitas.layanan.simpan')}}@else{{route('fasilitas.layanan.update', $layanan->id_layanan)}}@endif">
				@csrf @method('POST')
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="nama_desa">Nama Desa</label>
							<input type="hidden" class="form-control" id="id_desa" name="id_desa"
								value="{{ Auth::user()->id_desa }}">
							<input type="hidden" class="form-control" id="id_wilayah" name="id_wilayah"
								value="{{ Auth::user()->desa->kode_wilayah }}">
							<input type="text" class="form-control" id="nama_desa" name="nama_desa" disabled
								value="{{ Auth::user()->desa->nama_desa }}">
						</div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="nama_layanan">Nama Layanan</label>
							<input type="text" class="form-control" id="nama_layanan" name="nama_layanan" value="@if(!empty($layanan->id_layanan)){{$layanan->nama_layanan}}@elseif(!empty(old('nama_layanan'))){{old('nama_layanan')}}@else{{old('nama_layanan')}}@endif">
							@if($errors->has('nama_layanan'))
							<div class="text-danger">
								{{ $errors->first('nama_layanan')}}
							</div>
							@endif
                        </div>
                    </div>
                    <div class="form-row">
						<div class="form-group col-md-12">
							<label for="slug_layanan">Slug </label>
							<input type="text" class="form-control" id="slug_layanan" name="slug_layanan" value="@if(!empty($layanan->id_layanan)){{$layanan->slug_layanan}}@elseif(!empty(old('slug_layanan'))){{old('slug_layanan')}}@else{{old('slug_layanan')}}@endif">
							@if($errors->has('slug_layanan'))
							<div class="text-danger">
								{{ $errors->first('slug_layanan')}}
							</div>
							@endif
						</div>
					</div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
							<label for="deskripsi_layanan">Deskripsi Layanan</label>
							<textarea class="form-control" name="deskripsi_layanan" id="deskripsi_layanan">@if(!empty($layanan->id_layanan)){{$layanan->deskripsi_layanan}}@elseif(!empty(old('deskripsi_layanan'))){{old('deskripsi_layanan')}}@else{{old('deskripsi_layanan')}}@endif</textarea>
							@if($errors->has('deskripsi_layanan'))
							<div class="text-danger">
								{{ $errors->first('deskripsi_layanan')}}
							</div>
							@endif
                        </div>
                    </div>
                    <hr class="dotted">
                    <div class="form-row">
                        <div class="col-md-12 text-left">
							@if(!empty($layanan->id_layanan)) 
							<button type="button" class="btn btn-default" onclick="history.back(-1)"><i class="fas fa-reply"></i> Kembali</button>
							@else
							<button type="button" class="btn btn-default" onclick="window.location.href='{{route('fasilitas.layanan.index')}}'"><i class="fas fa-window-close"></i> Batal</button>
							@endif
                            <button type="submit" class="btn btn-primary"><i class="fas fa-save"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
</div>
@endsection
@section('javascript')
<script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
	$(function () {
		slugify('#nama_layanan', '#slug_layanan');
		CKEDITOR.replace('deskripsi_layanan',{
			language: 'en',
			toolbar: [
					[ 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord'],
					[ 'Link', 'Unlink' ],
					[ 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ],
					[ 'Table', 'HorizontalRule' ],
					[ 'Bold', 'Italic', 'Strike', '-', 'NumberedList', 'BulletedList' ],
					[ 'Source', '-', 'Maximize']
			],
			cloudServices_tokenUrl: "{{ url('/dashboard/home') }}",
			cloudServices_uploadUrl: "{{ asset('storage/images/') }}",
			autoParagraph: false,
			width: '100%',
			height: '260',
		})
		$('.li-fasilitas').addClass('nav-expanded nav-active');
		$('.li-layanan').addClass('nav-expanded nav-active');
		$('.li-tambah-layanan').addClass('nav-active');
	});

    function delete_data(id) {
        if (confirm("Apakah yakin akan menghapus Data?")) {
            $.ajax({
                url: "hapus/"+id,
                type: "POST",
                data: {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success: function (data) {
                    window.location.href = '{{ route('fasilitas.layanan.index') }}'
                },
                error: function () {    
                    alert("Error: Tidak dapat menghapus Data!");
                }
            });
        }
    }
</script>
@endsection