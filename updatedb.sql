/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.4.6-MariaDB : Database - db_sisfodes2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_sisfodes2` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `db_sisfodes2`;

/*Table structure for table `sfd_master_anggaran` */

DROP TABLE IF EXISTS `sfd_master_anggaran`;

CREATE TABLE `sfd_master_anggaran` (
  `id_anggaran` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `kode_rekening` varchar(10) DEFAULT NULL,
  `nama_anggaran` varchar(200) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_anggaran`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_master_anggaran` */

insert  into `sfd_master_anggaran`(`id_anggaran`,`id_desa`,`kode_rekening`,`nama_anggaran`,`id_user`,`created_at`,`updated_at`) values (4,11,'101.001','PENDAPATAN ASLI DESA',2,'2020-04-06 08:17:23','2020-04-06 08:17:23'),(5,11,'101.002','PENDAPATAN TRANSFER',2,'2020-04-06 09:18:29','2020-04-06 09:18:29');

/*Table structure for table `sfd_master_anggaran_belanja` */

DROP TABLE IF EXISTS `sfd_master_anggaran_belanja`;

CREATE TABLE `sfd_master_anggaran_belanja` (
  `id_anggaran_belanja` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `kode_rekening` varchar(8) DEFAULT NULL,
  `nama_angggaran_belanja` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_anggaran_belanja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_master_anggaran_belanja` */

/*Table structure for table `sfd_realisasi_anggaran_belanja` */

DROP TABLE IF EXISTS `sfd_realisasi_anggaran_belanja`;

CREATE TABLE `sfd_realisasi_anggaran_belanja` (
  `id_realisasi_belaja` int(11) NOT NULL AUTO_INCREMENT,
  `tgl` date DEFAULT NULL,
  `nobukti` varchar(25) DEFAULT NULL,
  `id_renacana_belanja` int(11) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_realisasi_belaja`),
  KEY `id_renacana_belanja` (`id_renacana_belanja`) USING BTREE,
  CONSTRAINT `realisasi_anggaran_belanja_ibfk_1` FOREIGN KEY (`id_renacana_belanja`) REFERENCES `sfd_rencana_anggaran_belanja` (`id_rencana_belanja`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_realisasi_anggaran_belanja` */

/*Table structure for table `sfd_realisasi_penerimaan_anggaran` */

DROP TABLE IF EXISTS `sfd_realisasi_penerimaan_anggaran`;

CREATE TABLE `sfd_realisasi_penerimaan_anggaran` (
  `id_realiasasi_penerimaan` int(11) NOT NULL AUTO_INCREMENT,
  `id_renacan_penerimaan` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `nominal_diterima` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_realiasasi_penerimaan`),
  KEY `id_renacan_penerimaan` (`id_renacan_penerimaan`) USING BTREE,
  CONSTRAINT `realisasi_penerimaan_anggaran_ibfk_1` FOREIGN KEY (`id_renacan_penerimaan`) REFERENCES `sfd_rencana_penerimaan_anggaran` (`id_rencana_penerimaan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_realisasi_penerimaan_anggaran` */

/*Table structure for table `sfd_rencana_anggaran_belanja` */

DROP TABLE IF EXISTS `sfd_rencana_anggaran_belanja`;

CREATE TABLE `sfd_rencana_anggaran_belanja` (
  `id_rencana_belanja` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `nobukti` varchar(20) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `id_anggaran_belanja` int(11) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `total` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_rencana_belanja`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `rencana_anggaran_belanja_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_rencana_anggaran_belanja` */

/*Table structure for table `sfd_rencana_penerimaan_anggaran` */

DROP TABLE IF EXISTS `sfd_rencana_penerimaan_anggaran`;

CREATE TABLE `sfd_rencana_penerimaan_anggaran` (
  `id_rencana_penerimaan` int(11) NOT NULL AUTO_INCREMENT,
  `nomer` varchar(100) DEFAULT NULL,
  `id_desa` int(11) DEFAULT NULL,
  `id_anggaran` int(11) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `nominal_rencana` decimal(20,2) DEFAULT NULL,
  `realisasi` decimal(20,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rencana_penerimaan`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  KEY `id_penerimaan` (`id_anggaran`) USING BTREE,
  CONSTRAINT `sfd_rencana_penerimaan_anggaran_ibfk_1` FOREIGN KEY (`id_anggaran`) REFERENCES `sfd_master_anggaran` (`id_anggaran`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `sfd_rencana_penerimaan_anggaran_ibfk_2` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_rencana_penerimaan_anggaran` */

insert  into `sfd_rencana_penerimaan_anggaran`(`id_rencana_penerimaan`,`nomer`,`id_desa`,`id_anggaran`,`tgl`,`tahun_anggaran`,`deskripsi`,`nominal_rencana`,`realisasi`,`created_at`,`updated_at`,`id_user`) values (3,'RK.2004060001',11,4,NULL,2021,'angaran pendapatan desa',250000.00,NULL,'2020-04-06 10:14:35','2020-04-06 17:17:26',2),(4,'RK.2004060002',11,5,NULL,2021,'Pendapatan penduduk',2500035.00,NULL,'2020-04-06 10:17:52','2020-04-06 10:17:52',2);

/* Procedure structure for procedure `sp_auto` */

/*!50003 DROP PROCEDURE IF EXISTS  `sp_auto` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_auto`(
  in tb varchar (100),
  in isField varchar (20),
  in kd varchar (4)
)
BEGIN
  declare digit int ;
  DECLARE isNomer INT ;
  declare isKode Varchar (100) ;
  declare kode varchar (50) ;
  declare str text ;
  set @str = CONCAT(
    ' SELECT IFNULL(MAX(RIGHT(',
    isField,
    ',4)),0) into @jml FROM ',
    tb,' WHERE nomer LIKE "%',DATE_FORMAT(CURDATE(),'%y%m%d'),'%"'
  ) ;
  PREPARE stmt FROM @str ;
  EXECUTE stmt ;
  DEALLOCATE PREPARE stmt ;
  set digit = @jml ;
  if digit = 0 
  then set isNomer = 1 ;
  else set isNomer = digit + 1 ;
  end if ;
  set isKode = Concat(kd,DATE_FORMAT(CURDATE(),'%y%m%d'),'0000') ;
  set kode = substring(isKode,1,LENGTH(iskode)-LENGTH(CONVERT(isNomer,char)));
  SET kode = RPAD(kode,LENGTH(isKode),isNomer);
 
  Select 
    kode ;
END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
