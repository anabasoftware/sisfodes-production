<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BeritaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        for ($i=1; $i < 10; $i++) { 
            DB::table('berita')->insert([
                [
                    "id_berita" => null,
                    "id_desa" => '11',
                    "id_kategori_berita" => rand(1,5),
                    "judul_berita" => 'Judul berita website ' . $i,
                    "isi_berita" => 'Lorem Ipsum adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.',
                    "publish" => '1',
                    // "id_tag" => '1,2,3,4',
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now()
                ]
            ]);   
        }
    }
}
