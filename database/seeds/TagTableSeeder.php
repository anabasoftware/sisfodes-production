<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tag = ["ekonomi", "sosial", "budaya", "hot news", "itech"];
        for ($i=1; $i < 5; $i++) { 
            DB::table('berita_tags')->insert([
                [
                    "id_tag" => null,
                    "nama_tag" => array_rand($tag,1),
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now()
                ]
            ]);   
        }
    }
}
