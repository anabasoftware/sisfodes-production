<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KategoriBeritaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i < 10; $i++) { 
            DB::table('berita_kategori')->insert([
                [
                    "id_kategori_berita" => null,
                    "id_desa" => '11',
                    "nama_kategori" => 'Kategori Berita ' .$i,
                    "slug_kategori" => 'sluk-kategori-' . $i,
                    "created_at" => Carbon::now(),
                    "updated_at" => Carbon::now()
                ]
            ]);   
        }
    }
}
