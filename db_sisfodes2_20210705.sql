/*
SQLyog Ultimate v10.42 
MySQL - 5.5.5-10.5.4-MariaDB : Database - db_sisfodes2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_sisfodes2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_sisfodes2`;

/*Table structure for table `sfd_akun_rekening` */

DROP TABLE IF EXISTS `sfd_akun_rekening`;

CREATE TABLE `sfd_akun_rekening` (
  `id_akun_rekening` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `nomer_akun` varchar(5) DEFAULT NULL,
  `nama_akun` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_akun_rekening`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_akun_rekening` */

insert  into `sfd_akun_rekening`(`id_akun_rekening`,`id_desa`,`nomer_akun`,`nama_akun`) values (1,11,'1','ASET'),(2,11,'2','KEWAJIBAN'),(3,11,'3','EKUITAS'),(4,11,'4','PENDAPATAN'),(5,11,'5','BELANJA'),(6,11,'6','PEMBIAYAN');

/*Table structure for table `sfd_bank_desa` */

DROP TABLE IF EXISTS `sfd_bank_desa`;

CREATE TABLE `sfd_bank_desa` (
  `id_bank` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `kode_rincian` varchar(50) DEFAULT NULL,
  `norek` varchar(150) DEFAULT NULL,
  `nama_bank` varchar(200) DEFAULT NULL,
  `kantor_cabang` varchar(200) DEFAULT NULL,
  `pemilik` varchar(200) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `notelepon` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id_bank`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `sfd_bank_desa` */

/*Table structure for table `sfd_berita` */

DROP TABLE IF EXISTS `sfd_berita`;

CREATE TABLE `sfd_berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `id_kategori_berita` int(11) DEFAULT NULL,
  `judul_berita` varchar(200) DEFAULT NULL,
  `slug_judul_berita` varchar(200) DEFAULT NULL,
  `isi_berita` text DEFAULT NULL,
  `dilihat` int(11) DEFAULT NULL,
  `publikasi` tinyint(1) DEFAULT 0,
  `post_img` varchar(191) DEFAULT 'img/blog/medium/blog-1.jpg',
  `pin_berita` int(11) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_berita`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  KEY `id_kategori` (`id_kategori_berita`) USING BTREE,
  CONSTRAINT `lnk_sfd_berita_sfd_berita_kategori` FOREIGN KEY (`id_kategori_berita`) REFERENCES `sfd_berita_kategori` (`id_kategori_berita`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `lnk_sfd_desa_sfd_berita` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_berita` */

insert  into `sfd_berita`(`id_berita`,`id_desa`,`id_kategori_berita`,`judul_berita`,`slug_judul_berita`,`isi_berita`,`dilihat`,`publikasi`,`post_img`,`pin_berita`,`created_at`,`updated_at`) values (43,11,3,'Lorem ipsum dolor sit amet','lorem-ipsum-dolor-sit-amet','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',2,1,NULL,0,'2021-01-11 14:39:24','2021-06-17 11:15:03'),(44,11,1,'ini adalah judul beritassssss nya','ini-adalah-judul-beritassssss-nya','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',7,1,NULL,1,'2021-02-02 07:30:23','2021-06-17 11:15:17'),(45,11,2,'nihil molestias vero aspernatur','nihil-molestias-vero-aspernatur','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',0,1,NULL,0,'2021-03-16 07:30:23','2021-06-17 11:15:07'),(46,11,4,'Consequatur id quasi at optio','consequatur-id-quasi-at-optio','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',1,1,NULL,1,'2021-04-19 07:30:23','2021-06-17 11:15:18'),(47,11,3,'amet consectetur adipisicing elit','amet-consectetur-adipisicing-elit','<p style=\"text-align:justify\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur lectus lacus, rutrum sit amet placerat et, bibendum nec mauris. Duis molestie, purus eget placerat viverra, nisi odio gravida sapien, congue tincidunt nisl ante nec tellus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sagittis, massa fringilla consequat blandit, mauris ligula porta nisi, non tristique enim sapien vel nisl. Suspendisse vestibulum lobortis dapibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Praesent nec tempus nibh. Donec mollis commodo metus et fringilla. Etiam venenatis, diam id adipiscing convallis, nisi eros lobortis tellus, feugiat adipiscing ante ante sit amet dolor. Vestibulum vehicula scelerisque facilisis. Sed faucibus placerat bibendum. Maecenas sollicitudin commodo justo, quis hendrerit leo consequat ac. Proin sit amet risus sapien, eget interdum dui. Proin justo sapien, varius sit amet hendrerit id, egestas quis mauris.</p>\r\n\r\n<p style=\"text-align:justify\">Ut ac elit non mi pharetra dictum nec quis nibh. Pellentesque ut fringilla elit. Aliquam non ipsum id leo eleifend sagittis id a lorem. Sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam massa mauris, viverra et rhoncus a, feugiat ut sem. Quisque ultricies diam tempus quam molestie vitae sodales dolor sagittis. Praesent commodo sodales purus. Maecenas scelerisque ligula vitae leo adipiscing a facilisis nisl ullamcorper. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur non erat quam, id volutpat leo. Nullam pretium gravida urna et interdum. Suspendisse in dui tellus. Cras luctus nisl vel risus adipiscing aliquet. Phasellus convallis lorem dui. Quisque hendrerit, lectus ut accumsan gravida, leo tellus porttitor mi, ac mattis eros nunc vel enim. Nulla facilisi. Nam non nulla sed nibh sodales auctor eget non augue. Pellentesque sollicitudin consectetur mauris, eu mattis mi dictum ac. Etiam et sapien eu nisl dapibus fermentum et nec tortor.</p>\r\n\r\n<p style=\"text-align:justify\">Curabitur nec nulla lectus, non hendrerit lorem. Quisque lorem risus, porttitor eget fringilla non, vehicula sed tortor. Proin enim quam, vulputate at lobortis quis, condimentum at justo. Phasellus nec nisi justo. Ut luctus sagittis nulla at dapibus. Aliquam ullamcorper commodo elit, quis ornare eros consectetur a. Curabitur nulla dui, fermentum sed dapibus at, adipiscing eget nisi. Aenean iaculis vehicula imperdiet. Donec suscipit leo sed metus vestibulum pulvinar. Phasellus bibendum magna nec tellus fringilla faucibus. Phasellus mollis scelerisque volutpat. Ut sed molestie turpis. Phasellus ultrices suscipit tellus, ac vehicula ligula condimentum et.</p>\r\n\r\n<p style=\"text-align:justify\">Aenean metus nibh, molestie at consectetur nec, molestie sed nulla. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam nec euismod urna. Donec gravida pharetra ipsum, non volutpat ipsum sagittis a. Phasellus ut convallis ipsum. Sed nec dui orci, nec hendrerit massa. Curabitur at risus suscipit massa varius accumsan. Proin eu nisi id velit ultrices viverra nec condimentum magna. Ut porta orci quis nulla aliquam at dictum mi viverra. Maecenas ultricies elit in tortor scelerisque facilisis. Mauris vehicula porttitor lacus, vel pretium est semper non. Ut accumsan rhoncus metus non pharetra. Quisque luctus blandit nisi, id tempus tellus pulvinar eu. Nam ornare laoreet mi a molestie. Donec sodales scelerisque congue.</p>',3,1,NULL,1,'2021-05-26 07:30:23','2021-06-23 16:02:29');

/*Table structure for table `sfd_berita_banner` */

DROP TABLE IF EXISTS `sfd_berita_banner`;

CREATE TABLE `sfd_berita_banner` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `nama_banner` varchar(255) NOT NULL,
  `deskripsi_banner` text NOT NULL,
  `status` int(2) DEFAULT 1,
  `gambar` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_banner`),
  KEY `lnk_sfd_desa_sfd_berita_banner` (`id_desa`),
  CONSTRAINT `lnk_sfd_desa_sfd_berita_banner` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_berita_banner` */

insert  into `sfd_berita_banner`(`id_banner`,`id_desa`,`nama_banner`,`deskripsi_banner`,`status`,`gambar`,`created_at`,`updated_at`) values (11,11,'Portal Desa Loce','Sahu Timur adalah sebuah kecamatan di Kabupaten Halmahera Barat, Maluku Utara, Indonesia.',1,'banner__11_11_20200714152338.jpg','2020-07-09 14:39:45','2020-07-14 15:34:42');

/*Table structure for table `sfd_berita_detail` */

DROP TABLE IF EXISTS `sfd_berita_detail`;

CREATE TABLE `sfd_berita_detail` (
  `id_berita_detail` int(11) NOT NULL AUTO_INCREMENT,
  `id_berita` varchar(50) DEFAULT NULL,
  `id_tag_berita` varchar(50) DEFAULT NULL,
  `id_desa` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_berita_detail`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_berita_detail` */

insert  into `sfd_berita_detail`(`id_berita_detail`,`id_berita`,`id_tag_berita`,`id_desa`,`created_at`,`updated_at`) values (21,NULL,NULL,NULL,'2021-04-06 00:37:34',NULL);

/*Table structure for table `sfd_berita_kategori` */

DROP TABLE IF EXISTS `sfd_berita_kategori`;

CREATE TABLE `sfd_berita_kategori` (
  `id_kategori_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `nama_kategori` varchar(200) DEFAULT NULL,
  `slug_kategori` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_kategori_berita`),
  KEY `lnk_sfd_desa_sfd_berita_kategori` (`id_desa`),
  CONSTRAINT `fk_sfd_desa_sfd_berita_kategori` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_berita_kategori` */

insert  into `sfd_berita_kategori`(`id_kategori_berita`,`id_desa`,`nama_kategori`,`slug_kategori`,`status`,`created_at`,`updated_at`) values (1,11,'Kategori Berita 1','sluk-kategori-1',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(2,11,'Kategori Berita 2','sluk-kategori-2',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(3,11,'Kategori Berita 3','sluk-kategori-3',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(4,11,'Kategori Berita 4','sluk-kategori-4',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(5,11,'Kategori Berita 5','sluk-kategori-5',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(6,11,'Kategori Berita 6','sluk-kategori-6',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(7,11,'Kategori Berita 7','sluk-kategori-7',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(8,11,'Kategori Berita 8','sluk-kategori-8',1,'2021-04-04 04:49:01','2021-04-04 04:49:01'),(9,11,'Kategori Berita 9','sluk-kategori-9',1,'2021-04-04 04:49:01','2021-04-04 04:49:01');

/*Table structure for table `sfd_berita_kategori_sub` */

DROP TABLE IF EXISTS `sfd_berita_kategori_sub`;

CREATE TABLE `sfd_berita_kategori_sub` (
  `id_sub_kategori` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `id_kategori_berita` int(11) NOT NULL,
  `nama_sub_kategori` varchar(255) NOT NULL,
  `slug_sub_kategori` varchar(200) NOT NULL,
  `status` int(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_sub_kategori`),
  KEY `lnk_sfd_desa_sfd_berita_kategori_sub` (`id_desa`),
  KEY `lnk_sfd_berita_kategori_sfd_berita_kategori_sub` (`id_kategori_berita`),
  CONSTRAINT `fk_sfd_berita_kategori_sfd_berita_kategori_sub` FOREIGN KEY (`id_kategori_berita`) REFERENCES `sfd_berita_kategori` (`id_kategori_berita`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `fk_sfd_desa_sfd_berita_kategori_sub` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_berita_kategori_sub` */

/*Table structure for table `sfd_berita_tags` */

DROP TABLE IF EXISTS `sfd_berita_tags`;

CREATE TABLE `sfd_berita_tags` (
  `id_tag_berita` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `nama_tag` varchar(255) NOT NULL,
  `slug_tag` varchar(255) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_tag_berita`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `fk_sfd_berita_tags_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_berita_tags` */

insert  into `sfd_berita_tags`(`id_tag_berita`,`id_desa`,`nama_tag`,`slug_tag`,`status`,`created_at`,`updated_at`) values (42,11,'eko sos bud',NULL,1,'2021-04-05 17:33:14','2021-04-05 17:33:14'),(43,11,'eko sos bud',NULL,1,'2021-04-05 17:33:37','2021-04-05 17:33:37'),(44,11,'eko sos bud',NULL,1,'2021-04-05 17:34:07','2021-04-05 17:34:07'),(45,11,'eko sos bud',NULL,1,'2021-04-05 17:35:25','2021-04-05 17:35:25'),(46,11,'sosial budaya',NULL,1,'2021-04-05 17:35:25','2021-04-05 17:35:25'),(47,11,'perekonomian',NULL,1,'2021-04-05 17:35:25','2021-04-05 17:35:25'),(48,11,'eko sos bud',NULL,1,'2021-04-05 17:37:16','2021-04-05 17:37:16'),(50,11,'sosial budaya',NULL,1,'2021-04-05 17:37:34','2021-04-05 17:37:34');

/*Table structure for table `sfd_bidang` */

DROP TABLE IF EXISTS `sfd_bidang`;

CREATE TABLE `sfd_bidang` (
  `id_bidang` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `kode_bidang` varchar(10) DEFAULT NULL,
  `nama_bidang` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_bidang`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_bidang` */

insert  into `sfd_bidang`(`id_bidang`,`id_desa`,`kode_bidang`,`nama_bidang`) values (6,11,'ABC','afasfafs'),(8,11,'JOS SEKALI','PERTANIAN');

/*Table structure for table `sfd_desa` */

DROP TABLE IF EXISTS `sfd_desa`;

CREATE TABLE `sfd_desa` (
  `id_desa` int(11) NOT NULL AUTO_INCREMENT,
  `kode_wilayah` varchar(100) DEFAULT NULL,
  `kepala_desa` varchar(200) DEFAULT NULL,
  `id_kecamatan` int(11) DEFAULT NULL,
  `nama_desa` varchar(100) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `desa_id` int(11) DEFAULT NULL,
  `no_telp` varchar(20) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `peta_wilayah` text DEFAULT NULL,
  `logo` varchar(200) DEFAULT NULL,
  `kode_pos` varchar(10) DEFAULT NULL,
  `visi` text DEFAULT NULL,
  `misi` text DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_desa`),
  KEY `id_kecamatan` (`id_kecamatan`) USING BTREE,
  KEY `id_kepala_desa` (`kepala_desa`) USING BTREE,
  KEY `kode_wilayah` (`kode_wilayah`) USING BTREE,
  CONSTRAINT `lnk_sfd_desa_kecamatan` FOREIGN KEY (`id_kecamatan`) REFERENCES `sfd_master_kecamatan` (`id_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_desa` */

insert  into `sfd_desa`(`id_desa`,`kode_wilayah`,`kepala_desa`,`id_kecamatan`,`nama_desa`,`alamat`,`desa_id`,`no_telp`,`fax`,`website`,`peta_wilayah`,`logo`,`kode_pos`,`visi`,`misi`,`status`,`updated_at`,`created_at`) values (0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,1,'2020-08-24 00:01:54',NULL),(11,'82.01.09.2001','Drs. Joko Purnomo, M.Pd, M.Si, M.Kom, MH',5483,'Loce','Jl. Loce, Kecamatan Sahu Timur, Kabupaten Halmahera Barat, Maluku Utara',165,'0853-4004-6724','0853-4004-6724','desaloce.desa.dev','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15956.47836281679!2d127.4550362767079!3d1.0723140374848803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x329b500beeadbcfb%3A0xfe9e3f84c455a46e!2sJl.%20Baru%2C%20Jailolo%2C%20Kabupaten%20Halmahera%20Barat%2C%20Maluku%20Utara!5e0!3m2!1sid!2sid!4v1582555104368!5m2!1sid!2sid\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\"></iframe>','logo__1594741540.png','97758','<p><em>&quot;Terwujudnya kehidupan masyarakat desa loce yang religius, aman, harmonis, maju, adil dan berkah&quot;</em></p>','<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto cupiditate ex ab tenetur laudantium ducimus tempora nulla quo unde distinctio doloribus eos, amet in et vero repellendus vel expedita praesentium?</p>',1,'2020-08-23 23:38:54','2020-07-27 22:48:38'),(26,'82.01.09.2018','Drs. Eko Nugroho, M.Pd, M.Si, M.Kom, MH',5483,'Air Panas','Jl. Air Panas, Kecamatan Sahu Timur, Kabupaten Halmahera Barat, Maluku Utara',NULL,NULL,NULL,'airpanas.desa.dev',NULL,NULL,'97753',NULL,NULL,1,'2021-06-17 14:02:13','2020-08-23 17:05:27');

/*Table structure for table `sfd_desa_dokumen` */

DROP TABLE IF EXISTS `sfd_desa_dokumen`;

CREATE TABLE `sfd_desa_dokumen` (
  `id_dokumen_desa` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `judul_dokumen` varchar(200) DEFAULT NULL,
  `file` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_dokumen_desa`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `fk_sfd_dokumen_desa_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_desa_dokumen` */

/*Table structure for table `sfd_desa_layanan` */

DROP TABLE IF EXISTS `sfd_desa_layanan`;

CREATE TABLE `sfd_desa_layanan` (
  `id_layanan` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `kode_wilayah` varchar(100) DEFAULT NULL,
  `slug_layanan` varchar(200) DEFAULT NULL,
  `nama_layanan` varchar(200) NOT NULL,
  `deskripsi_layanan` text DEFAULT NULL,
  `post_img` varchar(255) DEFAULT NULL,
  `dilihat` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_layanan`),
  KEY `id_desa` (`id_desa`),
  CONSTRAINT `FK_sfd_layanan_desa_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_desa_layanan` */

insert  into `sfd_desa_layanan`(`id_layanan`,`id_desa`,`kode_wilayah`,`slug_layanan`,`nama_layanan`,`deskripsi_layanan`,`post_img`,`dilihat`,`created_at`,`updated_at`) values (3,11,NULL,'surat-keterangan-lahir','Surat Keterangan Lahir','Salah satu tugas atau layanan desa ialah melakukan pengurusan akte lahir atau surat keterangan lahir.&nbsp;<br />\r\nUrus akte kelahiran&nbsp;di kantor desa harus membawa persyaratan, berupa fotokopi KK dan KTP, fotokopi surat nikah, surat pengantar dari Rt/Rw, fotokopi lunas PBB, fotokopi surat keterangan lahir yang diberikan oleh pihak Rumah sakit atau Bidan Desa.',NULL,NULL,'2021-06-21 21:16:59','2021-06-21 15:11:03'),(4,11,NULL,'pelatihan-desa','Pelatihan Desa','Pelatihan desa&nbsp;biasanya dilakukan untuk aparatur desa, dalam rangka meningkatkan kapasitas dari pihak pemerintah desa sekaligus BPD, dengan tujuan agar para peserta yang mengikuti pelatihan bisa mengetahui fungsi kerja dan tugas-tugas dalam pemerintah desa. Selain itu, para peserta juga bisa menguraikan tanggung jawab dan tugas kerja masing-masing. Seperti tugas-tugas dari pihak aparatur desa berikut ini :<br />\r\nSekretaris desa<br />\r\nTugasnya ialah membantu Kepala Desa untuk melaksanakan dan mempersiapkan pengelolaan administrasi desa sekaligus mempersiapkan penyusunan laporan mengenai penyelenggaraan pemerintah.<br />\r\nAdapun fungsinya ialah untuk penyelenggaraan kegiatan administrasi serta mempersiapkan bahan laporan guna kelancaran tugas Kades (Kepala Desa).<br />\r\nKaur Umum<br />\r\nBertugas untuk membantu sekretaris desa melalui pelaksanaan pengelolaan sumber APBD, mempersiapkan bahan laporan penyusunan APBDes dan pengelolaan administrasi untuk keuangan desa.&nbsp;<br />\r\nAdapun fungsinya yaitu untuk melaksanakan persiapan penyusunan APBDes, pengelolaan administrasi pada keuangan desa,&nbsp; dan sebagainya.<br />\r\nKaur Kesos<br />\r\nBertugas untuk mempersiapkan bahan perumusan tentang kebijakan teknis dari penyusunan keagamaan dan melaksanakan program berkaitan dengan pemberdayaan masyarakat.<br />\r\nAdapun fungsinya mempersiapkan bahan dalam program pemberdayaan masyarakat dan persiapan bahan pelaksanaan program yang berkaitan dengan kegiatan keagamaan.<br />\r\nKaur Ekbang<br />\r\nBertugas untuk membantu desa melaksanakan persiapan bahan perumusan tentang kebijakan teknis dalam pengembangan potensi desa dan ekonomi masyarakat, serta pengelolaan administrasi pembangunan.<br />\r\nFungsinya ialah mempersiapkan bantuan kajian perkembangan ekonomi masyarakat dan membantu pelaksanaan administrasi pembangunan serta pengelolaan tugas pembangunan yang diberikan kepala desa.',NULL,NULL,'2021-06-21 21:16:43','2021-06-21 15:10:59'),(5,11,NULL,'daftar-ktp','Daftar KTP','Daftar&nbsp;KTP&nbsp;online&nbsp;atau biasa disebut sebagai KTP elektronik maupun&nbsp;e-KTP juga dilakukan di kantor desa. KTP elektronik ialah kartu tanda penduduk dilengkapi oleh cip sebagai identitas resmi dari penduduk yang terdaftar. Jika tak ada perubahan berdasarkan kebijakan pemerintah, maka KTP dapat berlaku hingga seumur hidup.<br />\r\nUntuk persyaratan pengajuan e-KTP diantaranya meliputi perekaman, fotokopi KK, surat pengantar dari Kepala&nbsp; Desa yang diketahui camat, dan fotokopi akta nikah untuk pemohon yang usianya belum genap 17 tahun. Sesudah Anda merekam data, maka selanjutnya petugas akan mencetak KTP elektronik dengan syarat form permohonan cetak KTP-elektronik, surat keterangan atau KTP lama dan fotokopi KK.<br />\r\njika KTP Anda hilang, bisa mengajukan pencetakan KTP pengganti langsung di kantor kecamatan dan harus melampirkan fotokopi KK, surat keterangan KTP hilang dari pihak kepolisian, dan surat pernyataan kehilangan dengan materai.',NULL,NULL,'2021-06-21 21:15:49','2021-06-21 15:10:54'),(7,11,NULL,'pembuatan-ktp','Pembuatan KTP','Adapun syarat keterangan untuk pembuatan KTP para pemula, diwajibkan membawa surat pengantar dari RT atau RW, menyertakan fotokopi KK, fotokopi lunas PBB dan pas foto berukuran 3 x 4 sebanyak 3 lembar. Ketentuannya usia pemohon harus berusia 17 tahun minimal.<br />\r\nSementara untuk membuat surat keterangan perpanjangan KTP, pemohon harus menyertakan surat pengantar RT/RW, KTP asli, pas foto berukuran 3 x 4 sebanyak 3 lembar, fotokopi lunas PBB, dan fotokopi KK.',NULL,NULL,'2021-06-21 21:15:34','2021-06-21 15:10:50'),(8,11,NULL,'perubahan-data-kartu-keluarga','Perubahan Data Kartu Keluarga','Pelayanan birokrasi desa/administrasi lainnya yaitu untuk pembuatan kartu keluarga. Untuk urus kartu keluarga di desa, Anda harus membawa surat pengantar dari Rt/Rw dan KK asli.',NULL,NULL,'2021-06-21 21:15:20','2021-06-21 15:10:45'),(9,11,NULL,'pengurusan-surat-kematian','Pengurusan Surat Kematian','Urus surat kematian&nbsp;sangat penting untuk mencegah agar data-data almarhum/almarhumah disalahgunakan pihak tertentu yang tidak bertanggung jawab. Sementara tujuan pemerintah agar dapat memastikan keakuratan informasi dan data penduduk yang potensial untuk dijadikan pemilih dalam Pilkada atau Pemilu.&nbsp;<br />\r\nAdapun persyaratan yang harus dipenuhi saat mengurus surat keterangan kematian, ialah surat keterangan kematian dari Rumah Sakit jika yang meninggal di rumah sakit, fotokopi KTP yang meninggal dan surat pengantar RT/RW.',NULL,NULL,'2021-06-21 21:15:09','2021-06-21 15:10:40'),(10,11,NULL,'apbd-desa','APBD Desa','Anggaran Pendapatan dan Belanja Desa atau APBDesa, yaitu rencana keuangan pemerintah desa tahunan yang ditetapkan dan dibahas Kepala Desa dengan BPD atau Badan Permusyawaratan Desa lewat Peraturan Desa.<br />\r\nAPB Desa merupakan dokumen dengan kekuatan hukum untuk menjamin kepastian terhadap rencana kegiatan, artinya mengikat aparatur dan pemerintah desa terkait, dalam melaksanakan kegiatan berdasarkan rencana yang sudah ditetapkan sekaligus untuk menjamin ketersediaan anggaran dengan jumlah tertentu. APB Desa sendiri menjamin kelayakan kegiatan dalam hal pendanaan, karenanya bisa dipastikan kelayakan pada hasil aktivitas secara teknis.',NULL,NULL,'2021-06-21 21:14:55','2021-06-21 15:10:35'),(11,11,NULL,'jaringan-aspirasi-rakyat','Jaringan Aspirasi Rakyat','Badan Permusyawaratan Desa disingkat BPD ialah lembaga perwujudan dari demokrasi penyelenggaraan pemerintah desa. BPD inilah yang berfungsi untuk menetapkan semua peraturan desa dengan kepala desa, sekaligus dapat menyalurkan dan menampung aspirasi masyarakat sesuai peraturan Undang-Undang No. 6 Tahun 2014.&nbsp;',NULL,NULL,'2021-06-21 21:14:44','2021-06-21 15:10:31'),(12,11,NULL,'lapor-pemerintah-desa','Lapor Pemerintah Desa','Salah satu pelayanan desa yaitu dapat mempermudah masyarakat atau warga untuk melaporkan keluhan sekaligus aspirasi Anda mengenai pemerintah desa terkait. Masyarakat bisa melaporkan secara langsung ke Badan Permusyawaratan Desa. Selain itu, kini pemerintah juga menyediakan layanan berupa&nbsp;call center&nbsp;di 1500040 untuk masyarakat agar bisa melaporkan berbagai macam masalah yang berhubungan dengan desa. Itulah beberapa jenis dan bentuk pelayanan desa dalam mewadahi aktivitas masyarakat sekaligus sebagai jaringan aspirasi warga terkait hal-hal yang berkaitan dengan kegiatan desa. Sehingga semua fungsi dan bentuk layanan desa dapat berjalan dengan baik dalam lingkungan masyarakat, sebagaimana mestinya.',NULL,NULL,'2021-06-21 21:14:33','2021-06-21 15:07:49');

/*Table structure for table `sfd_desa_lembaga` */

DROP TABLE IF EXISTS `sfd_desa_lembaga`;

CREATE TABLE `sfd_desa_lembaga` (
  `id_lembaga` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) NOT NULL,
  `kode_wilayah` varchar(50) DEFAULT NULL,
  `nama_lembaga` varchar(200) DEFAULT NULL,
  `deskripsi_lembaga` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_lembaga`),
  KEY `index_id_desa` (`id_desa`),
  CONSTRAINT `FK_sfd_lembaga_desa_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_desa_lembaga` */

insert  into `sfd_desa_lembaga`(`id_lembaga`,`id_desa`,`kode_wilayah`,`nama_lembaga`,`deskripsi_lembaga`,`created_at`,`updated_at`) values (4,11,'82.01.09.2001','ss','sssss','2021-06-25 14:14:08','2021-06-25 14:14:08');

/*Table structure for table `sfd_desa_potensi` */

DROP TABLE IF EXISTS `sfd_desa_potensi`;

CREATE TABLE `sfd_desa_potensi` (
  `id_potensi` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `nama_potensi` varchar(200) DEFAULT NULL,
  `deskripsi_potensi` text DEFAULT NULL,
  `gambar` varchar(200) DEFAULT NULL,
  `slug_potensi` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_potensi`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `potensi_desa_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_desa_potensi` */

insert  into `sfd_desa_potensi`(`id_potensi`,`id_desa`,`nama_potensi`,`deskripsi_potensi`,`gambar`,`slug_potensi`,`created_at`,`updated_at`) values (1,11,'Pertanian Tanaman Pangan','Pertanian tanaman pangan memiliki andil besar dalam perekonomian di Kabupaten Wonosobo dengan komoditasnya meliputi tanaman padi dan palawija, tanaman sayuran dan tanaman buah-buahan. Pada tahun 2011, luas panen padi sawah mengalami penurunan sebesar 978 Ha (3,21%) diikuti penurunan produksi padi sawah sebesar 12.115 ton, begitu pula padi gogo mengalami penurunan sebesar 126 ton dari 1.225 ton di tahun 2010 yang disebabkan turunnya luas panen sebesar 335 Ha. Produksi jagung juga turun sebesar 8,99% sementara ubi kayu mengalami kenaikan 7,63%.\r\n<p>&nbsp;</p>\r\n\r\n<p>Sedangkan produksi sayuran mengalami peningkatan yang cukup signifikan kecuali kacang panjang, tomat, kobis dan kacang merah. Sementara produksi buah-buahan sebagian besar mengalami penurunan.</p>','potensi__11_1_20200723154252.jpeg','pertanian-tanaman-pangan','2020-07-21 15:32:26','2020-07-23 15:42:52'),(2,11,'Perkebunan','Tanaman perkebunan yang potensial di Kabupaten Wonosobo adalah kelapa dalam, kelapa deres, kopi dan tembakau.&nbsp;\r\n<p>&nbsp;</p>\r\n\r\n<p>Pada tahun 2011 produksi kelapa dalam naik 26,98%, produksi kelapa deres naik 34,24%, produksi tembakau naik 56,69% dan produksi kopi turun sangat drastis yaitu mencapai 99%.</p>','potensi__11_2_20200723154239.jpeg','perkebunan','2020-06-29 15:33:45','2020-07-24 21:27:07'),(3,11,'Peternakan','Jenis ternak yang banyak diusahakan di Kabupaten Wonosobo tahun 2010 dan populasinya cukup besar adalah sapi potong (27.687 ekor), kambing (136.706 ekor), domba (90.267 ekor) dan ayam buras (665.238 ekor).\r\n<p>&nbsp;</p>\r\n\r\n<p>Hasil produksi peternakan tahun 2011 yaitu daging naik menjadi 47.452 kw dari 42.873 kw pada tahun 2010, produksi susu meningkat dari tahun 2010 sebesar 556.316 lt menjadi 559.676 lt pada tahun 2011 dan produksi telor menurun dari 25.321 kw tahun 2010 menjadi 23.758 kw tahun 2011.</p>','potensi__11_3_20200723154110.jpeg','peternakan','2020-05-02 15:34:17','2020-07-24 20:47:17'),(4,11,'Perikananan','Usaha perikanan di Kabupaten Wonosobo banyak diusahakan di kolam, karamba, waduk, telaga dan sungai dengan produksi pada tahun 2011 mencapai 5.830,24 ton. Di samping itu juga ada usaha pembenihan yang dilakukan di pembenihan rakyat, balai benih ikan maupun pembenihan sawah.','potensi__11_4_20200723154034.jpeg','perikananan','2020-04-10 15:34:36','2020-07-24 20:47:26'),(5,11,'Kehutanan','Hutan yang ada di Kabupaten Wonosobo terdiri dari hutan rakyat dan hutan negara. Luas hutan rakyat sebesar 18.982 Ha dengan sebagian besar tanaman berupa sengon/albasia, mahoni, suren d an jemitri. Sedangkan hasil dari hutan negara adalah kayu pertukangan, kayu bakar, kopal, getah pinus, gondorukem dan terpenting.','potensi__11_5_20200723135659.jpg','kehutanan','2020-03-17 15:35:05','2020-07-24 20:47:39');

/*Table structure for table `sfd_desa_wilayah` */

DROP TABLE IF EXISTS `sfd_desa_wilayah`;

CREATE TABLE `sfd_desa_wilayah` (
  `id_wilayah` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `luas_wilayah_tanah` decimal(20,2) DEFAULT NULL,
  `luas_daratan` decimal(20,2) DEFAULT NULL,
  `luas_persawahan` decimal(20,2) DEFAULT NULL,
  `luas_tegalan` decimal(20,2) DEFAULT NULL,
  `luas_lain_lain` decimal(20,2) DEFAULT NULL,
  `luas_tanah_desa` decimal(20,2) DEFAULT NULL,
  `jumlah_dusun` int(11) DEFAULT NULL,
  `jumlah_rt` int(11) DEFAULT NULL,
  `jumlah_rw` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_wilayah`),
  UNIQUE KEY `id_wilayah` (`id_wilayah`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `fk_sfd_desa_wilayah_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_desa_wilayah` */

insert  into `sfd_desa_wilayah`(`id_wilayah`,`id_desa`,`luas_wilayah_tanah`,`luas_daratan`,`luas_persawahan`,`luas_tegalan`,`luas_lain_lain`,`luas_tanah_desa`,`jumlah_dusun`,`jumlah_rt`,`jumlah_rw`,`created_at`,`updated_at`) values (1,11,120.00,123.00,127.00,123.00,123.00,123.00,123,123,123,'2020-02-11 15:41:22','2020-02-11 15:54:11');

/*Table structure for table `sfd_geografi` */

DROP TABLE IF EXISTS `sfd_geografi`;

CREATE TABLE `sfd_geografi` (
  `id_geografi` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `barat` varchar(200) DEFAULT NULL,
  `utara` varchar(200) DEFAULT NULL,
  `selatan` varchar(200) DEFAULT NULL,
  `timur` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_geografi`),
  KEY `id_desa` (`id_desa`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_geografi` */

/*Table structure for table `sfd_jenis_rekening` */

DROP TABLE IF EXISTS `sfd_jenis_rekening`;

CREATE TABLE `sfd_jenis_rekening` (
  `id_jenis_rekening` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `id_kelompok_rekening` int(11) DEFAULT NULL,
  `kode_jenis_rekening` varchar(5) DEFAULT NULL,
  `nama_jenis_rekening` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_rekening`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_jenis_rekening` */

insert  into `sfd_jenis_rekening`(`id_jenis_rekening`,`id_desa`,`id_kelompok_rekening`,`kode_jenis_rekening`,`nama_jenis_rekening`) values (1,NULL,1,'1.1.1','Kas dan Bank'),(2,NULL,1,'1.1.2','Piutang'),(3,NULL,1,'1.1.3','Persediaan');

/*Table structure for table `sfd_keadaan_statistik` */

DROP TABLE IF EXISTS `sfd_keadaan_statistik`;

CREATE TABLE `sfd_keadaan_statistik` (
  `id_statistik` int(11) NOT NULL AUTO_INCREMENT,
  `kategori` enum('LAHAN SAWAH','LAHAN KERING','LAHAN LAINYA') DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `luas` decimal(10,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_statistik`),
  UNIQUE KEY `id_statistik` (`id_statistik`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_keadaan_statistik` */

/*Table structure for table `sfd_kegiatan` */

DROP TABLE IF EXISTS `sfd_kegiatan`;

CREATE TABLE `sfd_kegiatan` (
  `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `id_sub_bidang` int(11) DEFAULT NULL,
  `kode_kegiatan` varchar(10) DEFAULT NULL,
  `nama_kegiatan` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_kegiatan`),
  KEY `id_sub_bidang` (`id_sub_bidang`),
  KEY `kode_kegiatan` (`kode_kegiatan`),
  CONSTRAINT `sfd_kegiatan_ibfk_1` FOREIGN KEY (`id_sub_bidang`) REFERENCES `sfd_sub_bidang` (`id_sub_bidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_kegiatan` */

insert  into `sfd_kegiatan`(`id_kegiatan`,`id_desa`,`id_sub_bidang`,`kode_kegiatan`,`nama_kegiatan`) values (1,11,1,'191','aaawawa');

/*Table structure for table `sfd_kelompok_rekening` */

DROP TABLE IF EXISTS `sfd_kelompok_rekening`;

CREATE TABLE `sfd_kelompok_rekening` (
  `id_kelompok_rekening` int(11) NOT NULL AUTO_INCREMENT,
  `id_akun` int(11) DEFAULT NULL,
  `kode_kelompok` varchar(5) DEFAULT NULL,
  `nama_kelompok` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_rekening`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_kelompok_rekening` */

insert  into `sfd_kelompok_rekening`(`id_kelompok_rekening`,`id_akun`,`kode_kelompok`,`nama_kelompok`) values (1,1,'1.1','Aset Lancar'),(2,1,'1.2','Investasi'),(3,1,'1.3','Aset Tetap'),(4,1,'1.4','Dana Cadangan'),(5,1,'1.5','Aset Tidak Lancar Lainya'),(6,2,'2.1','Kewajiban Jangka Pendek'),(7,3,'3.1','Ekuitas'),(8,4,'4.1','Pendapatan'),(9,4,'4.2','Pendapatan Asli Desa');

/*Table structure for table `sfd_lok_kabupaten` */

DROP TABLE IF EXISTS `sfd_lok_kabupaten`;

CREATE TABLE `sfd_lok_kabupaten` (
  `id_kabupaten` int(10) NOT NULL AUTO_INCREMENT,
  `provinsi_id` int(10) DEFAULT NULL,
  `nama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_kabupaten`)
) ENGINE=InnoDB AUTO_INCREMENT=8273 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_lok_kabupaten` */

insert  into `sfd_lok_kabupaten`(`id_kabupaten`,`provinsi_id`,`nama`) values (8201,82,'Kabupaten Halmahera Barat'),(8202,82,'Kabupaten Halmahera Tengah'),(8203,82,'Kabupaten Kepulauan Sula'),(8204,82,'Kabupaten Halmahera Selatan'),(8205,82,'Kabupaten Halmahera Utara'),(8206,82,'Kabupaten Halmahera Timur'),(8207,82,'Kabupaten Pulau Morotai'),(8208,82,'Kabupaten Pulau Taliabu'),(8271,82,'Kota Ternate'),(8272,82,'Kota Tidore Kepulauan');

/*Table structure for table `sfd_master_anggaran_belanja` */

DROP TABLE IF EXISTS `sfd_master_anggaran_belanja`;

CREATE TABLE `sfd_master_anggaran_belanja` (
  `id_anggaran_belanja` int(11) NOT NULL AUTO_INCREMENT,
  `tahun_anggaran` int(11) DEFAULT NULL,
  `kode_rekening` varchar(8) DEFAULT NULL,
  `nama_angggaran_belanja` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_anggaran_belanja`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_master_anggaran_belanja` */

/*Table structure for table `sfd_master_desa` */

DROP TABLE IF EXISTS `sfd_master_desa`;

CREATE TABLE `sfd_master_desa` (
  `desa_id` int(11) NOT NULL AUTO_INCREMENT,
  `id_kecamatan` int(11) DEFAULT NULL,
  `nama_desa` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`desa_id`),
  UNIQUE KEY `desa_id` (`desa_id`),
  KEY `id_kecamatan` (`id_kecamatan`) USING BTREE,
  CONSTRAINT `sfd_masterdesa_ibfk_1` FOREIGN KEY (`id_kecamatan`) REFERENCES `sfd_master_kecamatan` (`id_kecamatan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_master_desa` */

insert  into `sfd_master_desa`(`desa_id`,`id_kecamatan`,`nama_desa`,`created_at`,`updated_at`) values (1,5476,'Ake Boso','2020-02-21 23:24:25','2020-02-21 23:24:25'),(2,5476,'Akesibu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(3,5476,'Gam Ici','2020-02-21 23:24:25','2020-02-21 23:24:25'),(4,5476,'Gam Lamo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(5,5476,'Kampung Baru','2020-02-21 23:24:25','2020-02-21 23:24:25'),(6,5476,'Kie Lei (Kie Ici)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(7,5476,'Maritango','2020-02-21 23:24:25','2020-02-21 23:24:25'),(8,5476,'Naga','2020-02-21 23:24:25','2020-02-21 23:24:25'),(9,5476,'Soana Masungi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(10,5476,'Tahafo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(11,5476,'Tobaol','2020-02-21 23:24:25','2020-02-21 23:24:25'),(12,5476,'Togola Sanger/Sangir','2020-02-21 23:24:25','2020-02-21 23:24:25'),(13,5476,'Togola Wayolo/Wayoli','2020-02-21 23:24:25','2020-02-21 23:24:25'),(14,5476,'Tongute Goin','2020-02-21 23:24:25','2020-02-21 23:24:25'),(15,5476,'Tongute Sungi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(16,5476,'Tongute Ternate','2020-02-21 23:24:25','2020-02-21 23:24:25'),(17,5476,'Tongute Ternate Selatan','2020-02-21 23:24:25','2020-02-21 23:24:25'),(18,5477,'Adu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(19,5477,'Baru','2020-02-21 23:24:25','2020-02-21 23:24:25'),(20,5477,'Bataka','2020-02-21 23:24:25','2020-02-21 23:24:25'),(21,5477,'Gamkonora','2020-02-21 23:24:25','2020-02-21 23:24:25'),(22,5477,'Gamsida','2020-02-21 23:24:25','2020-02-21 23:24:25'),(23,5477,'Gamsungi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(24,5477,'Jere','2020-02-21 23:24:25','2020-02-21 23:24:25'),(25,5477,'Nanas','2020-02-21 23:24:25','2020-02-21 23:24:25'),(26,5477,'Ngalo Ngalo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(27,5477,'Ngawet','2020-02-21 23:24:25','2020-02-21 23:24:25'),(28,5477,'Sarau','2020-02-21 23:24:25','2020-02-21 23:24:25'),(29,5477,'Talaga','2020-02-21 23:24:25','2020-02-21 23:24:25'),(30,5477,'Tobelos','2020-02-21 23:24:25','2020-02-21 23:24:25'),(31,5477,'Tobobol (Tabobol)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(32,5477,'Tosoa','2020-02-21 23:24:25','2020-02-21 23:24:25'),(33,5477,'Tuguaer','2020-02-21 23:24:25','2020-02-21 23:24:25'),(34,5478,'Aru Jaya','2020-02-21 23:24:25','2020-02-21 23:24:25'),(35,5478,'Borona','2020-02-21 23:24:25','2020-02-21 23:24:25'),(36,5478,'Duono','2020-02-21 23:24:25','2020-02-21 23:24:25'),(37,5478,'Goin','2020-02-21 23:24:25','2020-02-21 23:24:25'),(38,5478,'Pasalulu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(39,5478,'Podol','2020-02-21 23:24:25','2020-02-21 23:24:25'),(40,5478,'Sangaji Nyeku','2020-02-21 23:24:25','2020-02-21 23:24:25'),(41,5478,'Soasangaji','2020-02-21 23:24:25','2020-02-21 23:24:25'),(42,5478,'Tengowango (Teongowango)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(43,5478,'Todoke','2020-02-21 23:24:25','2020-02-21 23:24:25'),(44,5478,'Togoreba Sungi (Tugureba Sungi)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(45,5478,'Togoreba Tua','2020-02-21 23:24:25','2020-02-21 23:24:25'),(46,5478,'Togowo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(47,5478,'Tolisaor','2020-02-21 23:24:25','2020-02-21 23:24:25'),(48,5478,'Tuguis','2020-02-21 23:24:25','2020-02-21 23:24:25'),(49,5478,'Tukuoku','2020-02-21 23:24:25','2020-02-21 23:24:25'),(50,5479,'Acango','2020-02-21 23:24:25','2020-02-21 23:24:25'),(51,5479,'Akediri','2020-02-21 23:24:25','2020-02-21 23:24:25'),(52,5479,'Bobanehena','2020-02-21 23:24:25','2020-02-21 23:24:25'),(53,5479,'Bobo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(54,5479,'Bobo Jiko','2020-02-21 23:24:25','2020-02-21 23:24:25'),(55,5479,'Buku Bualawa','2020-02-21 23:24:25','2020-02-21 23:24:25'),(56,5479,'Buku Maadu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(57,5479,'Bukumatiti','2020-02-21 23:24:25','2020-02-21 23:24:25'),(58,5479,'Galala','2020-02-21 23:24:25','2020-02-21 23:24:25'),(59,5479,'Gamlamo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(60,5479,'Gamtala','2020-02-21 23:24:25','2020-02-21 23:24:25'),(61,5479,'Guaemaadu (Guaimaadu)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(62,5479,'Guaeria','2020-02-21 23:24:25','2020-02-21 23:24:25'),(63,5479,'Gufasa','2020-02-21 23:24:25','2020-02-21 23:24:25'),(64,5479,'Hate Bicara','2020-02-21 23:24:25','2020-02-21 23:24:25'),(65,5479,'Hoku Hoku Kie (Huku-Huku Kie)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(66,5479,'Idamdehe','2020-02-21 23:24:25','2020-02-21 23:24:25'),(67,5479,'Idamdehe Gamsungi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(68,5479,'Jalan Baru','2020-02-21 23:24:25','2020-02-21 23:24:25'),(69,5479,'Kuripasai','2020-02-21 23:24:25','2020-02-21 23:24:25'),(70,5479,'Loloy (Lolori)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(71,5479,'Marmabati (Marimbati)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(72,5479,'Matui','2020-02-21 23:24:25','2020-02-21 23:24:25'),(73,5479,'Pateng','2020-02-21 23:24:25','2020-02-21 23:24:25'),(74,5479,'Payo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(75,5479,'Pornity (Porniti)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(76,5479,'Saria','2020-02-21 23:24:25','2020-02-21 23:24:25'),(77,5479,'Soakonora','2020-02-21 23:24:25','2020-02-21 23:24:25'),(78,5479,'Taboso (Tobosa/Tabaso)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(79,5479,'Tauro','2020-02-21 23:24:25','2020-02-21 23:24:25'),(80,5479,'Tedeng','2020-02-21 23:24:25','2020-02-21 23:24:25'),(81,5479,'Todowangi (Todowongi)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(82,5479,'Tuada','2020-02-21 23:24:25','2020-02-21 23:24:25'),(83,5479,'Ulo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(84,5480,'Ake Jailolo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(85,5480,'Akeara','2020-02-21 23:24:25','2020-02-21 23:24:25'),(86,5480,'Akelaha','2020-02-21 23:24:25','2020-02-21 23:24:25'),(87,5480,'Bangkit Rahmat','2020-02-21 23:24:25','2020-02-21 23:24:25'),(88,5480,'Biamaahi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(89,5480,'Bobane Dano','2020-02-21 23:24:25','2020-02-21 23:24:25'),(90,5480,'Braha','2020-02-21 23:24:25','2020-02-21 23:24:25'),(91,5480,'Dodinga','2020-02-21 23:24:25','2020-02-21 23:24:25'),(92,5480,'Domato','2020-02-21 23:24:25','2020-02-21 23:24:25'),(93,5480,'Gamlenge','2020-02-21 23:24:25','2020-02-21 23:24:25'),(94,5480,'Hijrah','2020-02-21 23:24:25','2020-02-21 23:24:25'),(95,5480,'Moiso','2020-02-21 23:24:25','2020-02-21 23:24:25'),(96,5480,'Ratem','2020-02-21 23:24:25','2020-02-21 23:24:25'),(97,5480,'Rioribati','2020-02-21 23:24:25','2020-02-21 23:24:25'),(98,5480,'Sidangoli Dehe','2020-02-21 23:24:25','2020-02-21 23:24:25'),(99,5480,'Sidangoli Gam','2020-02-21 23:24:25','2020-02-21 23:24:25'),(100,5480,'Suka Damai','2020-02-21 23:24:25','2020-02-21 23:24:25'),(101,5480,'Taba Damai (Db)','2020-02-21 23:24:25','2020-02-21 23:24:25'),(102,5480,'Tataleka','2020-02-21 23:24:25','2020-02-21 23:24:25'),(103,5480,'Tewe','2020-02-21 23:24:25','2020-02-21 23:24:25'),(104,5480,'Toniku','2020-02-21 23:24:25','2020-02-21 23:24:25'),(105,5480,'Tuguraci','2020-02-21 23:24:25','2020-02-21 23:24:25'),(106,5481,'Aruku','2020-02-21 23:24:25','2020-02-21 23:24:25'),(107,5481,'Baja','2020-02-21 23:24:25','2020-02-21 23:24:25'),(108,5481,'Bakun','2020-02-21 23:24:25','2020-02-21 23:24:25'),(109,5481,'Bakun Pantai','2020-02-21 23:24:25','2020-02-21 23:24:25'),(110,5481,'Bantol','2020-02-21 23:24:25','2020-02-21 23:24:25'),(111,5481,'Barataku','2020-02-21 23:24:25','2020-02-21 23:24:25'),(112,5481,'Bilote','2020-02-21 23:24:25','2020-02-21 23:24:25'),(113,5481,'Bosala','2020-02-21 23:24:25','2020-02-21 23:24:25'),(114,5481,'Buo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(115,5481,'Gamkahe','2020-02-21 23:24:25','2020-02-21 23:24:25'),(116,5481,'Jangailulu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(117,5481,'Jano','2020-02-21 23:24:25','2020-02-21 23:24:25'),(118,5481,'Kahatola','2020-02-21 23:24:25','2020-02-21 23:24:25'),(119,5481,'Kedi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(120,5481,'Laba Besar','2020-02-21 23:24:25','2020-02-21 23:24:25'),(121,5481,'Laba Kecil','2020-02-21 23:24:25','2020-02-21 23:24:25'),(122,5481,'Linggua','2020-02-21 23:24:25','2020-02-21 23:24:25'),(123,5481,'Pumadada','2020-02-21 23:24:25','2020-02-21 23:24:25'),(124,5481,'Salu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(125,5481,'Soa-Sio','2020-02-21 23:24:25','2020-02-21 23:24:25'),(126,5481,'Tasye','2020-02-21 23:24:25','2020-02-21 23:24:25'),(127,5481,'Tolofuo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(128,5481,'Tomodo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(129,5481,'Tosomolo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(130,5481,'Totala','2020-02-21 23:24:25','2020-02-21 23:24:25'),(131,5481,'Totala Jaya','2020-02-21 23:24:25','2020-02-21 23:24:25'),(132,5481,'Tuakara','2020-02-21 23:24:25','2020-02-21 23:24:25'),(133,5481,'Tuguis','2020-02-21 23:24:25','2020-02-21 23:24:25'),(134,5482,'Balisoan','2020-02-21 23:24:25','2020-02-21 23:24:25'),(135,5482,'Balisoan Utara','2020-02-21 23:24:25','2020-02-21 23:24:25'),(136,5482,'Dere','2020-02-21 23:24:25','2020-02-21 23:24:25'),(137,5482,'Golo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(138,5482,'Goro Goro','2020-02-21 23:24:25','2020-02-21 23:24:25'),(139,5482,'Jaraoke','2020-02-21 23:24:25','2020-02-21 23:24:25'),(140,5482,'Lako Akediri','2020-02-21 23:24:25','2020-02-21 23:24:25'),(141,5482,'Lako Akelamo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(142,5482,'Peot','2020-02-21 23:24:25','2020-02-21 23:24:25'),(143,5482,'Ropu Tengah Hulu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(144,5482,'Sasur','2020-02-21 23:24:25','2020-02-21 23:24:25'),(145,5482,'Sasur Pantai','2020-02-21 23:24:25','2020-02-21 23:24:25'),(146,5482,'Susupu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(147,5482,'Tacici','2020-02-21 23:24:25','2020-02-21 23:24:25'),(148,5482,'Tacim','2020-02-21 23:24:25','2020-02-21 23:24:25'),(149,5482,'Taraudu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(150,5482,'Taruba','2020-02-21 23:24:25','2020-02-21 23:24:25'),(151,5482,'Todahe','2020-02-21 23:24:25','2020-02-21 23:24:25'),(152,5482,'Worat Worat','2020-02-21 23:24:25','2020-02-21 23:24:25'),(153,5483,'Air Panas','2020-02-21 23:24:25','2020-02-21 23:24:25'),(154,5483,'Akelamo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(155,5483,'Aketola','2020-02-21 23:24:25','2020-02-21 23:24:25'),(156,5483,'Awer','2020-02-21 23:24:25','2020-02-21 23:24:25'),(157,5483,'Campaka','2020-02-21 23:24:25','2020-02-21 23:24:25'),(158,5483,'Gamnyial','2020-02-21 23:24:25','2020-02-21 23:24:25'),(159,5483,'Gamomeng','2020-02-21 23:24:25','2020-02-21 23:24:25'),(160,5483,'Gamsungi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(161,5483,'Goal','2020-02-21 23:24:25','2020-02-21 23:24:25'),(162,5483,'Golago Kusuma','2020-02-21 23:24:25','2020-02-21 23:24:25'),(163,5483,'Hoku Hoku Gam','2020-02-21 23:24:25','2020-02-21 23:24:25'),(164,5483,'Idamgamlamo','2020-02-21 23:24:25','2020-02-21 23:24:25'),(165,5483,'Loce','2020-02-21 23:24:25','2020-02-21 23:24:25'),(166,5483,'Ngaon','2020-02-21 23:24:25','2020-02-21 23:24:25'),(167,5483,'Sidodadi','2020-02-21 23:24:25','2020-02-21 23:24:25'),(168,5483,'Taba Campaka','2020-02-21 23:24:25','2020-02-21 23:24:25'),(169,5483,'Taraudu Kusu','2020-02-21 23:24:25','2020-02-21 23:24:25'),(170,5483,'Tibobo','2020-02-21 23:24:25','2020-02-21 23:24:25');

/*Table structure for table `sfd_master_kecamatan` */

DROP TABLE IF EXISTS `sfd_master_kecamatan`;

CREATE TABLE `sfd_master_kecamatan` (
  `id_kecamatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(200) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_kecamatan`)
) ENGINE=InnoDB AUTO_INCREMENT=5484 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_master_kecamatan` */

insert  into `sfd_master_kecamatan`(`id_kecamatan`,`nama_kecamatan`,`created_at`,`updated_at`) values (5476,'Kecamatan Ibu','2020-02-04 23:35:26','2020-02-21 23:18:42'),(5477,'Kecamatan Ibu Selatan','2020-02-04 23:35:26','2020-02-21 23:18:48'),(5478,'Kecamatan Ibu Utara','2020-02-04 23:35:26','2020-02-21 23:18:55'),(5479,'Kecamatan Jailolo','2020-02-04 23:35:26','2020-02-21 23:19:09'),(5480,'Kecamatan Jailolo Selatan','2020-02-04 23:35:26','2020-02-21 23:19:16'),(5481,'Kecamatan Loloda','2020-02-04 23:35:26','2020-02-21 23:19:35'),(5482,'Kecamatan Sahu','2020-02-04 23:35:26','2020-02-21 23:19:25'),(5483,'Kecamatan Sahu Timur','2020-02-04 23:35:26','2020-02-21 23:19:30');

/*Table structure for table `sfd_objek_rekening` */

DROP TABLE IF EXISTS `sfd_objek_rekening`;

CREATE TABLE `sfd_objek_rekening` (
  `id_objek_rekening` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `id_jenis_rekening` int(11) DEFAULT NULL,
  `kode_objek_rekening` varchar(5) DEFAULT NULL,
  `nama_objek_rekening` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_objek_rekening`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_objek_rekening` */

insert  into `sfd_objek_rekening`(`id_objek_rekening`,`id_desa`,`id_jenis_rekening`,`kode_objek_rekening`,`nama_objek_rekening`) values (1,NULL,2,'1.1.1','Ojek JOKE');

/*Table structure for table `sfd_output_dd` */

DROP TABLE IF EXISTS `sfd_output_dd`;

CREATE TABLE `sfd_output_dd` (
  `id_output_dd` int(11) NOT NULL AUTO_INCREMENT,
  `id_kegiatan` int(11) DEFAULT NULL,
  `kode_output` varchar(50) DEFAULT NULL,
  `uraian` varchar(250) DEFAULT NULL,
  `satuan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_output_dd`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_output_dd` */

insert  into `sfd_output_dd`(`id_output_dd`,`id_kegiatan`,`kode_output`,`uraian`,`satuan`) values (2,1,'232','23232','232323');

/*Table structure for table `sfd_pemerintahaan` */

DROP TABLE IF EXISTS `sfd_pemerintahaan`;

CREATE TABLE `sfd_pemerintahaan` (
  `id_pemerintahan` int(11) NOT NULL AUTO_INCREMENT,
  `jumlah_kecamatan` int(11) DEFAULT NULL,
  `jumlah_kelurahan` int(11) DEFAULT NULL,
  `jumlah_desa` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_pemerintahan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_pemerintahaan` */

/*Table structure for table `sfd_profil_kabupaten` */

DROP TABLE IF EXISTS `sfd_profil_kabupaten`;

CREATE TABLE `sfd_profil_kabupaten` (
  `id_kabupaten` int(11) NOT NULL AUTO_INCREMENT,
  `kode_wilayah` varchar(50) DEFAULT NULL,
  `nama_kabupaten` varchar(100) DEFAULT NULL,
  `id_kepala` int(11) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `maps` text DEFAULT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_kabupaten`),
  KEY `kode_wilayah` (`kode_wilayah`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_profil_kabupaten` */

/*Table structure for table `sfd_roles` */

DROP TABLE IF EXISTS `sfd_roles`;

CREATE TABLE `sfd_roles` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_roles` */

insert  into `sfd_roles`(`id`,`role_name`,`created_at`,`updated_at`) values (1,'Superadmin',NULL,NULL),(2,'Administrator',NULL,NULL),(3,'Staff',NULL,NULL),(4,'Tamu',NULL,NULL);

/*Table structure for table `sfd_sejarah_desa` */

DROP TABLE IF EXISTS `sfd_sejarah_desa`;

CREATE TABLE `sfd_sejarah_desa` (
  `id_sejarah_desa` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `sejarah` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_sejarah_desa`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `sejarah_desa_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_sejarah_desa` */

insert  into `sfd_sejarah_desa`(`id_sejarah_desa`,`id_desa`,`sejarah`,`created_at`,`updated_at`) values (1,11,'<strong>Sahu Timur</strong> adalah sebuah kecamatan di Kabupaten Halmahera Barat, Maluku Utara, Indonesia.','2020-02-09 15:38:22','2020-07-14 15:51:20');

/*Table structure for table `sfd_sejarah_kabupaten` */

DROP TABLE IF EXISTS `sfd_sejarah_kabupaten`;

CREATE TABLE `sfd_sejarah_kabupaten` (
  `id_sejarah` int(11) NOT NULL AUTO_INCREMENT,
  `sejarah` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_sejarah`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_sejarah_kabupaten` */

/*Table structure for table `sfd_statistik_desa` */

DROP TABLE IF EXISTS `sfd_statistik_desa`;

CREATE TABLE `sfd_statistik_desa` (
  `id_statistik_desa` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `jumlah_kk` int(11) DEFAULT NULL,
  `jumlah_penduduk` int(11) DEFAULT NULL,
  `jumlah_laki_laki` int(11) DEFAULT NULL,
  `jumlah_perempuan` int(11) DEFAULT NULL,
  `balita` int(11) DEFAULT NULL,
  `anak` int(11) DEFAULT NULL,
  `remaja` int(11) DEFAULT NULL,
  `dewasa` int(11) DEFAULT NULL,
  `orang_tua` int(11) DEFAULT NULL,
  `tgl_statistik` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_statistik_desa`),
  UNIQUE KEY `id_statistik_desa` (`id_statistik_desa`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `statistik_desa_ibfk_1` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `sfd_statistik_desa` */

insert  into `sfd_statistik_desa`(`id_statistik_desa`,`id_desa`,`jumlah_kk`,`jumlah_penduduk`,`jumlah_laki_laki`,`jumlah_perempuan`,`balita`,`anak`,`remaja`,`dewasa`,`orang_tua`,`tgl_statistik`,`created_at`,`updated_at`) values (2,11,123,3,3,3,3,3,3,3,3,'2020-11-14','2020-11-14 15:58:45','2020-11-14 16:02:29');

/*Table structure for table `sfd_sub_bidang` */

DROP TABLE IF EXISTS `sfd_sub_bidang`;

CREATE TABLE `sfd_sub_bidang` (
  `id_sub_bidang` int(11) NOT NULL AUTO_INCREMENT,
  `id_bidang` int(11) DEFAULT NULL,
  `kode_sub_bidang` varchar(10) DEFAULT NULL,
  `nama_sub_bidang` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_sub_bidang`),
  KEY `id_bidang` (`id_bidang`),
  CONSTRAINT `sfd_sub_bidang_ibfk_1` FOREIGN KEY (`id_bidang`) REFERENCES `sfd_bidang` (`id_bidang`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_sub_bidang` */

insert  into `sfd_sub_bidang`(`id_sub_bidang`,`id_bidang`,`kode_sub_bidang`,`nama_sub_bidang`) values (1,8,'123','JOS KE KEKEKE'),(3,6,'8565','OKE BROOO');

/*Table structure for table `sfd_sumber_dana` */

DROP TABLE IF EXISTS `sfd_sumber_dana`;

CREATE TABLE `sfd_sumber_dana` (
  `id_sumber_dana` int(11) NOT NULL AUTO_INCREMENT,
  `id_desa` int(11) DEFAULT NULL,
  `kode_sumber` varchar(50) DEFAULT NULL,
  `nama_sumber` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_sumber_dana`),
  UNIQUE KEY `kode_sumber` (`kode_sumber`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `sfd_sumber_dana` */

insert  into `sfd_sumber_dana`(`id_sumber_dana`,`id_desa`,`kode_sumber`,`nama_sumber`) values (1,11,'ADD','ALOKASI DANA DESA'),(3,11,'DDS','DANA DESA (APBN)');

/*Table structure for table `sfd_users` */

DROP TABLE IF EXISTS `sfd_users`;

CREATE TABLE `sfd_users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_desa` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_user`) USING BTREE,
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `id_desa` (`id_desa`) USING BTREE,
  CONSTRAINT `FK_sfd_users_sfd_desa` FOREIGN KEY (`id_desa`) REFERENCES `sfd_desa` (`id_desa`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Tabel pengguna web / aplikasi';

/*Data for the table `sfd_users` */

insert  into `sfd_users`(`id_user`,`user_name`,`fullname`,`email`,`email_verified_at`,`password`,`role_id`,`status`,`remember_token`,`id_desa`,`created_at`,`updated_at`) values (1,'administrator','Administrator','admin@mail.com',NULL,'$2y$10$x0NQnJqJL83Sc.6SP8PfauhBdhpq1YMyRoBfQZhOTRiR3Q7GAwaPO',0,1,'LiqCHOJOjRikzfzR83ySKqE9ZW9puXCMXbQUHiKRAhTK5AX31plbCzTvCxvu',0,NULL,'2021-06-24 21:23:27'),(2,'desaloce','Desa Loce','desaloce@mail.com',NULL,'$2y$10$RYkZLd4zjW1khLaR.1IK6uH0ReOkGawgQVaNbaA1.OGfutPLSTvPW',2,1,'3EIbsk4Ba86zmms78OxVjdQIFTqNxxfqJuutcOnGZPA06fmKgU0Ux0P5gews',11,NULL,'2021-06-17 14:00:42'),(4,'desaairpanas@mail.com',NULL,'desaairpanas@mail.com',NULL,'$2y$10$IOdEaX1MoUAejHMTO.5wxuGRUvO8I.qHTiwaQQUHETmNKNyXkqCPm',2,1,NULL,26,'2020-08-23 17:05:27','2020-08-23 17:05:27');

/*Table structure for table `sfd_visi_misi` */

DROP TABLE IF EXISTS `sfd_visi_misi`;

CREATE TABLE `sfd_visi_misi` (
  `id_visimisi` int(11) NOT NULL AUTO_INCREMENT,
  `visi` text DEFAULT NULL,
  `misi` text DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id_visimisi`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `sfd_visi_misi` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
