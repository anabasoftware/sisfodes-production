<?php
Auth::routes();
/*
|--------------------------------------------------------------------------
| Backend Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['auth:web']], function() {
	Route::prefix('/dashboard')->group(function () {
		Route::prefix('/home', 'Backend\HomeController@index')->group(function () {
			Route::get('/', 'Backend\HomeController@index')->name('home');
			Route::get('/desa/json', 'Backend\HomeController@desa_data');
		});
		/*
		|--------------------------------------------------------------------------
		| Route Informasi Desa
		|--------------------------------------------------------------------------
		*/
		Route::prefix('/info')->group(function () {

			/*
			|--------------------------------------------------------------------------
			| Route Kabupaten
			|--------------------------------------------------------------------------
			*/
			Route::get('/kabupaten','Backend\KabupatenController@index')->name('kabupaten.index');
			Route::post('/kabupaten/simpan', 'Backend\KabupatenController@kabupaten_store')->name('kabupaten.store');

			
			/*
			|--------------------------------------------------------------------------
			| Route Identitas Desa
			|--------------------------------------------------------------------------
			*/
			Route::get('/identitas', 'Backend\IdentitasController@index')->name('identitas.index');
			Route::post('/identitas/simpan', 'Backend\IdentitasController@identitas_desa_simpan')->name('identitas.simpan');
			Route::post('/identitas/update/{id}','Backend\IdentitasController@identitas_desa_update')->name('identitas.update');
			Route::get('/identitas/detail/{id}','Backend\IdentitasController@identitas_desa_detail')->name('identitas.detail');
			Route::get('/identitas/kecamatan/{id}','Backend\IdentitasController@get_desa')->name('identitas.kecamatan');
			//------------------ Route temporary --------------------------------------------------------------------------------------------------
			Route::post('/penerimaan/adddetail','Backend\RencanaPenerimaanController@rencana_terima_addtemporary')->name('rencanaterima.adddetail');
			Route::get('/penerimaan/tempdata','Backend\RencanaPenerimaanController@rencana_terima_datatemp')->name('rencanaterima.tempdata');
			Route::delete('/penerimaan/hapusdetail/{id}', 'Backend\RencanaPenerimaanController@rencana_terima_temphapus')->name('rencanaterima.hapusdetail');

		});
		/*
		|--------------------------------------------------------------------------
		| Route Identitas Desa
		|--------------------------------------------------------------------------
		*/
		Route::get('/identitas', 'Backend\IdentitasController@index')->name('identitas.index');
		Route::post('/identitas/simpan', 'Backend\IdentitasController@identitas_desa_simpan')->name('identitas.simpan');
		Route::post('/identitas/update/{id}','Backend\IdentitasController@identitas_desa_update')->name('identitas.update');
		Route::get('/identitas/detail/{id}','Backend\IdentitasController@identitas_desa_detail')->name('identitas.detail');
		Route::get('/identitas/kecamatan/{id}','Backend\IdentitasController@get_desa')->name('identitas.kecamatan');
			/*
			|--------------------------------------------------------------------------
			| Route Potensi Desa
			|--------------------------------------------------------------------------
			*/
			Route::get('/potensi', 'Backend\PotensiController@index')->name('potensi.index');
			Route::get('/potensi/data', 'Backend\PotensiController@potensi_data')->name('potensi.data');
			Route::post('/potensi/simpan', 'Backend\PotensiController@potensi_simpan')->name('potensi.simpan');
			Route::get('/potensi/detail/{id}', 'Backend\PotensiController@potensi_detail')->name('potensi.detail');
			Route::delete('/potensi/hapus/{id}', 'Backend\PotensiController@potensi_hapus')->name('potensi.hapus');
			Route::patch('/potensi/update/{id}', 'Backend\PotensiController@potensi_update')->name('potensi.update');
			Route::get('/potensi/tambah', 'Backend\PotensiController@potensi_tambah')->name('potensi.tambah');
			Route::get('/potensi/detail/json/{id}', 'Backend\PotensiController@potensi_detail_json')->name('potensi.detail_json');

			/*
			|--------------------------------------------------------------------------
			| Route Statistik Desa
			|--------------------------------------------------------------------------
			*/
			Route::get('/statistik', 'Backend\StatistikController@index')->name('statistik.index');
			Route::get('/statistik/data', 'Backend\StatistikController@statistik_data')->name('statistik.data');
			Route::post('/statistik/simpan', 'Backend\StatistikController@statistik_simpan')->name('statistik.simpan');
			Route::get('/statistik/detail/{id}', 'Backend\StatistikController@statistik_detail')->name('statistik.detail');
			Route::delete('/statistik/hapus/{id}', 'Backend\StatistikController@statistik_hapus')->name('statistik.hapus');
			Route::patch('/statistik/update/{id}', 'Backend\StatistikController@statistik_update')->name('statistik.update');
			Route::get('/statistik/tambah', 'Backend\StatistikController@statistik_tambah')->name('statistik.tambah');
			Route::get('/statistik/detail/json/{id}', 'Backend\StatistikController@statistik_detail_json')->name('statistik.detail_json');

			/*
			|--------------------------------------------------------------------------
			| Route Wilayah Desa
			|--------------------------------------------------------------------------
			*/
			Route::get('/wilayah', 'Backend\WilayahController@index')->name('wilayah.index');
			Route::post('/wilayah/simpan', 'Backend\WilayahController@wilayah_simpan')->name('wilayah.simpan');
			Route::post('/wilayah/update/{id}','Backend\WilayahController@wilayah_update')->name('wilayah.update');
			Route::get('/wilayah/detail/{id}','Backend\WilayahController@wilayah_detail')->name('wilayah.detail');


			/*
			|--------------------------------------------------------------------------
			| Route Lembaga Desa
			|--------------------------------------------------------------------------
			*/
			Route::get('/lembaga', 'Backend\LembagaController@index')->name('lembaga.index');
			Route::get('/lembaga/data', 'Backend\LembagaController@lembaga_data')->name('lembaga.data');
			Route::get('/lembaga/tambah', 'Backend\LembagaController@lembaga_tambah')->name('lembaga.tambah');
			Route::post('/lembaga/simpan', 'Backend\LembagaController@lembaga_simpan')->name('lembaga.simpan');
			Route::get('/lembaga/detail/{id}', 'Backend\LembagaController@lembaga_detail')->name('lembaga.detail');
			Route::delete('/lembaga/hapus/{id}', 'Backend\LembagaController@lembaga_hapus')->name('lembaga.hapus');
			Route::patch('/lembaga/update/{id}', 'Backend\LembagaController@lembaga_update')->name('lembaga.update');
			Route::get('/lembaga/detail/json/{id}', 'Backend\LembagaController@lembaga_detail_json')->name('lembaga.detail_json');
			

			/*
			|--------------------------------------------------------------------------
			| Route Sejarah Desa
			|--------------------------------------------------------------------------
			*/
			Route::get('/sejarah', 'Backend\SejarahController@index')->name('sejarah.index');
			Route::post('/sejarah/simpan', 'Backend\SejarahController@sejarah_simpan')->name('sejarah.simpan');
			Route::post('/sejarah/update/{id}','Backend\SejarahController@sejarah_update')->name('sejarah.update');
			Route::get('/sejarah/detail/{id}','Backend\SejarahController@sejarah_detail')->name('sejarah.detail');

			/*
			|--------------------------------------------------------------------------
			| Route Visi dan Misi Desa
			|--------------------------------------------------------------------------
			*/
			Route::get('/visimisi', 'Backend\VisiMisiController@index')->name('visimisi.index');
			Route::post('/visimisi/simpan', 'Backend\VisiMisiController@visimisi_simpan')->name('visimisi.simpan');
			Route::post('/visimisi/update/{id}','Backend\VisiMisiController@visimisi_update')->name('visimisi.update');
			Route::get('/visimisi/detail/{id}','Backend\VisiMisiController@visimisi_detail')->name('visimisi.detail');
		});

		/*
		|--------------------------------------------------------------------------
		| Route APBDES
		|--------------------------------------------------------------------------
		*/
		Route::prefix('/apbdes')->group(function () {

			Route::get('/refbidang', 'Backend\apbdes\ReferensiBidangController@index')->name('refbidang.index');
			/*
			|--------------------------------------------------------------------------
			| Route Referensi Bidang
			|--------------------------------------------------------------------------
			*/
		
						
			Route::get('/refbidang/showform/{id}',['as' => 'refbidang.showform','uses' => 'Backend\apbdes\ReferensiBidangController@show_form_bidang']);
			Route::get('/refbidang/data','Backend\apbdes\ReferensiBidangController@bidang_data')->name('refbidang.data');
			Route::post('/refbidang/simpan','Backend\apbdes\ReferensiBidangController@bidang_simpan')->name('refbidang.simpan');
			Route::delete('/refbidang/hapus/{id}', 'Backend\apbdes\ReferensiBidangController@bidang_hapus')->name('refbidang.hapus');


			/*
			|--------------------------------------------------------------------------
			| Route Referensi Sub Bidang
			|--------------------------------------------------------------------------
			*/
						
			Route::get('/refsubbidang/showform/{id}',['as' => 'refsubbidang.showform','uses' => 'Backend\apbdes\ReferensiBidangController@show_form_subbidang']);
			Route::get('/refsubbidang/data','Backend\apbdes\ReferensiBidangController@subbidang_data')->name('refsubbidang.data');
			Route::post('/refsubbidang/simpan','Backend\apbdes\ReferensiBidangController@subbidang_simpan')->name('refsubbidang.simpan');
			Route::delete('/refsubbidang/hapus/{id}', 'Backend\apbdes\ReferensiBidangController@subbidang_hapus')->name('refsubbidang.hapus');

			/*
			|--------------------------------------------------------------------------
			| Route Referensi Kegiatan
			|--------------------------------------------------------------------------
			*/
						
			Route::get('/refkegiatan/showform/{id}',['as' => 'refkegiatan.showform','uses' => 'Backend\apbdes\ReferensiBidangController@show_form_kegiatan']);
			Route::get('/refkegiatan/data','Backend\apbdes\ReferensiBidangController@kegiatan_data')->name('refkegiatan.data');
			Route::post('/refkegiatan/simpan','Backend\apbdes\ReferensiBidangController@kegiatan_simpan')->name('refkegiatan.simpan');
			Route::delete('/refkegiatan/hapus/{id}', 'Backend\apbdes\ReferensiBidangController@kegiatan_hapus')->name('refkegiatan.hapus');



			/*
			|--------------------------------------------------------------------------
			| Route Referensi Sumber Dana
			|--------------------------------------------------------------------------
			*/
			Route::get('/refsumberdana', 'Backend\apbdes\SumberDanaController@index')->name('refsumberdana.index');			
			Route::get('/refsumberdana/showform/{id}',['as' => 'refsumberdana.showform','uses' => 'Backend\apbdes\SumberDanaController@show_form_sumberdana']);
			Route::get('/refsumberdana/data','Backend\apbdes\SumberDanaController@sumberdana_data')->name('refsumberdana.data');
			Route::post('/refsumberdana/simpan','Backend\apbdes\SumberDanaController@sumberdana_simpan')->name('refsumberdana.simpan');
			Route::delete('/refsumberdana/hapus/{id}', 'Backend\apbdes\SumberDanaController@sumberdana_hapus')->name('refsumberdana.hapus');



			/*
			|--------------------------------------------------------------------------
			| Route Referensi Akun Rekening
			|--------------------------------------------------------------------------
			*/
			Route::get('/refakun', 'Backend\apbdes\RekeningApbdesController@index')->name('refakun.index');			
			Route::get('/refakun/showform/{id}',['as' => 'refakun.showform','uses' => 'Backend\apbdes\RekeningApbdesController@show_form_akun']);
			Route::get('/refakun/data','Backend\apbdes\RekeningApbdesController@akun_data')->name('refakun.data');
			Route::post('/refakun/simpan','Backend\apbdes\RekeningApbdesController@akun_simpan')->name('refakun.simpan');
			Route::delete('/refakun/hapus/{id}', 'Backend\apbdes\RekeningApbdesController@akun_hapus')->name('refakun.hapus');


			/*
			|--------------------------------------------------------------------------
			| Route Referensi Kelompok Rekening
			|--------------------------------------------------------------------------
			*/
			Route::get('/refkelompok', 'Backend\apbdes\RekeningApbdesController@index')->name('refkelompok.index');			
			Route::get('/refkelompok/showform/{id}',['as' => 'refkelompok.showform','uses' => 'Backend\apbdes\RekeningApbdesController@show_form_kelompok']);
			Route::get('/refkelompok/data','Backend\apbdes\RekeningApbdesController@kelompok_data')->name('refkelompok.data');
			Route::post('/refkelompok/simpan','Backend\apbdes\RekeningApbdesController@kelompok_simpan')->name('refkelompok.simpan');
			Route::delete('/refkelompok/hapus/{id}', 'Backend\apbdes\RekeningApbdesController@kelompok_hapus')->name('refkelompok.hapus');

			/*
			|--------------------------------------------------------------------------
			| Route Referensi Jenis Rekening
			|--------------------------------------------------------------------------
			*/
			Route::get('/refjenisrekening', 'Backend\apbdes\RekeningApbdesController@index')->name('refjenisrekening.index');			
			Route::get('/refjenisrekening/showform/{id}',['as' => 'refjenisrekening.showform','uses' => 'Backend\apbdes\RekeningApbdesController@show_form_jenis']);
			Route::get('/refjenisrekening/data','Backend\apbdes\RekeningApbdesController@jenis_data')->name('refjenisrekening.data');
			Route::post('/refjenisrekening/simpan','Backend\apbdes\RekeningApbdesController@jenis_simpan')->name('refjenisrekening.simpan');
			Route::delete('/refjenisrekening/hapus/{id}', 'Backend\apbdes\RekeningApbdesController@jenis_hapus')->name('refjenisrekening.hapus');


			/*
			|--------------------------------------------------------------------------
			| Route Referensi Objek Rekening
			|--------------------------------------------------------------------------
			*/
			Route::get('/refobjekrekening', 'Backend\apbdes\RekeningApbdesController@index')->name('refobjekrekening.index');			
			Route::get('/refobjekrekening/showform/{id}',['as' => 'refobjekrekening.showform','uses' => 'Backend\apbdes\RekeningApbdesController@show_form_objek']);
			Route::get('/refobjekrekening/data','Backend\apbdes\RekeningApbdesController@objek_data')->name('refobjekrekening.data');
			Route::post('/refobjekrekening/simpan','Backend\apbdes\RekeningApbdesController@objek_simpan')->name('refobjekrekening.simpan');
			Route::delete('/refobjekrekening/hapus/{id}', 'Backend\apbdes\RekeningApbdesController@objek_hapus')->name('refobjekrekening.hapus');


			/*
			|--------------------------------------------------------------------------
			| Route Referensi Ouput Kegiatan
			|--------------------------------------------------------------------------
			*/
			Route::get('/refoutputkegiatan', 'Backend\apbdes\OuputKegiatanController@index')->name('refoutputkegiatan.index');			
			Route::get('/refoutputkegiatan/showform/{id}',['as' => 'refoutputkegiatan.showform','uses' => 'Backend\apbdes\OuputKegiatanController@show_form_outputkegiatan']);
			Route::get('/refoutputkegiatan/data','Backend\apbdes\OuputKegiatanController@outputkegiatan_data')->name('refoutputkegiatan.data');
			Route::post('/refoutputkegiatan/simpan','Backend\apbdes\OuputKegiatanController@outputkegiatan_simpan')->name('refoutputkegiatan.simpan');
			Route::delete('/refoutputkegiatan/hapus/{id}', 'Backend\apbdes\OuputKegiatanController@outputkegiatan_hapus')->name('refoutputkegiatan.hapus');


			/*
			|--------------------------------------------------------------------------
			| Route Referensi Rekening Bank
			|--------------------------------------------------------------------------
			*/
			Route::get('/refbankdesa', 'Backend\apbdes\BankDesaController@index')->name('refbankdesa.index');			
			Route::get('/refbankdesa/showform/{id}',['as' => 'refbankdesa.showform','uses' => 'Backend\apbdes\BankDesaController@show_form_rekening']);
			Route::get('/refbankdesa/data','Backend\apbdes\BankDesaController@rekening_data')->name('refbankdesa.data');
			Route::post('/refbankdesa/simpan','Backend\apbdes\BankDesaController@rekening_simpan')->name('refbankdesa.simpan');
			Route::delete('/refbankdesa/hapus/{id}', 'Backend\apbdes\BankDesaController@rekening_hapus')->name('refbankdesa.hapus');

		});
		/*
		|--------------------------------------------------------------------------
		| Route Fasilitas
		|--------------------------------------------------------------------------
		*/
		Route::prefix('/fasilitas')->group(function(){
			/*
			|--------------------------------------------------------------------------
			| Route Pengaduan
			|--------------------------------------------------------------------------
			*/
			Route::get('/pengaduan', 'Backend\PengaduanController@index')->name('fasilitas.pengaduan');

		
			/*
			|--------------------------------------------------------------------------
			| Route Layanan
			|--------------------------------------------------------------------------
			*/
			Route::prefix('/layanan')->group(function(){
				Route::get('/', 'Backend\LayananController@index')->name('fasilitas.layanan.index');
				Route::get('/data', 'Backend\LayananController@layanan_data')->name('fasilitas.layanan.data');
				Route::get('/tambah', 'Backend\LayananController@layanan_tambah')->name('fasilitas.layanan.tambah');
				Route::post('/simpan', 'Backend\LayananController@layanan_simpan')->name('fasilitas.layanan.simpan');
				Route::get('/detail/{id}', 'Backend\LayananController@layanan_detail')->name('fasilitas.layanan.detail');
				Route::post('/update/{id}', 'Backend\LayananController@layanan_update')->name('fasilitas.layanan.update');
				Route::get('/hapus/{id}', 'Backend\LayananController@layanan_hapus')->name('fasilitas.layanan.hapus');
			});
		});

		/*
		|--------------------------------------------------------------------------
		| Route Website Kabar Berita
		|--------------------------------------------------------------------------
		*/
		Route::prefix('/website')->group(function(){
			/*
			|--------------------------------------------------------------------------
			| Route Konfigurasi Web
			|--------------------------------------------------------------------------
			*/
			Route::get('/konfigurasi', 'Backend\WebconfigController@index')->name('website.webconfig');
			/*
			|--------------------------------------------------------------------------
			| Route Berita
			|--------------------------------------------------------------------------
			*/
			Route::prefix('/berita')->group(function(){
				Route::get('/', 'Backend\BeritaController@index')->name('website.berita');
				Route::get('/data', 'Backend\BeritaController@berita_data')->name('berita.data');
				Route::get('/tambah', 'Backend\BeritaController@berita_tambah')->name('berita.tambah');
				Route::post('/simpan', 'Backend\BeritaController@berita_simpan')->name('berita.simpan');
				Route::get('/detail/{id}', 'Backend\BeritaController@berita_detail')->name('berita.detail');
				Route::post('/update/{id}', 'Backend\BeritaController@berita_update')->name('berita.update');
				Route::get('/hapus/{id}', 'Backend\BeritaController@berita_hapus')->name('berita.hapus');
				
				/*
				|--------------------------------------------------------------------------
				| Route Kategori Berita
				|--------------------------------------------------------------------------
				*/
				Route::prefix('/kategori')->group(function(){
					Route::get('/', 'Backend\KategoriberitaController@index')->name('website.berita.kategori');
					Route::get('/data', 'Backend\KategoriberitaController@kategori_data')->name('website.berita.kategori.data');
					Route::post('/simpan', 'Backend\KategoriberitaController@kategori_simpan')->name('website.berita.kategori.simpan');
					Route::get('/detail/{id}', 'Backend\KategoriberitaController@kategori_detail')->name('website.berita.kategori.detail');
					Route::post('/update/{id}', 'Backend\KategoriberitaController@kategori_update')->name('website.berita.kategori.update');
					Route::get('/hapus/{id}', 'Backend\KategoriberitaController@kategori_hapus')->name('website.berita.kategori.hapus');
					Route::prefix('/sub')->group(function(){
						Route::get('/', 'Backend\KategoriberitaSubController@index')->name('website.berita.kategori.sub');
						Route::get('/data', 'Backend\KategoriberitaSubController@sub_kategori_data')->name('website.berita.kategori.sub.data');
						Route::post('/simpan', 'Backend\KategoriberitaSubController@sub_kategori_simpan')->name('website.berita.kategori.sub.simpan');
						Route::get('/detail/{id}', 'Backend\KategoriberitaSubController@sub_kategori_detail')->name('website.berita.kategori.sub.detail');
						Route::post('/update/{id}', 'Backend\KategoriberitaSubController@sub_kategori_update')->name('website.berita.kategori.sub.update');
						Route::get('/hapus/{id}', 'Backend\KategoriberitaSubController@sub_kategori_hapus')->name('website.berita.kategori.sub.hapus');
					});
				});
				
				/*
				|--------------------------------------------------------------------------
				| Route Banner Berita
				|--------------------------------------------------------------------------
				*/
				Route::prefix('/banner')->group(function(){
					Route::get('/', 'Backend\BannerController@banner_index')->name('website.berita.banner');
					Route::get('/data', 'Backend\BannerController@banner_data')->name('website.berita.banner.data');
					Route::post('/simpan', 'Backend\BannerController@banner_simpan')->name('website.berita.banner.simpan');
					Route::get('/detail/{id}', 'Backend\BannerController@banner_detail')->name('website.berita.banner.detail');
					Route::post('/update/{id}', 'Backend\BannerController@banner_update')->name('website.berita.banner.update');
					Route::get('/hapus/{id}', 'Backend\BannerController@banner_hapus')->name('website.berita.banner.hapus');
				});


				/*
				|--------------------------------------------------------------------------
				| Route Tag Berita
				|--------------------------------------------------------------------------
				*/
				Route::prefix('/tag')->group(function(){
					Route::get('/', 'Backend\TagController@index')->name('website.berita.tag');
					Route::get('/data', 'Backend\TagController@tag_data')->name('website.berita.tag.data');
					Route::post('/simpan', 'Backend\TagController@tag_simpan')->name('website.berita.tag.simpan');
					Route::get('/detail/{id}', 'Backend\TagController@tag_detail')->name('website.berita.tag.detail');
					Route::post('/update/{id}', 'Backend\TagController@tag_update')->name('website.berita.tag.update');
					Route::get('/hapus/{id}', 'Backend\TagController@tag_hapus')->name('website.berita.tag.hapus');
				});
			});
		});

		/*
		|--------------------------------------------------------------------------
		| Route Pengaturan App
		|--------------------------------------------------------------------------
		*/
		Route::prefix('/pengaturan')->group(function(){
			/*
			|--------------------------------------------------------------------------
			| Route Pengguna App
			|--------------------------------------------------------------------------
			*/
			Route::get('/pengguna', 'Backend\PenggunaController@index')->name('pengaturan.pengguna');
			Route::post('/pengguna/update/{id}', 'Backend\PenggunaController@pengguna_update')->name('pengguna.update');

			/*
			|--------------------------------------------------------------------------
			| Route Manajemen File
			|--------------------------------------------------------------------------
			*/
			Route::get('/manajerfile', 'Backend\ManajerfileController@index')->name('pengaturan.file');
		});

		/*
		|--------------------------------------------------------------------------
		| Route Manajemen Desa
		|--------------------------------------------------------------------------
		*/
		Route::prefix('/manajemen')->group(function(){
			Route::prefix('/desa')->group(function(){
				Route::get('/', 'Backend\DesaController@index')->name('manajemen.desa.index');
				Route::get('/data', 'Backend\DesaController@desa_data')->name('manajemen.desa.data');
				Route::get('/tambah', 'Backend\DesaController@desa_tambah')->name('manajemen.desa.tambah');
				Route::post('/simpan', 'Backend\DesaController@desa_simpan')->name('manajemen.desa.simpan');
				Route::get('/detail/{id}', 'Backend\DesaController@desa_detail')->name('manajemen.desa.detail');
				Route::get('/show/{id}', 'Backend\DesaController@desa_show')->name('manajemen.desa.show');
				Route::post('/update/{id}', 'Backend\DesaController@desa_update')->name('manajemen.desa.update');
				Route::get('/hapus/{id}', 'Backend\DesaController@desa_hapus')->name('manajemen.desa.hapus');
			});
		});


});

/*
|--------------------------------------------------------------------------
| Frontend Routes
|--------------------------------------------------------------------------
*/
Route::get('/', function () {
    return view('auth.login');
});
Route::prefix('/')->group(function () {
	Route::name('frontpage.')->group(function () {
		Route::get('/', 'Frontend\IndexController@frontpage_index')->name('index');
		Route::get('profil','Frontend\IndexController@frontpage_profil')->name('profil');
		
		Route::prefix('/potensi')->group(function () {
			Route::get('/','Frontend\IndexController@frontpage_potensi')->name('potensi');
			Route::get('/detail/{potensi_slug}', 'Frontend\IndexController@detail_potensi')->name('detail_potensi');
		});
		
		Route::get('layanan','Frontend\IndexController@frontpage_layanan')->name('layanan');
		Route::get('layanan/detail/{id_desa}/{slug}','Frontend\IndexController@frontpage_layanan_detail')->name('layanan.detail');

		Route::get('lapor','Frontend\IndexController@frontpage_lapor')->name('lapor');
		
		Route::prefix('/info')->group(function () {
			Route::get('/','Frontend\IndexController@frontpage_info')->name('info');
			Route::get('/detail','Frontend\IndexController@frontpage_info_detail')->name('info-detail');
		});
		
		Route::prefix('/berita')->name('berita.')->group(function () {
			Route::get('/','Frontend\IndexController@frontpage_berita')->name('index');
			Route::get('/detail/{id_desa}/{slug}','Frontend\IndexController@frontpage_berita_detail')->name('post.detail');
		});
	});
});
